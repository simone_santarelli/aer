
This folder contains in subfolders the tools, signals, and utilities that can be 
used for AER tuning:
   - drc_tool: DRC design tool
   - eq_tool: Equalizer design tool
   - nl_tool: Nonlinearity measurement and analysis tool
   - optdl_tool: Optimum delay estimation tool
   - bf_tool: Beamformer filter coefficients design tool   
   - signals: Signals for calibrating gains and verify convergence
   - utils: Windows utilities for sending and receiving signals over packet network

The AER Quick Tuning Guide document describes how to tune AER with the provided 
tools and signals.

The tools were developed in Matlab and converted to executables using Matlab compiler.
To run these tools, the MATLAB Compiler Runtime (MCR) 2007b is needed, which is 
installed during the AER installation. 
