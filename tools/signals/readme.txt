This folder contains signals that can be used for AER tuning. All signals are
in big endian (Motorola) binary format.
   - css_16k_-18dBFS.pcm: Composite source signal at 16kHz sampling rate. The power
                          is -18dBFS (-18dB from full scale).  
   - pink_16k_-18dBFS.pcm: Pink noise at 16kHz sampling rate with -18dBFS power. 
   
   