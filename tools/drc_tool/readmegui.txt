DRC README

The dynamic range compression (DRC) tool provides a graphical interface to 
configure the parameters of the DRC. Compression and analysis is performed 
on audio files so that suitable parameters can be obtained. Design tools 
are also provided to help aid in configuring the DRC.


RUNNING THE DRC TOOL

When the DRC tool is opened, it is initialized with a set of default 
parameters. Before specifying an input file, the input file type should be 
specified to prevent any erroneous results. The order of specifying an input 
file and file type is not crucial as long as they are chosen before processing 
and analysis. The file types supported are PCM (big and little endian), u-Law, 
A-Law, and WAV. For WAV files, once the input file is specified, the sampling 
frequency is set based on that file and can no longer be changed. No assumptions 
on the sampling rate are made for any other file type.

After providing an input file, the "Execute" button is enabled. Also, the input 
signal's power spectrum and histogram can be viewed by checking the respective 
boxes in the Analysis panel. To view both the input and output power spectrum 
and histogram, the boxes must be checked before running the DRC. Clicking the 
"Execute" button will begin processing, and if an output file has not already 
been specified, the user will be prompted to choose one before the compressor is
executed. 

Under the "Tools" menu, a gain curve design tool and a filterbank design 
tool are available to help determine parameter values. The gain curve 
design tool plots the transfer and gain functions that effect the level of 
compression. The values obtained in the tool can be used by selecting the 
"Ok" button or discarded by selecting the "Cancel" button. The "Reset" 
button can be pressed for no compression. 

If multiband mode is enabled, a filterbank is used to seperate the input 
signal into bass, mid, and treble subbands. The filterbank tool allows the 
user to alter the cross-over frequencies while displaying the transfer 
functions of the filters. The resulting filters can be used by selecting 
"Ok" or discarded by selecting "Cancel".