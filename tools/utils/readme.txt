
This folder contains PC utilities that can be used to send signals to or receive 
signals from a device on which AER is tuned. To find out the syntax of each
utility, just type the command without any argument and it will return the usage
format. 

   - rtp_send8k.exe: send 8kHz binary file in big endian (Motorola) format from 
                     PC to device. 
					 Usage: rtp_send8k <filename> <IP> <port> [vifsize (bytes)]
   - rtp_send16k.exe: send 16kHz binary file in big endian (Motorola) format from 
                     PC to device. 
					 Usage: rtp_send16k <filename> <IP> <port> [vifsize (bytes)]
   - UDPdump.exe: receive User Datagram Protocol (UDP) packets sent from a device
                  to PC. 
				  Usage: UDPdump deviceID output_filename IP_src port_src IP_des port_des num_pkts, 
				  where deviceID is the ID for network interface card. To find
				  out the deviceID of available network cards, just type a full
				  command with 0 as deviceID and the command will return the IDs 
				  of available network cards.
				  
				  Below is an example of a command with deviceID 0 and its response:
                  E:>UDPdump 0 data.txt 158.218.103.74 50000 158.218.103.66 50000 100
				  E:>1. \Device\NPF_{E5B877F4-F4DA-4EDA-A5B6-E23FA69CDA9F} 
				        (NETGEAR GA311 Gigabit Adapter (Microsoft's Packet Scheduler) )
                     2. \Device\NPF_{731D3761-1433-4F86-BFAF-D50C101DC263} 
					    (Broadcom NetXtreme Gigabi Ethernet Driver (Microsoft's Packet Scheduler) )
                     Adapter number out of range.
				  
   