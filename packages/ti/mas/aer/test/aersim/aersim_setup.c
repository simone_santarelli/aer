/****************************************************************************** 
 * FILE PURPOSE: Setup routines for the simulation of AER.  
 ****************************************************************************** 
 * FILE NAME:   aersim_setup.c 
 * 
 * DESCRIPTION: Contains routines for setting up the simulation testing of AER.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/
/* ANSI C headers */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/agc.h>
#include <ti/mas/aer/drc.h>

#include <ti/mas/sdk/aep.h>

#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/aersim_fileIO.h>
#include <ti/mas/aer/test/aersim/aerbuffs.h>
#include <ti/mas/aer/test/aepsim/aepsim.h>
#include <ti/mas/aer/test/sgnsim/sgnsim.h>
#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/piusim/piuloc.h>        
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuport.h>        
#include <ti/mas/aer/test/siusim/siuaer.h>
#include <ti/mas/aer/test/siusim/siudrc.h>
#include <ti/mas/aer/test/siusim/siubf.h>

/* profiling */
#ifndef gnu_targets_arm_GCArmv7A
#include <ti/mas/sdk/profile.h>
#include <ti/mas/sdk/memArchCfg.h>
#endif

/* Tx/Rx pointers for signal path */
tint      *piu_segment_in;       /* Input PCM buffer for piuPcmIo()  */
tint      *piu_segment_out;      /* Output PCM buffer for piuPcmIo() */
linSample *segment_in;           /* linear buffer for Tx generator   */
linSample *rx_segment_out;       /* linear buffer for Rx out display */

void aer_sim_setup_sgn(sgnSimPars_t *rx_sgn, sgnSimPars_t *tx_sgn);
void aer_sim_setup_aep(aepSimPars_t *aep_params);
void aer_sim_test_api(void *aer_inst);
extern void siu_new_mss(siuInst_t *inst, tint num_mic, tint samp_rates);
extern void siu_open_mss(siuInst_t *inst, tint num_mic, tint samp_rates);

/* Equalizers having flat response for measuring MIPS without screwing up the signal */
const aerEqConfig_t sampleEqBqParamsRx = 
{7, 65249, 31950, -28861, 27987, 32667, 1942, 21895, 32138, 3564, 21895, 30274, 
3998, 13573, 23170, 3563, -11123, -3212, 3564};

const aerEqConfig_t sampleEqBqParamsTx = 
{7, 60145, 31950, -28861, 29504, 32667, 1833, 28718, 31640, 3564, 13573, 30274, 
1942, 13573, 18205, 3998, 4205, -28590, 10929};

/* BF FIR filter coefficients for 0 degree steering angle */
Fract bf_coeffs_0[AER_SIM_NUM_BF_MIC][SIU_BF_FLT_LEN] = {
  {0,0,0,0,0,0,0,0,0,0,0,32767,0,0,0,0,0,0,0,0,0,0,0,0}, 
  {0,0,0,0,0,0,0,0,0,0,0,32767,0,0,0,0,0,0,0,0,0,0,0,0}, 
  {0,0,0,0,0,0,0,0,0,0,0,32767,0,0,0,0,0,0,0,0,0,0,0,0}, 
  {0,0,0,0,0,0,0,0,0,0,0,32767,0,0,0,0,0,0,0,0,0,0,0,0}  };
  
/* BF FIR filter coefficients for 90 degree steering angle */
Fract bf_coeffs_90[AER_SIM_NUM_BF_MIC][SIU_BF_FLT_LEN] = {
   {-22,  98, -241,  471, -817, 1331, -2120,  3470, -6434, 20542, 20967, -6847,  3861, -2478,  1649, -1089,   692, -409,  213,  -88,   20,   0,   0, 0},
   {  0, -22,   98, -241,  471, -817,  1331, -2120,  3470, -6434, 20542, 20967, -6847,  3861, -2478,  1649, -1089,  692, -409,  213,  -88,  20,   0, 0},
   {  0,  20,  -88,  213, -409,  692, -1089,  1649, -2478,  3861, -6847, 20967, 20542, -6434,  3470, -2120,  1331, -817,  471, -241,   98, -22,   0, 0},
   {  0,   0,   20,  -88,  213, -409,   692, -1089,  1649, -2478,  3861, -6847, 20967, 20542, -6434,  3470, -2120, 1331, -817,  471, -241,  98, -22, 0} };

/* BF FIR filter coefficients for 45 degree steering angle */
Fract bf_coeffs_45[AER_SIM_NUM_BF_MIC][SIU_BF_FLT_LEN] = {
   {-4,  18, -43,   83, -143,  228,  -355,   556,  -938,  2061, 32572, -1825,   883,  -534,   344,  -223,   140,  -82,   42,  -17,   4,   0,  0, 0},
   { 0, -20,  86, -211,  412, -712,  1152, -1818,  2929, -5239, 14200, 26501, -6782,  3665, -2312,  1523, -1000,  632, -372,  194, -80,  18,  0, 0},
   { 0,  18, -80,  194, -372,  632, -1000,  1523, -2312,  3665, -6782, 26501, 14200, -5239,  2929, -1818,  1152, -712,  412, -211,  86, -20,  0, 0},
   { 0,   0,   4,  -17,   42,  -82,   140,  -223,   344,  -534,   883, -1825, 32572,  2061,  -938,   556,  -355,  228, -143,   83, -43,  18, -4, 0} };

/******************************************************************************
 * FUNCTION PURPOSE: Initialize AER simulation environment.
 *
 *****************************************************************************/
void aer_sim_init(siuSetup_t *siu_setup)
{
  siuSysCfg_t     *sys_params;
  siuAerSysPars_t *aer_params;
  
  sys_params = &siu_setup->system_cfg;
  aer_params = &siu_setup->aer_sys_params;

  /* default system parameters for AER simulation */  
  sys_params->ulaw = const_COMP_LINEAR;  /* Linear comapnding        */
  sys_params->pcm_bits = 16;             /* 16 bit linear comapnding */
  sys_params->num_HF_mic = 1;            /* 1 hands free microphone */
  sys_params->segment_size =  20;        /* default PIU (sub)segment @ 8kHz */
                                         /* must be <=40, multiple of frame_size */
                                         /* double to get PIU y2x_delay (Tx+Rx)  */
  sys_params->frame_size = 80;           /* 10ms frame size @8kHz - 80 samples */
  sys_params->sampling_rates = 7;        /* 16 kHz on all I/O interfaces */
  sys_params->band_split_flag = FALSE;   /* full band, don't use bandsplit */
  
  /* AER parameters that depend on the system */
  aer_params->aer_filter_len_HF  = AER_SIM_HF_MAX_FILT_LENGTH; /* 200ms for HF tail        */
  aer_params->aer_filter_len_HES = AER_SIM_HES_MAX_FILT_LENGTH; /* 20ms for HES tail        */
  aer_params->y2x_delay  = AER_SIM_PIU_DEL_RX + AER_SIM_PIU_DEL_TX - AER_SIM_SIU_Y2X_DEL_ADJ;
  aer_params->ag_chg_delay_tx = AER_SIM_PIU_DEL_TX;
  aer_params->ag_chg_delay_rx = AER_SIM_PIU_DEL_RX;
  aer_params->ag_chg_interp_samp = 2;  

  siu_setup->p651Pars.enabled = FALSE;
  
  siu_setup->aep_gain_l2[0] = 0;          /* linear gain = 2^0 = 1, ERL = 0dB */
  siu_setup->aep_gain_l2[1] = 1;          /* linear gain = 2^1 = 2, ERL = -6dB */  
  siu_setup->aep_gain_l2[2] = 2;          /* linear gain = 2^2 = 4, ERL = -12dB */  
  siu_setup->aep_gain_start_exp = 0;      /* for G.167 test only, 0dB starting point */
  siu_setup->aep_gain_start_mant = 32767; /* for G.167 test only */
  siu_setup->aep_gain_adj_exp = 0;        /* gain adjustment exponent, 0dB  */
  siu_setup->aep_gain_adj_mant = 21689;   /* gain adjustment mantissa (S.15): 
                                             21689(-3.5838dB) for CSS, 
                                             32767 (0dB) for white noise */
      
  /* 
   * Don't toggle AER by default. Have to manually change this bit during debug
     to change AER mode. 
   * AER control bitfield 0 and bitfield 1 are configured through aersimcfg.txt
   */
  siu_setup->toggle_aer = FALSE; 
    
  /* 
   * Don't toggle AGC by default. Have to manually change this bit during debug
     to change AGC mode. 
   * AGC control bitfield is configured through aersimcfg.txt
   */
  siu_setup->toggle_agc = FALSE; 
  siu_setup->toggle_veu = FALSE; 

  siu_setup->toggle_bf  = FALSE; 
  siu_setup->toggle_siugain = FALSE;  
  siu_setup->siuGainCtl.ctl_code = siu_SET_MIC_GAIN;
  siu_setup->siuGainCtl.u.mic_gain = 0;

  /* do not toggle acoustic echo path paramrs*/
  siu_setup->toggle_aep = FALSE;  
  siu_setup->aepCtl.code = aep_CTL_GAIN;   /* default control command        */
  siu_setup->aepCtl.u.gain_l2 = 0;         /* log2 gain = 0                  */

  /* do not toggle Tx or Rx signal generator */
  siu_setup->toggle_tx_sg = FALSE;  
  siu_setup->toggle_rx_sg = FALSE;  
  
  /* do not toggle PIU tone generator */
  siu_setup->toggle_piu_tg = FALSE;  
  siu_setup->piu_tg_state  = FALSE;  /* init PIU tone generator state is OFF  */

  /* do not ask for filter coefficients */
  siu_setup->toggle_getflt = FALSE;     
  siu_setup->toggle_putflt = FALSE;
  siu_setup->putflt_flag_last = TRUE;
  
  siu_setup->run_time = 3000;         /* 30 seconds for G.167 test */
  siu_setup->aep_vary_time = 0;       /* aep varying time */
  siu_setup->aer_mips_profile = FALSE;/* disable MIPS profiling */
  siu_setup->aer_dbgstat_freq = 10;   /* query AER debug stats every 10 frames */
  siu_setup->aer_reset_bitfield = 0;  /* reset nothing when AER is reopened    */
  siu_setup->aer_valid_bitfield = 0;  /* reconfigure nothing when AER reopened */
  siu_setup->toggle_phone_mode = FALSE;
  siu_setup->txout_cng_only    = FALSE;
  siu_setup->use_fdnlp         = FALSE;
  siu_setup->asnr_enable       = FALSE;
  siu_setup->cng_enable        = FALSE;
  siu_setup->test_double_talk  = FALSE;
  siu_setup->new_phone_mode = aer_PHM_HF;

  siu_setup->pow_track_enabled = FALSE;
  siu_setup->txin_file_with_echo = FALSE;
  
  /* DRC related */
  siu_setup->drc_on_flag        = 0;   /* OFF */
  siu_setup->drc_cfgbits        = 0;
  siu_setup->drc_mips_profile   = TRUE;/* enable DRC MIPS profiling           */
  siu_setup->toggle_drcCtrl     = FALSE; 
  siu_setup->toggle_drcGetParam = FALSE; 

  /* VEU related */
  siu_setup->veu_enabled        = FALSE;
  
  /* enabling cache */
  siu_setup->doCacheEnable = TRUE;
  aersim_cacheEnable();
  aersim_cacheFlush();
  
  /* profiling */
  siu_setup->RxCyclesMax = (tlong)0;
  siu_setup->RxCyclesAvg = (tlong)0;
  siu_setup->TxCyclesMax = (tlong)0;
  siu_setup->TxCyclesAvg = (tlong)0;
  
  siu_setup->aer_trace_size_rx = (tsize)0;
  siu_setup->aer_trace_size_tx = (tsize)0;
  siu_setup->aer_trace_cfg     = (tuint)0;

  /* set up signal generators */
  aer_sim_setup_sgn (&siu_setup->rxSGN, &siu_setup->txSGN);

  /* set up acoustic echo path */
  aer_sim_setup_aep (&siu_setup->aep_params);
  
} /* aer_sim_init */

/******************************************************************************
 * FUNCTION PURPOSE: Setup signal generators for AER simulation.
 *
 *****************************************************************************/
void aer_sim_setup_sgn(sgnSimPars_t *rx_sgn, sgnSimPars_t *tx_sgn)
{
  /* Rx path signal generator parameters */
  rx_sgn->state = TRUE;         /* default state is on */
  rx_sgn->samp_freq = 8000;     /* default sampling frequency is 8000Hz */
  rx_sgn->dcoffset = 0;         /* default dc-offset  */
  rx_sgn->wftype = sgn_WFT_CSS; /* default waveform */
  rx_sgn->f1 = 1000;            /* default frequencies for tone generator (Hz) */
  rx_sgn->f2 = 0;  
  rx_sgn->amp1 = 30000;             /* default amplitudes of the two frequencies */
  rx_sgn->amp2 = 0;             
  rx_sgn->nlevel = -65;         /* default noise generator level in dBm0 */
  rx_sgn->nseed = 1531;         /* initial noise generator seed */
  rx_sgn->ntype = sgn_NT_GAUSS; /* Gaussian white noise */
  rx_sgn->pow_level = -10;      /* default HOTH/CSS power level in dBm0 */
  rx_sgn->hoth_seed = 918401;   /* initial HOTH generator seed */  
  rx_sgn->exception = NULL;
  rx_sgn->debug = NULL;
  rx_sgn->file_io_fcn = NULL;

  /* Tx path signal generator parameters */
  tx_sgn->state = TRUE;             /* default state is on */
  tx_sgn->samp_freq = 8000;         /* default sampling frequency is 8000Hz */
  tx_sgn->dcoffset = 0;             /* default dc-offset   */
  tx_sgn->wftype = sgn_WFT_CSS_DTK; /* default waveform    */
  tx_sgn->f1 = 601;                 /* default frequencies for tone generator(Hz)*/
  tx_sgn->f2 = 0;                   /* default amplitudes of the two frequencies */  
  tx_sgn->amp1 = 16000;
  tx_sgn->amp2 = 0;                 
  tx_sgn->nlevel = -65;             /* default noise generator level in dBm0 */
  tx_sgn->nseed = 0;                /* initial noise generator seed */
  tx_sgn->ntype = sgn_NT_GAUSS;     /* Gaussian white noise */
  tx_sgn->pow_level = -20;          /* default HOTH/CSS power level in dBm0  */
  tx_sgn->hoth_seed = 84771;        /* initial HOTH generator seed */
  tx_sgn->exception = NULL;
  tx_sgn->debug = NULL;
  tx_sgn->file_io_fcn = NULL;
  
} /* aer_sim_setup_sgn */

/******************************************************************************
 * FUNCTION PURPOSE: Setup AEP simulator for AER simulation.
 *
 *****************************************************************************/
void aer_sim_setup_aep(aepSimPars_t *aep_params)
{
  aep_params->aep_seg1_duration = 20*8;  /* 20ms aep segment1 @ 8kHz */
  aep_params->aep_seg2_duration = 20*8;  /* 20ms aep segment2 (periodic)  */
  aep_params->aep_delay = 0;             /* 0*125us */
  aep_params->aep_gain_l2 = 0;           /* gain = 2^0 = 1 */
  aep_params->aep_frame_M60dB = 20;      /* 20 frames for AEP to decay to -60dB */
  aep_params->aep_alpha = 0;
  aep_params->aep_delta_a = 0;
  aep_params->aep_morph_delay = 0;
} /* aer_sim_setup_aep */

/******************************************************************************
 * FUNCTION PURPOSE: Read AER simulation configuration 
 *
 *****************************************************************************/
void aer_sim_read_cfg(siuSetup_t *siu_setup, aersimFileIO_t *fio, FILE *test_cfg)
{
  int mic_type, num_mic, bf_steer_angle, rxin_fio_flag, rxout_fio_flag;
  int i, txin_fio_echo_flag, drc_on_flag, txout_fio_flag, txin_fio_flag;  
  int sampling_rates, profile_flag, fe_sgn_flag, ne_sgn_flag, band_split_flag;
  unsigned int aer_ctl_bitfield_0, aer_ctl_bitfield_1, agc_ctl_bitfield, bf_ctl_bitfield;
  unsigned int drc_cfgbits, bf_mics;
   
  fscanf(test_cfg, "%d\n", &mic_type);
  fscanf(test_cfg, "%d\n", &num_mic); 
  if(mic_type == AER_SIM_MIC_TYPE_ARRAY) {
    if(num_mic > AER_SIM_NUM_BF_MIC) {
      printf("Error! Number of BF mics is larger than the configured maximum.\n"); 
      printf("Change AER_SIM_NUM_BF_MIC in aersimcfg.h and recompile code.\n");
      exit(0);
    }
    siu_setup->system_cfg.num_BF_mic = (tint)num_mic;
    siu_setup->system_cfg.num_HF_mic = 1;
  }
  else if(mic_type == AER_SIM_MIC_TYPE_STAND_ALONE) {
    if(num_mic > AER_SIM_NUM_HF_MIC) {
      printf("Error! Number of HF mics is larger than the configured maximum.\n"); 
      printf("Change AER_SIM_NUM_HF_MIC in aersimcfg.h and recompile code.\n");
      exit(0);
    }
    siu_setup->system_cfg.num_HF_mic = (tint)num_mic;
    siu_setup->system_cfg.num_BF_mic = 0;
  }
  else {
    printf("Error! Mic type should be either 0 or 1.\n"); 
  }
  
  fscanf(test_cfg, "%d\n", &sampling_rates);
  fscanf(test_cfg, "%d\n", &band_split_flag);
  fscanf(test_cfg, "%x\n", &aer_ctl_bitfield_0);
  fscanf(test_cfg, "%x\n", &aer_ctl_bitfield_1);
  fscanf(test_cfg, "%x\n", &agc_ctl_bitfield);
  fscanf(test_cfg, "%x\n", &bf_ctl_bitfield);
  fscanf(test_cfg, "%x\n", &bf_mics);
  fscanf(test_cfg, "%d\n", &drc_on_flag);
  fscanf(test_cfg, "%x\n", &drc_cfgbits);
  fscanf(test_cfg, "%d\n", &fe_sgn_flag);
  fscanf(test_cfg, "%d\n", &ne_sgn_flag);
 
  fscanf(test_cfg, "%d\n", &profile_flag);
  
  fscanf(test_cfg, "%d\n", &rxin_fio_flag);
  fscanf(test_cfg, "%d\n", &rxout_fio_flag);
  fscanf(test_cfg, "%s\n", fio->rx_in_file_name);

  fscanf(test_cfg, "%d\n", &txin_fio_flag);
  fscanf(test_cfg, "%d\n", &txin_fio_echo_flag);
  fscanf(test_cfg, "%d\n", &txout_fio_flag);
  
  for(i=0; i<num_mic; i++) {
    fscanf(test_cfg, "%s\n", fio->tx_in_file_name[i]);
  }

  fscanf(test_cfg, "%d\n", &bf_steer_angle);
  fclose(test_cfg);
  
  siu_setup->system_cfg.mult_mic_type  = mic_type;
  siu_setup->system_cfg.sampling_rates = sampling_rates;
  siu_setup->system_cfg.band_split_flag = band_split_flag;
  siu_setup->aer_ctl_bitfield_0 = (tuint)aer_ctl_bitfield_0;
  siu_setup->aer_ctl_bitfield_1 = (tuint)aer_ctl_bitfield_1;
  siu_setup->agc_ctl_bitfield   = (tuint)agc_ctl_bitfield;
  siu_setup->bf_ctl_bitfield    = (tuint)bf_ctl_bitfield;
  siu_setup->bf_mics            = (tuint)bf_mics;
  siu_setup->aer_mips_profile = (tbool)profile_flag;
  siu_setup->ne_sgn_flag = ne_sgn_flag;
  siu_setup->fe_sgn_flag = fe_sgn_flag;
  siu_setup->drc_on_flag = drc_on_flag;
  siu_setup->drc_cfgbits = drc_cfgbits;
  siu_setup->system_cfg.bf_steer_angle = (tint)bf_steer_angle;

  fio->flag_rxin_rd   = (tbool)rxin_fio_flag;
  fio->flag_rxout_wr  = (tbool)rxout_fio_flag;
  fio->flag_txin_rd   = (tbool)txin_fio_flag;
  fio->flag_txout_wr  = (tbool)txout_fio_flag;
  fio->flag_txin_echo = (tbool)txin_fio_echo_flag;  
  
} /* aer_sim_read_cfg */

/******************************************************************************
 * FUNCTION PURPOSE: Configure simulation environment based on configuration file
 *
 *****************************************************************************/
void aer_sim_apply_config(siuSetup_t *siu_setup)
{
  siuSysCfg_t   *sys_cfg;
  aepSimPars_t  *aep_pars;  
  tint sampling_rates, aer_srate_rxin, aer_srate_txout, aer_srate_rxout;

  sys_cfg  = &siu_setup->system_cfg;
  aep_pars = &siu_setup->aep_params;

  sampling_rates = sys_cfg->sampling_rates;
  aer_srate_rxin  = aersim_chkbit(sampling_rates, aer_SRATE_RXIN_16K)
                      ? AER_SIM_SRATE_16000_DIV_8K : AER_SIM_SRATE_8000_DIV_8K;
  aer_srate_txout = aersim_chkbit(sampling_rates, aer_SRATE_TXOUT_16K)
                      ? AER_SIM_SRATE_16000_DIV_8K : AER_SIM_SRATE_8000_DIV_8K;
  aer_srate_rxout = aersim_chkbit(sampling_rates, aer_SRATE_RXOUT_TXIN_16K)
                      ? AER_SIM_SRATE_16000_DIV_8K : AER_SIM_SRATE_8000_DIV_8K;

  siu_setup->segment_size      = sys_cfg->segment_size * aer_srate_rxout;
  siu_setup->aer_rin_frm_size  = sys_cfg->frame_size * aer_srate_rxin;    
  siu_setup->aer_rout_frm_size = sys_cfg->frame_size * aer_srate_rxout;    
  siu_setup->aer_sin_frm_size  = sys_cfg->frame_size * aer_srate_rxout;    
  siu_setup->aer_sout_frm_size = sys_cfg->frame_size * aer_srate_txout;    
  aep_pars->aep_delay_samp     = aep_pars->aep_delay * aer_srate_rxout;
  aep_pars->aep_hseg1_length   = aep_pars->aep_seg1_duration * aer_srate_rxout;
  aep_pars->aep_hseg2_length   = aep_pars->aep_seg2_duration * aer_srate_rxout; 
  aep_pars->aep_filter_length  = aep_pars->aep_hseg1_length + aep_pars->aep_hseg2_length;
  aep_pars->aep_buf_length     = aep_pars->aep_hseg2_length * AEP_PARTIAL_ECHO_SIZ;
  
  if(siu_setup->aer_mips_profile) {
    if (!aersim_initCycleProfiling() ) {
      printf("Could not acquire profiling resources..Exiting\n");
      exit(0);
    }
  }  
  
} /* aer_sim_apply_config */

/******************************************************************************
 * FUNCTION PURPOSE: Initialize SIU simulator instance
 *
 *****************************************************************************/
void aer_sim_init_siu(siuInst_t *siu_inst_ptr, siuSetup_t *siu_setup_ptr)
{
  siuConfig_t   siu_cfg;
  siuTxConfig_t siutx_cfg;
  siuRxConfig_t siurx_cfg;

  siu_cfg.ID             = siuMakeID (SIU_MID_SIU, 0); /* use chnum 0 this time */ 
  siu_cfg.companding_law = siu_setup_ptr->system_cfg.ulaw;  
  siu_cfg.pcm_bits       = siu_setup_ptr->system_cfg.pcm_bits; 
  siu_cfg.core_heap      = NULL;
  siu_cfg.voice_heap     = NULL;
  siu_cfg.exception      = NULL;
  siu_cfg.debug          = NULL;
  
  siuInit (siu_inst_ptr, &siu_cfg);      /* inits siu context & siu instance */
  
  /* Open Tx Task */
  siutx_cfg.send_out_frame_length = siu_setup_ptr->aer_sout_frm_size;
  siutx_cfg.send_in_frame_length  = siu_setup_ptr->aer_sin_frm_size;
  siutx_cfg.num_HF_mic            = siu_setup_ptr->system_cfg.num_HF_mic;
  siuTxOpen (siu_inst_ptr, &siutx_cfg);
  
  /* Open Rx Task */  
  siurx_cfg.rout_frm_len = siu_setup_ptr->aer_rout_frm_size;
  siurx_cfg.rin_frm_len  = siu_setup_ptr->aer_rin_frm_size;
  siuRxOpen (siu_inst_ptr, &siurx_cfg);
 
  /* Set SIU receive in buffer to be consistent with legacy AEP code - need to
     be cleaned up. */
  siu_inst_ptr->aer_recv_in_buf = &siu_rx_del_buf[0];

} /* aer_sim_init_siu */

/******************************************************************************
 * FUNCTION PURPOSE: Initialize PIU simulator
 *
 *****************************************************************************/
void aer_sim_init_piu(piuInst_t *piu_inst, siuSetup_t *siu_setup)
{
  piuInit(piu_inst, siu_setup->aer_rout_frm_size);
}

/******************************************************************************
 * FUNCTION PURPOSE: Instantiate AER modules - AER, AGC, DRC, BF, MSS
 *
 *****************************************************************************/
void aer_sim_instantiate_aer(siuInst_t *siu_inst, siuSetup_t *siu_setup)
{
  tint num_mic, samp_rates;
  
  num_mic    = siu_setup->system_cfg.num_HF_mic;
  samp_rates = siu_setup->system_cfg.sampling_rates;
  
  /* Instantiate AER module */
  siu_new_aer (siu_inst, num_mic, siu_setup->system_cfg.band_split_flag);

  /* Instantiate AGC module */
  siu_new_agc (siu_inst, num_mic);

  /* Instantiate DRC module */
  siu_new_drc(siu_inst);

  /* Instantiate BF module */
  if(siu_setup->system_cfg.mult_mic_type == AER_SIM_MIC_TYPE_ARRAY) {
    siu_new_bf(siu_inst, samp_rates);
  }
  
  /* Instantiate MSS module */
  siu_new_mss(siu_inst, num_mic, samp_rates);
  
  /* Open AER module instances */
  siu_open_aer(siu_inst, &siu_setup->aer_sys_params, samp_rates, num_mic);
  
  /* Open AGC module instances */
  siu_open_agc(siu_inst, samp_rates, num_mic);
  
  /* Open DRC module instances */
  siu_open_drc(siu_inst, siu_setup->drc_cfgbits, samp_rates); 

  /* Open BF module */
  if(siu_setup->system_cfg.mult_mic_type == AER_SIM_MIC_TYPE_ARRAY) {
    siu_open_bf(siu_inst, samp_rates, siu_setup->system_cfg.num_BF_mic);
  }
  
  /* Open MSS module */
  siu_open_mss(siu_inst, num_mic, samp_rates);
  
  /* Activate HF mic 0 */
  siu_mmic_init(siu_inst);
    
} /* aer_sim_instantiate_aer */

/******************************************************************************
 * FUNCTION PURPOSE: Open signal generators (SGN)
 *
 *****************************************************************************/
void aer_sim_open_txrx_sgns(sgnInstBuf_t *sgn_inst_tx, 
                            sgnInstBuf_t *sgn_inst_rx, siuSetup_t *siu_setup)
{
  int i;
  tint sgn_size;
  
  /* Get memory requirements for SGN */
  sgnGetSizes(&sgn_size);
  
  if(sgn_size > sizeof(sgnInstBuf_t)) {
    printf("Error! SGN buffer has no enough memory.\n");
    exit(0);
  }

  /* Turn on/off SGN based on simulation configuration */
  siu_setup->txSGN.state = siu_setup->ne_sgn_flag;
  siu_setup->rxSGN.state = siu_setup->fe_sgn_flag;
  
  /* Open Rx SGN */
  siu_setup->rxSGN.samp_freq = aersim_chkbit(siu_setup->system_cfg.sampling_rates, 
                                             aer_SRATE_RXIN_16K) ? 16000 : 8000;
  aer_sim_sgn_init(sgn_inst_rx, &siu_setup->rxSGN);
  aer_sim_sgn_set (sgn_inst_rx, &siu_setup->rxSGN);
  
  /* Open Tx SGNs */
  siu_setup->txSGN.samp_freq = aersim_chkbit(siu_setup->system_cfg.sampling_rates, 
                                             aer_SRATE_RXOUT_TXIN_16K) ? 16000 : 8000;
  for(i=0; i<siu_setup->system_cfg.num_HF_mic; i++) {
    aer_sim_sgn_init(&sgn_inst_tx[i], &siu_setup->txSGN);
    aer_sim_sgn_set (&sgn_inst_tx[i], &siu_setup->txSGN);
  }  
  
} /* aer_sim_open_txrx_sgn */

/******************************************************************************
 * FUNCTION PURPOSE: Open acoustic echo path module (AEP)
 *
 *****************************************************************************/
void aer_sim_open_aep(aepSimInst_t *aep_inst, siuSetup_t *siu_setup)
{
  tint delay, samp_rate, frm_size;
  tbool aep_morph_flag;

  /* Initialize AEP */
  delay = siu_setup->aep_params.aep_delay_samp;
  samp_rate = aersim_chkbit(siu_setup->system_cfg.sampling_rates, aer_SRATE_RXOUT_TXIN_16K)
                            ? AER_SIM_SRATE_16000_DIV_8K : AER_SIM_SRATE_8000_DIV_8K;
  aep_morph_flag = siu_setup->aep_vary_time > 0;
  aer_sim_aep_init(aep_inst, delay, samp_rate, aep_morph_flag);

  /* Open AEP */
  frm_size = siu_setup->aer_rout_frm_size;
  aer_sim_aep_open(aep_inst, &siu_setup->aep_params, frm_size, samp_rate);
  
} /* aer_sim_open_aep */


/******************************************************************************
 * FUNCTION PURPOSE: Configure AER parameters for simulation
 ******************************************************************************
 * DESCRIPTION: This function configures AER parameterst to verify functionality
 *              of AER modules: AER, AGC, DRC, BF, MSS.
 *
 *****************************************************************************/
void aer_sim_config_aer_params(siuInst_t *inst, siuSetup_t *siu_setup)
{
  void *aerInst;
  aerControl_t aer_ctl;
  agcControl_t agcCtl;
  bfControl_t bf_ctl;
  siuGainControl_t siuGainCtl;
  tint error_code;
  int i;
  
  for (i=0; i<inst->phone_sys.num_HF_mic; i++) {
    aerInst = inst->aerHFInst[i];

    /*== AER valid bitfield 0 ==*/
    aer_ctl.valid_bitfield_0 =  aer_CTL_VALID0_MODES_CTL0
                              | aer_CTL_VALID0_MODES_CTL1
                              | aer_CTL_VALID0_RX_EQ_PARAMS
                              | aer_CTL_VALID0_TX_EQ_PARAMS;
    /* Set AER operation modes: siu_setup->aer_ctl_bitfield_0 and
       siu_setup->aer_ctl_bitfield_1 are set through simulation configuration
       file: aersimcfg.txt. 
     * The mask is set to 0xffff to set all bits, either to 0 or to 1.     */
    aer_ctl.modes_0.mask  = 0xffff; 
    aer_ctl.modes_0.value = siu_setup->aer_ctl_bitfield_0;
    aer_ctl.modes_1.mask  = 0xffff; 
    aer_ctl.modes_1.value = siu_setup->aer_ctl_bitfield_1;
                            
    /* Load equalizer coefficients (unity tap) in both tx and rx directions */
    aer_ctl.rxeq_params = (aerEqConfig_t *)&sampleEqBqParamsRx;
    aer_ctl.txeq_params = (aerEqConfig_t *)&sampleEqBqParamsTx;

    /*== AER valid bitfield 1 ==*/
    /* configure NLP */
    aer_ctl.valid_bitfield_1 =  aer_CTL_VALID1_NLP_CLOSS_TARGET
                              | aer_CTL_VALID1_NLP_TX_CLIPAGG
                              | aer_CTL_VALID1_NLP_LINATTN_MAX_ERLE
                              | aer_CTL_VALID1_NLP_CLIPPER_MAX_ERLE
                              | aer_CTL_VALID1_NLP_TX_IN_TC           
                              | aer_CTL_VALID1_NLP_TX_OUT_TC          
                              | aer_CTL_VALID1_NLP_RX_IN_TC           
                              | aer_CTL_VALID1_NLP_RX_OUT_TC          
                              | aer_CTL_VALID1_GAIN_SPLIT_TC          
                              | aer_CTL_VALID1_CNG_RX_LEVEL
                              | aer_CTL_VALID1_CNG_TX_LEVEL;
    aer_ctl.nlp_combloss_target  = 700;
    aer_ctl.nlp_clip_agg_l2      = 4;
    aer_ctl.nlp_linattn_max_erle = 30; 
    aer_ctl.nlp_clipper_max_erle = 30;
    aer_ctl.nlp_tx_in_tc         = 4;
    aer_ctl.nlp_tx_out_tc        = 1;
    aer_ctl.nlp_rx_in_tc         = 4;
    aer_ctl.nlp_rx_out_tc        = 1;
    aer_ctl.gain_split_tc        = 3;
    aer_ctl.cng_rx_level         = -70;
    aer_ctl.cng_tx_level         = -70;

    /*== AER valid bitfield 2 ==*/
    aer_ctl.valid_bitfield_2 =  aer_CTL_VALID2_TX_FDNLP_CNG_MAX
                              | aer_CTL_VALID2_NR_FBIN1_LIM          
                              | aer_CTL_VALID2_NR_FBIN2_LIM          
                              | aer_CTL_VALID2_NR_FBAND1_MAX_ATTEN   
                              | aer_CTL_VALID2_NR_FBAND2_MAX_ATTEN   
                              | aer_CTL_VALID2_NR_FBAND3_MAX_ATTEN       
                              | aer_CTL_VALID2_NR_NOISE_THRESH;
    aer_ctl.txfdnlp_cng_max = -40; 
    aer_ctl.nr_noise_thresh = -65; 
    aer_ctl.nr_fbin1_lim    = 5;
    aer_ctl.nr_fbin2_lim    = 32;
    aer_ctl.nr_fband1_max_atten = 9;
    aer_ctl.nr_fband2_max_atten = 9;
    aer_ctl.nr_fband3_max_atten = 9;
    
    /*== AER valid bitfield 3 ==*/
    aer_ctl.valid_bitfield_3 =  aer_CTL_VALID3_HLC_THRESHOLD
                              | aer_CTL_VALID3_HLC_RAMP_OUT_TC
                              | aer_CTL_VALID3_HLC_POWER_TC                              
                              | aer_CTL_VALID3_COH_HANGOVER     
                              | aer_CTL_VALID3_COH_RATIO_THRESH  
                              | aer_CTL_VALID3_COH_CNT_THRESH;
    aer_ctl.thresh_hlc      = -20; 
    aer_ctl.ramp_out_tc_hlc = 700; 
    aer_ctl.power_tc_hlc    = 1; 
    aer_ctl.coh_hangover    = 600;
    aer_ctl.coh_ratio_thresh= 6000;
    aer_ctl.coh_cnt_thresh  = 30;

    /*== AER valid bitfield 4 ==*/
    aer_ctl.valid_bitfield_4 =  aer_CTL_VALID4_DT_THRESH
                              | aer_CTL_VALID4_CLIP_SCALE
                              | aer_CTL_VALID4_DT_HANGOVER;
    aer_ctl.dt_thresh = 64;
    aer_ctl.clip_scale_curve = 0x3c404050; 
    aer_ctl.dt_hangover = 5;

    error_code = aerControl (aerInst,(aerControl_t *)&aer_ctl);

    if(error_code != aer_NOERR) {
      printf("AER parameters configuration error!\n");
      exit(0);
    }
    
    /*== AGC configuration  ==*/
    agcCtl.valid_bitfield =  agc_CTL_VALID_MODES
                           | agc_CTL_VALID_SAT_THRESHOLD 
                           | agc_CTL_VALID_SAT_HANGOVER
                           | agc_CTL_VALID_MIC_GAIN_MAX
                           | agc_CTL_VALID_MIC_GAIN_MIN
                           | agc_CTL_VALID_MICGAIN_SWING_MAX;  
    
    /* Set AGC operation modes: siu_setup->agc_ctl_bitfield is set through
       simulation config file: aersimcfg.txt. 
     * The mask is set to 0xffff to set all bits, either to 0 or to 1.
     */
    agcCtl.modes.mask  = 0xffff; 
    agcCtl.modes.value = siu_setup->agc_ctl_bitfield;
    agcCtl.sat_threshold = AER_SIM_AGC_SAT_THRESHOLD;
    agcCtl.sat_hangover  = AER_SIM_AGC_SAT_HANGOVER;
    agcCtl.max_micgain   = AER_SIM_AGC_MICGAIN_MAX;
    agcCtl.min_micgain   = AER_SIM_AGC_MICGAIN_MIN;
    agcCtl.max_micgain_swing = AER_SIM_AGC_GAINSWING_MAX;
    
    error_code = agcControl (inst->agcHFInst[i],(agcControl_t *)&agcCtl);

    if(error_code != agc_NOERR) {
      printf("AGC parameters configuration error!\n");
      exit(0);
    }    
    
    /* Set speaker gain or Rx analog gain in simulation as well as AER and AGC 
       record of this gain. */    
    siuGainCtl.ctl_code = siu_SET_SPK_GAIN;    
    siuGainCtl.u.spk_gain = AER_SIM_HF_SPKGAIN_0dB; 
    siuGainControl(inst->agcHFhandles[i], &siuGainCtl);

    /* Set mic gain or Tx analog gain in simulation as well as AER and AGC 
       record of this gain. */    
    siuGainCtl.ctl_code = siu_SET_MIC_GAIN;    
    siuGainCtl.u.mic_gain = AER_SIM_HF_MICGAIN_0dB; 
    siuGainControl(inst->agcHFhandles[i], &siuGainCtl);
  } /* for loop ends here */

  siu_control_drc(inst, siu_setup->system_cfg.sampling_rates, siu_setup->drc_on_flag);

  if(siu_setup->system_cfg.mult_mic_type == AER_SIM_MIC_TYPE_ARRAY) {
  
    /* config bf parameters */  
    bf_ctl.valid_bitfield = bf_CTL_CONFIG|bf_CTL_MICS; /* Enable BF, operation mode change */
    bf_ctl.config.mask    = bf_CTL_CFG_ENABLE|bf_CTL_CFG_OPERATION;
    bf_ctl.config.value   = siu_setup->bf_ctl_bitfield;/* read from aersimcfg.txt */ 
    bf_ctl.mics           = siu_setup->bf_mics;        /* read from aersimcfg.txt */
    
    if(siu_setup->system_cfg.bf_steer_angle == AER_SIM_BF_STEER_ANGLE_0) {
      siu_config_bf(inst, &bf_ctl, bf_coeffs_0, siu_setup->system_cfg.sampling_rates, 
                    siu_setup->system_cfg.num_BF_mic);
    }                  
    if(siu_setup->system_cfg.bf_steer_angle == AER_SIM_BF_STEER_ANGLE_45) {
      siu_config_bf(inst, &bf_ctl, bf_coeffs_45, siu_setup->system_cfg.sampling_rates, 
                    siu_setup->system_cfg.num_BF_mic);
    }                  
    if(siu_setup->system_cfg.bf_steer_angle == AER_SIM_BF_STEER_ANGLE_90) {
      siu_config_bf(inst, &bf_ctl, bf_coeffs_90, siu_setup->system_cfg.sampling_rates, 
                    siu_setup->system_cfg.num_BF_mic);
    }                  
  }    
} /* aer_sim_config_aer_params */


/* enable cache */
void aersim_cacheEnable()
{
#ifndef gnu_targets_arm_GCArmv7A
  memarchcfg_cacheEnable();
#endif  
}

/* flush cache for profiling - need to recompile with "profile" option */
void aersim_cacheFlush()
{
#ifndef gnu_targets_arm_GCArmv7A
#ifdef AERSIM_PROFILE_FLUSH_CACHE
  memarchcfg_cacheFlush();
#endif
#endif
}

/* prepare for MIPS profiling through direct reading of CPU cycle count */
tbool aersim_initCycleProfiling()
{
#ifndef gnu_targets_arm_GCArmv7A
  return profile_initCycleProfiling();
#else
  return TRUE;
#endif  
}

void aersim_profile_get_cycle_before_aer(siuSetup_t *siu_setup)
{
#ifndef gnu_targets_arm_GCArmv7A
  siu_setup->cycles_before_aer = profile_getCPUcycleCount();
#endif  
}  

void aersim_profile_get_cycle_after_aer_send(siuSetup_t *siu_setup)
{
#ifndef gnu_targets_arm_GCArmv7A
  tlong cycles_aer;
  
  cycles_aer = (tlong)(profile_getCPUcycleCount() - siu_setup->cycles_before_aer);
  if(cycles_aer < 0) {
    cycles_aer += sdk_PROFILE_MAXCNT;      
  }

  if(cycles_aer > siu_setup->TxCyclesMax) {
    siu_setup->TxCyclesMax = cycles_aer;
  }
  siu_setup->TxCyclesAvg = (siu_setup->TxCyclesAvg*511+cycles_aer) >> 9; 
#endif  
}

void aersim_profile_get_cycle_after_aer_receive(siuSetup_t *siu_setup)
{
#ifndef gnu_targets_arm_GCArmv7A
  tlong cycles_aer;
  
  cycles_aer = (tlong)(profile_getCPUcycleCount() - siu_setup->cycles_before_aer);
  if(cycles_aer < 0) {
    cycles_aer += sdk_PROFILE_MAXCNT;      
  }

  if(cycles_aer > siu_setup->RxCyclesMax) {
    siu_setup->RxCyclesMax = cycles_aer;
  }
  siu_setup->RxCyclesAvg = (siu_setup->RxCyclesAvg*511+cycles_aer) >> 9; 
#endif  
}

/******************************************************************************
 * FUNCTION PURPOSE: Configure all AER parameters to test the API
 *****************************************************************************/
void aer_sim_test_api(void *aer_inst)
{
  aerControl_t aer_ctl;
  tint error_code;
  
  /* valid bitfield 0 */
  aer_ctl.valid_bitfield_0 =   aer_CTL_VALID0_MODES_CTL0
                             | aer_CTL_VALID0_MODES_CTL1         
                             | aer_CTL_VALID0_MODES_CTL2         
                             | aer_CTL_VALID0_Y2X_DELAY          
                             | aer_CTL_VALID0_SAMPLING_RATES
                             | aer_CTL_VALID0_TAIL_LENGTH        
                             | aer_CTL_VALID0_TX_AG_CHG_DELAY 
                             | aer_CTL_VALID0_RX_AG_CHG_DELAY    
                             | aer_CTL_VALID0_TX_AG_CHG_INTERP  
                             | aer_CTL_VALID0_PHONE_MODE              
                             | aer_CTL_VALID0_RX_ANALOG         
                             | aer_CTL_VALID0_TX_ANALOG         
                             | aer_CTL_VALID0_RX_DIGITAL        
                             | aer_CTL_VALID0_TX_DIGITAL        
                             | aer_CTL_VALID0_RX_EQ_PARAMS      
                             | aer_CTL_VALID0_TX_EQ_PARAMS; 

  aer_ctl.modes_0.mask     = 0xffff; 
  aer_ctl.modes_0.value    = 0x188f;
  aer_ctl.modes_1.mask     = 0xffff; 
  aer_ctl.modes_1.value    = 0xc1ce;
  aer_ctl.modes_2.mask     = 0xffff; 
  aer_ctl.modes_2.value    = 0x001d;
  aer_ctl.y2x_delay        = AER_SIM_PIU_DEL_RX + AER_SIM_PIU_DEL_TX - AER_SIM_SIU_Y2X_DEL_ADJ - 1;
  aer_ctl.srate_bitfield   = 6;       /* Rx in 8kHz, Tx out 16kHz, Rx out/Tx in 16kHz */
  aer_ctl.tail_length      = 800;     /* 800 = 100msec/125usec */
  aer_ctl.delay_tx_ag_chg  = AER_SIM_PIU_DEL_TX + 1;
  aer_ctl.delay_rx_ag_chg  = AER_SIM_PIU_DEL_RX + 1;
  aer_ctl.num_samp_interp  = 5;
  aer_ctl.phone_mode       = aer_PHM_HES;
  aer_ctl.gain_rx_analog   = AER_SIM_HF_MICGAIN_0dB - 16;
  aer_ctl.gain_tx_analog   = AER_SIM_HF_SPKGAIN_0dB - 32;
  aer_ctl.gain_rx_digital  = 16;
  aer_ctl.gain_tx_digital  = 32; 
  aer_ctl.rxeq_params = (aerEqConfig_t *)&sampleEqBqParamsRx;
  aer_ctl.txeq_params = (aerEqConfig_t *)&sampleEqBqParamsTx;
                             
  /* valid bitfield 1 */
  aer_ctl.valid_bitfield_1 =   aer_CTL_VALID1_NLP_CLOSS_TARGET       
                             | aer_CTL_VALID1_NLP_TX_CLIPAGG         
                             | aer_CTL_VALID1_NLP_LINATTN_MAX_ERLE   
                             | aer_CTL_VALID1_NLP_CLIPPER_MAX_ERLE   
                             | aer_CTL_VALID1_NLP_TOTAL_LINATTN_MIN  
                             | aer_CTL_VALID1_NLP_RX_LINATTN_MIN     
                             | aer_CTL_VALID1_NLP_RX_LINATTN_MAX     
                             | aer_CTL_VALID1_NLP_TX_LINATTN_MIN     
                             | aer_CTL_VALID1_NLP_TX_LINATTN_MAX     
                             | aer_CTL_VALID1_NLP_TX_IN_TC           
                             | aer_CTL_VALID1_NLP_TX_OUT_TC          
                             | aer_CTL_VALID1_NLP_RX_IN_TC           
                             | aer_CTL_VALID1_NLP_RX_OUT_TC          
                             | aer_CTL_VALID1_GAIN_SPLIT_TC          
                             | aer_CTL_VALID1_CNG_RX_LEVEL           
                             | aer_CTL_VALID1_CNG_TX_LEVEL;           
  aer_ctl.nlp_combloss_target   = 900;
  aer_ctl.nlp_clip_agg_l2       = 6;
  aer_ctl.nlp_linattn_max_erle  = 27; 
  aer_ctl.nlp_clipper_max_erle  = 27;
  aer_ctl.nlp_total_linattn_min = 2;
  aer_ctl.nlp_rx_linattn_min    = 1;
  aer_ctl.nlp_rx_linattn_max    = 12;
  aer_ctl.nlp_tx_linattn_min    = 1;
  aer_ctl.nlp_tx_linattn_max    = 12;
  aer_ctl.nlp_tx_in_tc          = 5;
  aer_ctl.nlp_tx_out_tc         = 5;
  aer_ctl.nlp_rx_in_tc          = 5;
  aer_ctl.nlp_rx_out_tc         = 5;
  aer_ctl.gain_split_tc         = 5;
  aer_ctl.cng_rx_level          = -60;
  aer_ctl.cng_tx_level          = -60;
                                  
  
  /* valid bitfield 2 */
  aer_ctl.valid_bitfield_2 =   aer_CTL_VALID2_TX_FDNLP_MSEC_DELAY   
                             | aer_CTL_VALID2_TX_FDNLP_BIN_LO1     
                             | aer_CTL_VALID2_TX_FDNLP_BIN_LO2     
                             | aer_CTL_VALID2_TX_FDNLP_BIN_HI1     
                             | aer_CTL_VALID2_TX_FDNLP_BIN_HI2     
                             | aer_CTL_VALID2_TX_FDNLP_CNG_MAX     
                             | aer_CTL_VALID2_NR_FBIN1_LIM         
                             | aer_CTL_VALID2_NR_FBIN2_LIM         
                             | aer_CTL_VALID2_NR_FBAND1_MAX_ATTEN  
                             | aer_CTL_VALID2_NR_FBAND2_MAX_ATTEN  
                             | aer_CTL_VALID2_NR_FBAND3_MAX_ATTEN  
                             | aer_CTL_VALID2_NR_SIG_UPD_RATE_MAX  
                             | aer_CTL_VALID2_NR_SIG_UPD_RATE_MIN  
                             | aer_CTL_VALID2_NR_NOISE_THRESH      
                             | aer_CTL_VALID2_NR_NOISE_HANGOVER;

  aer_ctl.txfdnlp_msec_delay  = 6;
  aer_ctl.txfdnlp_bin_lo1     = 2;
  aer_ctl.txfdnlp_bin_lo2     = 5;
  aer_ctl.txfdnlp_bin_hi1     = 60;
  aer_ctl.txfdnlp_bin_hi2     = 62;
  aer_ctl.txfdnlp_cng_max     = -62;
  aer_ctl.nr_fbin1_lim        = 12;
  aer_ctl.nr_fbin2_lim        = 40;
  aer_ctl.nr_fband1_max_atten = 12;
  aer_ctl.nr_fband2_max_atten = 12;
  aer_ctl.nr_fband3_max_atten = 12;
  aer_ctl.nr_sig_upd_rate_max = 32000;
  aer_ctl.nr_sig_upd_rate_min = 30000;
  aer_ctl.nr_noise_thresh     = -70;
  aer_ctl.nr_noise_hangover   = 100;  

  /* valid bitfield 3 */
  aer_ctl.valid_bitfield_3 =   aer_CTL_VALID3_COH_HANGOVER     
                             | aer_CTL_VALID3_COH_RATIO_THRESH  
                             | aer_CTL_VALID3_COH_CNT_THRESH    
                             | aer_CTL_VALID3_HLC_THRESHOLD     
                             | aer_CTL_VALID3_HLC_RAMP_OUT_TC   
                             | aer_CTL_VALID3_HLC_POWER_TC      
                             | aer_CTL_VALID3_THRESH_RX_NLD     
                             | aer_CTL_VALID3_THRESH_NOISE_RX   
                             | aer_CTL_VALID3_THRESH_NOISE_TX   
                             | aer_CTL_VALID3_HANGOVER_RXTOTX   
                             | aer_CTL_VALID3_HANGOVER_TXTORX   
                             | aer_CTL_VALID3_TX_SLIM_MODE      
                             | aer_CTL_VALID3_RX_SLIM_MODE      
                             | aer_CTL_VALID3_NG_HANGOVER       
                             | aer_CTL_VALID3_NG_RAMPIN_PERIOD  
                             | aer_CTL_VALID3_NG_NOISE_LEVEL;
  aer_ctl.coh_hangover      = 500;
  aer_ctl.coh_ratio_thresh  = 7000;
  aer_ctl.coh_cnt_thresh    = 25;
  aer_ctl.thresh_hlc        = -12;
  aer_ctl.ramp_out_tc_hlc   = 500;
  aer_ctl.power_tc_hlc      = 2;
  aer_ctl.thresh_rx_nld     = 32500;
  aer_ctl.thresh_rx_noise   = 6000;
  aer_ctl.thresh_tx_noise   = 6000;
  aer_ctl.hangover_rx_to_tx = 20;
  aer_ctl.hangover_tx_to_rx = 20;
  aer_ctl.tx_slim_mode      = 1;
  aer_ctl.rx_slim_mode      = 1;
  aer_ctl.hangover_ng       = 500;
  aer_ctl.rampin_period_ng  = 700;
  aer_ctl.noise_level_ng    = -65;   

  /*== AER valid bitfield 4 ==*/
  aer_ctl.valid_bitfield_4 =  aer_CTL_VALID4_DT_THRESH
                            | aer_CTL_VALID4_CLIP_SCALE
                            | aer_CTL_VALID4_DT_HANGOVER;
  aer_ctl.dt_thresh = 80;
  aer_ctl.clip_scale_curve = 0x30405060; 
  aer_ctl.dt_hangover = 10;

  error_code = aerControl (aer_inst, &aer_ctl);

  if(error_code != aer_NOERR) {
    printf("AER parameters configuration error in aer_sim_test_api()!\n");
    exit(0);
  }   
} /* aer_sim_test_api */

/* nothing past this line */

