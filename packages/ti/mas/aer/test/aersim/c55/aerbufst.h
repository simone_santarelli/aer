/******************************************************************************
 * FILE PURPOSE: Memory Buffer Size and Alignment Definitions for C55x
 ******************************************************************************
 * FILE NAME:   c55\aerbufst.h
 *
 * DESCRIPTION: Contains size and alignment definitions for AER/AGC/DRC buffers
 *              for C55x.  
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#ifndef _AERBUFSTC55_H
#define _AERBUFSTC55_H

/* Buffer sizes for AER - returned by aerGetSizes().
   Buffer size is in unit of tword. For C55x, tword is 16-bit. */
#define AER_SIM_BUF0_SIZE  426
#define AER_SIM_BUF1_SIZE  1028
#define AER_SIM_BUF2_SIZE_HF  5150   /* 200 msec tail */
//#define AER_SIM_BUF2_SIZE_HF  3090   /* 120 msec tail */
//#define AER_SIM_BUF2_SIZE_HS  1545   /* 60  msec tail */
#define AER_SIM_BUF2_SIZE_HS  515    /* 20  msec tail */
#define AER_SIM_BUF3_SIZE_HF  9785  
//#define AER_SIM_BUF3_SIZE_HF  5665  
#define AER_SIM_BUF3_SIZE_HS  2575
#define AER_SIM_BUF4_SIZE  514
#define AER_SIM_BUF5_SIZE  640
#define AER_SIM_BUF6_SIZE  1028
#define AER_SIM_BUF7_SIZE  96
#define AER_SIM_BUF8_SIZE  5150
//#define AER_SIM_BUF8_SIZE  3090
#define AER_SIM_BUF9_SIZE  1542
#define AER_SIM_BUF10_SIZE 34
#define AER_SIM_BUF11_SIZE 33
#define AER_SIM_BUF12_SIZE 1    //dummy
//#define AER_SIM_BUF12_SIZE 80   for bandsplit operation
#define AER_SIM_BUF13_SIZE 160
//#define AER_SIM_BUF13_SIZE 400  for bandsplit operation
#define AER_SIM_BUF14_SIZE 1    //dummy
//#define AER_SIM_BUF14_SIZE 160  for bandsplit operation
#define AER_SIM_BUF15_SIZE 1    //dummy  
//#define AER_SIM_BUF15_SIZE 96   for bandsplit operation
#define AER_SIM_BUF16_SIZE 129
#define AER_SIM_BUF17_SIZE 609
#define AER_SIM_BUF18_SIZE 646
#define AER_SIM_BUF19_SIZE 160
#define AER_SIM_BUF20_SIZE 320

/* Buffer size for AGC - returned by agcGetSizes(). */
#define AGC_SIM_BUF0_SIZE 58

/* Buffer sizes for DRC - returned by drcGetSizes(). */
#define DRC_SIM_BUF0_SIZE 74
#define DRC_SIM_BUF1_SIZE 640
#define DRC_SIM_BUF2_SIZE 32
#define DRC_SIM_BUF3_SIZE 150
#define DRC_SIM_BUF4_SIZE 10

/* Buffer alignment for AER - returned by aerGetSizes().
   Buffers must be aligned on boundary of 2^(AER_SIM_BUFx_ALGN_LOG2) twords. */
#define AER_SIM_BUF0_ALGN_LOG2  1
#define AER_SIM_BUF1_ALGN_LOG2  1
#define AER_SIM_BUF2_ALGN_LOG2  0
#define AER_SIM_BUF3_ALGN_LOG2  0
#define AER_SIM_BUF4_ALGN_LOG2  1
#define AER_SIM_BUF5_ALGN_LOG2  0
#define AER_SIM_BUF6_ALGN_LOG2  1
#define AER_SIM_BUF7_ALGN_LOG2  1
#define AER_SIM_BUF8_ALGN_LOG2  1
#define AER_SIM_BUF9_ALGN_LOG2  1
#define AER_SIM_BUF10_ALGN_LOG2 1
#define AER_SIM_BUF11_ALGN_LOG2 1
#define AER_SIM_BUF12_ALGN_LOG2 0
#define AER_SIM_BUF13_ALGN_LOG2 0
#define AER_SIM_BUF14_ALGN_LOG2 0
#define AER_SIM_BUF15_ALGN_LOG2 0
#define AER_SIM_BUF16_ALGN_LOG2 0
#define AER_SIM_BUF17_ALGN_LOG2 0
#define AER_SIM_BUF18_ALGN_LOG2 1
#define AER_SIM_BUF19_ALGN_LOG2 0
#define AER_SIM_BUF20_ALGN_LOG2 0

/* Buffer alignment for AGC - returned by agcGetSizes(). */
#define AGC_SIM_BUF0_ALGN_LOG2  1

/* Buffer alignment for DRC - returned by drcGetSizes(). */
#define DRC_SIM_BUF0_ALGN_LOG2  1 
#define DRC_SIM_BUF1_ALGN_LOG2  1
#define DRC_SIM_BUF2_ALGN_LOG2  0
#define DRC_SIM_BUF3_ALGN_LOG2  0
#define DRC_SIM_BUF4_ALGN_LOG2  1

/* Define macros used by #pragma DATA_ALIGN in aersimbufs.c.
   The pragma DATA_ALIGN (symbol, constant) aligns the symbol to an alignment 
   boundary. For C55x, the boundary is the value of the constant in words. 
   For example, a constant of 2 specifies a 2-word or 32-bit alignment.     */ 
#define AER_SIM_BUF0_ALGN  (1<<AER_SIM_BUF0_ALGN_LOG2)
#define AER_SIM_BUF1_ALGN  (1<<AER_SIM_BUF1_ALGN_LOG2)
#define AER_SIM_BUF2_ALGN  (1<<AER_SIM_BUF2_ALGN_LOG2)
#define AER_SIM_BUF3_ALGN  (1<<AER_SIM_BUF3_ALGN_LOG2)
#define AER_SIM_BUF4_ALGN  (1<<AER_SIM_BUF4_ALGN_LOG2)
#define AER_SIM_BUF5_ALGN  (1<<AER_SIM_BUF5_ALGN_LOG2)
#define AER_SIM_BUF6_ALGN  (1<<AER_SIM_BUF6_ALGN_LOG2)
#define AER_SIM_BUF7_ALGN  (1<<AER_SIM_BUF7_ALGN_LOG2)
#define AER_SIM_BUF8_ALGN  (1<<AER_SIM_BUF8_ALGN_LOG2)
#define AER_SIM_BUF9_ALGN  (1<<AER_SIM_BUF9_ALGN_LOG2)
#define AER_SIM_BUF10_ALGN (1<<AER_SIM_BUF10_ALGN_LOG2)
#define AER_SIM_BUF11_ALGN (1<<AER_SIM_BUF11_ALGN_LOG2)
#define AER_SIM_BUF12_ALGN (1<<AER_SIM_BUF12_ALGN_LOG2)
#define AER_SIM_BUF13_ALGN (1<<AER_SIM_BUF13_ALGN_LOG2)
#define AER_SIM_BUF14_ALGN (1<<AER_SIM_BUF14_ALGN_LOG2)
#define AER_SIM_BUF15_ALGN (1<<AER_SIM_BUF15_ALGN_LOG2)
#define AER_SIM_BUF16_ALGN (1<<AER_SIM_BUF16_ALGN_LOG2)
#define AER_SIM_BUF17_ALGN (1<<AER_SIM_BUF17_ALGN_LOG2)
#define AER_SIM_BUF18_ALGN (1<<AER_SIM_BUF18_ALGN_LOG2)
#define AER_SIM_BUF19_ALGN (1<<AER_SIM_BUF19_ALGN_LOG2)
#define AER_SIM_BUF20_ALGN (1<<AER_SIM_BUF20_ALGN_LOG2)

#define AGC_SIM_BUF0_ALGN  (1<<AGC_SIM_BUF0_ALGN_LOG2)

#define DRC_SIM_BUF0_ALGN  (1<<DRC_SIM_BUF0_ALGN_LOG2)
#define DRC_SIM_BUF1_ALGN  (1<<DRC_SIM_BUF1_ALGN_LOG2)
#define DRC_SIM_BUF2_ALGN  (1<<DRC_SIM_BUF2_ALGN_LOG2)
#define DRC_SIM_BUF3_ALGN  (1<<DRC_SIM_BUF3_ALGN_LOG2)
#define DRC_SIM_BUF4_ALGN  (1<<DRC_SIM_BUF4_ALGN_LOG2)

#endif	/* _AERBUFSTC55_H */

/* nothing past this point */
