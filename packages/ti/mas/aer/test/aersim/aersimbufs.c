/******************************************************************************
 * FILE PURPOSE: Memory Buffer Definitions and Allocations
 ******************************************************************************
 * FILE NAME:   aersimbufs.c
 *
 * DESCRIPTION: Contains definitions and allocations for AER/AGC/DRC buffers.
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#include <ti/mas/types/types.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/aerbufst.h>

/* Allocate memory for AER buffers - size and alignment */
#pragma DATA_SECTION(aer_buff0_hf, ".aer_buff0_hf");
#pragma DATA_ALIGN  (aer_buff0_hf, AER_SIM_BUF0_ALGN)
tword aer_buff0_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF0_SIZE];

#pragma DATA_SECTION(aer_buff0_hs, ".aer_buff0_hs");
#pragma DATA_ALIGN  (aer_buff0_hs, AER_SIM_BUF0_ALGN)
tword aer_buff0_hs[AER_SIM_BUF0_SIZE];

#pragma DATA_SECTION(aer_buff1, ".aer_buff1");
#pragma DATA_ALIGN  (aer_buff1, AER_SIM_BUF1_ALGN)
tword aer_buff1[AER_SIM_BUF1_SIZE];

#pragma DATA_SECTION(aer_buff2_hf, ".aer_buff2_hf");
#pragma DATA_ALIGN  (aer_buff2_hf, AER_SIM_BUF2_ALGN)
tword aer_buff2_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF2_SIZE_HF];

#pragma DATA_SECTION(aer_buff2_hs, ".aer_buff2_hs");
#pragma DATA_ALIGN  (aer_buff2_hs, AER_SIM_BUF2_ALGN)
tword aer_buff2_hs[AER_SIM_BUF2_SIZE_HS];

#pragma DATA_SECTION(aer_buff3, ".aer_buff3");
#pragma DATA_ALIGN  (aer_buff3, AER_SIM_BUF3_ALGN)
tword aer_buff3[AER_SIM_BUF3_SIZE_HF];

#pragma DATA_SECTION(aer_buff4_hf, ".aer_buff4_hf");
#pragma DATA_ALIGN  (aer_buff4_hf, AER_SIM_BUF4_ALGN)
tword aer_buff4_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF4_SIZE];

#pragma DATA_SECTION(aer_buff4_hs, ".aer_buff4_hs");
#pragma DATA_ALIGN  (aer_buff4_hs, AER_SIM_BUF4_ALGN)
tword aer_buff4_hs[AER_SIM_BUF4_SIZE];

#pragma DATA_SECTION(aer_buff5, ".aer_buff5");
#pragma DATA_ALIGN  (aer_buff5, AER_SIM_BUF5_ALGN)
tword aer_buff5[AER_SIM_BUF5_SIZE];

#pragma DATA_SECTION(aer_buff6, ".aer_buff6");
#pragma DATA_ALIGN  (aer_buff6, AER_SIM_BUF6_ALGN)
tword aer_buff6[AER_SIM_NUM_HF_MIC][AER_SIM_BUF6_SIZE];

#pragma DATA_SECTION(aer_buff7, ".aer_buff7");
#pragma DATA_ALIGN  (aer_buff7, AER_SIM_BUF7_ALGN)
tword aer_buff7[AER_SIM_NUM_HF_MIC][AER_SIM_BUF7_SIZE];

#pragma DATA_SECTION(aer_buff8, ".aer_buff8");
#pragma DATA_ALIGN  (aer_buff8, AER_SIM_BUF8_ALGN)
tword aer_buff8[AER_SIM_BUF8_SIZE];

#pragma DATA_SECTION(aer_buff9, ".aer_buff9");
#pragma DATA_ALIGN  (aer_buff9, AER_SIM_BUF9_ALGN)
tword aer_buff9[AER_SIM_BUF9_SIZE];

#pragma DATA_SECTION(aer_buff10, ".aer_buff10");
#pragma DATA_ALIGN  (aer_buff10, AER_SIM_BUF10_ALGN)
tword aer_buff10[AER_SIM_NUM_HF_MIC][AER_SIM_BUF10_SIZE];

#pragma DATA_SECTION(aer_buff11, ".aer_buff11");
#pragma DATA_ALIGN  (aer_buff11, AER_SIM_BUF11_ALGN)
tword aer_buff11[AER_SIM_BUF11_SIZE];

#pragma DATA_SECTION(aer_buff12, ".aer_buff12");
#pragma DATA_ALIGN  (aer_buff12, AER_SIM_BUF12_ALGN)
tword aer_buff12[AER_SIM_BUF12_SIZE];

#pragma DATA_SECTION(aer_buff13, ".aer_buff13");
#pragma DATA_ALIGN  (aer_buff13, AER_SIM_BUF13_ALGN)
tword aer_buff13[AER_SIM_BUF13_SIZE];

#pragma DATA_SECTION(aer_buff14, ".aer_buff14");
#pragma DATA_ALIGN  (aer_buff14, AER_SIM_BUF14_ALGN)
tword aer_buff14[AER_SIM_BUF14_SIZE];

#pragma DATA_SECTION(aer_buff15, ".aer_buff15");
#pragma DATA_ALIGN  (aer_buff15, AER_SIM_BUF15_ALGN)
tword aer_buff15[AER_SIM_NUM_HF_MIC][AER_SIM_BUF15_SIZE];

#pragma DATA_SECTION(aer_buff16, ".aer_buff16");
#pragma DATA_ALIGN  (aer_buff16, AER_SIM_BUF16_ALGN)
tword aer_buff16[AER_SIM_NUM_HF_MIC][AER_SIM_BUF16_SIZE];

#pragma DATA_SECTION(aer_buff17, ".aer_buff17");
#pragma DATA_ALIGN  (aer_buff17, AER_SIM_BUF17_ALGN)
tword aer_buff17[AER_SIM_NUM_HF_MIC][AER_SIM_BUF17_SIZE];

#pragma DATA_SECTION(aer_buff18, ".aer_buff18");
#pragma DATA_ALIGN  (aer_buff18, AER_SIM_BUF18_ALGN)
tword aer_buff18[AER_SIM_NUM_HF_MIC][AER_SIM_BUF18_SIZE];

#pragma DATA_SECTION(aer_buff19, ".aer_buff19");
#pragma DATA_ALIGN  (aer_buff19, AER_SIM_BUF19_ALGN)
tword aer_buff19[AER_SIM_BUF19_SIZE];

#pragma DATA_SECTION(aer_buff20, ".aer_buff20");
#pragma DATA_ALIGN  (aer_buff20, AER_SIM_BUF20_ALGN)
tword aer_buff20[AER_SIM_NUM_HF_MIC][AER_SIM_BUF20_SIZE];

/* Allocate memory for AGC buffers - size and alignment */
#pragma DATA_SECTION(agc_buff0_hf, ".agc_buff0_hf");
#pragma DATA_ALIGN  (agc_buff0_hf, AGC_SIM_BUF0_ALGN)
tword agc_buff0_hf[AER_SIM_NUM_HF_MIC][AGC_SIM_BUF0_SIZE];

#pragma DATA_SECTION(agc_buff0_hs, ".agc_buff0_hs");
#pragma DATA_ALIGN  (agc_buff0_hs, AGC_SIM_BUF0_ALGN)
tword agc_buff0_hs[AGC_SIM_BUF0_SIZE];

/* Allocate memory for DRC buffers - size and alignment */
#pragma DATA_SECTION(drc_buff0, ".drc_state_buff")
#pragma DATA_ALIGN  (drc_buff0, DRC_SIM_BUF0_ALGN)
tword drc_buff0[DRC_SIM_BUF0_SIZE];

//#pragma DATA_SECTION(drc_buff1, ".drc_scratch_buf")
//#pragma DATA_ALIGN  (drc_buff1, DRC_SIM_BUF1_ALGN)
//tword drc_buff1[DRC_SIM_BUF1_SIZE];

#pragma DATA_SECTION(drc_buff2, ".drc_state_buff")
#pragma DATA_ALIGN  (drc_buff2, DRC_SIM_BUF2_ALGN)
tword drc_buff2[DRC_SIM_BUF2_SIZE];

#pragma DATA_SECTION(drc_buff3, ".drc_state_buff")
#pragma DATA_ALIGN  (drc_buff3, DRC_SIM_BUF3_ALGN)
tword drc_buff3[DRC_SIM_BUF3_SIZE];

#pragma DATA_SECTION(drc_buff4, ".drc_state_buff")
#pragma DATA_ALIGN  (drc_buff4, DRC_SIM_BUF4_ALGN)
tword drc_buff4[DRC_SIM_BUF4_SIZE];

/* Allocate memory for buffers used in simulation */
#pragma DATA_SECTION(siu_rx_del_buf, ".siu_rx_del_buff")
linSample siu_rx_del_buf[SIU_RX_DELAY_LINE_LENGTH]; 

#pragma DATA_SECTION(aer_in_buffer, ".aer_io_buff")
#pragma DATA_SECTION(aer_out_buffer, ".aer_io_buff")
Fract aer_in_buffer[AER_SIM_MAX_FRAME_LENGTH];
Fract aer_out_buffer[AER_SIM_MAX_FRAME_LENGTH];
tword tempIoBuf[AERSIM_FIO_BUF_SIZE*AERSIM_FIO_SIZEOF_WORD];

/* nothing past this point */
