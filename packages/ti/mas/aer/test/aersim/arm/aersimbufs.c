/******************************************************************************
 * FILE PURPOSE: Memory Buffer Definitions and Allocations
 ******************************************************************************
 * FILE NAME:   aersimbufs.c
 *
 * DESCRIPTION: Contains definitions and allocations for AER/AGC/DRC buffers.
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#include <ti/mas/types/types.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/arm/aerbufst.h>

/* Allocate memory for AER buffers - size and alignment */
tword aer_buff0_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF0_SIZE] __attribute__ ((aligned (AER_SIM_BUF0_ALGN)));
tword aer_buff0_hs[AER_SIM_BUF0_SIZE] __attribute__ ((aligned (AER_SIM_BUF0_ALGN)));
tword aer_buff1[AER_SIM_BUF1_SIZE] __attribute__ ((aligned (AER_SIM_BUF1_ALGN)));
tword aer_buff2_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF2_SIZE_HF] __attribute__ ((aligned (AER_SIM_BUF2_ALGN)));
tword aer_buff2_hs[AER_SIM_BUF2_SIZE_HS] __attribute__ ((aligned (AER_SIM_BUF2_ALGN)));
tword aer_buff3[AER_SIM_BUF3_SIZE_HF] __attribute__ ((aligned (AER_SIM_BUF3_ALGN)));
tword aer_buff4_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF4_SIZE] __attribute__ ((aligned (AER_SIM_BUF4_ALGN)));
tword aer_buff4_hs[AER_SIM_BUF4_SIZE] __attribute__ ((aligned (AER_SIM_BUF4_ALGN)));
tword aer_buff5[AER_SIM_BUF5_SIZE] __attribute__ ((aligned (AER_SIM_BUF5_ALGN))); 
tword aer_buff6[AER_SIM_BUF6_SIZE] __attribute__ ((aligned (AER_SIM_BUF6_ALGN)));
tword aer_buff7[AER_SIM_NUM_HF_MIC][AER_SIM_BUF7_SIZE] __attribute__ ((aligned (AER_SIM_BUF7_ALGN)));
tword aer_buff8[AER_SIM_BUF8_SIZE] __attribute__ ((aligned (AER_SIM_BUF8_ALGN)));
tword aer_buff9[AER_SIM_BUF9_SIZE] __attribute__ ((aligned (AER_SIM_BUF9_ALGN)));
tword aer_buff10[AER_SIM_NUM_HF_MIC][AER_SIM_BUF10_SIZE] __attribute__ ((aligned (AER_SIM_BUF10_ALGN)));
tword aer_buff11[AER_SIM_BUF11_SIZE] __attribute__ ((aligned (AER_SIM_BUF11_ALGN)));
tword aer_buff12[AER_SIM_BUF12_SIZE] __attribute__ ((aligned (AER_SIM_BUF12_ALGN)));
tword aer_buff13[AER_SIM_BUF13_SIZE] __attribute__ ((aligned (AER_SIM_BUF13_ALGN)));
tword aer_buff14[AER_SIM_BUF14_SIZE] __attribute__ ((aligned (AER_SIM_BUF14_ALGN)));
tword aer_buff15[AER_SIM_NUM_HF_MIC][AER_SIM_BUF15_SIZE] __attribute__ ((aligned (AER_SIM_BUF15_ALGN)));
tword aer_buff16[AER_SIM_NUM_HF_MIC][AER_SIM_BUF16_SIZE] __attribute__ ((aligned (AER_SIM_BUF16_ALGN)));
tword aer_buff17[AER_SIM_NUM_HF_MIC][AER_SIM_BUF17_SIZE] __attribute__ ((aligned (AER_SIM_BUF17_ALGN)));
tword aer_buff18[AER_SIM_NUM_HF_MIC][AER_SIM_BUF18_SIZE] __attribute__ ((aligned (AER_SIM_BUF18_ALGN)));
tword aer_buff19[AER_SIM_BUF19_SIZE] __attribute__ ((aligned (AER_SIM_BUF19_ALGN)));
tword aer_buff20[AER_SIM_NUM_HF_MIC][AER_SIM_BUF20_SIZE] __attribute__ ((aligned (AER_SIM_BUF20_ALGN)));

/* Allocate memory for AGC buffers - size and alignment */
tword agc_buff0_hf[AER_SIM_NUM_HF_MIC][AGC_SIM_BUF0_SIZE] __attribute__ ((aligned (AGC_SIM_BUF0_ALGN)));
tword agc_buff0_hs[AGC_SIM_BUF0_SIZE] __attribute__ ((aligned (AGC_SIM_BUF0_ALGN)));

/* Allocate memory for DRC buffers - size and alignment */
tword drc_buff0[DRC_SIM_BUF0_SIZE] __attribute__ ((aligned (DRC_SIM_BUF0_ALGN)));
tword drc_buff1[DRC_SIM_BUF1_SIZE] __attribute__ ((aligned (DRC_SIM_BUF1_ALGN)));
tword drc_buff2[DRC_SIM_BUF2_SIZE] __attribute__ ((aligned (DRC_SIM_BUF2_ALGN)));
tword drc_buff3[DRC_SIM_BUF3_SIZE] __attribute__ ((aligned (DRC_SIM_BUF3_ALGN)));
tword drc_buff4[DRC_SIM_BUF4_SIZE] __attribute__ ((aligned (DRC_SIM_BUF4_ALGN)));

/* Allocate memory for buffers used in simulation */
linSample siu_rx_del_buf[SIU_RX_DELAY_LINE_LENGTH] __attribute__ ((aligned (4))); 
Fract aer_in_buffer[AER_SIM_MAX_FRAME_LENGTH] __attribute__ ((aligned (4)));
Fract aer_out_buffer[AER_SIM_MAX_FRAME_LENGTH] __attribute__ ((aligned (4)));
tword tempIoBuf[AERSIM_FIO_BUF_SIZE*AERSIM_FIO_SIZEOF_WORD];

/* nothing past this point */
