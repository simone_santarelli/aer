/******************************************************************************
 * FILE PURPOSE: Memory Buffer Size and Alignment Definitions
 ******************************************************************************
 * FILE NAME:   aerbufst.h
 *
 * DESCRIPTION: Contains size and alignment definitions for AER/AGC/DRC buffers.
 *              This file includes a target specific header file with the 
 *              actual definitions. 
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/
#ifndef _AERBUFST_H
#define _AERBUFST_H

#ifdef ti_targets_C55_large
#define _AERTESTPORT_C55L 1
#else
#define _AERTESTPORT_C55L 0
#endif

#ifdef ti_targets_C64P
#define _AERTESTPORT_C64P 1
#else
#define _AERTESTPORT_C64P 0
#endif

#ifdef ti_targets_elf_C64P
#define _AERTESTPORT_C64P_ELF 1
#else
#define _AERTESTPORT_C64P_ELF 0
#endif

#ifdef ti_targets_C64P_big_endian
#define _AERTESTPORT_C64P_BIG_ENDIAN 1
#else
#define _AERTESTPORT_C64P_BIG_ENDIAN 0
#endif

#ifdef ti_targets_elf_C64P_big_endian
#define _AERTESTPORT_C64P_BIG_ENDIAN_ELF 1
#else
#define _AERTESTPORT_C64P_BIG_ENDIAN_ELF 0
#endif

#ifdef ti_targets_C674
#define _AERTESTPORT_C674 1
#else
#define _AERTESTPORT_C674 0
#endif

#ifdef ti_targets_elf_C674
#define _AERTESTPORT_C674_ELF 1
#else
#define _AERTESTPORT_C674_ELF 0
#endif

/* C674 devices do not support big endian */

#ifdef gnu_targets_arm_GCArmv7A
#define _AERTESTPORT_ARM_GCARMV7A 1
#else
#define _AERTESTPORT_ARM_GCARMV7A 0
#endif

#if _AERTESTPORT_C55L
#include <ti/mas/aer/test/aersim/c55/aerbufst.h>
#elif _AERTESTPORT_C64P || _AERTESTPORT_C64P_BIG_ENDIAN || _AERTESTPORT_C674 || _AERTESTPORT_C64P_ELF || _AERTESTPORT_C64P_BIG_ENDIAN_ELF || _AERTESTPORT_C674_ELF
#include <ti/mas/aer/test/aersim/c64P/aerbufst.h>
#elif _AERTESTPORT_ARM_GCARMV7A
#include <ti/mas/aer/test/aersim/arm/aerbufst.h>
#else
#error invalid target
#endif


#endif	/* _AERBUFST_H */

/* nothing past this point */
