/****************************************************************************** 
 * FILE PURPOSE: File I/O for the simulation of AER.  
 ****************************************************************************** 
 * FILE NAME:   aersim_fileIO.c 
 * 
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/
/* ANSI C headers */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/aersim/aersim_fileIO.h>
#include <ti/mas/aer/test/aersim/aerbuffs.h>

/******************************************************************************
 * FUNCTION PURPOSE: Performs an fread() operations and packs bytes into words
 ******************************************************************************
 * DESCRIPTION: fread() function reads bytes from the host-based files into
 *              words in the DSP memory.  In order to make the data stream
 *              useful for the DSP, this data must be packed.  This function
 *              calls the fread() and packs the data for DSP programs to use
 *
 *              fread() returns data like this (16 bit DSP memory):
 *                        0000 0000 aaaa bbbb
 *                        0000 0000 cccc dddd
 *                        0000 0000 eeee ffff
 *                        0000 0000 gggg hhhh
 *
 *              aerSimFread() returns data like this (16 bit DSP memory):
 *                        aaaa bbbb cccc dddd
 *                        eeee ffff gggg hhhh
 *
 *              This function uses a global temporary buffer to store fread()
 *              results, then packs the data into a user-provided buffer.
 *
 *              Return value is TRUE if the number of bytes read is the same
 *              the number of bytes requested in buf_size.  False otherwise
 *              NOTE:  FALSE return value usually indicated end of file.
 *
 *  tbool aerSimFread (
 *   tint *buf      - pointer to the buffer to store read data
 *   tint *buf_size - size of buffer
 *   FILE *file)    - pointer to the file descriptor
 *
 *****************************************************************************/
tbool aerSimFread(tint *buf, tint buf_size, FILE *file)
{
  tuint numRead;
  tint  i, j;

  if(file == NULL){
    return FALSE;
  }
  
  /* read the file*/
  numRead = fread(tempIoBuf, buf_size, AERSIM_FIO_SIZEOF_WORD, file);
  
  /* check for endo of file */
  if (numRead != AERSIM_FIO_SIZEOF_WORD)
  {
    return FALSE;
  }

  /* pack bytes into words */
  for (i = 0, j = 0; i < buf_size; i++, j+=2)
  {
    buf[i]  = tempIoBuf[j] << 8;
    buf[i] |= tempIoBuf[j+1];
  }

  return TRUE;
} /* aerSimFread */

/******************************************************************************
 * FUNCTION PURPOSE: Performs an fwrite() operations and packs bytes into words
 ******************************************************************************
 * DESCRIPTION: fwrite() function writes bytes to the host-based files from
 *              words in the DSP memory.  In order to make the data stream
 *              comprehendable by the host, this data must be un-packed.  This
 *              function un-packs DSP data and calls the fwrite().
 *
 *              fwrite() expectss data like this (16 bit DSP memory):
 *                        0000 0000 aaaa bbbb
 *                        0000 0000 cccc dddd
 *                        0000 0000 eeee ffff
 *                        0000 0000 gggg hhhh
 *
 *              aerSimFwrite() receivess data like this (16 bit DSP memory):
 *                        aaaa bbbb cccc dddd
 *                        eeee ffff gggg hhhh
 *
 *              This function uses a global temporary buffer to store the
 *              un-packed data and pass it to fwrite() as an argument.
 *
 *  void aerSimFwrite (
 *   tint *buf      - pointer to the buffer to be written
 *   tint *buf_size - size of buffer
 *   FILE *file)    - pointer to the file descriptor
 *
 *****************************************************************************/
void aerSimFwrite(tint *buf, tint buf_size, FILE *file)
{
  tint  i, j;
  
  if(file == NULL){
    return;
  }
    
  for (i = 0, j = 0; i < buf_size; i++, j+=2)
  {
    tempIoBuf[j]   = (buf[i] >> 8) & 0x00ff;
    tempIoBuf[j+1] = (buf[i])      & 0x00ff;
  }
  fwrite(tempIoBuf, buf_size, AERSIM_FIO_SIZEOF_WORD, file);
} /* aerSimFwrite */

/*****************************************************************************
* This function writes AER debug status to a file.
*****************************************************************************/
void aer_debug_stat(void * aer_inst, FILE *dbg_file)
{
  aerDebugStat_t aer_dbg;
  aerVersion_t aer_ver;
  tint *aer_dbg_tint;
  
  if (dbg_file == NULL) {
    return;
  }
      
  aerGetPerformance(aer_inst, &aer_dbg, &aer_ver, FALSE);
  
  aer_dbg_tint = (tint *)&aer_dbg;
  aerSimFwrite(aer_dbg_tint, sizeof(aerDebugStat_t)/sizeof(tint), dbg_file);
}  /* aer_debug_stat */
  
/*****************************************************************************
* This function opens files to reand and write for AER simulation.
*****************************************************************************/
void aer_sim_open_files(aersimFileIO_t  *fio, tint num_mic)
{
  int i;
  char fname[50];

  /* initialize all file pointers to NULL */
  for(i=0; i<num_mic; i++) {
    fio->fp_txin[i] = NULL;
  }
  fio->fp_txout    = NULL;
  fio->fp_rxin     = NULL;
  fio->fp_rxout    = NULL;
  fio->fp_debug_aer= NULL;
  fio->fp_debug_mss= NULL;
  fio->fp_trace_tx = NULL;
  fio->fp_trace_rx = NULL;
  
  /* open Tx path input and output files */
  if (fio->flag_txin_rd) {
    for(i=0; i<num_mic; i++) {
      strcpy(fname, AER_SIM_ROOT_FOLDER);
      strcat(fname, fio->tx_in_file_name[i]);
      fio->fp_txin[i] = fopen(fname, "rb");
      if (fio->fp_txin[i] == NULL) {
        printf("Tx input file does NOT exist!\n");
        exit(0);
      }
    }
  }

  if (fio->flag_txout_wr) {
    strcpy(fname, AER_SIM_ROOT_FOLDER);
    strcat(fname, "vectors/out/txout.pcm");
    fio->fp_txout = fopen(fname, "wb");
    if (fio->fp_txout == NULL) {
      printf("Can NOT open file to write Tx out data!\n");
      exit(0);
    }
  }
 
  /* open Rx path input and output files */
  if (fio->flag_rxin_rd) {
    strcpy(fname, AER_SIM_ROOT_FOLDER);
    strcat(fname, fio->rx_in_file_name);
    fio->fp_rxin = fopen(fname, "rb");
    
    if (fio->fp_rxin == NULL) {
      printf("Rx input file does NOT exist!\n");
      exit(0);
    }    
  }
  
  if (fio->flag_rxout_wr) {
    strcpy(fname, AER_SIM_ROOT_FOLDER);
    strcat(fname, "vectors/out/rxout.pcm");
    fio->fp_rxout = fopen(fname, "wb");
    if (fio->fp_rxout == NULL) {
      printf("Can NOT open file to write Rx out data!\n");
      exit(0);
    }
  }
  
  /* Open AER debug and trace files */
/*  strcpy(fname, AER_SIM_ROOT_FOLDER);
  strcat(fname, "vectors/out/aer_debug.pcm");
  fio->fp_debug_aer= fopen(fname, "wb");
  if (fio->fp_debug_aer == NULL) {
    printf("Can NOT open file to write AER debug data!\n");
    exit(0);
  }
*/
  /* Open MSS debug file */
/*  fio->fp_debug_mss = fopen("mss_debug.pcm", "wb");
  if (fio->fp_debug_mss == NULL) {
    printf("Can NOT open file to write MSS debug data!\n");
    exit(0);
  }
*/  
  fio->flag_rxin_end = FALSE;
  fio->flag_txin_end = FALSE;
  
} /* aer_sim_open_files */

/*****************************************************************************
* This function closes all files.
*****************************************************************************/
void aer_sim_close_files(aersimFileIO_t  *fio, tint num_mic)
{
  int i;
  
  if (fio->fp_rxin != NULL) {
    fclose(fio->fp_rxin);  
  }
  if (fio->fp_rxout != NULL) {
    fclose(fio->fp_rxout);  
  }  
  
  for(i=0; i<num_mic; i++) {
    if(fio->fp_txin[i] == NULL) {
      fclose(fio->fp_txin[i]);
    }
  }
  if (fio->fp_txout != NULL) {
    fclose(fio->fp_txout);
  } 

  if (fio->fp_trace_rx != NULL) {
    fclose(fio->fp_trace_rx);
  }  
  if (fio->fp_trace_tx != NULL) {
    fclose(fio->fp_trace_tx);
  }  
  if (fio->fp_debug_aer != NULL) {
    fclose(fio->fp_debug_aer);
  }
  if (fio->fp_debug_mss != NULL) {
    fclose(fio->fp_debug_mss);
  }
  
} /* aer_sim_close_files */

/* nothing past this line */
