#ifndef _AERSIM_H
#define _AERSIM_H

/****************************************************************************** 
 * FILE PURPOSE: Main program and supporting routines for the simulation of AER.  
 ****************************************************************************** 
 * FILE NAME:   aersim.h 
 * 
 * DESCRIPTION: Contains routines for for the simulation testing of AER
 * 
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include <ti/mas/aer/test/sgnsim/sgnsim.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/piusim/piuloc.h>
#include <ti/mas/aer/test/aepsim/aepsim.h>
#include <ti/mas/aer/test/aersim/aersim_fileIO.h>

#include <ti/mas/aer/test/siusim/siuaer.h>

/* System level header files */
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/agc.h>
#include <ti/mas/aer/drc.h>
#include <ti/mas/aer/bf.h>
#include <ti/mas/aer/test/siusim/siuagc.h>
#include <ti/mas/aer/test/siusim/siudrc.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>


#include <ti/mas/aer/test/piusim/piuloc.h>        

/* Internal Constants */
#define SIU_MAX_GETFLT_CNT    64
#define AECHO_LOOPCNT         3
#define LRIN_LOOPCNT          5
#define LRIN_START          (-30)
#define LRIN_STEP             5
#define PTABLE_LENGTH       (AECHO_LOOPCNT*LRIN_LOOPCNT)

#define POWER_TRACK_RX_PATH 0
#define POWER_TRACK_TX_PATH 1
#define POWER_TRACK_TX_OUT 2

/* Useful macros */
#define loop    for(;;)     /* endless loop */

typedef struct siuSysCfg_s {
  tint ulaw;            /* companding law -1 (transparent),     */
                        /*  0 (A-law), 1 (u-law), 2 (linear)    */
  tint pcm_bits;        /* (1-16), num of bits in a pcm-sample  */
  tint num_BF_mic;      /* number of beamforming mics           */
  tint num_HF_mic;      /* number of remote hands-free mics     */
  tint segment_size;    /* # samples @8kHz in PIU (sub)segment  */
  tint frame_size;      /* frame size in # of samples @8kHz     */
  tint sampling_rates;  /* bitfield:                            */
                        /*  Bit 0: Available Rx in. 0=8kHz 1=16kHz        */
                        /*  Bit 1: Requested Tx out. 0=8kHz 1=16kHz       */
                        /*  Bit 2: Available Rx out/Tx in. 0=8kHz 1=16kHz */
  tint bf_steer_angle;
  tbool band_split_flag;/* 0: full band AER for 16kHz 
                           1: bandsplit AER for 16kHz */
  tbool mult_mic_type;  /* Multiple-mic type: 
                           0: microphone array for Beam Forming (BF)
                           1: remote hands-free microphones for Multi-Source 
                              Selection (MSS)                             */
}siuSysCfg_t;

/* IEC 651 Power Measurement Parameters */
typedef struct {                                                               
  tbool       enabled;  /* TRUE: Power estimation is enabled */
  Fract       Px_high;  /* high 8 bits of Rx in signal's power in Q8 */
  ULFract     Px_low;   /* low 32 bits of Rx in signal's power in Q8 */
  LFract      Px_max;   /* maximum power of Rx in signal in Q0 */
  Fract       Py1_high; /* high 8 bits of NE signal's (without echo) power in Q8 */
  ULFract     Py1_low;  /* low 32 bits of NE signal's (without echo) power in Q8 */
  LFract      Py1_max;  /* maximum power of NE signal's (without echo) in Q0 */
  Fract       Py_high;  /* high 8 bits of Tx in signal's power in Q8 */
  ULFract     Py_low;   /* low 32 bits of Tx in signal's power in Q8 */
  LFract      Py_max;   /* maximum power of Tx in  signal in Q0 */
  Fract       Pe_high;  /* high 8 bits of Tx out signal's power in Q8 */
  ULFract     Pe_low;   /* low 32 bits of Tx out signal's power in Q8 */
  LFract      Pe_max;   /* maximum power of Tx out signal in Q0 */
  Fract       Pr_high;  /* high 8 bits of Rx out signal's power in Q8 */
  ULFract     Pr_low;   /* low 32 bits of Rx out signal's power in Q8 */
  LFract      Pr_max;   /* maximum power of Rx out signal in Q0 */
} p651Pars_t;  


/******************************************************************************
 * DATA DEFINITION: Control structure to select parameters for AER simulation
 ******************************************************************************/
typedef struct siuSetup_s {
  siuSysCfg_t     system_cfg;
  p651Pars_t      p651Pars;              /* IEC 651 Power Parameters */
  aepSimPars_t    aep_params;
  siuAerSysPars_t aer_sys_params;        /* system dependent AER parameters */
  tint            segment_size;          /* in # of samples, 8kHz or 16kHz */
  tint            aer_kernel_srate;      /* LMS sampling rate, 1(8kHz) or 2(16kHz) */
  tint            aer_rin_frm_size;      /* in # of samples, 8kHz or 16kHz */
  tint            aer_rout_frm_size;     /* in # of samples, 8kHz or 16kHz */
  tint            aer_sin_frm_size;      /* in # of samples, 8kHz or 16kHz */
  tint            aer_sout_frm_size;     /* in # of samples, 8kHz or 16kHz */
  tint            aer_receive_out_length;/* in # of samples, 8kHz or 16kHz */ 
  tint            aep_gain_l2[AECHO_LOOPCNT];/* echo path gain in log2(gain) */
  tint            aep_gain_start_exp;    /* AEP gain staring point, exponent */
  tint            aep_gain_start_mant;   /* AEP gain staring point, mantissa */
  tint            aep_gain_adj_exp;      /* AEP gain adjustment exponent */
  Fract           aep_gain_adj_mant;     /* AEP gain adjustment mantissa */
  tuint           aer_ctl_bitfield_0;
  tuint           aer_ctl_bitfield_1;
  tuint           bf_ctl_bitfield;
  tuint           bf_mics;
  tint            eqbq_tx_flag;
  tint            eqbq_rx_flag;
  tuint           agc_ctl_bitfield;
  tsize           aer_trace_size_rx;
  tsize           aer_trace_size_tx;
  tuint           aer_trace_cfg;
  tbool           aer_trace_flag;
  tbool           toggle_aer;         /* TRUE: change AER control parameters  */
  aerControl_t    aerCtl;             /* AER control parameters               */
  tbool           toggle_bf;          /* TRUE: change BF control parameters  */
  bfControl_t     bfCtl;              /* AER control parameters               */
  tbool           toggle_agc;         /* TRUE: change AGC control parameters  */
  agcControl_t    agcCtl;             /* AGC control parameters               */ 
  tbool           toggle_veu;         /* TRUE: change VEU control parameters  */
  aerControl_t    veuCtl;             /* VEU control parameters               */
  tbool           toggle_siugain;     /* TRUE: change AGC control parameters  
                                         in event of gain changes             */
  siuGainControl_t siuGainCtl;        /* AGC control gain parameters          */
  tbool           toggle_aep;         /* TRUE: change acoustic echo path      
                                         parameters                           */
  aepControl_t    aepCtl;             /* acoustic echo path control params    */
  tbool           toggle_tx_sg;       /* TRUE: toggle Tx signal generator     */
  sgnSimPars_t    txSGN;              /* Tx Signal Generator Parameters       */
  tbool           toggle_rx_sg;       /* TRUE: toggle Rx signal generator     */
  sgnSimPars_t    rxSGN;              /* Rx Signal Generator Parameters       */
  tbool           toggle_piu_tg;      /* TRUE: toggle PIU tone generator      */
  tbool           piu_tg_state;       /* TRUE: PIU has tone on                */
  tbool           piu_getlev;         /* TRUE: calculate levels in PIU        */
  tbool           toggle_getflt;      /* TRUE: get filter coefficients        */
  tbool           toggle_putflt;      /* TRUE: put filter coefficients        */
  tbool           putflt_flag_last;   /* TRUE: last call of aerPutFilter()    */
  tbool           aer_mips_profile;   /* Profile AER for peak MIPS usage      */
  tbool           fe_sgn_flag;        /* FE signal generator                  */
  tbool           ne_sgn_flag;        /* FE signal generator                  */
  tint            aer_dbgstat_freq;   /* Query AER Debug stats every such     
                                         frames                               */
  tint            drc_on_flag;        /* 1: Enable DRC. 0: Disable DRC        */
  tuint           drc_cfgbits;        /* to select DRC operation mode         */
                                      /* Bit 0: if 1, Full band is enabled */
                                      /* Bit 1: if 1, Low band is enabled  */
                                      /* Bit 2: if 1, Mid band is enabled  */
                                      /* Bit 3: if 1, High band is enabled */
                                      /* Bit 5: if 1, limiter is enabled   */
                                      /* Bit 4: if 1, vad is enabled       */
  tbool            drc_mips_profile;  /* Profile AER for peak MIPS usage      */
  tbool            toggle_drcCtrl;    /* TRUE: toggle DRC control             */
  drcControl_t     drcCtl;            /* DRC control configurations           */
  tbool            toggle_drcGetParam;/* TRUE: toggle DRC get parameters      */
  tint             drcGetCode;        /* DRC get parameter control code       */
  drcReportParam_t drcGetParam;       /* DRC get parameter return value       */
  tbool            txin_file_with_echo;  /* Tx input file already has echo    */
  void             (*g167_power_calc_fnc) (struct siuSetup_s *siu_setup);
  FILE *           pow_track_file1;
  FILE *           pow_track_file2;
  tuint           aer_reset_bitfield; /* reset bit field for aerOpen          */
  tuint           aer_valid_bitfield; /* valid bit field for aerOpen          */
  tuint           new_phone_mode;
  tint            run_time;           /* default run time */
  tint            aep_vary_time;
  tint            pow_track_option;
  tbool           g167_save_sig_flag;
  tbool           AEC_enabled;
  tbool           pow_track_enabled;
  tbool           dt_on;
  tbool           toggle_phone_mode;
  tbool           txout_cng_only;
  tbool           use_fdnlp;
  tbool           asnr_enable;
  tbool           cng_enable;
  tbool           test_double_talk;
  tbool           veu_enabled;
  int             doCacheFlush;
  int             doCacheEnable;
  tulong          cycles_before_aer;
  tulong          RxCyclesMax;
  tulong          RxCyclesAvg;
  tulong          TxCyclesMax;
  tulong          TxCyclesAvg;
  
} siuSetup_t;


extern void aer_sim_init(siuSetup_t *siu_setup);
extern void aer_sim_read_cfg(siuSetup_t *siu_setup, aersimFileIO_t *fio, FILE *test_cfg);
extern void aer_sim_open_aep(aepSimInst_t *aep_inst, siuSetup_t *siu_setup);
extern void aer_sim_apply_config(siuSetup_t *siu_setup);
extern void aer_sim_init_siu(siuInst_t *siu_inst_ptr, siuSetup_t *siu_setup_ptr);
extern void aer_sim_init_piu(piuInst_t *piu_inst_ptr, siuSetup_t *siu_setup_ptr);
extern void aer_sim_instantiate_aer(siuInst_t *siu_inst, siuSetup_t *siu_setup);
extern void aer_sim_open_txrx_sgns(sgnInstBuf_t *sgn_inst_tx, 
                                   sgnInstBuf_t *sgn_inst_rx, siuSetup_t *siu_setup);
extern void aer_sim_config_aer_params(siuInst_t *inst, siuSetup_t *siu_setup);


extern void aersim_cacheEnable();
extern void aersim_cacheFlush();
extern tbool aersim_initCycleProfiling();
extern void aersim_profile_get_cycle_before_aer(siuSetup_t *siu_setup);
extern void aersim_profile_get_cycle_after_aer_send(siuSetup_t *siu_setup);
extern void aersim_profile_get_cycle_after_aer_receive(siuSetup_t *siu_setup);

#endif /* _AERSIM_H */

/* nothing past this point */
