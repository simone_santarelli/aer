#ifndef _AERBUFFS_H
#define _AERBUFFS_H
/******************************************************************************
 * FILE PURPOSE: Memory Buffer External References for AER Simulation
 ******************************************************************************
 * FILE NAME:   aerbuffs.h
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#include <ti/mas/aer/test/aersim/aerbufst.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>

extern tword aer_buff0_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF0_SIZE];
extern tword aer_buff0_hs[AER_SIM_BUF0_SIZE];
extern tword aer_buff1[AER_SIM_BUF1_SIZE];
extern tword aer_buff2_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF2_SIZE_HF];
extern tword aer_buff2_hs[AER_SIM_NUM_HF_MIC][AER_SIM_BUF2_SIZE_HS];
extern tword aer_buff3[AER_SIM_BUF3_SIZE_HF];
extern tword aer_buff4_hf[AER_SIM_NUM_HF_MIC][AER_SIM_BUF4_SIZE];
extern tword aer_buff4_hs[AER_SIM_BUF4_SIZE];
extern tword aer_buff5[AER_SIM_BUF5_SIZE];
extern tword aer_buff6[AER_SIM_NUM_HF_MIC][AER_SIM_BUF6_SIZE];
extern tword aer_buff7[AER_SIM_NUM_HF_MIC][AER_SIM_BUF7_SIZE];
extern tword aer_buff8[AER_SIM_BUF8_SIZE];
extern tword aer_buff9[AER_SIM_BUF9_SIZE];
extern tword aer_buff10[AER_SIM_NUM_HF_MIC][AER_SIM_BUF10_SIZE];
extern tword aer_buff11[AER_SIM_BUF11_SIZE];
extern tword aer_buff12[AER_SIM_BUF12_SIZE];
extern tword aer_buff13[AER_SIM_BUF13_SIZE];
extern tword aer_buff14[AER_SIM_BUF14_SIZE];
extern tword aer_buff15[AER_SIM_NUM_HF_MIC][AER_SIM_BUF15_SIZE];
extern tword aer_buff16[AER_SIM_NUM_HF_MIC][AER_SIM_BUF16_SIZE];
extern tword aer_buff17[AER_SIM_NUM_HF_MIC][AER_SIM_BUF17_SIZE];
extern tword aer_buff18[AER_SIM_NUM_HF_MIC][AER_SIM_BUF18_SIZE];
extern tword aer_buff19[AER_SIM_BUF19_SIZE];
extern tword aer_buff20[AER_SIM_NUM_HF_MIC][AER_SIM_BUF20_SIZE];
extern tword agc_buff0_hf[AER_SIM_NUM_HF_MIC][AGC_SIM_BUF0_SIZE];
extern tword agc_buff0_hs[AGC_SIM_BUF0_SIZE];
extern tword drc_buff0[DRC_SIM_BUF0_SIZE];
extern tword drc_buff1[DRC_SIM_BUF1_SIZE];
extern tword drc_buff2[DRC_SIM_BUF2_SIZE];
extern tword drc_buff3[DRC_SIM_BUF3_SIZE];
extern tword drc_buff4[DRC_SIM_BUF4_SIZE];
extern linSample siu_rx_del_buf[SIU_RX_DELAY_LINE_LENGTH]; 
extern Fract aer_in_buffer[AER_SIM_MAX_FRAME_LENGTH];
extern Fract aer_out_buffer[AER_SIM_MAX_FRAME_LENGTH];
extern tword tempIoBuf[AERSIM_FIO_BUF_SIZE*AERSIM_FIO_SIZEOF_WORD];

#endif	/* _AERBUFFS_H */

/* nothing past this point */
