/****************************************************************************** 
 * FILE PURPOSE: File I/O for the simulation of AER.  
 ****************************************************************************** 
 * FILE NAME:   aersim_fileIO.h 
 * 
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#ifndef _AERSOM_FILEIO_H
#define _AERSOM_FILEIO_H

/* ANSI C headers */
#include <stdlib.h>
#include <stdio.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>                      /* DSP types            */

#include <ti/mas/aer/test/aersim/aersimcfg.h>

#ifdef  __CCS_PROJECT_REL_PATH
#define AER_SIM_ROOT_FOLDER "../../../"
#else
#ifdef AER_TEST_AUTOMATE_PATH
#define AER_SIM_ROOT_FOLDER       "./"
#else
#define AER_SIM_ROOT_FOLDER "../../"
#endif  
#endif  

/* File I/O for AER simulation */
typedef struct aersimFileIO_s{
  tbool flag_rxin_rd;
  tbool flag_rxout_wr;
  tbool flag_rxin_end;

  tbool flag_txin_rd;
  tbool flag_txout_wr;
  tbool flag_txin_end;
  tbool flag_txin_echo;

  char rx_in_file_name[50];
  char tx_in_file_name[AER_SIM_NUM_BF_MIC][50];
  char bf_filter_file_name[AER_SIM_NUM_BF_MIC][50];
  
  FILE *fp_rxin;
  FILE *fp_rxout;
  FILE *fp_txin[AER_SIM_NUM_BF_MIC];
  FILE *fp_bf_filter[AER_SIM_NUM_BF_MIC];
  FILE *fp_txout;
  FILE *drcReceiveOut;
  FILE *drcDebugOut;
  FILE *fp_debug_aer;
  FILE *fp_debug_mss;
  FILE *hcoefOut;
  FILE *fp_trace_tx;
  FILE *fp_trace_rx;
} aersimFileIO_t;


extern void aer_sim_open_files(aersimFileIO_t  *fio, tint num_mic);
extern void aer_sim_close_files(aersimFileIO_t  *fio, tint num_mic);
extern tbool aerSimFread(tint *buf, tint buf_size, FILE *file);
extern void aerSimFwrite(tint *buf, tint buf_size, FILE *file);
extern void aer_debug_stat(void * aer_inst, FILE *dbg_file);

#endif

/* nothing past this line */
