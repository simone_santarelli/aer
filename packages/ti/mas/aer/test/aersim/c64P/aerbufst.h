/******************************************************************************
 * FILE PURPOSE: Memory Buffer Size and Alignment Definitions for C64x+
 ******************************************************************************
 * FILE NAME:   c64P\aerbufst.h
 *
 * DESCRIPTION: Contains size and alignment definitions for AER/AGC/DRC buffers
 *              for C64x+.  
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#ifndef _AERBUFSTC64P_H
#define _AERBUFSTC64P_H

/* Buffer sizes for AER - returned by aerGetSizes().
   Buffer size is in unit of tword. For C64x+, tword is 8-bit. */
#define AER_SIM_BUF0_SIZE  856        /* 0x358, must be integer multiple of 8 bytes */
#define AER_SIM_BUF1_SIZE  2056
#define AER_SIM_BUF2_SIZE_HF  10300   /* 200 msec tail */
#define AER_SIM_BUF2_SIZE_HS  3090    /* 60  msec tail */
#define AER_SIM_BUF3_SIZE_HF  19570  
#define AER_SIM_BUF3_SIZE_HS  5150
#define AER_SIM_BUF4_SIZE  1028
#define AER_SIM_BUF5_SIZE  1280
#define AER_SIM_BUF6_SIZE  2056
#define AER_SIM_BUF7_SIZE  192   /* 0xC0, must be integer multiple of 8 bytes */
#define AER_SIM_BUF8_SIZE  10300
#define AER_SIM_BUF9_SIZE  3084
#define AER_SIM_BUF10_SIZE 72    /* 0x48, must be integer multiple of 8 bytes */
#define AER_SIM_BUF11_SIZE 66
#define AER_SIM_BUF12_SIZE 1    //dummy
//#define AER_SIM_BUF12_SIZE 160  for bandsplit operation
#define AER_SIM_BUF13_SIZE 320
//#define AER_SIM_BUF13_SIZE 800  for bandsplit operation
#define AER_SIM_BUF14_SIZE 1    //dummy
//#define AER_SIM_BUF14_SIZE 320  for bandsplit operation
#define AER_SIM_BUF15_SIZE 4    /* dummy, must be integer multiple of 4 bytes */
//#define AER_SIM_BUF15_SIZE 192  for bandsplit operation
#define AER_SIM_BUF16_SIZE 260  /* 0x104, must be integer multiple of 4 bytes */
#define AER_SIM_BUF17_SIZE 1220 /* 0x4C4, must be integer multiple of 4 bytes */ 
#define AER_SIM_BUF18_SIZE 1292 /* 0x50C, must be integer multiple of 4 bytes */
#define AER_SIM_BUF19_SIZE 320  
#define AER_SIM_BUF20_SIZE 640  /* 0x280, must be integer multiple of 4 bytes */

/* Buffer size for AGC - returned by agcGetSizes(). */
#define AGC_SIM_BUF0_SIZE 128   /* 0x80, must be integer multiple of  8 bytes */

/* Buffer sizes for DRC - returned by drcGetSizes(). */
#define DRC_SIM_BUF0_SIZE 148
#define DRC_SIM_BUF1_SIZE 1280
#define DRC_SIM_BUF2_SIZE 64
#define DRC_SIM_BUF3_SIZE 300
#define DRC_SIM_BUF4_SIZE 20

/* Buffer alignment for AER - returned by aerGetSizes().
   Buffers must be aligned on boundary of 2^(AER_SIM_BUFx_ALGN_LOG2) twords. */
#define AER_SIM_BUF0_ALGN_LOG2  3
#define AER_SIM_BUF1_ALGN_LOG2  3
#define AER_SIM_BUF2_ALGN_LOG2  2
#define AER_SIM_BUF3_ALGN_LOG2  2
#define AER_SIM_BUF4_ALGN_LOG2  2
#define AER_SIM_BUF5_ALGN_LOG2  2
#define AER_SIM_BUF6_ALGN_LOG2  2
#define AER_SIM_BUF7_ALGN_LOG2  3
#define AER_SIM_BUF8_ALGN_LOG2  3
#define AER_SIM_BUF9_ALGN_LOG2  3
#define AER_SIM_BUF10_ALGN_LOG2 3
#define AER_SIM_BUF11_ALGN_LOG2 3
#define AER_SIM_BUF12_ALGN_LOG2 2
#define AER_SIM_BUF13_ALGN_LOG2 2
#define AER_SIM_BUF14_ALGN_LOG2 2
#define AER_SIM_BUF15_ALGN_LOG2 2
#define AER_SIM_BUF16_ALGN_LOG2 2
#define AER_SIM_BUF17_ALGN_LOG2 2
#define AER_SIM_BUF18_ALGN_LOG2 2
#define AER_SIM_BUF19_ALGN_LOG2 2
#define AER_SIM_BUF20_ALGN_LOG2 2

/* Buffer alignment for AGC - returned by agcGetSizes(). */
#define AGC_SIM_BUF0_ALGN_LOG2  3

/* Buffer alignment for DRC - returned by drcGetSizes(). */
#define DRC_SIM_BUF0_ALGN_LOG2  3 
#define DRC_SIM_BUF1_ALGN_LOG2  2
#define DRC_SIM_BUF2_ALGN_LOG2  1
#define DRC_SIM_BUF3_ALGN_LOG2  1
#define DRC_SIM_BUF4_ALGN_LOG2  3

/* Define macros used by #pragma DATA_ALIGN in aersimbufs.c.
   The pragma DATA_ALIGN (symbol, constant) aligns the symbol to an alignment 
   boundary. For C64x+, the boundary is the value of the constant in bytes. 
   For example, a constant of 4 specifies a 4-byte or 32-bit alignment.      */ 
#define AER_SIM_BUF0_ALGN  (1<<AER_SIM_BUF0_ALGN_LOG2)
#define AER_SIM_BUF1_ALGN  (1<<AER_SIM_BUF1_ALGN_LOG2)
#define AER_SIM_BUF2_ALGN  (1<<AER_SIM_BUF2_ALGN_LOG2)
#define AER_SIM_BUF3_ALGN  (1<<AER_SIM_BUF3_ALGN_LOG2)
#define AER_SIM_BUF4_ALGN  (1<<AER_SIM_BUF4_ALGN_LOG2)
#define AER_SIM_BUF5_ALGN  (1<<AER_SIM_BUF5_ALGN_LOG2)
#define AER_SIM_BUF6_ALGN  (1<<AER_SIM_BUF6_ALGN_LOG2)
#define AER_SIM_BUF7_ALGN  (1<<AER_SIM_BUF7_ALGN_LOG2)
#define AER_SIM_BUF8_ALGN  (1<<AER_SIM_BUF8_ALGN_LOG2)
#define AER_SIM_BUF9_ALGN  (1<<AER_SIM_BUF9_ALGN_LOG2)
#define AER_SIM_BUF10_ALGN (1<<AER_SIM_BUF10_ALGN_LOG2)
#define AER_SIM_BUF11_ALGN (1<<AER_SIM_BUF11_ALGN_LOG2)
#define AER_SIM_BUF12_ALGN (1<<AER_SIM_BUF12_ALGN_LOG2)
#define AER_SIM_BUF13_ALGN (1<<AER_SIM_BUF13_ALGN_LOG2)
#define AER_SIM_BUF14_ALGN (1<<AER_SIM_BUF14_ALGN_LOG2)
#define AER_SIM_BUF15_ALGN (1<<AER_SIM_BUF15_ALGN_LOG2)
#define AER_SIM_BUF16_ALGN (1<<AER_SIM_BUF16_ALGN_LOG2)
#define AER_SIM_BUF17_ALGN (1<<AER_SIM_BUF17_ALGN_LOG2)
#define AER_SIM_BUF18_ALGN (1<<AER_SIM_BUF18_ALGN_LOG2)
#define AER_SIM_BUF19_ALGN (1<<AER_SIM_BUF19_ALGN_LOG2)
#define AER_SIM_BUF20_ALGN (1<<AER_SIM_BUF20_ALGN_LOG2)

#define AGC_SIM_BUF0_ALGN  (1<<AGC_SIM_BUF0_ALGN_LOG2)

#define DRC_SIM_BUF0_ALGN  (1<<DRC_SIM_BUF0_ALGN_LOG2)
#define DRC_SIM_BUF1_ALGN  (1<<DRC_SIM_BUF1_ALGN_LOG2)
#define DRC_SIM_BUF2_ALGN  (1<<DRC_SIM_BUF2_ALGN_LOG2)
#define DRC_SIM_BUF3_ALGN  (1<<DRC_SIM_BUF3_ALGN_LOG2)
#define DRC_SIM_BUF4_ALGN  (1<<DRC_SIM_BUF4_ALGN_LOG2)

#endif	/* _AERBUFSTC64P_H */

/* nothing past this point */
