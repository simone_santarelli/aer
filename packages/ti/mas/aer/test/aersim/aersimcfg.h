/****************************************************************************** 
 * FILE PURPOSE: Main program and supporting routines for the simulation of AER.  
 ****************************************************************************** 
 * FILE NAME:   aersimcfg.h 
 * 
 * DESCRIPTION: Contains simulation configurations for the testing of AER
 * 
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/
#ifndef _AERSIMCFG_H
#define _AERSIMCFG_H
 
#include <ti/mas/aer/aer.h>

#define AER_SIM_SRATE_16000_DIV_8K (1<<aer_SRATE_16K)
#define AER_SIM_SRATE_8000_DIV_8K  (1<<aer_SRATE_8K)

/* System configuration */
#ifdef ti_targets_C55_large
#define AER_SIM_NUM_HF_MIC 2 /* set to 2 for multi-mic test (max number of 
                                mic is 2 due to memory limitation */
#else
#define AER_SIM_NUM_HF_MIC 3
#endif

#define AER_SIM_NUM_BF_MIC 4

#if AER_SIM_NUM_BF_MIC > AER_SIM_NUM_HF_MIC
#define AER_SIM_NUM_MIC AER_SIM_NUM_BF_MIC
#else
#define AER_SIM_NUM_MIC AER_SIM_NUM_HF_MIC
#endif

#define AER_SIM_MIC_TYPE_ARRAY  0
#define AER_SIM_MIC_TYPE_STAND_ALONE 1
#define AER_SIM_BF_STEER_ANGLE_0    0 
#define AER_SIM_BF_STEER_ANGLE_45  45 
#define AER_SIM_BF_STEER_ANGLE_90  90

#define AER_SIM_HF_MAX_NUM_HVEC 10 
#define AER_SIM_HES_MAX_NUM_HVEC 1
#define AER_SIM_HF_MAX_FILT_LENGTH (AER_SIM_HF_MAX_NUM_HVEC*160)
#define AER_SIM_HES_MAX_FILT_LENGTH (AER_SIM_HES_MAX_NUM_HVEC*160)
#define AER_SIM_HES_MAX_Y2X_DELAY 80 /* 10 msec */
#define AER_SIM_HF_MAX_Y2X_DELAY  80 /* 10 msec */
#define AER_SIM_AEP_COEF_LEN_8K  320
#define AER_SIM_AEP_COEF_LEN_16K 640
#define AER_SIM_MAX_FRAME_LENGTH 160
#define AER_SIM_MAX_BF_FILT_LEN  32

/* Maximum PIU delay to simulate long y2x delay */
#define AER_SIM_MAX_PIU_DEL_RX 3200  /* 200 msec at 16kHz */
#define AER_SIM_MAX_PIU_DEL_TX 80    /* 5 msec at 16kHz */
#define AER_SIM_PIU_DEL_RX     20
#define AER_SIM_PIU_DEL_TX     20
#define AER_SIM_SIU_Y2X_DEL_ADJ 20
#define AER_SIM_PIU_DEL_BUF_LEN_RX (AER_SIM_MAX_FRAME_LENGTH+AER_SIM_MAX_PIU_DEL_RX)
#define AER_SIM_PIU_DEL_BUF_LEN_TX (AER_SIM_MAX_FRAME_LENGTH+AER_SIM_MAX_PIU_DEL_TX)
#define AER_SIM_TRACE_SIZE_MAX_TX 4096
#define AER_SIM_TRACE_SIZE_MAX_RX 1024

#define AER_SIM_HF_SPKGAIN_0dB      0    /* 0dB */
#define AER_SIM_HF_MICGAIN_0dB      0    /* 0dB */
#define AER_SIM_AGC_SAT_THRESHOLD   32500
#define AER_SIM_AGC_SAT_HANGOVER    20      /* 200msec */
#define AER_SIM_AGC_MICGAIN_MAX     (384)   /* 24dB in S11.4 */
#define AER_SIM_AGC_MICGAIN_MIN     (-384)  /* -24dB in S11.4 */
#define AER_SIM_AGC_GAINSWING_MAX   (480)   /* 30dB */

/* AER internal constants */
#define AER_MAX_FRAME_LENGTH       160 /* to remove */
#define AER_LEN_FFT_16K            512  
#define AER_LEN_FFT_8K             256
#define AER_LEN_FFT_MAX            AER_LEN_FFT_16K
#define AER_NUM_EQ_PARS            64//42
#define AER_ORDER_EQIIR            8       
#define AER_LENGTH_EQFIR           19  
#define AER_CORE_MAX_FRAME_LENGTH  80
#define SIU_AER_MAX_FRAME_LENGTH   160   /* max number of samples/10ms frame */
#define SIU_MAX_AEP_LENGTH (MAX_AEP_PERIOD_LENGTH*AEP_PARTIAL_ECHO_SIZ)
#define SIU_MAX_SYS_DELAY  0
#define SIU_MAX_Y2X_DELAY  2*PIU_MAX_SUBSEGLENGTH  /* delay from PIU Tx & Rx */
/*#define SIU_RX_DELAY_LINE_LENGTH  \
    (((2*SIU_MAX_FRAME_LENGTH + SIU_MAX_SYS_DELAY + SIU_MAX_Y2X_DELAY - 1 \
       + SIU_MAX_AEP_LENGTH)/SIU_MAX_FRAME_LENGTH)*SIU_MAX_FRAME_LENGTH) */
       
#define SIU_RX_DELAY_LINE_LENGTH         1600 /* to revisit */

#define aersim_chkbit(field,b) (((field) &  (  b)) == (b))
#define aersim_setbit(field,b) ( (field) |= (  b))
#define aersim_clrbit(field,b) ( (field) &= (~(b)))

#define SIU_AER_TRACE_SIZE_MAX    4096

/* File I/O: buffers */
#define AERSIM_FIO_SIZEOF_WORD        2   /* number of 8 bit bites in a       */
                                          /* 16 bit word                      */
                                          /* CCS file i/o only does bytes     */
                                          /* so we need this temp buffer to   */
                                          /* read and write data in words     */
#define AERSIM_FIO_BUF_SIZE (SIU_AER_MAX_FRAME_LENGTH)

#endif

/* nothing past this point */
