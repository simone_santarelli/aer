/****************************************************************************** 
 * FILE PURPOSE: Main program for simulation/testing of AER
 ****************************************************************************** 
 * FILE NAME:   aersim.c 
 * 
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

/* ANSI C headers */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>               

/* AER, DRC, AGC, MSS and BF header files */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/agc.h>
#include <ti/mas/aer/drc.h>
#include <ti/mas/aer/mss.h>
#include <ti/mas/aer/bf.h>

#include <ti/mas/aer/test/aersim/threadfork.h>

/* AER simulation constants and data structures */
#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/aersim/aersim_fileIO.h>
#include <ti/mas/aer/test/aersim/aerbuffs.h>

/* SIU simulation header files */
#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuaer.h>
#include <ti/mas/aer/test/siusim/siubf.h>
#include <ti/mas/aer/test/piusim/piuloc.h>

#define AER_SIM_PEAK_MIPS_ERLE_THRESH 10
#define AER_SIM_DURATION_CONVERGED    3000 /* Run 30 seconds after converged */

/* Global structures for AER simulation */
siuSetup_t siuSetup;        /* simulation setup/configuration structure */
siuInst_t  siu_inst;        /* system integration unit (SIU) instance structure */
piuInst_t  piu_inst;        /* PCM interface unit (PIU) instance structure */
aepSimInst_t aepSimInst;    /* Acoustic echo path (AEP) simulator structure */
aersimFileIO_t  aersim_fio; /* File I/O structure */
sgnInstBuf_t sgnTxInst[AER_SIM_NUM_MIC]; /* Signal Generator (SGN) for Tx path (Near End) */
sgnInstBuf_t sgnRxInst;                  /* Signal Generator (SGN) for Rx path (Far End)  */ 

/* Input and output buffers for AER simulation */
linSample sim_data_rx_in    [SIU_AER_MAX_FRAME_LENGTH];
linSample sim_data_rx_out   [SIU_AER_MAX_FRAME_LENGTH];
linSample sim_data_dac      [SIU_AER_MAX_FRAME_LENGTH];  /* signal to D/A */
linSample sim_data_rx_sync  [SIU_AER_MAX_FRAME_LENGTH];  /* reference signal */
linSample sim_data_ne_speech[AER_SIM_NUM_MIC][SIU_AER_MAX_FRAME_LENGTH];
linSample sim_data_mic      [AER_SIM_NUM_MIC][SIU_AER_MAX_FRAME_LENGTH];
linSample sim_data_tx_in    [AER_SIM_NUM_MIC][SIU_AER_MAX_FRAME_LENGTH];
linSample sim_data_tx_out   [SIU_AER_MAX_FRAME_LENGTH];

/* Buffers for loading or reading AER tail model coefficients */
#define NUM_H_VEC_GET 3
#define NUM_H_VEC_PUT 3
Fract aer_coef_mant[aer_NUM_WORDS_PER_H_VEC_16K*NUM_H_VEC_PUT];
tint aer_coef_exp[NUM_H_VEC_PUT];
aerTailModel_t aer_tail_model;

/* Buffers for reading BF filter coefficients */
Fract bf_filt_coef[AER_SIM_NUM_BF_MIC][bf_MAX_SUPPORTED_FILT_LEN];
tint bf_filt_len;

/* Prototypes of the funtions only referenced in this file */
tbool aer_sim_fio_read_fe(aersimFileIO_t *fio, linSample *data_fe);
tbool aer_sim_fio_read_ne(aersimFileIO_t *fio,  
                          linSample data_ne[][SIU_AER_MAX_FRAME_LENGTH],tint num_input);
void aer_sim_fio_write_data(aersimFileIO_t *fio);
tint aer_sim_get_erle(siuInst_t *inst);
void aer_sim_print_version_mips(siuInst_t *inst);
void aer_sim_cfg_on_the_fly(siuInst_t *inst);

/* global variables for debugging purpose - to set conditional breakpoint */
int frm_cnt = 0;           
int num_frames_converged = 0;

/* -------------------- MAIN SIMULATION FUNCTION --------------------------- */
int main()
{
  FILE * fp_sim_cfg;
  tint num_mic, mic_type;
  char fname[50];

  printf("Inizio registrazione del file di near end...\n");
  forkthread();
  sleep(2);
  
  /* Initialize simulation environment */
  printf("Inizializzo l'ambiente di simulazione.\n");
  aer_sim_init(&siuSetup);
  
  /* Read simulation configuration parameters */
  printf("Leggo i parametri di configurazione.\n");  
  
  strcpy(fname, AER_SIM_ROOT_FOLDER);
  strcat(fname, "aersimcfg.txt"); 
  fp_sim_cfg = fopen(fname, "r");
  
  if(fp_sim_cfg == NULL) {
    printf("Il file di configurazione non esiste!\n");
    exit(0);
  }  
  aer_sim_read_cfg(&siuSetup, &aersim_fio, fp_sim_cfg);
  
  mic_type = siuSetup.system_cfg.mult_mic_type;
  if(mic_type == AER_SIM_MIC_TYPE_ARRAY) {
    num_mic = siuSetup.system_cfg.num_BF_mic;
  }
  else {  
    num_mic = siuSetup.system_cfg.num_HF_mic;
  }
  
  /* Configure simulation environment based on configuration file */
  printf("Configuro la simulazione in base al file aersimcfg.txt.\n");  
  aer_sim_apply_config(&siuSetup);
  
  /* SIU initialization */
  aer_sim_init_siu(&siu_inst, &siuSetup);

  /* PIU initialization */
  aer_sim_init_piu(&piu_inst, &siuSetup);
  
  /* AER instantiation */  
  printf("Creo una o più istanze AER.\n");
  aer_sim_instantiate_aer(&siu_inst, &siuSetup);
  
  /* Initialize near end (Tx) and far end (Rx) signal generators.
   *  - signal generator is set on or off depending on aersimcfg.txt. */  
  printf("Inizializzo i generatori di segnale near end e far end.\n");
  aer_sim_open_txrx_sgns (&sgnTxInst[0], &sgnRxInst, &siuSetup);

  /* Initialize the Acoustic Echo Path (AEP) simulator. */
  printf("Inizializzo il simulatore Acoustic Echo Path (AEP).\n");
  aer_sim_open_aep(&aepSimInst, &siuSetup);
  
  /* File I/O */
  printf("Apro i file di simulazione di input e output.\n");
  aer_sim_open_files(&aersim_fio, num_mic);
  
  /* Configure AER, DRC, AGC parameters. */
  printf("Configuro i parametri per AER, DRC, AGC e BF.\n");
  aer_sim_config_aer_params(&siu_inst, &siuSetup);

  /* AEP configuration */
  aer_sim_aep_config(&aepSimInst, &siuSetup.aep_params, 
                     siuSetup.aep_gain_adj_exp, siuSetup.aep_gain_adj_mant);
  
  /* -----------------------  MAIN SIMULATION  LOOP --------------------------- */

  printf("Inizio a processare i dati...\n");
  
  /* Enter main loop */
  loop {
    frm_cnt++;

    /* Reconfigure AER on the fly during simulation: set breakpoint here and 
       step into the function. */
    aer_sim_cfg_on_the_fly(&siu_inst);        
    
    /* Get far end data for Rx processing */    
    if(aersim_fio.flag_rxin_rd) {
      /* Get far end data from file */
      if (!aer_sim_fio_read_fe(&aersim_fio, sim_data_rx_in)) {
        printf("Fine del file di far end raggiunta!\n");
        break;
      }      
    }
    else if (siuSetup.fe_sgn_flag) {
      /* Get far end data from signal generator */
      aer_sim_sgn_gen_rx_frame (&sgnRxInst, sim_data_rx_in, siuSetup.aer_rin_frm_size);
    }
    else {
      memset(sim_data_rx_in, 0, sizeof(linSample)*SIU_AER_MAX_FRAME_LENGTH);
    }
    
    /* Receive path processing: DRC -> AER */    
    siuReceiveIn(&siu_inst, sim_data_rx_in, sim_data_rx_out, NULL, 
                 siuSetup.system_cfg.num_HF_mic);
    
    /* PCM interface - receive side: AER -> DAC (speaker) */    
    piuReceiveIn(&piu_inst, sim_data_rx_out, sim_data_dac, sim_data_rx_sync);

    /* Get near end data for Tx processing */
    if(aersim_fio.flag_txin_rd) {
      /* Get from file near end data (with or without echo depending on configuration) */
      if(!aer_sim_fio_read_ne(&aersim_fio, sim_data_ne_speech, num_mic)) {
        printf("Fine del file di near end raggiunta!\n");
        break;
      }
    }
    else if (siuSetup.ne_sgn_flag) {
      /* Get data from signal generator - pure signal without echo */
      aer_sim_sgn_gen_tx_frame(&sgnTxInst[0], sim_data_ne_speech, 
                               siuSetup.aer_sin_frm_size, num_mic);
    }
    else {
      memset(sim_data_ne_speech, 0, 
             sizeof(linSample)*num_mic*SIU_AER_MAX_FRAME_LENGTH);
    }

    /* Generate echo and mix with NE speech depending on flag_txin_echo:
       flag_txin_echo = 1: Tx input already has echo, e.g. captured in real time,
                           don't generate echo in AEP. 
       flag_txin_echo = 0: Tx input doesn't have echo, generate echo using AEP. */
    aer_sim_aep_add_echo(&aepSimInst, sim_data_dac, sim_data_ne_speech, 
                         sim_data_mic, num_mic, aersim_fio.flag_txin_echo);

    /* PCM interface - send side: ADC -> AER */    
    piuSendIn(&piu_inst, sim_data_mic, sim_data_tx_in, num_mic);

    /* Send path processing: AGC -> AER -> MMC -> DRC or BF -> AER -> DRC */
    siuSendIn(&siu_inst, sim_data_rx_sync, sim_data_tx_in, sim_data_tx_out, 
              NULL, num_mic, mic_type, siuSetup.aer_sout_frm_size);

    /* Write output to files */
    aer_sim_fio_write_data(&aersim_fio);

    if (siuSetup.aer_mips_profile) {
      /* Check convergence and stop simulation is convergence is enough. */
      if (aer_sim_get_erle(&siu_inst) >= AER_SIM_PEAK_MIPS_ERLE_THRESH) {
        num_frames_converged++;
        if(num_frames_converged > AER_SIM_DURATION_CONVERGED) {
          printf("Convergenza raggiunta. Stop della simulazione.\n");
          break;
        }
      }
    } /* aer_mips_profile */

  } /* loop */

  /* Print AER version information and MIPS measurements */
  aer_sim_print_version_mips(&siu_inst);
  aer_sim_close_files(&aersim_fio, num_mic);
  
  printf("Simulazione AER conclusa con successo.\n");

  return(0);
} /* end of aersim */

/******************************************************************************
 * FUNCTION PURPOSE: Read Far End Signal
 *****************************************************************************/
tbool aer_sim_fio_read_fe(aersimFileIO_t *fio, linSample *data_fe)
{  
  return(aerSimFread(data_fe, siuSetup.aer_rin_frm_size, fio->fp_rxin));
} /* aer_sim_fio_read_fe */

/******************************************************************************
 * FUNCTION PURPOSE: Read Near End Signal
 *****************************************************************************/
tbool aer_sim_fio_read_ne(aersimFileIO_t *fio, 
                          linSample data_ne[][SIU_AER_MAX_FRAME_LENGTH], 
                          tint num_input)
{
  int i;
  tbool flag_data_good;
  
  flag_data_good = TRUE;
  for(i=0; i<num_input; i++) {
    flag_data_good &= aerSimFread(&data_ne[i][0], siuSetup.aer_sin_frm_size, 
                                  fio->fp_txin[i]);
  }
  
  return(flag_data_good);
} /* aer_sim_fio_read_ne */

/******************************************************************************
 * FUNCTION PURPOSE: Write Rx/Tx/debug Output Data
 *****************************************************************************/
void aer_sim_fio_write_data(aersimFileIO_t *fio)
{
  int i;
  aerDebugStat_t aer_dbg;
  aerVersion_t aer_ver;
  tint *aer_dbg_tint;
  
  /* Write Rx path output */
  if(fio->flag_rxout_wr) {
    aerSimFwrite(sim_data_rx_out, siuSetup.aer_rout_frm_size, fio->fp_rxout);
  }
  
  /* Write Tx path output */
  if(fio->flag_txout_wr) {
    aerSimFwrite(sim_data_tx_out, siuSetup.aer_sout_frm_size, fio->fp_txout);
  }

  /* Write AER debug status data */
  for(i=0; i<siuSetup.system_cfg.num_HF_mic; i++) {    
    aerGetPerformance(siu_inst.aerHFInst[i], &aer_dbg, &aer_ver, FALSE);
    aer_dbg_tint = (tint *)&aer_dbg;
    aerSimFwrite(aer_dbg_tint, sizeof(aerDebugStat_t)/sizeof(tint), fio->fp_debug_aer);
  }

  /* Write MSS debug data */
  // mssDebugStat(siu_inst.mssInst, &mss_dbg);
  // mss_dbg_tint = (tint *)&mss_dbg;
  // aerSimFwrite(mss_dbg_tint, sizeof(mssDebugStat_t)/sizeof(tint), fio->fp_debug_mss);
  
} /* aer_sim_fio_write_data */

/******************************************************************************
 * FUNCTION PURPOSE: Get ERLE Measurement form AER
 *****************************************************************************/
tint aer_sim_get_erle(siuInst_t *inst)
{
  aerDebugStat_t aer_dbg;
  aerVersion_t aer_ver;

  /* get ERLE from the first mic */
  aerGetPerformance(inst->aerHFInst[0], &aer_dbg, &aer_ver, FALSE);

  return(aer_dbg.max_canc_l2);
} /* aer_sim_get_erle */

/******************************************************************************
 * FUNCTION PURPOSE: Simulate real time on-the-fly configuration/control of AER.
 *****************************************************************************/
void aer_sim_cfg_on_the_fly(siuInst_t *inst) 
{
  int i;
  tint num_coeff_words, ret_code;

  /* Control AER: First, stop the program and change toggle_aer to TRUE. */
  if (siuSetup.toggle_aer) { /* --> break point can be set here to stop the code */
    /* Second, change aerCtl in Code Composer watch window to configure AER. */
    for(i=0; i<inst->phone_sys.num_HF_mic; i++){
      aerControl (inst->aerHFInst[i], (aerControl_t *)&siuSetup.aerCtl);
    }
    siuSetup.toggle_aer = FALSE;
  }  
  
  /* Control AGC: First, stop the program and change toggle_agc to TRUE. */
  if (siuSetup.toggle_agc) { /* --> break point can be set here to stop the code */
    /* Second, change agcCtl in Code Composer watch window to configure AGC. */
    for(i=0; i<inst->phone_sys.num_HF_mic; i++){
      agcControl (inst->agcHFInst[i], (agcControl_t *)&siuSetup.agcCtl);
    }
    siuSetup.toggle_agc = FALSE;
  }  
  
  /* Change SIU gains: follow same procedure as above */
  if (siuSetup.toggle_siugain) {
    for(i=0; i<inst->phone_sys.num_HF_mic; i++){
      siuGainControl (inst->agcHFhandles[i], (siuGainControl_t *)&siuSetup.siuGainCtl);
    }
    siuSetup.toggle_siugain = FALSE;      
  }
  
  /* Read tail model filter from AER */
  if (siuSetup.toggle_getflt) {
    if(siuSetup.aer_kernel_srate == AER_SIM_SRATE_16000_DIV_8K) {
      num_coeff_words = aer_NUM_WORDS_PER_H_VEC_16K*NUM_H_VEC_GET;
    }
    else {
      num_coeff_words = aer_NUM_WORDS_PER_H_VEC_8K*NUM_H_VEC_GET;
    }
    ret_code = aerGetFilter(inst->aerHFInst[0], &aer_tail_model, &aer_coef_mant[0], 
                            &aer_coef_exp[0], 0, num_coeff_words); 
    if(ret_code != aer_NOERR) {
      printf("Error when calling aerGetFilter with error code %d!\n", ret_code);
      exit(0);
    }

    for(i=0; i<siuSetup.system_cfg.num_BF_mic; i++) 
    {
      ret_code = bfGetFilter(inst->bfInst, &bf_filt_coef[i][0], &bf_filt_len, bf_FG_BF, i);
      if(ret_code != bf_NOERR) {
        printf("Error when calling bfGetFilter with error code %d!\n", ret_code);
        exit(0);
      }
    }
    
    siuSetup.toggle_getflt = FALSE;       
  }
  
  /* pre-load AER tail model filter */
  if (siuSetup.toggle_putflt) {
    if(siuSetup.aer_kernel_srate == AER_SIM_SRATE_16000_DIV_8K) {
      num_coeff_words = aer_NUM_WORDS_PER_H_VEC_16K*NUM_H_VEC_PUT;
    }
    else {
      num_coeff_words = aer_NUM_WORDS_PER_H_VEC_8K*NUM_H_VEC_PUT;
    }
    
    /* load valid tail model through CCS before executing code below */    
    ret_code = aerPutFilter(inst->aerHFInst[0], &aer_tail_model, &aer_coef_mant[0], 
                 &aer_coef_exp[0], 0, aer_NUM_WORDS_PER_H_VEC_16K, FALSE); 
    if(ret_code != aer_NOERR) {
      printf("Error when calling aerPutFilter with error code %d!\n", ret_code);
      exit(0);
    }
    ret_code = aerPutFilter(inst->aerHFInst[0], &aer_tail_model, 
                            &aer_coef_mant[aer_NUM_WORDS_PER_H_VEC_16K], 
                            &aer_coef_exp[0], aer_NUM_WORDS_PER_H_VEC_16K, 
                            aer_NUM_WORDS_PER_H_VEC_16K, FALSE); 
    if(ret_code != aer_NOERR) {
      printf("Error when calling aerPutFilter with error code %d!\n", ret_code);
      exit(0);
    }
    ret_code = aerPutFilter(inst->aerHFInst[0], &aer_tail_model, 
                            &aer_coef_mant[aer_NUM_WORDS_PER_H_VEC_16K*2], 
                            &aer_coef_exp[0], aer_NUM_WORDS_PER_H_VEC_16K*2, 
                            aer_NUM_WORDS_PER_H_VEC_16K, TRUE); 
    if(ret_code != aer_NOERR) {
      printf("Error when calling aerPutFilter with error code %d!\n", ret_code);
      exit(0);
    }

    siuSetup.toggle_putflt = FALSE;       
  }
  
/* code below can be used to test adaptive AGC
  if(frm_cnt == 1000) {
    siuSetup.agcCtl.valid_bitfield = agc_CTL_VALID_SAT_THRESHOLD;   
    siuSetup.agcCtl.sat_threshold  = 1024;
    agcControl (inst->agcHFInst[0], (agcControl_t *)&siuSetup.agcCtl);
    
    printf("Start filter rescaling test. Reduce AGC saturation threshold.\n");
  }

  if(frm_cnt == 2000) {
    siuSetup.agcCtl.valid_bitfield = agc_CTL_VALID_SAT_THRESHOLD;   
    siuSetup.agcCtl.sat_threshold  = 32500;
    agcControl (inst->agcHFInst[0], (agcControl_t *)&siuSetup.agcCtl);
    printf("Change AGC saturation threshold back to what it was.\n");
  }
*/  
} /* aer_sim_cfg_on_the_fly */


/******************************************************************************
 * FUNCTION PURPOSE: Print AER version number and measured MIPS.
 *****************************************************************************/
void aer_sim_print_version_mips(siuInst_t *inst)
{
  aerDebugStat_t aer_dbg;
  aerVersion_t aer_ver;

  aerGetPerformance(inst->aerHFInst[0], &aer_dbg, &aer_ver, FALSE);
  
  printf("AER version: %d.%d.%d.%d.%d.%d\n", aer_ver.major, aer_ver.minor, 
          aer_ver.patch, aer_ver.build, aer_ver.quality, aer_ver.btype);

  if(siuSetup.aer_mips_profile) { 
#ifdef ti_targets_C55_large
    printf("C55x: AER Tx Max Cycles = %ld, Avg Cycles = %ld\n", 
           siuSetup.TxCyclesMax, siuSetup.TxCyclesAvg);
    printf("C55x: AER Rx Max Cycles = %ld, Avg Cycles = %ld\n", 
           siuSetup.RxCyclesMax, siuSetup.RxCyclesAvg);
#else
#ifndef gnu_targets_arm_GCArmv7A/* c64x+ */
    printf("C64x+/C674x: AER Tx Max Cycles = %d, Avg Cycles = %d\n", 
           siuSetup.TxCyclesMax, siuSetup.TxCyclesAvg);
    printf("C64x+/C674x: AER Rx Max Cycles = %d, Avg Cycles = %d\n", 
           siuSetup.RxCyclesMax, siuSetup.RxCyclesAvg);
#endif        
#endif

    printf("MIPS measurement done\n");
  }          
} /* aer_sim_print_version_mips */


/* nothing past this line */
