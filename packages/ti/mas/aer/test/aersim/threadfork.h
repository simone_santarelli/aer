// threadfork.h

// Standard Linux headers
#include     <stdio.h>              // Always include this header
#include     <stdlib.h>             // Always include this header
#include     <signal.h>             // Defines signal-handling functions (i.e. trap Ctrl-C)
#include     <unistd.h>

// Application headers
#include     "debug.h"
#include     "audio_thread_playback.h"
#include     "audio_thread_record.h"



/* Store previous signal handler and call it */
void (*pSigPrev)(int sig);

// Callback called when SIGINT is sent to the process (Ctrl-C)
void signal_handler(int sig);


void forkthread();
