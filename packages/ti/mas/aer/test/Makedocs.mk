# ==============================================================================
# File            : Makedocs.mk
# Description     : 
#
#   GNU makefile to generate AER Simulation document from sources using Doxygen
#
# ==============================================================================

# The target for generating documents using doxygen is gendocs

AERSIM_DOC=./docs/doxygen/aersim.chm

release : $(AERSIM_DOC)

gendocs $(AERSIM_DOC): ./docs/doxygen/doxygen.h ./siusim/siuaer.c ./siusim/siuagc.c \
                       ./siusim/siutx.c ./siusim/siurx.c ./aersim/aersim.c
	-@echo generating AER simulation Doxygen on-line documentation ...
	if test ! -d ./docs/doxygen/html; then mkdir ./docs/doxygen/html; fi
	cp $(TI_DOXYGEN_TEMPLATES)/*.* ./docs/doxygen/html
	doxygen ./docs/doxygen/aersim_doxconfig

# End of Makedocs.mk

