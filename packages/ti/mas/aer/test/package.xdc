requires ti.mas.types[5,0,8,1];
requires ti.mas.fract[2,0,8,1];
requires ti.mas.util[5,0,0,0];
requires ti.mas.vpe[4,0,1,0];
requires ti.mas.sdk[4,0,0,0];
requires ti.mas.aer[7,0,0,0];
requires internal ti.mas.swtools[3,0,0,0];


/*! AER unit test package
 * 
 * @a(NAME)  `ti.mas.aer.test`
 *
 * @a(DESCRIPTION)
 *
 *        This package creates the AER unit test system. It depends on the following packages:
 * @p(dlist)
 * - `ti.mas.types`
 * - `ti.mas.fract`
 * - `ti.mas.util`
 * - 'ti.mas.aer'
 * - 'ti.mas.sdk'
 * - 'ti.mas.mhm'
 *
 * @a(CONTENTS)
 * @p(dlist)
 * - 'aersim' module
 *      provides simulation to verify functionality of AER.
 * - 'g167' module
 *      provides simulation to run G.167 tests for AER.
 * - 'p340' module
 *      provides simulation to run P.340 tests for AER.
 * - `version` module
 *      provides version numbering for AER package and is described in
 *      `version.xdc`. Note that `version.xdc` is generated from the template file 
 *      `version.xdt`.
 * @p
 * @a(TARGETS) The following architecture targets are supported:
 *
 * @p(dlist)
 * - `C55`
 * @p
 * @a(BUILD)  The build options are implemented in the file `package.bld`.
 *
 * Build command: `xdc XDCARGS="option [g167p340]"`.
 * option = [c55L, c64P, all]
 * If any other option other than above is used then the package is not built correctly
 * and will crash when used in conjunction with other packages. 
 *
 * If "g167p340" is present, it will build G.167/P.340 tests.
 *
 * Generated files:
 *
 * @p(dlist)
 * -  `version.h`
 * @p
 *
 * Command to clean: `xdc clean`
 *
 * @a(RELEASES)
 *
 * Release creation: `xdc XDCARGS="option1 [g167p340] option2" release`
 * option1 = [c55L, c64P, all]
 * option2 = [obj, src, docs]
 *
 * If "g167p340" is present, source code for G.167/P.340 tests will be releases.
 *
 * Using the option1 as given and option2="obj" creates the released tar file containing object code only:
 * `ti_mas_aer_test_<arch>_src_<v>_<r>_<x>_<p>.tar` ???? src or obj ???
 * or `ti_mas_aer_test_all_src_<v>_<r>_<x>_<p>.tar` if option 1="all" ???? src or obj ???
 * 
 * Using the option1 as given and option2="src" creates the released tar file containing source with object code:
 * `ti_mas_aer_test_<arch>_src_<v>_<r>_<x>_<p>.tar`
 * or `ti_mas_aer_test_all_src_<v>_<r>_<x>_<p>.tar` if option 1="all"
 *
 * If option2 = "obj" or empty then `ti_mas_aer_test_all_<v>_<r>_<x>_<p>.tar` is created containing only object code
 *
 * To generate document only the following should be used:
 * `xdc XDCARGS="docs" release` or `xdc XDCARGS="option1,docs"`
 * and `ti_mas_aer_test_docs_<v>_<r>_<x>_<p>.tar` is created
 * 
 * If any other option other than the ones listed above is used, no release is created.
 * 
 *
 * 
 *
 * @a(Copyright)
 * @p(html)
 * &#169; 2006 Texas Instruments, Inc.
 * @p
 */

package ti.mas.aer.test[5,0,0,0] {
}

/* nothing past this point */
