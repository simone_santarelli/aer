/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of AER
 ****************************************************************************** 
 * FILE NAME:   siuaer.c
 * 
 * DESCRIPTION: Contains routines for simulation testing of AER
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 * 
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/fract/fract.h>
#include <ti/mas/aer/aer.h>
#include <ti/mas/vpe/svd.h>

/* Signal generator and AEP header files */
#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuaer.h>
#include <ti/mas/aer/test/siusim/siuport.h>        

#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/aerbuffs.h>


#define SIU_NUM_AER_BUFFS 21        /* static buffer allocation */

tint siu_alloc_aer_buffs(const ecomemBuffer_t *bufs_req, ecomemBuffer_t *bufs_aloc, 
                         tint num_bufs, tuint phone_mode, tint mic_num, tbool multi_mic_flag);
void siu_print_buff_usage(const ecomemBuffer_t *bufs, tint num_bufs);
		   

/******************************************************************************
 * FUNCTION PURPOSE: Create AER module.
 ******************************************************************************
 * DESCRIPTION: Obtains info on AER buffers, and creates one instance of AER.
 *
 *              NOTE: Should save number of buffers and buffer descriptors
 *                    in order to make delete possible.
 *
 *                    Allocates AER receive-in buffer that is used in PIU.
 *                    Assumes receive-in buffer is offset 5.
 *
 *  void siu_new_aer (
 *    (siuInst_t *)siu_inst,  - SIU instance pointer
 *    tint num_HF_mic         - number of hands-free mics
 *    tbool band_split_flag)  - band split option flag
 *
 *****************************************************************************/
void siu_new_aer(siuInst_t *inst, tint num_HF_mic, tbool band_split_flag)
{
  tint err_code, num_bufs_req_by_aer, mic_num;
  const ecomemBuffer_t *bufs_req_by_aer;
  ecomemBuffer_t        bufs_aloc_by_siu[SIU_NUM_AER_BUFFS];
  aerCreateConfig_t aer_create_cfg;
  aerSizeConfig_t   aer_size_cfg;
  aerNewConfig_t    aer_new_cfg;
  
  inst->phone_sys.num_HF_mic = num_HF_mic; /* number of HF microphones */

  /* 
   * Call aerCreate() to pass system information that is shared with all 
   * AER instances.
   */  
  /* check if there are multiple hands free microphones */
  aer_create_cfg.config_bitfield  = 0x0;   
  if(num_HF_mic > 1) {  
    aersim_setbit(aer_create_cfg.config_bitfield, aer_CRT_MULT_MIC_MODE);
  }
  
  /* pass debug and call out functions */
  aer_create_cfg.debugInfo = siuDbgAer;
  aer_create_cfg.eventOut  = siuAgcAerEvent; /* call out function pointer to relay  
                                            * AER information to other modules */
  /* pass maximum sampling rate supported by AER */
  aer_create_cfg.max_sampling_rate = aer_SRATE_16K;  /* defined in aer.h */

  /* call AER API function aerCreate() */
  err_code = aerCreate(&aer_create_cfg);
  if(err_code != aer_NOERR) {
    printf("Error when calling aerCreate() with error code %d!\n", err_code);
    exit(1);
  }
  
  /* 
   * Create AER instance shared by Handset and Headset 
   */
  /* set up configurations for aerGetSizes() */
  aer_size_cfg.max_tail_length = AER_SIM_HES_MAX_FILT_LENGTH; 
  aer_size_cfg.max_y2x_delay = AER_SIM_HES_MAX_Y2X_DELAY;
  aer_size_cfg.config_bitfield = 0;  /* use TDNLP for HS and HES */

  /* Call API function aerGetSizes() to get memory requirements:
     - num_bufs_req_by_aer: number of buffers required by AER 
     - bufs_req_by_aer:   pointer to buffer descriptors defined inside AER */
  err_code = aerGetSizes(&num_bufs_req_by_aer, &bufs_req_by_aer, &aer_size_cfg);
  if (err_code != aer_NOERR) {
    printf("Error when calling aerGetSizes() for HES with error code %d!\n", err_code);
    exit(1);
  }
 
  /* check if enough memory is allocated for SIU buffer descriptors */
  if (num_bufs_req_by_aer > SIU_NUM_AER_BUFFS) {
    printf("No enough memory allocated by SIU for AER buffer descriptors!\n");
	exit(1);
  }
  
  printf("AER required buffers for hand set/head set :\n");
  siu_print_buff_usage(bufs_req_by_aer, num_bufs_req_by_aer);
  
  /* allocate memory according to the requirements returned by aerGetSizes():
     - single mic for HES/HS.  */
  err_code = siu_alloc_aer_buffs(bufs_req_by_aer, bufs_aloc_by_siu, 
                                 num_bufs_req_by_aer, aer_PHM_HES, 0, (num_HF_mic > 1)); 
  if (err_code != aer_NOERR) {
    printf("Error when allocating buffers for HES with error code %d!\n", err_code);
    exit(1);
  }
  
  printf("Buffers allocated by SIU for hand set/head set AER:\n");
  siu_print_buff_usage(bufs_aloc_by_siu, num_bufs_req_by_aer);

  /* set up configurations for aerNew() */
  aer_new_cfg.handle  = (void *) siuMakeID( (SIU_MID_AER|AER_HES_ID), 0);
  aer_new_cfg.sizeCfg = aer_size_cfg;
  inst->aerHEShandle  = aer_new_cfg.handle;

  /* Call API function aerNew() to create an instance for HS/HES:
     - pass buffer descriptors that are set by SIU
     - pass number of buffer descriptors   */
  err_code = aerNew (&inst->aerHESInst, num_bufs_req_by_aer, bufs_aloc_by_siu, &aer_new_cfg);
  if (err_code != aer_NOERR) {
    printf("Error when calling aerNew() for HES with error code %d!\n", err_code);
    exit(1);
  }

  /* 
   * Create AER instances for handsfree - one instance per handsfree mic
   */
  /* Call aerGetSizes() to get memory requirements. */
  aer_size_cfg.max_tail_length = AER_SIM_HF_MAX_FILT_LENGTH; 
  aer_size_cfg.max_y2x_delay   = AER_SIM_HF_MAX_Y2X_DELAY;
  aer_size_cfg.config_bitfield = aer_SIZE_FDNLP_REQ;      /* use FDNLP for HF */
  if(band_split_flag) {
    aer_size_cfg.config_bitfield |= aer_SIZE_USE_BANDSPLIT; /* bandsplit option */
  }
  
  err_code = aerGetSizes(&num_bufs_req_by_aer, &bufs_req_by_aer, &aer_size_cfg);
  if (err_code != aer_NOERR) {
    printf("Error when calling aerGetSizes() for HF with error code %d!\n", err_code);
    exit(1);
  }

  printf("AER required buffers for hands free:\n");
  siu_print_buff_usage(bufs_req_by_aer, num_bufs_req_by_aer);

  /* Create instances for handsfree - one per mic */
  for(mic_num = 0; mic_num < num_HF_mic; mic_num++) {
    /* allocate memory according to the requirements returned by aerGetSizes():
       - multi-mic or single-mic for HF depends on AER sim configuration.     */
    err_code = siu_alloc_aer_buffs(bufs_req_by_aer, bufs_aloc_by_siu, 
                                   num_bufs_req_by_aer, aer_PHM_HF, mic_num,
								   (num_HF_mic > 1));

    if (err_code != aer_NOERR) {
      printf("Error when allocating buffers for HF with error code %d!\n", err_code);
      exit(1);
    }

    printf("Buffers allocated by SIU for hands-free AER:\n");
    siu_print_buff_usage(bufs_aloc_by_siu, num_bufs_req_by_aer);
    
    /* Call aerNew() to create one instance per mic */	
    aer_new_cfg.handle = (void *)siuMakeID((SIU_MID_AER|(AER_HF0_ID+mic_num)), 0);
    aer_new_cfg.sizeCfg = aer_size_cfg;
    inst->aerHFhandles[mic_num] = aer_new_cfg.handle;
    err_code = aerNew(&inst->aerHFInst[mic_num], num_bufs_req_by_aer, 
	                  bufs_aloc_by_siu, &aer_new_cfg);
    if (err_code != aer_NOERR) {
      printf("Error when calling aerNew() for HF with error code %d!\n", err_code);
      exit(1);
    }
  }    
} /* siu_new_aer */



/******************************************************************************
 * FUNCTION PURPOSE: Open AER instances.
 *
 *****************************************************************************/
void siu_open_aer(siuInst_t *inst, siuAerSysPars_t *aer_pars, 
                  tint samp_rates, tint num_mic)
{
  aerConfig_t aerCfg;
  tint err_code, mic_num, srate_rxout_txin;

  /* Open the AER instance shared by headset and handset */
  /* parameter y2x_delay is in units of 125-usec */
  srate_rxout_txin = aersim_chkbit(samp_rates, aer_SRATE_RXOUT_TXIN_16K);
  aerCfg.y2x_delay        = aer_pars->y2x_delay >> srate_rxout_txin;
  aerCfg.srate_bitfield   = samp_rates;
  aerCfg.tail_length      = aer_pars->aer_filter_len_HES;
  aerCfg.delay_tx_ag_chg  = aer_pars->ag_chg_delay_tx >> srate_rxout_txin;
  aerCfg.delay_rx_ag_chg  = aer_pars->ag_chg_delay_rx >> srate_rxout_txin;
  aerCfg.num_samp_interp  = aer_pars->ag_chg_interp_samp;
  aerCfg.phone_mode       = aer_PHM_HES;
  aerCfg.reset_bitfield   = 0xffff;
  aerCfg.valid_bitfield   = 0x007f;
  
  /* Call AER API to open the HS/HES instance */
  err_code = aerOpen (inst->aerHESInst, &aerCfg);
  if (err_code != aer_NOERR) {
    printf("Error when calling aerOpen() for HES with error code %d!\n", err_code);
    exit(1);
  }
  
  /* Open multiple hands free instances - one per mic */
  aerCfg.tail_length = aer_pars->aer_filter_len_HF;
  aerCfg.phone_mode  = aer_PHM_HF;
  for(mic_num = 0; mic_num < num_mic; mic_num++) {
    err_code = aerOpen (inst->aerHFInst[mic_num], &aerCfg);
    if (err_code != aer_NOERR) {
      printf("Error when calling aerOpen() for HF with error code %d!\n", err_code);
      exit(1);
    }
  }
} /* siu_open_aer */

/******************************************************************************
 * FUNCTION PURPOSE: Close AER instances.
 *
 *****************************************************************************/
void siu_close_aer(siuInst_t *inst)
{
  int i;
  
  aerClose(inst->aerHESInst);
  
  for(i=0; i<inst->phone_sys.num_HF_mic; i++) {
    aerClose(inst->aerHFInst[i]);
  }
}


/******************************************************************************
 * FUNCTION PURPOSE: Allocate buffers for AER instance
 *
 *****************************************************************************/
tint siu_alloc_aer_buffs(const ecomemBuffer_t *bufs_req, ecomemBuffer_t *bufs_aloc, 
                         tint num_bufs, tuint phone_mode, tint mic_num, 
						 tbool multi_mic_flag)
{
  tint stat;
  int i;
  
  stat = aer_NOERR;
  for (i = 0; i < num_bufs; i++) {
    bufs_aloc[i].mclass = ecomem_CLASS_INTERNAL; /* internal memory for all  */
    bufs_aloc[i].volat  = FALSE;                 /* initialized to permanent */
  }
  
  /* buffer 0 - permanent, per HF mic, not shared by HF and HS */
  if (bufs_req[0].size <= AER_SIM_BUF0_SIZE) {
    if(phone_mode == aer_PHM_HF) {
      bufs_aloc[0].base  = &(aer_buff0_hf[mic_num][0]);  /* one per HF mic */
      bufs_aloc[0].size  = AER_SIM_BUF0_SIZE;    
    }
    else {
      bufs_aloc[0].base  = &(aer_buff0_hs[0]);           /* one for HS */
      bufs_aloc[0].size  = AER_SIM_BUF0_SIZE;    
    }  
    bufs_aloc[0].log2align = AER_SIM_BUF0_ALGN_LOG2;
  }
  else {
    stat = !aer_NOERR;
  }
  
  /* buffer 1 - volatile scratch */
  bufs_aloc[1].volat = TRUE;
  bufs_aloc[1].base  = &(aer_buff1[0]);
  bufs_aloc[1].size  = AER_SIM_BUF1_SIZE;
  bufs_aloc[1].log2align = AER_SIM_BUF1_ALGN_LOG2;

  /* buffer 2 and buffer 4, per HF mic, not shared by HF and HS */
  if(phone_mode == aer_PHM_HF) {
    bufs_aloc[2].base  = &(aer_buff2_hf[mic_num][0]);
    bufs_aloc[2].size  = AER_SIM_BUF2_SIZE_HF;
    bufs_aloc[4].base  = &(aer_buff4_hf[mic_num][0]);
    bufs_aloc[4].size  = AER_SIM_BUF4_SIZE;
  }
  else {
    bufs_aloc[2].base  = &(aer_buff2_hs[0]);
    bufs_aloc[2].size  = AER_SIM_BUF2_SIZE_HS;
    bufs_aloc[4].base  = &(aer_buff4_hs[0]);
    bufs_aloc[4].size  = AER_SIM_BUF4_SIZE;
  }
  bufs_aloc[2].log2align = AER_SIM_BUF2_ALGN_LOG2;
  bufs_aloc[4].log2align = AER_SIM_BUF4_ALGN_LOG2;

  /* buffer 3, permanent, shared by HS and all HF mics */
  bufs_aloc[3].base  = &(aer_buff3[0]);
  bufs_aloc[3].size  = AER_SIM_BUF3_SIZE_HF;
  bufs_aloc[3].log2align = AER_SIM_BUF3_ALGN_LOG2;

  /* buffer 5, permanent, shared by HS and all HF mics */
  bufs_aloc[5].base  = &(aer_buff5[0]);
  bufs_aloc[5].size  = AER_SIM_BUF5_SIZE;
  bufs_aloc[5].log2align = AER_SIM_BUF5_ALGN_LOG2;

  /* buffer 6, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[6].base  = &(aer_buff6[mic_num][0]);
  bufs_aloc[6].size  = AER_SIM_BUF6_SIZE;
  bufs_aloc[6].log2align = AER_SIM_BUF6_ALGN_LOG2;

  /* buffer 7, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[7].base  = &(aer_buff7[mic_num][0]);
  bufs_aloc[7].size  = AER_SIM_BUF7_SIZE;
  bufs_aloc[7].log2align = AER_SIM_BUF7_ALGN_LOG2;
  
  /* buffer 8 - volatile scratch */
  bufs_aloc[8].base  = &(aer_buff8[0]);
  bufs_aloc[8].size  = AER_SIM_BUF8_SIZE;
  bufs_aloc[8].volat = TRUE;
  bufs_aloc[8].log2align = AER_SIM_BUF8_ALGN_LOG2;

  /* buffer 9 - volatile scratch */
  bufs_aloc[9].base  = &(aer_buff9[0]);
  bufs_aloc[9].size  = AER_SIM_BUF9_SIZE;
  bufs_aloc[9].volat = TRUE;
  bufs_aloc[9].log2align = AER_SIM_BUF9_ALGN_LOG2;

  /* buffer 10, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[10].base  = &(aer_buff10[mic_num][0]);
  bufs_aloc[10].size  = AER_SIM_BUF10_SIZE;
  bufs_aloc[10].log2align = AER_SIM_BUF10_ALGN_LOG2;

  /* buffer 11, permanent, shared by HS and all HF mics */
  bufs_aloc[11].base  = &(aer_buff11[0]);
  bufs_aloc[11].size  = AER_SIM_BUF11_SIZE;
  bufs_aloc[11].log2align = AER_SIM_BUF11_ALGN_LOG2;

  /* buffer 12, permanent, shared by HS and all HF mics */
  bufs_aloc[12].base  = &(aer_buff12[0]);
  bufs_aloc[12].size  = AER_SIM_BUF12_SIZE;
  bufs_aloc[12].log2align = AER_SIM_BUF12_ALGN_LOG2;
 
  /* buffer 13 - volatile scratch */
  bufs_aloc[13].volat = TRUE;
  bufs_aloc[13].base  = &(aer_buff13[0]);
  bufs_aloc[13].size  = AER_SIM_BUF13_SIZE;
  bufs_aloc[13].log2align = AER_SIM_BUF13_ALGN_LOG2;

  /* buffer 14, permanent, shared by all HF mics */
  if(multi_mic_flag) { /* this buffer is only used for multi-mics */
    bufs_aloc[14].base  = &(aer_buff14[0]);
    bufs_aloc[14].size  = AER_SIM_BUF14_SIZE;
  }
  else {
    bufs_aloc[14].base  = NULL;
    bufs_aloc[14].size  = 0;
  }
  bufs_aloc[14].log2align = AER_SIM_BUF14_ALGN_LOG2;
    
  /* buffer 15, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[15].base  = &(aer_buff15[mic_num][0]);
  bufs_aloc[15].size  = AER_SIM_BUF15_SIZE;   
  bufs_aloc[15].log2align = AER_SIM_BUF15_ALGN_LOG2;

  /* buffer 16, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[16].base  = &aer_buff16[mic_num][0];
  bufs_aloc[16].size  = AER_SIM_BUF16_SIZE;   
  bufs_aloc[16].log2align = AER_SIM_BUF16_ALGN_LOG2;

  /* buffer 17, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[17].base  = &aer_buff17[mic_num][0];
  bufs_aloc[17].size  = AER_SIM_BUF17_SIZE;   
  bufs_aloc[17].log2align = AER_SIM_BUF17_ALGN_LOG2;
  
  /* buffer 18, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[18].base  = &aer_buff18[mic_num][0];
  bufs_aloc[18].size  = AER_SIM_BUF18_SIZE;   
  bufs_aloc[18].log2align = AER_SIM_BUF18_ALGN_LOG2;
  
  /* buffer 19, permanent, shared by HS and all HF mics */
  bufs_aloc[19].base  = &aer_buff19[0];
  bufs_aloc[19].size  = AER_SIM_BUF19_SIZE;   
  bufs_aloc[19].log2align = AER_SIM_BUF19_ALGN_LOG2;

  /* buffer 20, permanent, per HF mic. HS and HF 1st mic overlay */
  bufs_aloc[20].base  = &aer_buff20[mic_num][0];
  bufs_aloc[20].size  = AER_SIM_BUF20_SIZE;   
  bufs_aloc[20].log2align = AER_SIM_BUF20_ALGN_LOG2;
  
  return(stat);
} /* siu_alloc_aer_buffs */

void siu_print_buff_usage(const ecomemBuffer_t *bufs, tint num_bufs)
{
  int i;
  
  printf("    Buffer    Size(twords)    Alignment    Volatile   address\n");
  for(i=0; i<num_bufs; i++) {
    printf("     %3d       %7d         %4d       ", 
                 i, (int)bufs[i].size, (int)bufs[i].log2align);
    if(bufs[i].volat)
      printf("    yes");
    else  
      printf("    no ");
	
	printf("    0x%x\n", bufs[i].base);
  }
}

/* nothing past this point */
