#ifndef _SIUAER_H
#define _SIUAER_H

/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of AER
 ****************************************************************************** 
 * FILE NAME:   siuaer.h
 * 
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/


#include <ti/mas/types/types.h>                      
#include <ti/mas/aer/test/siusim/siuloc.h>           

typedef struct siuAerSysPars_s {
  tint aer_filter_len_HF;  /* # taps @8kHz in AER filter model for hands free */
  tint aer_filter_len_HES; /* # taps @8kHz in AER filter model for HS/HES     */
  tint y2x_delay;
  tint ag_chg_delay_tx;  
  tint ag_chg_delay_rx;  
  tint ag_chg_interp_samp;  
} siuAerSysPars_t;

void siu_new_aer(siuInst_t *inst, tint num_HF_mic, tbool band_split_flag);                   
void siu_open_aer(siuInst_t *inst, siuAerSysPars_t *aer_pars, 
                  tint samp_rates, tint num_mic);
void siu_close_aer(siuInst_t *inst);
            
#endif /* _SIUAER_H */

/* nothing past this point */
