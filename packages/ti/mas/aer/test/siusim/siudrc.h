#ifndef _SIUDRC_H
#define _SIUDRC_H

/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of DRC
 ****************************************************************************** 
 * FILE NAME:   siudrc.h
 * 
 * DESCRIPTION: Contains constants and arrays for simulation testing of DRC
 * 
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/aer/drc.h>

extern const Fract drc_lowband_coef_8k[];
extern const Fract drc_midband_coef_8k[];
extern const Fract drc_highband_coef_8k[];
extern const Fract drc_lowband_coef_16k[];
extern const Fract drc_midband_coef_16k[];
extern const Fract drc_highband_coef_16k[];
        
void siu_new_drc    (siuInst_t * inst);
void siu_open_drc   (siuInst_t *inst, tuint cfg_bits, tint sampling_rates);
void siu_close_drc  (siuInst_t *inst);
void siu_control_drc(siuInst_t *inst, tint sampling_rates, tint drc_on_flag);
             
#endif /* _SIUDRC_H */

/* nothing past this point */
