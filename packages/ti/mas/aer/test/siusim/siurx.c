/******************************************************************************
 * FILE PURPOSE: Software Integration Unit Receive Task.
 ******************************************************************************
 * FILE NAME:   siurx.c
 *
 * DESCRIPTION: Contains routines that implement SIU Receive Task.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/

/* System definitions and utilities */
#include <ti/mas/types/types.h>                      /* DSP types            */

/* AER and AEP header files */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/drc.h>
#ifdef TEST_VEU
#include <ti/mas/veu/veu.h>
#endif

#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/aersim/aersim.h>

extern siuSetup_t siuSetup;
linSample siu_rxout_temp[SIU_AER_MAX_FRAME_LENGTH];

/******************************************************************************
 * FUNCTION PURPOSE: Receive path processing main routine.
 *   Description: Call AER receive path functions: DRC, AER Rx
 ******************************************************************************/
void siuReceiveIn(siuInst_t *inst, linSample *rxin, linSample *rxout, 
                  tuint *trace, tint num_mic)
{
  int i;

  if(siuSetup.aer_mips_profile) {
    /* flush cache for profiling */  
    aersim_cacheFlush();

    /* read CPU cycle count before AER processing */
    aersim_profile_get_cycle_before_aer(&siuSetup);
  }
  
  /* DRC processing: input and output buffers MUST be different */
  drcProcess (inst->drcInst, rxin, siu_rxout_temp);

  /* AER processing: only active instance writes to buffer rxout */
  for(i=0; i<num_mic; i++) {
    aerReceiveIn (inst->aerHFInst[i], siu_rxout_temp, rxout, trace);
  }
  
  /* read CPU cycle count after AER processing and update average and max */ 
  if(siuSetup.aer_mips_profile) {
    aersim_profile_get_cycle_after_aer_receive(&siuSetup);
  }
  
} /* siuReceiveIn */


/* nothing past this line */
