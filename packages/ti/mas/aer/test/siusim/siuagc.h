#ifndef _SIUAGC_H
#define _SIUAGC_H

/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of AER
 ****************************************************************************** 
 * FILE NAME:   siuagc.h
 * 
 * DESCRIPTION: Contains constants and arrays for simulation testing of AER
 * 
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/agc.h>

/* used for gain control through SIU */
typedef struct siuGainControl_s {
  tint    ctl_code;             
  union { 
    Fract mic_gain;  /* microphone gain */
	Fract spk_gain;  /* speaker gain */
  }u;
}siuGainControl_t;

enum {
  siu_SET_MIC_GAIN              = 0,  /* to set microphone gain as requested 
                                         from outside of AGC */
  siu_SET_SPK_GAIN              = 1,  /* to set speaker gain */
  siu_AGC_EVENT_MIC_GAIN_CHANGE = 2   /* to set microphone gain as requested by AGC */
};

void siuAgcAerEvent (void *handle, aerInfo_t *event);
void siuAgcEvent    (void *handle, agcEvent_t *event);
void siuGainControl (void *handle, siuGainControl_t *siu_agc_ctl);

#endif /* _SIUAGC_H */

/* nothing past this point */
