/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of BF
 ****************************************************************************** 
 * FILE NAME:   siubf.c
 * 
 * DESCRIPTION: Contains constants and arrays for simulation testing of bf
 *
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/fract/fract.h>
#include <ti/mas/util/ecomem.h>
#include <ti/mas/aer/bf.h>

/* Signal generator and AEP header files */
#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuport.h>
#include <ti/mas/aer/test/siusim/siubf.h>

#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>


#define SIU_NUM_bf_BUFFS              5        /* buffer allocation */

/************************************************************
 * Memory heap sizes
 ************************************************************/
#define bf_INT_HEAP_SIZE 0x600
#define bf_EXT_HEAP_SIZE 0x600

//#pragma DATA_SECTION (bfIntMem, "int_heap");
tword bfIntMem[bf_INT_HEAP_SIZE];
//#pragma DATA_SECTION (bfExtMem, "ext_heap");
tword bfExtMem[bf_EXT_HEAP_SIZE];
bfMemHeap_t bfIntHeap;
bfMemHeap_t bfExtHeap;

tint siu_alloc_bf_buffs(const ecomemBuffer_t *bufs_req, ecomemBuffer_t *bufs_aloc, 
                         tint num_bufs);
void siu_print_buff_usage(const ecomemBuffer_t *bufs, tint num_bufs);
       

/******************************************************************************
 * FUNCTION PURPOSE: Create bf module.
 ******************************************************************************
 * DESCRIPTION: Obtains info on bf buffers, and creates one instance of bf.
 *
 *              NOTE: Should save number of buffers and buffer descriptors
 *                    in order to make delete possible.
 *
 *                    Allocates bf receive-in buffer that is used in PIU.
 *                    Assumes receive-in buffer is offset 5.
 *
 *  void siu_new_bf (
 *    (siuInst_t *)siu_inst)  - SIU instance pointer
 *
 *****************************************************************************/
void siu_new_bf(siuInst_t *inst, tint samp_rates)
{
  tint err_code, num_bufs_req_by_bf;
  const ecomemBuffer_t *bufs_req_by_bf;
  ecomemBuffer_t        bufs_aloc_by_siu[SIU_NUM_bf_BUFFS];
  bfSizeConfig_t        bf_size_cfg;
  bfNewConfig_t         bf_new_cfg;
  
  /* Set up configurations for bfGetSizes() */
  bf_size_cfg.max_sampling_rate  = bf_SAMP_RATE_16K;
  bf_size_cfg.max_num_mics       = AER_SIM_NUM_BF_MIC;
  bf_size_cfg.max_filter_length  = AER_SIM_MAX_BF_FILT_LEN;
  bf_size_cfg.bf_type            = bf_TYPE_FIXED;

  /* Call API function bfGetSizes() to get memory requirements:
     - num_bufs_req_by_bf: number of buffers required by bf 
     - bufs_req_by_bf:   pointer to buffer descriptors defined inside bf */
  err_code = bfGetSizes(&num_bufs_req_by_bf, &bufs_req_by_bf, &bf_size_cfg);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfGetSizes() with error code %d!\n", err_code);
    exit(1);
  }

  printf("bf required buffers:\n");
  siu_print_buff_usage(bufs_req_by_bf, num_bufs_req_by_bf);
  
  /* Check if there is enough memory allocated for bf */
  if(   num_bufs_req_by_bf > SIU_NUM_bf_BUFFS ) {
    printf("Memory allocated by SIU is not enough for bf!\n");
    exit(0);
  }

  /* Provide memory to bf through bfNew() */
  bf_mem_init_heap ();
  err_code = siu_alloc_bf_buffs(bufs_req_by_bf, bufs_aloc_by_siu, 
                                 num_bufs_req_by_bf); 
  if (err_code != bf_NOERR) {
    printf("Error when allocating buffers for bf with error code %d!\n", err_code);
    exit(1);
  }
  printf("Buffers allocated by SIU for bf:\n");
  siu_print_buff_usage(bufs_aloc_by_siu, num_bufs_req_by_bf); 
  
  /* Set up configurations for bfNew() */
  bf_new_cfg.handle  = (void *) siuMakeID(SIU_MID_BF, 0);
  bf_new_cfg.sizeCfg = bf_size_cfg;

  /* Call API function bfNew() to create an bf instance:
     - pass buffer descriptors that are set by SIU
     - pass number of buffer descriptors   */
  err_code = bfNew (&inst->bfInst, num_bufs_req_by_bf, bufs_aloc_by_siu, &bf_new_cfg);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfNew() with error code %d!\n", err_code);
    exit(1);
  }
   
} /* siu_new_bf */



/******************************************************************************
 * FUNCTION PURPOSE: Open bf instances.
 *
 *****************************************************************************/
void siu_open_bf(siuInst_t *inst, tint samp_rates, tint num_mic)
{
  /*ecomemBuffer_t bufs[SIU_NUM_bf_BUFFS];*/
  bfConfig_t bfCfg;
  tint err_code;

  /* Open the bf instance */
  if ((samp_rates & 0x04) == 0) {
    bfCfg.sampling_rate    = bf_SAMP_RATE_8K;
  }
  else {
    bfCfg.sampling_rate    = bf_SAMP_RATE_16K;
  }
  bfCfg.num_mics = num_mic;
  
  /* Call bf API to open the bf instance */
  err_code = bfOpen (inst->bfInst, &bfCfg);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfOpen()with error code %d!\n", err_code);
    exit(1);
  }

  /* Temporary code to test bfClose() and bfDelete() */
/*  
  err_code = bfClose(inst->bfInst);  
  if (err_code != bf_NOERR) {
    printf("Error when calling bfClose() with error code %d!\n", err_code);
    exit(1);
  }

  err_code = bfDelete (&inst->bfInst, SIU_NUM_bf_BUFFS, bufs);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfDelete() with error code %d!\n", err_code);
    exit(1);
  }
*/  
} /* siu_open_bf */

/******************************************************************************
 * FUNCTION PURPOSE: Set the operating mode of a bf
 ******************************************************************************
 * DESCRIPTION: Controls the operating mode and internal parameters of the bf.
 *
 *  void siu_config_bf (
 *    siuInst_t *inst,
 *    tint sampling_rate)  - 1 or 2 indicate 8 or 16 kHz sampling rate                                 
 *
 *****************************************************************************/
void siu_config_bf (siuInst_t *inst, bfControl_t *ctl, Fract coeffs[][SIU_BF_FLT_LEN], 
                    tint sampling_rates, tint num_mics)
{
  tint err_code, i;
 
  if (inst->bfInst == NULL) {
    printf("Error when config BF with NULL pointer!\n");
    exit(1);
  }
     
  /* put in filter coefficients for BF */
  for (i=0; i<num_mics; i++)
  {
    bfPutFilter (inst->bfInst, &coeffs[i][0], bf_FG_BF, i, SIU_BF_FLT_LEN);
  }
  
  err_code = bfControl(inst->bfInst, ctl);
  if (err_code != bf_NOERR) {
    printf("Error when configure BF with error code %d!\n", err_code);
    exit(1);
  }
} /* siu_config_bf */   


/******************************************************************************
 * FUNCTION PURPOSE: Close BF module.
 ******************************************************************************
 * DESCRIPTION: Close one instance of BF module.
 *
 *  void siu_close_bf (
 *    siuInst_t *inst)          - siu instance
 *
 *****************************************************************************/
void siu_close_bf(siuInst_t *inst)
{
  tint err_code;
  
  err_code = bfClose(inst->bfInst); /* the instance state is set to bf_CLOSED */

  if (err_code != bf_NOERR) {
    printf("Error when calling bfClose() with error code %d!\n", err_code);
    exit(1);
  }
 
} /* siu_close_bf */

/******************************************************************************
 * FUNCTION PURPOSE: Delete BF module.
 ******************************************************************************
 * DESCRIPTION: Delete one instance of BF module.
 *
 *  void siu_delete_bf (
 *    siuInst_t *inst)          - siu instance
 *
 *****************************************************************************/
void siu_delete_bf(siuInst_t *inst)
{
  tint err_code,i;
  ecomemBuffer_t bufs[SIU_NUM_bf_BUFFS];

  err_code = bfDelete (&inst->bfInst, SIU_NUM_bf_BUFFS, bufs);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfDelete() with error code %d!\n", err_code);
    exit(1);
  }
  
  for(i=0;i<SIU_NUM_bf_BUFFS;i++)
  {
     free(bufs[i].base);
  }
   
} /* siu_delete_bf */

/******************************************************************************
 * FUNCTION PURPOSE: Init heap memory.
 ******************************************************************************
 * DESCRIPTION: 
 *
 *  void bf_mem_heap_init (
 *    bfMemHeap_t *ptr,
 *    tword *heapBase,
 *    tulong size)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/

void bf_mem_heap_init (bfMemHeap_t *ptr, tword *heapBase, tulong size)
{
  ptr->base = heapBase;
  ptr->size = size;
  ptr->indx = 0;
  /* Note : memory is not initialized deliberately to expose 
            uninitialized memory read problems */
}

void bf_mem_init_heap (void)
{
  /* Internal heap */
  bf_mem_heap_init (&bfIntHeap, bfIntMem, bf_INT_HEAP_SIZE);
  /* External heap */
  bf_mem_heap_init (&bfExtHeap, bfExtMem, bf_EXT_HEAP_SIZE);
}

/******************************************************************************
 * FUNCTION PURPOSE: align memory
 ******************************************************************************
 * DESCRIPTION: 
 *
 *  void *bf_mem_align (
 *    void *addr,
 *    tuint lin_align)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/

void *bf_mem_align (void *addr, tuint lin_align) 
{
  tulong align_addr;

  align_addr = ((tulong) addr + lin_align - 1UL) & (0xFFFFFFFFUL - lin_align + 1UL);
  return ((void *) align_addr);  
}

/******************************************************************************
 * FUNCTION PURPOSE: allocate heap memory
 ******************************************************************************
 * DESCRIPTION: 
 *
 *  tword *bf_mem_heap_alloc (
 *    bfMemHeap_t *ptr,
 *    tint size)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/

tword *bf_mem_heap_alloc (bfMemHeap_t *ptr, tint size)
{
  tword *alloc = NULL;
   
  if ((ptr->indx + size) <= ptr->size) {
    alloc = &ptr->base[ptr->indx];
    ptr->indx += size;
  }
  return (alloc);
}

void *bf_mem_alloc_align (tint size, tint mclass, tint linAlign)
{
  void *allocPtr;
  bfMemHeap_t *ptr;

  if (mclass == ecomem_CLASS_INTERNAL) {
    ptr = &bfIntHeap;
  }
  else if (mclass == ecomem_CLASS_EXTERNAL) {
    ptr = &bfExtHeap;
  }
  else {
    printf("Unrecognized memory class, exiting\n");
    exit (0);
  }

  if ((allocPtr = (void *) bf_mem_heap_alloc (ptr, size + linAlign - 1)) == NULL) {
    printf("Not enough heap, exiting\n");
    exit (0);
  }

  return (bf_mem_align (allocPtr, linAlign));
}


/******************************************************************************
 * FUNCTION PURPOSE: Allocate buffers for bf instance
 *
 *****************************************************************************/
tint siu_alloc_bf_buffs(const ecomemBuffer_t *bufs_req, ecomemBuffer_t *bufs_aloc, 
                         tint num_bufs)
{
  tint err_code;
  tint i;
  tuint  linAlign, log2align;
  
  err_code = bf_NOERR;
  for (i = 0; i < num_bufs; i++) {
    log2align = bufs_req[i].log2align;
    linAlign = (tuint) 1 << log2align;
    
    bufs_aloc[i].mclass    = bufs_req[i].mclass;   /* internal memory for all  */
    bufs_aloc[i].log2align = bufs_req[i].log2align;/* meet alignment requirement */
    bufs_aloc[i].volat     = FALSE;                /* initialized to permanent */
    bufs_aloc[i].size      = bufs_req[i].size;     /* size of the buffer */
    bufs_aloc[i].base      = bf_mem_alloc_align (bufs_aloc[i].size, bufs_aloc[i].mclass, linAlign);
  }

  bufs_aloc[3].volat     = TRUE;                /* initialized to permanent */
  bufs_aloc[4].volat     = TRUE;                /* initialized to permanent */

  return(err_code);
} /* siu_alloc_bf_buffs */

/* nothing past this point */
