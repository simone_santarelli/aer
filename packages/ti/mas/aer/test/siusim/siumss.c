/******************************************************************************
 * FILE PURPOSE: Multi-Source Selection (MSS) Module API Simulation
 ******************************************************************************
 * FILE NAME:   siumms.c
 *
 * DESCRIPTION: Contains routines that use MSS API.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>
#include <ti/mas/aer/mss.h>

#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuport.h>

#define AER_SIM_MSS_INST_SIZE 50
#define AER_SIM_SVD_INST_SIZE 10
#define SIU_NUM_MSS_BUFFS 1        /* static buffer allocation */


typedef struct {
  LFract buf[AER_SIM_MSS_INST_SIZE];
} mssinstbuf_t;

typedef struct {
  LFract buf[AER_SIM_SVD_INST_SIZE];
} svdinstbuf_t;

typedef struct {
  mssinstbuf_t mss_inst;
  svdinstbuf_t svd_inst[AER_SIM_NUM_HF_MIC];  
} mssbuf_t;

#ifdef gnu_targets_arm_GCArmv7A
mssbuf_t mss_buf __attribute__ ((aligned (8)));
#else
#ifdef ti_targets_C55_large
mssbuf_t mss_buf;
#else
#pragma DATA_ALIGN(mss_buf, 8);
mssbuf_t mss_buf;
#endif
#endif

/******************************************************************************
 * FUNCTION PURPOSE: Create MSS module.
 ******************************************************************************
 * DESCRIPTION: Obtains info MSS AER buffers and creates one instance of MSS.
 *
 *****************************************************************************/
void siu_new_mss(siuInst_t *inst, tint num_mic, tint samp_rates)
{
  const ecomemBuffer_t *bufs_req_by_mss;
  ecomemBuffer_t        bufs_aloc_by_siu[SIU_NUM_MSS_BUFFS];
  mssSizeConfig_t   mss_size_cfg;
  mssNewConfig_t    mss_new_cfg;
  tint err_code, num_bufs_req_by_mss;
  
  /* Set up configurations for mssGetSizes() */
  mss_size_cfg.max_sampling_rate  = mss_SAMP_RATE_16K;
  mss_size_cfg.max_num_mic_fixed  = AER_SIM_NUM_HF_MIC;
  mss_size_cfg.max_num_mic_remote = 0;
  mss_size_cfg.max_num_mic_clean  = 0;
  mss_size_cfg.max_num_beam       = 0;

  /* Call API function mssGetSizes() to get memory requirements:
     - num_bufs_req_by_mss: number of buffers required by MSS 
     - bufs_req_by_mss:   pointer to buffer descriptors defined inside MSS */
  err_code = mssGetSizes(&num_bufs_req_by_mss, &bufs_req_by_mss, &mss_size_cfg);

  printf("MSS required buffers:\n");
  siu_print_buff_usage(bufs_req_by_mss, num_bufs_req_by_mss);
  
  /* Check if there is enough memory allocated for MSS */
  if(   num_bufs_req_by_mss > SIU_NUM_MSS_BUFFS 
     || bufs_req_by_mss[0].size > sizeof(mssbuf_t)) {
    printf("Memory allocated by SIU is not enough for MSS!\n");
    exit(0);
  }

  /* Provide memory to MSS through mssNew() */
  bufs_aloc_by_siu[0].mclass    = ecomem_CLASS_INTERNAL;
  bufs_aloc_by_siu[0].log2align = bufs_req_by_mss[0].log2align;
  bufs_aloc_by_siu[0].volat     = FALSE;
  bufs_aloc_by_siu[0].size      = sizeof(mssbuf_t);
  bufs_aloc_by_siu[0].base      = &mss_buf;

  printf("Buffers allocated by SIU for MSS:\n");
  siu_print_buff_usage(bufs_aloc_by_siu, num_bufs_req_by_mss); 
  
  /* Set up configurations for mssNew() */
  mss_new_cfg.handle  = (void *) siuMakeID(SIU_MID_MSS, 0);
  mss_new_cfg.sizeCfg = mss_size_cfg;

  /* Call API function mssNew() to create an MSS instance:
     - pass buffer descriptors that are set by SIU
     - pass number of buffer descriptors   */
  err_code = mssNew (&inst->mssInst, num_bufs_req_by_mss, bufs_aloc_by_siu, &mss_new_cfg);
  if (err_code != mss_NOERR) {
    printf("Error when calling mssNew() with error code %d!\n", err_code);
    exit(1);
  }
  
} /* siu_new_mss */

/******************************************************************************
 * FUNCTION PURPOSE: Open and configure MSS module.
 *
 *****************************************************************************/
void siu_open_mss(siuInst_t *inst, tint num_mic, tint samp_rates)
{
  mssControl_t  mss_ctl_cfg;
  mssConfig_t   mss_open_cfg;
  tint err_code;
  
  /* Set up configuration for mssOpen() */  
  mss_open_cfg.sampling_rate  = aersim_chkbit(samp_rates, aer_SRATE_TXOUT_16K)
                                ? mss_SAMP_RATE_16K : mss_SAMP_RATE_8K;
  mss_open_cfg.num_mic_fixed  = num_mic;
  mss_open_cfg.num_mic_remote = 0;
  mss_open_cfg.num_mic_clean  = 0;
  mss_open_cfg.num_mic_array  = 0;
  mss_open_cfg.num_beam       = 0;  

  err_code = mssOpen (inst->mssInst, &mss_open_cfg);
  if (err_code != mss_NOERR) {
    printf("Error when calling mssOpen() with error code %d!\n", err_code);
    exit(1);
  }
  
  mss_ctl_cfg.valid_bitfield =  mss_CTL_SWITCH_THRESH 
                              | mss_CTL_SWITCH_DURATION
                              | mss_CTL_SWITCH_HNAGOVER;
  
  mss_ctl_cfg.switch_thresh   = 250;  
  mss_ctl_cfg.switch_duration = 300;  
  mss_ctl_cfg.switch_hangover = 1000; 
  err_code = mssControl (inst->mssInst, &mss_ctl_cfg);
  
  if (err_code != mss_NOERR) {
    printf("Error when calling mssControl() to configure MSS with error code %d!\n", 
           err_code);
    exit(1);
  }
} /* siu_open_mss */

/* nothing past this point */
