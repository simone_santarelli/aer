/******************************************************************************
 * FILE PURPOSE: Software Integration Unit Transmit Task.
 ******************************************************************************
 * FILE NAME:   siutx.c
 *
 * DESCRIPTION: Contains routines that implement SIU Transmit Task.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/

/* System definitions and utilities */
#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>

/* AER, AGC, MSS and SVD header files */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/agc.h>
#include <ti/mas/aer/bf.h>
#include <ti/mas/aer/mss.h>
#include <ti/mas/vpe/svd.h>
#ifdef TEST_VEU
#include <ti/mas/veu/veu.h>
#endif

#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/siusim/siuloc.h>

extern siuSetup_t siuSetup;

linSample siu_mss_buff[AER_SIM_NUM_HF_MIC][SIU_AER_MAX_FRAME_LENGTH];

/******************************************************************************
 * FUNCTION PURPOSE: Send path processing main routine.
 *   Description: Call AER send path functions according to configuration:
 *                Microphone array:  BF, AGC, AER TX
 *                Multi remote mics: AGC, AER Tx, MSS
 ******************************************************************************/
void siuSendIn(siuInst_t *inst, linSample *rx_sync, 
               linSample txin[][AER_SIM_MAX_FRAME_LENGTH], linSample *txout,
               tuint *trace, tint num_mic, tint mic_type, tint frm_len)
{
  void *bf_micin[AER_SIM_NUM_BF_MIC];
  void *fixed_mics[AER_SIM_NUM_HF_MIC];
  int i;
  tint mic_selected;

  if(siuSetup.aer_mips_profile) {
    /* flush cache for profiling */  
    aersim_cacheFlush();

    /* read CPU cycle count before AER processing */
    aersim_profile_get_cycle_before_aer(&siuSetup);
  }
  
  /* Run beamforming and then AER if multi-mic is a mic array */
  if(mic_type == AER_SIM_MIC_TYPE_ARRAY) {
    for (i=0; i<num_mic; i++) {
      bf_micin[i] = (void *)&txin[i][0];
    }
    bfProcess(inst->bfInst, bf_micin, txout);
    
    aerSendIn(inst->aerHFInst[0], txout, txout, NULL, rx_sync, NULL, trace);
  } /* mic array */
  else { 
    /* Run AER and then MSS for multiple remote mics */
    mic_selected = inst->phone_sys.active_mic;
   
    /* First process the instance of selected/active mic */
    agcSendIn(inst->agcHFInst[mic_selected], &txin[mic_selected][0]);
    aerSendIn(inst->aerHFInst[mic_selected], &txin[mic_selected][0], 
              &siu_mss_buff[mic_selected][0], NULL, rx_sync, NULL, trace);
    
    /* Then process all other instances */
    for (i=0; i<num_mic; i++) {
      if(i != mic_selected ) {
        agcSendIn(inst->agcHFInst[i], &txin[i][0]);
        aerSendIn(inst->aerHFInst[i], &txin[i][0], &siu_mss_buff[i][0], NULL, 
                  rx_sync, NULL, trace);                 
      }
    }
  
    /* check if there are pending gain requests from AGC */
    for (i=0; i<num_mic; i++) {
      if(inst->pending_gain_request[i]) {
        siuGainControl(inst->agcHFhandles[i], &inst->gain_ctl);
        inst->pending_gain_request[i] = FALSE;
      }   
     
      /* prepare for MSS processing - only one soure for the case of mic-array */
      fixed_mics[i] = (void *)&siu_mss_buff[i][0];
    }
  
    /* multi-source selection module to select the optimum source (mic) */
    mssProcess(inst->mssInst, txout, rx_sync, fixed_mics, NULL, NULL, NULL, NULL);

    /* If necessary, activate newly selected mic instance and deactivate old instance */
    siu_mmic_control(inst);
    
    if(num_mic > 1) {
      /* rotate inactive mic to low processing load mode */
      siu_mmic_proc_load_control(inst, num_mic);
    }
    
  } /* multiple remote mics */
  
  /* read CPU cycle count after AER processing and update average and max */ 
  if(siuSetup.aer_mips_profile) {
    aersim_profile_get_cycle_after_aer_send(&siuSetup);
  }
  
} /* siuSendIn */

/* Nothing past this point */

