/******************************************************************************
 * FILE PURPOSE: Software Integration Unit.
 ******************************************************************************
 * FILE NAME:   siu.c
 *
 * DESCRIPTION: Contains routines necessary for integrating DSP software.
 *
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/ 

/*#include <math.h> */         /* used for cosine initialization only */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/fract/fract.h>                      /* fractional macros    */
#include <ti/mas/util/debug.h>                       /* debuging utility     */

#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuport.h>

/* Nice way for doing endless loop */
#define loop      for(;;)

siuContext_t  siuContext;                 /* SIU Global Context */

tuint siu_dbg_id;     /* debug ID used in debug and exception functions */
char siu_dbg_message[SIU_DBGMSG_SIZE];    /* debug message */

#define NUM_AER_DBG_FILE 3
FILE * siu_aer_data_file[NUM_AER_DBG_FILE];


/******************************************************************************
 * FUNCTION PURPOSE: Debug & exception functions.
 ******************************************************************************
 * DESCRIPTION: Debug & exception functions.
 *
 *****************************************************************************/

static inline void siuDebugInternal (tuint id, char *str)
{
  int i;
  char *src, *dst;

  /* Copy id and message in a safe place */
  siu_dbg_id = id;
  src = str; dst = (char*)&siu_dbg_message[0];
  for (i = 0; i < SIU_DBGMSG_SIZE; i++)
    *dst++ = *src++;
} /* siuDebugInternal */

void siuDebug (tuint id, char *s)
{
  siuDebugInternal (id, s);
} /* siuDebug */

void siuException (tuint id, char *s)
{
  siuDebugInternal (id, s);

  loop;     /* loop forever */
} /* siuException */

/******************************************************************************
 * FUNCTION PURPOSE: SIU Initialization Function.
 ******************************************************************************
 * DESCRIPTION: Initializes SIU Global Context and all SIU channel instances.
 *
 *****************************************************************************/
tint siuInit (void *inst, siuConfig_t *cfg) 
{
  siuInst_t * siu_inst;
  int j;

  siu_inst = (siuInst_t *)inst;
  
  /* Initialize global context */
  siuContext.ID           = cfg->ID;        /* has SIU MID and 0 for chnum */
  siuContext.exception    = cfg->exception;
  siuContext.debug        = cfg->debug;
  siuContext.cheap        = cfg->core_heap;
  siuContext.cheap_used   = 0;
  siuContext.vheap        = cfg->voice_heap;
  siuContext.vheap_used   = 0;

  /* Initialize SIU instance */
  siu_inst->ID              = siuMakeID (SIU_MID_SIU, 1);
  siu_inst->exception       = siuContext.exception;
  siu_inst->debug           = siuContext.debug;
  siu_inst->cheap_used      = 0;
  siu_inst->vheap_used      = 0;
  siu_inst->companding_law  = cfg->companding_law;  /* default companding */
  siu_inst->pcm_bits        = cfg->pcm_bits;
  siu_inst->aer_recv_in_buf = NULL;
  siu_inst->piuInst         = NULL;
  siu_inst->aerHESInst      = NULL;
  siu_inst->agcHESInst      = NULL;
  siu_inst->drcInst         = NULL;
  siu_inst->veuInst         = NULL;
  siu_inst->mssInst         = NULL;
  siu_inst->bfInst          = NULL;
  
  for(j=0; j<AER_SIM_NUM_HF_MIC; j++) {
    siu_inst->aerHFInst[j]  = NULL;
    siu_inst->agcHFInst[j]  = NULL;
    siu_inst->pending_gain_request[j] = FALSE;
  }
        
  /* Tx Task Part */
  siu_inst->TxInst.enabled               = FALSE;
  siu_inst->TxInst.send_out_frame_length = 0;
  siu_inst->TxInst.send_in_frame_length  = 0;
  siu_inst->TxInst.send_frame_ptr        = NULL;
  siu_inst->TxInst.recv_frame_ptr        = NULL;
  siu_inst->TxInst.TxSendOut.fcn         = NULL;
  siu_inst->TxInst.TxSendOut.targetInst  = NULL;
 
  return(SIU_NOERR);
} /* siuInit */

/******************************************************************************
 * FUNCTION PURPOSE: SIU Rx Task configuration.
 ******************************************************************************
 * DESCRIPTION: Configures and enables SIU Rx task.
 *
 *****************************************************************************/

void siuRxOpen (siuInst_t *inst, siuRxConfig_t *cfg)
{
  inst->RxInst.rin_frm_len  = cfg->rin_frm_len;
  inst->RxInst.rout_frm_len  = cfg->rout_frm_len;
} /* siuRxOpen */

/******************************************************************************
 * FUNCTION PURPOSE: SIU Tx Task configuration.
 ******************************************************************************
 * DESCRIPTION: Configures and enables SIU Tx task.
 *
 *****************************************************************************/

void siuTxOpen (siuInst_t *inst, siuTxConfig_t *cfg)
{
  int i;
  Fract mic_scl[3] = {32767, 29204, 26028};
  
  inst->TxInst.send_out_frame_length = cfg->send_out_frame_length;
  inst->TxInst.send_in_frame_length  = cfg->send_in_frame_length;
  inst->TxInst.send_frame_ptr        = NULL;
  inst->TxInst.recv_frame_ptr        = NULL;
  inst->TxInst.TxSendOut             = cfg->TxSendOut;
  inst->TxInst.enabled               = TRUE;
  memset(inst->TxInst.send_frm_del, 0, sizeof(linSample)*cfg->send_in_frame_length);

  /* phone system information */
  inst->phone_sys.active_mic = 0;  /* microhpone 0 */
  inst->phone_sys.mic_active_time = 100;  /* 100 frames, or 1sec */

  for(i=0; i<inst->phone_sys.num_HF_mic; i++) {
    /* 0, 1, or 2 msec delay in units of samples */
    inst->phone_sys.mic_delay[i] = i * cfg->send_in_frame_length/10; /* i msec */

    /* initial mic processing order mic 0 first, mic 1 second, mic 3 third */
    inst->phone_sys.mic_proc_order[i] = i;

    /* mic scaling: 
       mic 0: referemce mic, 0dB scaling,
       mic 1: -1dB from mic 0,
       mic 2: -2dB from mic 0. */
    inst->phone_sys.mic_scale[i] = mic_scl[i]; 
  }
  inst->phone_sys.num_HF_mic = cfg->num_HF_mic;
  inst->phone_sys.phone_mode = aer_PHM_HF;
  inst->phone_sys.round_robin_cntr = 0;  
  
} /* siuTxOpen */

/******************************************************************************
 * FUNCTION PURPOSE: AER data tracing.
 ******************************************************************************
 * DESCRIPTION: 
 *
 *   tuint msg             - tells what kind of information AER is reporting:
 *                           0 - warning message
 *                           1 - debug data
 *   tuint code            - tells what kind of debug data AER is reporting
 *                           and for each value, AER data should be saved to a
 *                           separate file. There are totally 3 kinds of data.
 *   tuint size            - size of AER data in number of tint elements.
 *   tuint * buf           - points to the memory where AER data is stored. 
 *   
 *****************************************************************************/
void siuDbgAer(void *handle, tuint msg, tuint code, tuint size, tuint *buf)
{
  tuint id, mic, mod;
  int i;
    
  id = (tuint)handle;
  mic = siuExtrMicID(id);
  mod = siuExtrModID(id);

  switch (msg) {
    case dbg_TRACE_INFO:
    /* For some reason, binary file I/O is not working properly. This needs 
       some investigation!
       aerSimFwrite(buf, size, siu_aer_data_file[code]); 
    */
      for(i=0; i<size; i++) {
        fprintf(siu_aer_data_file[code], "%d\n", (tint)buf[i]);
      }  
    break;

    case dbg_WARNING:
      printf("mod %d has warning with mic %d. Exception msg is %d, code is %d\n", 
              mod, mic, msg, code);
    break;

    case dbg_EXCEPTION_FATAL:
      printf("mod %d has exception with mic %d. Exception msg is %d, code is %d\n", 
              mod, mic, msg, code);
    break;

    default:
    break;
  }   
}


/******************************************************************************
 * FUNCTION PURPOSE: AGC data tracing.
 ******************************************************************************/
void siuDbgAgc(void *handle, tuint msg, tuint code, tuint unused1, tuint *unused2)
{
  tuint id, mic, mod;
  
  id = (tuint)handle;
  
  mic = siuExtrMicID(id);
  mod = siuExtrModID(id);
  printf("mod %d has exception with mic %d. Exception msg is %d, code is %d\n", 
         mod, mic, msg, code);

  loop;
}


/******************************************************************************
 * FUNCTION PURPOSE: DRC data tracing.
 ******************************************************************************/
void siuDbgDrc(void *handle, tuint msg, tuint code, tuint unused1, tuint *unused2)
{
  tuint id, mic, mod;
  
  id = (tuint)handle;
  
  mic = siuExtrMicID(id);
  mod = siuExtrModID(id);
  printf("mod %d has exception with mic %d. Exception msg is %d, code is %d\n", 
         mod, mic, msg, code);

  loop;
}

/* nothing past this point */
