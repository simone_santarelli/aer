/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of DRC
 ****************************************************************************** 
 * FILE NAME:   siudrcsim.c
 * 
 * DESCRIPTION: Contains constants and arrays for simulation testing of DRC
 * 
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/
/* System definitions and utilities */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ti/mas/types/types.h>                      
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/drc.h>

#include <ti/mas/aer/test/siusim/siu.h>
#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuport.h>
#include <ti/mas/aer/test/siusim/siudrc.h>
#include <ti/mas/aer/test/aersim/aerbuffs.h>
#include <ti/mas/aer/test/aersim/aersim.h>
#include <ti/mas/aer/test/aersim/aersim_fileIO.h>

#define SIU_NUM_DRC_BUFFS 5

/* DRC filter coefficients:

   Filter Coefficients are in Q15 format. Since symmetric FIR is used,
   only half plus 1 of the filter coefficients are specified in tables. 
   The lowband FIR is a low pass filter with cutoff frequency at 700Hz.
   The midband FIR is a band pass filter with frequency range 700Hz - 1700Hz.
   The highband FIR is a high pass filter with frequency above 1700Hz.
   
   The coefs in tables are from h[0] to h[half+1] for low, mid and highband.
*/ 
const Fract drc_lowband_coef_8k[drc_FIR_HALF_COEF_8KHZ] = {
  -103, -115, -61, 301, 1182, 2575, 4172, 5457, 5950 
};

const Fract drc_midband_coef_8k[drc_FIR_HALF_COEF_8KHZ] = {
  3, 129, 430, -10, -2324, -4473, -2118, 4356, 8013
};

const Fract drc_highband_coef_8k[drc_FIR_HALF_COEF_8KHZ] = {
  99, -13, -370, -291, 1142, 1898, -2054, -9813, 18804
};

const Fract drc_lowband_coef_16k[drc_FIR_HALF_COEF_16KHZ] = {
  -51, -53, -58, -55, -30, 33, 151,  335, 590, 912, 1286,        
  1686, 2083,  2440, 2724, 2907, 2970        
};

const Fract drc_midband_coef_16k[drc_FIR_HALF_COEF_16KHZ] = {
  2, 19, 64, 141, 215, 203, -5, -476, -1160, -1851, -2233,      
  -2010, -1057, 476, 2174, 3500, 4000      
};

const Fract drc_highband_coef_16k[drc_FIR_HALF_COEF_16KHZ] = {
  50, 34, -7,  -86, -185, -236, -145, 142, 570, 939, 948,
  324, -1025, -2916, -4899, -6407, 25797
};

enum {                      
  setup_test =1,        /* test initialization  */
  full_multi_band_test, /* system tests which include:
                           Full Band tests with two inputs(speech & sine ramp up
                           Multi Band tests with two inputs(speech & sine ramp up */

  func_blocks_test,     /* Functional block verification tests which include:
                           1 - verify compressor signal power (in dBm0) estimation
                           2 - verify compressor gain (in dB) estimation          
                           3 - verify exponential average of compressor gain (dB) 
                           4 - verify limiter signal power (in dBm0) estimation   
                           5 - verify limiter gain (in dB) estimation             
                           6 - verify exponential average of limiter gain (linear)
                           7 - verify voice activity indication                   
                           8 - verify output of low band filter                   
                           9 - verify output of mid band filter                   
                           10- verify output of high band filter            */ 
  api_test,             /* DRC API of control and getParameter verification  */
  close_test            /* Save the results        */
};

enum {                      
  fullband_test_w_input1  = 1,
  multiband_test_w_input1 = 2,
  fullband_test_w_input2  = 3,
  multiband_test_w_input2 = 4
};

FILE *drc_test_cfg;
FILE *drc_in = NULL;
FILE *drc_out= NULL;
FILE *dbg_out= NULL;
FILE *drc_ref= NULL;

char *file_name;
char *in_file_16k = NULL;
char *in_file_8k = NULL;
char *out_file_16k = NULL;
char *out_file_8k = NULL;
char *ref_file_16k = NULL;
char *ref_file_8k = NULL;
char input_name_16k[40];
char input_name_8k[40];
char in_file_name_16k[40];
char in_file_name_8k[40];
char out_file_name_16k[40];
char out_file_name_8k[40];
char ref_file_name_16k[40];
char ref_file_name_8k[40];

tbool openfile = TRUE;
Fract drc_buffer[160];
Fract drc_buffer2[160];
tint  drc_samp_rate;
tuint full_multi_band_test_result = 0;
tuint func_blocks_test_result = 0;
tuint drc_api_err = 0;
tuint drc_api_err2 = 0;
tint  drc_test = 0;
tint  test_case = 0;
extern Fract drc_debug_buffer[];

#ifdef drc_DEBUG
extern tint drc_dbg_flag;   
#endif  

/******************************************************************************
 * FUNCTION PURPOSE: Create DRC module.
 ******************************************************************************
 * DESCRIPTION: Obtains info on DRC buffers, and creates one instance of DRC.
 *
 *              NOTE: Should save number of buffers and buffer descriptors
 *                    in order to make delete possible.
 *
 *
 *  void siu_new_drc (
 *    tint chnum,              - channel number (1 to SIU_MAX_CHANNELS)
 *    tint max_sampling_rate)  - maximum sampling rate
 *
 *****************************************************************************/
void siu_new_drc(siuInst_t * inst)
{
  tint err_code, drcNbufs;
  const ecomemBuffer_t *bufs_dscrp_by_drc;
  ecomemBuffer_t        bufs_dscrp_by_siu[SIU_NUM_DRC_BUFFS];
  drcCreateConfig_t    drcCreateCfg;
  drcSizeConfig_t      drcCfgSize;
  drcNewConfig_t       drcCfgNew;
  
  memset(&drcCreateCfg, 0, sizeof(drcCreateConfig_t));
  drcCreateCfg.debugInfo = siuDbgDrc;
  drcCreate(&drcCreateCfg);
  
  /* DRC get sizes. Memory requirement are calculated based on max. sampling rate
     and max. limiter delay length.  */
  drcCfgSize.samp_rate = drc_SAMP_RATE_16K; 
  drcCfgSize.max_delay_len = drcCfgSize.samp_rate*8*2; /* 2ms delay */

  err_code = drcGetSizes (&drcNbufs, &bufs_dscrp_by_drc, &drcCfgSize);
  if (err_code != drc_NOERR) {
    printf("Error when calling drcGetSizes() for with error code %d!\n", err_code);
    exit(1);
  }

  printf("DRC required buffers:\n");
  siu_print_buff_usage(bufs_dscrp_by_drc, drcNbufs);

  /* check if enough memory is allocated for SIU buffer descriptors */
  if (drcNbufs > SIU_NUM_DRC_BUFFS) {
    printf("No enough memory allocated by SIU for DRC buffer descriptors!\n");
	exit(1);
  }

  bufs_dscrp_by_siu[0].base = &drc_buff0[0]; 
  bufs_dscrp_by_siu[0].size = DRC_SIM_BUF0_SIZE; 
  bufs_dscrp_by_siu[0].log2align = DRC_SIM_BUF0_ALGN_LOG2; 
  bufs_dscrp_by_siu[0].volat = FALSE;

  bufs_dscrp_by_siu[1].base = &aer_buff1[0];   /* overlap with AER volatile buffers */
  bufs_dscrp_by_siu[1].size = AER_SIM_BUF1_SIZE; 
  bufs_dscrp_by_siu[1].log2align = AER_SIM_BUF1_ALGN_LOG2; 
  bufs_dscrp_by_siu[1].volat = TRUE;

  bufs_dscrp_by_siu[2].base = &drc_buff2[0]; 
  bufs_dscrp_by_siu[2].size = DRC_SIM_BUF2_SIZE; 
  bufs_dscrp_by_siu[2].log2align = DRC_SIM_BUF2_ALGN_LOG2; 
  bufs_dscrp_by_siu[2].volat = FALSE;

  bufs_dscrp_by_siu[3].base = &drc_buff3[0]; 
  bufs_dscrp_by_siu[3].size = DRC_SIM_BUF3_SIZE; 
  bufs_dscrp_by_siu[3].log2align = DRC_SIM_BUF3_ALGN_LOG2; 
  bufs_dscrp_by_siu[3].volat = FALSE;

  bufs_dscrp_by_siu[4].base = &drc_buff4[0]; 
  bufs_dscrp_by_siu[4].size = DRC_SIM_BUF4_SIZE; 
  bufs_dscrp_by_siu[4].log2align = DRC_SIM_BUF4_ALGN_LOG2; 
  bufs_dscrp_by_siu[4].volat = FALSE;

  printf("Buffers allocated by SIU for DRC:\n");
  siu_print_buff_usage(bufs_dscrp_by_siu, drcNbufs);
  
  /* drcNew */
  drcCfgNew.handle  = (void *) siuMakeID(SIU_MID_DRC, 0);
  drcCfgNew.sizeCfg = drcCfgSize;
  err_code = drcNew (&inst->drcInst, drcNbufs, bufs_dscrp_by_siu, &drcCfgNew);
  if (err_code != drc_NOERR) {
    printf("Error when calling drcNew() for with error code %d!\n", err_code);
    exit(1);
  }     
} /* siu_new_drc */

/******************************************************************************
 * FUNCTION PURPOSE: Open DRC module.
 ******************************************************************************
 * DESCRIPTION: Opens one instance of DRC module.
 *
 *  void siu_open_drc (
 *    tint chnum,          - channel number (1 to SIU_MAX_CHANNELS)
 *    tint sampling_rate)  - 1 or 2 indicate 8 or 16 kHz sampling rate                                 
 *
 *****************************************************************************/
void siu_open_drc(siuInst_t *inst, tuint cfg_bits, tint sampling_rates)
{
  tint err_code;
  drcOpenConfig_t drcCfg;

  /* Open the DRC instance with certain sampling rate and operation mode. */
  drcCfg.samp_rate  = (sampling_rates & drc_SAMP_RATE_8K)+drc_SAMP_RATE_8K;
  drcCfg.cfgbits    = cfg_bits;
  err_code = drcOpen (inst->drcInst, &drcCfg);
  if (err_code != drc_NOERR) {
    printf("Error when calling drcOpen() with error code %d!\n", err_code);
    exit(1);
  }

  drc_samp_rate     =  drcCfg.samp_rate; /* for unit test */
} /* siu_open_drc */

/******************************************************************************
 * FUNCTION PURPOSE: Close DRC module.
 ******************************************************************************
 * DESCRIPTION: Close one instance of DRC module.
 *
 *  void siu_close_drc (
 *    tint chnum)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/
void siu_close_drc(siuInst_t *inst)
{
  tint stat;
  
  stat = drcClose(inst->drcInst);  /* the instance state is set to drc_CLOSED */
  siu_exc_assert (stat == drc_NOERR, inst);
  
}

/******************************************************************************
 * FUNCTION PURPOSE: Close DRC module.
 ******************************************************************************
 * DESCRIPTION: Close one instance of DRC module.
 *
 *  void siu_close_drc (
 *    tint chnum)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/
void siu_delete_drc(siuInst_t *inst)
{
  tint stat;
  ecomemBuffer_t bufs_dscrp_by_drc;

  stat = drcDelete (&inst->drcInst, 5, &bufs_dscrp_by_drc);
  siu_exc_assert (stat == drc_NOERR, inst);
  
}

/******************************************************************************
 * FUNCTION PURPOSE: Set the operating mode of an DRC
 ******************************************************************************
 * DESCRIPTION: Controls the operating mode and internal parameters of the DRC.
 *
 *  void drcControl (
 *    tint chnum,          - channel number (1 to SIU_MAX_CHANNELS)
 *    tint sampling_rate)  - 1 or 2 indicate 8 or 16 kHz sampling rate                                 
 *
 *****************************************************************************/
void siu_control_drc (siuInst_t *inst, tint sampling_rates, tint drc_on_flag)
{
  tint stat, samp_rate;
  drcControl_t ctl;

  samp_rate  = (sampling_rates & drc_SAMP_RATE_8K)+drc_SAMP_RATE_8K;

  /* filter coefficients are configured here */
  ctl.ctl_code = drc_CTL_SET_FIR_LBAND;
  if (samp_rate == drc_SAMP_RATE_8K ) {
    memcpy(&ctl.u.coef[0], drc_lowband_coef_8k, drc_FIR_HALF_COEF_8KHZ*sizeof(Fract));
  }
  else {
    memcpy(&ctl.u.coef[0], drc_lowband_coef_16k, drc_FIR_HALF_COEF_16KHZ*sizeof(Fract));
  }
  stat = drcControl(inst->drcInst, &ctl);
  if (stat != drc_NOERR) {
    printf("Error when configuring DRC low band filter with error code %d!\n", stat);
    exit(1);
  }

  ctl.ctl_code = drc_CTL_SET_FIR_MBAND;
  if (samp_rate == drc_SAMP_RATE_8K ) {
    memcpy(&ctl.u.coef[0], drc_midband_coef_8k, drc_FIR_HALF_COEF_8KHZ*sizeof(Fract));
  }
  else {
    memcpy(&ctl.u.coef[0], drc_midband_coef_16k, drc_FIR_HALF_COEF_16KHZ*sizeof(Fract));
  }
  stat = drcControl(inst->drcInst, &ctl);
  if (stat != drc_NOERR) {
    printf("Error when configuring DRC middle band filter with error code %d!\n", stat);
    exit(1);
  }

  ctl.ctl_code = drc_CTL_SET_FIR_HBAND;
  if (samp_rate == drc_SAMP_RATE_8K ) {
    memcpy(&ctl.u.coef[0], drc_highband_coef_8k, drc_FIR_HALF_COEF_8KHZ*sizeof(Fract));
  }
  else {
    memcpy(&ctl.u.coef[0], drc_highband_coef_16k, drc_FIR_HALF_COEF_16KHZ*sizeof(Fract));
  }
  stat = drcControl(inst->drcInst, &ctl);
  if (stat != drc_NOERR) {
    printf("Error when configuring DRC high band filter with error code %d!\n", stat);
    exit(1);
  }

  /* hard coded parameters for full band configuration */
  ctl.ctl_code = drc_CTL_SET_FBAND;
  ctl.u.band.valid_bitfield = 1<<drc_BAND_CFG_VALID_CURVE_PARAM_BIT;
  ctl.u.band.curve.ratios = 0x2020;
  ctl.u.band.curve.exp_knee = -50;
  ctl.u.band.curve.max_amp  = 6;
  ctl.u.band.curve.com_knee = -15;
  ctl.u.band.curve.energy_lim = -3;
  stat = drcControl(inst->drcInst, &ctl);
  if (stat != drc_NOERR) {
    printf("Error when configuring DRC full band with error code %d!\n", stat);
    exit(1);
  }

  /* hard coded parameters for limiter configuration */
  ctl.ctl_code = drc_CTL_SET_LIM;
  ctl.u.limiter.valid_bitfield = 1<<drc_LIM_CFG_VALID_THRESH_BIT;
  ctl.u.limiter.thresh_dBm = -3;
  stat = drcControl(inst->drcInst, &ctl);
  if (stat != drc_NOERR) {
    printf("Error when configuring DRC limiter with error code %d!\n", stat);
    exit(1);
  }
  
  /* DRC is enabled based on the configuration defined in siuSetup */
  if (drc_on_flag) {
    ctl.ctl_code = drc_CTL_DRC_ON;
  }
  else {
    ctl.ctl_code = drc_CTL_DRC_OFF;
  }
  stat = drcControl(inst->drcInst, &ctl);
  if (stat != drc_NOERR) {
    printf("Error when enabling DRC with error code %d!\n", stat);
    exit(1);
  }
} /* siu_control_drc */   
        
/******************************************************************************
 * FUNCTION PURPOSE: DRC process and tests
 ******************************************************************************
 * DESCRIPTION: Performs DRC process and tests.
 *
 *  void drcProcess (
 *    void *drcInst,     - a pointer to instance data
 *    void *sin,         - a pointer to input receive frame samples
 *    void *sout)        - a pointer to output receive frame samples
 *
 *****************************************************************************/
void siu_drc_process (void *inst, void *sin, void *sout)
{
  tint            i, frame_len;
  drcOpenConfig_t drcCfg;
  drcControl_t    ctl;
  drcReportParam_t param;
#ifdef drc_DEBUG
  char *c_ptr;
#endif

  switch (drc_test) {

    case setup_test:
      file_name = strtok(".\\drctest\\drcsimcfg.txt", "\n");
      drc_test_cfg = fopen(file_name, "r");

      file_name = strtok(".\\drctest\\drc_test_result.txt", "\n");
      dbg_out = fopen(file_name, "w");
      fprintf(dbg_out, "Sampling rate (Hz)= %d\n\n", (drc_samp_rate*8000));

      drc_test  = full_multi_band_test;
      test_case = fullband_test_w_input1;

      break;

    case full_multi_band_test: /*  Full Band and Multi Band Test */

  	  if(openfile) {

        ctl.ctl_code = drc_CTL_DRC_OFF;
        drcControl(inst, &ctl);

        switch (test_case) {
          case fullband_test_w_input1:
          case fullband_test_w_input2:
            ctl.u.cfgbits    = 0x31;

            fscanf(drc_test_cfg, "%s\n", input_name_16k);
            fscanf(drc_test_cfg, "%s\n", input_name_8k);

            strcpy (in_file_name_16k, ".\\drctest\\in\\");
            strcpy (in_file_name_8k, ".\\drctest\\in\\");

            strcat(in_file_name_16k, input_name_16k);
            strcat(in_file_name_8k, input_name_8k);
            strcat(in_file_name_16k, ".pcm");
            strcat(in_file_name_8k, ".pcm");

            strcpy (out_file_name_16k, ".\\drctest\\out\\");
            strcpy (out_file_name_8k, ".\\drctest\\out\\");

            strcat(out_file_name_16k, input_name_16k);
            strcat(out_file_name_8k, input_name_8k);
            strcat(out_file_name_16k, "_out_fb.pcm");
            strcat(out_file_name_8k, "_out_fb.pcm");

            strcpy (ref_file_name_16k, ".\\drctest\\ref\\");
            strcpy (ref_file_name_8k, ".\\drctest\\ref\\");

            strcat(ref_file_name_16k, input_name_16k);
            strcat(ref_file_name_8k, input_name_8k);
            strcat(ref_file_name_16k, "_ref_fb.pcm");
            strcat(ref_file_name_8k, "_ref_fb.pcm");

          break;

          case multiband_test_w_input1:
          case multiband_test_w_input2:
            ctl.u.cfgbits    = 0x3E;

            strcpy (in_file_name_16k, ".\\drctest\\in\\");
            strcpy (in_file_name_8k, ".\\drctest\\in\\");

            strcat(in_file_name_16k, input_name_16k);
            strcat(in_file_name_8k, input_name_8k);
            strcat(in_file_name_16k, ".pcm");
            strcat(in_file_name_8k, ".pcm");

            strcpy (out_file_name_16k, ".\\drctest\\out\\");
            strcpy (out_file_name_8k, ".\\drctest\\out\\");

            strcat(out_file_name_16k, input_name_16k);
            strcat(out_file_name_8k, input_name_8k);
            strcat(out_file_name_16k, "_out_mb.pcm");
            strcat(out_file_name_8k, "_out_mb.pcm");

            strcpy (ref_file_name_16k, ".\\drctest\\ref\\");
            strcpy (ref_file_name_8k, ".\\drctest\\ref\\");

            strcat(ref_file_name_16k, input_name_16k);
            strcat(ref_file_name_8k, input_name_8k);
            strcat(ref_file_name_16k, "_ref_mb.pcm");
            strcat(ref_file_name_8k, "_ref_mb.pcm");

          break;
        }

        ctl.ctl_code = drc_CTL_SET_BITS;
        drcControl(inst, &ctl);

        ctl.ctl_code = drc_CTL_DRC_ON;
        drcControl(inst, &ctl);

        openfile = FALSE;

        in_file_16k = strtok(in_file_name_16k, "\n");
        in_file_8k  = strtok(in_file_name_8k, "\n");

        out_file_16k = strtok(out_file_name_16k, "\n");
        out_file_8k = strtok(out_file_name_8k, "\n");

        ref_file_16k = strtok(ref_file_name_16k, "\n");
        ref_file_8k = strtok(ref_file_name_8k, "\n");

        printf("Test %d\n", test_case);
        if (drc_samp_rate == drc_SAMP_RATE_16K) {
          drc_in  = fopen(in_file_16k, "rb");
          printf("input: %s\n", in_file_16k);
          drc_out = fopen(out_file_16k, "wb");
          printf("output: %s\n", out_file_16k);

          fprintf(dbg_out, "Input signal file: %s\n", in_file_16k);
          fprintf(dbg_out, "Output signal file: %s\n", out_file_16k);

          frame_len = 160;
        }
        else {
          drc_in  = fopen(in_file_8k, "rb");
          printf("input: %s\n", in_file_8k);
          drc_out = fopen(out_file_8k, "wb");
          printf("output: %s\n", out_file_8k);

          fprintf(dbg_out, "Input signal file: %s\n", in_file_8k);
          fprintf(dbg_out, "Output signal file: %s\n", out_file_8k);

          frame_len = 80;
        }
      }

      if (aerSimFread(&drc_buffer[0], frame_len, drc_in)) {

        drcProcess (inst, drc_buffer, sout);

        aerSimFwrite(sout, frame_len, drc_out);

      }
		  else {
        fclose(drc_in);
        fclose(drc_out);

        if (drc_samp_rate == drc_SAMP_RATE_16K) {
          drc_ref = fopen(ref_file_16k, "rb");
          drc_out = fopen(out_file_16k, "rb");
          fprintf(dbg_out, "Reference signal file: %s\n", ref_file_16k);

          frame_len = 160;
        }
        else {
          drc_ref = fopen(ref_file_8k, "rb");
          drc_out = fopen(out_file_8k, "rb");
          fprintf(dbg_out, "Reference signal file: %s\n", ref_file_8k);

          frame_len = 80;
        }

        while (aerSimFread(&drc_buffer[0], frame_len, drc_ref)) {

          aerSimFread(&drc_buffer2[0], frame_len, drc_out);

          for (i=0; i<frame_len; i++) {
            if (drc_buffer[i] !=  drc_buffer2[i]) {
              full_multi_band_test_result |= (1<<test_case);
            }
          }
        }
        fclose(drc_ref);
        fclose(drc_out);
              
        if (full_multi_band_test_result & (1<<test_case)) {
          printf("Test Failed! \n");
          fprintf(dbg_out, "Test Failed! \n\n");
        }
        else {
          printf("Test Passed! \n");
          fprintf(dbg_out, "Test Passed! \n\n");
        }
        openfile = TRUE;
        test_case++;

        if ( test_case > 4) {  

          fclose(drc_test_cfg);

#ifdef    drc_DEBUG
          drc_test = func_blocks_test;
          test_case    = 0;

          drc_dbg_flag = 1;
#else
          drc_test = api_test;
#endif
        }
      }

      break;

    case func_blocks_test: 
      /* Functional block tests which includes:
          1 - verify compressor signal power (in dBm0) estimation
          2 - verify compressor gain (in dB) estimation          
          3 - verify exponential average of compressor gain (dB) 
          4 - verify limiter signal power (in dBm0) estimation   
          5 - verify limiter gain (in dB) estimation             
          6 - verify exponential average of limiter gain (linear)
          7 - verify voice activity indication                   
          8 - verify output of low band filter                   
          9 - verify output of mid band filter                   
          10- verify output of high band filter                  
      */

#ifdef drc_DEBUG
  	  if(openfile && (drc_dbg_flag==1)) {
        fprintf(dbg_out, "Functional block tests which includes: \n");
        fprintf(dbg_out, "1 - verify compressor signal power (in dBm0) estimation. \n");
        fprintf(dbg_out, "2 - verify compressor gain (in dB) estimation. \n");          
        fprintf(dbg_out, "3 - verify exponential average of compressor gain (dB). \n"); 
        fprintf(dbg_out, "4 - verify limiter signal power (in dBm0) estimation. \n");   
        fprintf(dbg_out, "5 - verify limiter gain (in dB) estimation. \n");            
        fprintf(dbg_out, "6 - verify exponential average of limiter gain (linear). \n");
        fprintf(dbg_out, "7 - verify voice activity indication. \n");                   
        fprintf(dbg_out, "8 - verify output of low band filter. \n");                   
        fprintf(dbg_out, "9 - verify output of mid band filter. \n");                   
        fprintf(dbg_out, "10- verify output of high band filter. \n\n");                  
      }

  	  if(openfile) {

        ctl.ctl_code = drc_CTL_DRC_OFF;
        drcControl(inst, &ctl);

        if (drc_dbg_flag > 7)
          ctl.u.cfgbits    = 0x3E;
        else
          ctl.u.cfgbits    = 0x31;

        ctl.ctl_code = drc_CTL_SET_BITS;
        drcControl(inst, &ctl);

        ctl.ctl_code = drc_CTL_DRC_ON;
        drcControl(inst, &ctl);
      
        openfile = FALSE;

        if (drc_samp_rate == drc_SAMP_RATE_16K) {

          if (drc_dbg_flag == 7)
            drc_in  = fopen(".\\drctest\\in\\speech_16k.pcm", "rb");
          else
            drc_in  = fopen(".\\drctest\\in\\sinramp_16k.pcm", "rb");

          strcpy(out_file_name_16k, ".\\drctest\\out\\drc_debug_16k_n.pcm");
          c_ptr = strchr(out_file_name_16k, 'n');
          *c_ptr = test_case+'0';

          out_file_16k = strtok(out_file_name_16k, "\n");
          drc_out = fopen(out_file_16k, "wb");

          frame_len = 160;
        }
        else {
          if (drc_dbg_flag == 7)
            drc_in  = fopen(".\\drctest\\in\\speech_8k.pcm", "rb");
          else
            drc_in  = fopen(".\\drctest\\in\\sinramp_8k.pcm", "rb");

          strcpy(out_file_name_8k, ".\\drctest\\out\\drc_debug_8k_n.pcm");
          c_ptr = strchr(out_file_name_8k, 'n');
          *c_ptr = test_case+'0';

          out_file_8k = strtok(out_file_name_8k, "\n");
          drc_out = fopen(out_file_8k, "wb");

          frame_len = 80;
        }

      }  
      if (aerSimFread(&drc_buffer[0], frame_len, drc_in)) {

        drcProcess (inst, drc_buffer, sout);

        aerSimFwrite(drc_debug_buffer, frame_len, drc_out);

      }
		  else {
        fclose(drc_in);
        fclose(drc_out);

        if (drc_samp_rate == drc_SAMP_RATE_16K) {
          drc_out = fopen(out_file_16k, "rb");

          strcpy(ref_file_name_16k, ".\\drctest\\ref\\drc_ref_16k_n.pcm");
          c_ptr = strchr(ref_file_name_16k, 'n');
          *c_ptr = test_case+'0';

          ref_file_16k = strtok(ref_file_name_16k, "\n");
          drc_ref  = fopen(ref_file_16k, "rb");

          frame_len = 160;
        }
        else {
          drc_out = fopen(out_file_8k, "rb");

          strcpy(ref_file_name_8k, ".\\drctest\\ref\\drc_ref_8k_n.pcm");
          c_ptr = strchr(ref_file_name_8k, 'n');
          *c_ptr = test_case+'0';

          ref_file_8k = strtok(ref_file_name_8k, "\n");
          drc_ref  = fopen(ref_file_8k, "rb");

          frame_len = 80;
        }
        while (aerSimFread(&drc_buffer[0], frame_len, drc_ref)) {
          aerSimFread(&drc_buffer2[0], frame_len, drc_out);
          for (i=0; i<frame_len; i++) {
            if (drc_buffer[i] !=  drc_buffer2[i]) {
              func_blocks_test_result |= (1<<test_case);
            }
          }
        }
        fclose(drc_ref);
        fclose(drc_out);

        openfile = TRUE;

        if ( func_blocks_test_result & (1<<test_case)) {
          printf("DRC Functional Block Test %d failed. \n", drc_dbg_flag);
          fprintf(dbg_out, "DRC Functional Block Test %d failed. \n", drc_dbg_flag);
        }
        else {
          printf("DRC Functional Block Test %d passed. \n", drc_dbg_flag);
          fprintf(dbg_out, "DRC Functional Block Test %d passed. \n", drc_dbg_flag);
        }
        test_case++;
        drc_dbg_flag++;

        if ( drc_dbg_flag > 10) {
          drc_test = api_test;
        }
		  }
#     endif
      break;

    case api_test:
        fprintf(dbg_out, "\n");
        fprintf(dbg_out, "DRC API Test : \n\n");

        /* verify 
           drc_CTL_DRC_ON   - ENABLE DRC                
           drc_CTL_DRC_OFF  - DISABLE DRC 
           drc_GET_CFGBITS  - GET: Operation mode configuration       */

        ctl.ctl_code = drc_CTL_DRC_OFF;
        drcControl(inst, &ctl);

        fprintf(dbg_out, "Test Control Code - drc_CTL_DRC_OFF \n");
        drcGetParameter (inst, drc_GET_CFGBITS, &param);

        if (param.u.bits & (0x1<<6)) {
          drc_api_err |= 0x1;
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }
        ctl.ctl_code = drc_CTL_DRC_ON;
        drcControl(inst, &ctl);

        fprintf(dbg_out, "Test Control Code - drc_CTL_DRC_ON \n");

        drcGetParameter (inst, drc_GET_CFGBITS, &param);
        if (!(param.u.bits & (0x1<<6))) {
          drc_api_err |= 0x2;
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }


        /* verify  drc_CTL_SET_BITS - ENABLE: DRC configuration bitfield change*/
        ctl.ctl_code = drc_CTL_SET_BITS;
        fprintf(dbg_out, "Test Control Code - drc_CTL_SET_BITS \n");

        ctl.u.cfgbits= 0;
        drcControl(inst, &ctl);
        drcGetParameter (inst, drc_GET_CFGBITS, &param);
        if ((param.u.bits & 0x003F)!=0) {
          drc_api_err |= 0x4;
        }

        ctl.u.cfgbits= 0x3F;
        drcControl(inst, &ctl);
        drcGetParameter (inst, drc_GET_CFGBITS, &param);
        if ((param.u.bits & 0x003F)!=0x003F) {
          drc_api_err |= 0x8;
        }
        if (drc_api_err & 0x000C) {
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }
        
        /* verify  drc_CTL_SET_FBAND- ENABLE: full band param configuration    
                   drc_GET_FBAND_PARAM - GET: full band configuration parameters */

        fprintf(dbg_out, "Test Control Code - drc_CTL_SET_FBAND \n");

        ctl.ctl_code = drc_CTL_SET_FBAND;
        ctl.u.band.valid_bitfield = 0x7;   
        ctl.u.band.tc_log2        = 0x789A; 
        ctl.u.band.curve.exp_knee = -80;     /* should be set to -78 by DRC boundary check */
        ctl.u.band.curve.ratios   = 0x0102;  /* should be set to 0x1010 by DRC boundary check */         
        ctl.u.band.curve.max_amp  = 3;       /* should be set to 0 since exp_ratio =1 */        
        ctl.u.band.curve.com_knee = -15;         
        ctl.u.band.curve.energy_lim = 4;     /* should be set to 3 by DRC boundary check */       

        drcControl(inst, &ctl);

        drcGetParameter (inst, drc_GET_FBAND_PARAM, &param);
        if (param.u.bandParam.tc_log2 != 0x789A)
          drc_api_err |= 0x10;
        if (param.u.bandParam.curve.exp_knee != (-78))
          drc_api_err |= 0x20;
        if (param.u.bandParam.curve.ratios != (0x1010))
          drc_api_err |= 0x40;
        if (param.u.bandParam.curve.max_amp != (0))
          drc_api_err |= 0x80;
        if (param.u.bandParam.curve.com_knee != (-15))
          drc_api_err |= 0x100;
        if (param.u.bandParam.curve.energy_lim != (3))
          drc_api_err |= 0x200;

        if (drc_api_err & 0x03F0) {
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }

        /* verify  drc_CTL_SET_LBAND- ENABLE: low band param configuration     
                   drc_GET_LBAND_PARAM - GET: low band configuration parameters  */

        fprintf(dbg_out, "Test Control Code - drc_CTL_SET_LBAND \n");

        ctl.ctl_code = drc_CTL_SET_LBAND;
        ctl.u.band.valid_bitfield = 0x7;   
        ctl.u.band.tc_log2        = 0x89AB; 
        ctl.u.band.curve.exp_knee = -55;     
        ctl.u.band.curve.ratios   = 0x2020;  /* should be set to 0x1020 since energy_lim < com_knee+max_amp */         
        ctl.u.band.curve.max_amp  = 20;      /* should be set to 18 by DRC boundary check */        
        ctl.u.band.curve.com_knee = -15;     /* should be set to 3 since energy_lim < com_knee+max_amp */        
        ctl.u.band.curve.energy_lim = -20;          

        drcControl(inst, &ctl);

        drcGetParameter (inst, drc_GET_LBAND_PARAM, &param);
        if (param.u.bandParam.tc_log2 != 0x89AB)
          drc_api_err |= 0x400;
        if (param.u.bandParam.curve.ratios != (0x1020))
          drc_api_err |= 0x800;
        if (param.u.bandParam.curve.max_amp != (18))
          drc_api_err |= 0x1000;
        if (param.u.bandParam.curve.com_knee != (3))
          drc_api_err |= 0x2000;

        if (drc_api_err & 0x3C00) {
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }

        /* verify  drc_CTL_SET_MBAND - ENABLE: mid band param configuration     
                   drc_GET_MBAND_PARAM - GET: mid band configuration parameters  */

        fprintf(dbg_out, "Test Control Code - drc_CTL_SET_MBAND \n");

        ctl.ctl_code = drc_CTL_SET_MBAND;
        ctl.u.band.valid_bitfield = 0x5;   
        ctl.u.band.tc_log2        = 0x1234;  /* only power TC will be changed */
        ctl.u.band.curve.exp_knee = -15;     /* should be set to -20 since energy_lim < exp_knee */
        ctl.u.band.curve.ratios   = 0x2020;  /* should be set to 0x1010 since energy_lim < exp_knee */         
        ctl.u.band.curve.max_amp  = 20;      /* should be set to 0 since energy_lim < exp_knee */        
        ctl.u.band.curve.com_knee = -15;     /* should be set to 3 since energy_lim < com_knee+max_amp */        
        ctl.u.band.curve.energy_lim = -20;          

        drcControl(inst, &ctl);

        drcGetParameter (inst, drc_GET_MBAND_PARAM, &param);
        if (param.u.bandParam.tc_log2 != 0x9934)
          drc_api_err |= 0x4000;
        if (param.u.bandParam.curve.ratios != (0x1010))
          drc_api_err |= 0x8000;
        if (param.u.bandParam.curve.max_amp != (0))
          drc_api_err2 |= 0x1;
        if (param.u.bandParam.curve.exp_knee != (-20))
          drc_api_err2 |= 0x2;
        if (param.u.bandParam.curve.com_knee != (3))
          drc_api_err2 |= 0x4;
        if (param.u.bandParam.curve.energy_lim != (-20))
          drc_api_err2 |= 0x8;

        if ((drc_api_err & 0xC000) || (drc_api_err2 & 0x000F) ){
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }

        /* verify  drc_CTL_SET_HBAND- ENABLE: high band param configuration    
                   drc_GET_HBAND_PARAM - GET: high band configuration parameters */

        fprintf(dbg_out, "Test control code - drc_CTL_SET_HBAND \n");

        ctl.ctl_code = drc_CTL_SET_HBAND;
        ctl.u.band.valid_bitfield = 0x6;   
        ctl.u.band.tc_log2        = 0x1234;  /* only gain TC will be changed */
        ctl.u.band.curve.exp_knee = -15;     /* should be set to -20 exp_knee > com_knee */
        ctl.u.band.curve.ratios   = 0x2020;  /* should be set to 0x2010 since exp_knee > com_knee */         
        ctl.u.band.curve.max_amp  = 3;       /* should be set to 0 since exp_knee > com_knee */        
        ctl.u.band.curve.com_knee = -20;            
        ctl.u.band.curve.energy_lim = -10;        

        drcControl(inst, &ctl);

        drcGetParameter (inst, drc_GET_HBAND_PARAM, &param);
        if (param.u.bandParam.tc_log2 != 0x1299)
          drc_api_err2 |= 0x10;
        if (param.u.bandParam.curve.ratios != (0x2010))
          drc_api_err2 |= 0x20;
        if (param.u.bandParam.curve.max_amp != (0))
          drc_api_err2 |= 0x40;
        if (param.u.bandParam.curve.exp_knee != (-20))
          drc_api_err2 |= 0x80;

        if (drc_api_err2 & 0x00F0) {
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }

        /* verify  drc_CTL_SET_LIM -  ENABLE: limiter param configuration      
                   drc_GET_LIM_PARAM - GET: limiter parameters in an instance  */

        fprintf(dbg_out, "Test Control Ccode - drc_CTL_SET_LIM \n");

        ctl.ctl_code = drc_CTL_SET_LIM;
        ctl.u.limiter.valid_bitfield = 0xF;   
        ctl.u.limiter.tc_log2        = 0x1234; /* should be 0x1230 */
        ctl.u.limiter.thresh_dBm     = 4;      /* should be 3*16   */
        ctl.u.limiter.delay_len      = 5; 

        drcControl(inst, &ctl);
        drcGetParameter (inst, drc_GET_LIM_PARAM, &param);
        if (param.u.limParam.tc_log2 != 0x1230)
          drc_api_err2 |= 0x100;
        if (param.u.limParam.thresh_dBm != (3*16))
          drc_api_err2 |= 0x200;
        if (param.u.limParam.delay_len != 0x2005)
          drc_api_err2 |= 0x400;

        if (drc_api_err2 & 0x0700) {
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }

        /* verify  drc_SET_LBAND_FIR_COEF - GET: low band filter coefficients  */
        /* verify  drc_SET_MBAND_FIR_COEF - GET: mid band filter coefficients  */
        /* verify  drc_SET_HBAND_FIR_COEF - GET: high band filter coefficients */

        fprintf(dbg_out, "Test Control Code - drc_SET_LBAND_FIR_COEF, ");
        fprintf(dbg_out, "drc_SET_MBAND_FIR_COEF and ");
        fprintf(dbg_out, "drc_SET_HBAND_FIR_COEF \n");

        for (i=0;i<drc_FIR_HALF_COEF_16KHZ;i++) {
          ctl.u.coef[i] = i;
        }

        ctl.ctl_code = drc_CTL_SET_FIR_LBAND;
        drcControl(inst, &ctl);

        ctl.ctl_code = drc_CTL_SET_FIR_MBAND;
        drcControl(inst, &ctl);

        ctl.ctl_code = drc_CTL_SET_FIR_HBAND;
        drcControl(inst, &ctl);

        drcGetParameter (inst, drc_GET_LBAND_FIR_COEF, &param);
        for (i=0;i<drc_FIR_HALF_COEF_16KHZ/2;i++) {
          if (param.u.coef[i] != i)
            drc_api_err2 |= 0x800; 
        }

        drcGetParameter (inst, drc_GET_MBAND_FIR_COEF, &param);
        for (i=0;i<drc_FIR_HALF_COEF_16KHZ/2;i++) {
          if (param.u.coef[i] != i)
            drc_api_err2 |= 0x1000; 
        }

        drcGetParameter (inst, drc_GET_HBAND_FIR_COEF, &param);
        for (i=0;i<drc_FIR_HALF_COEF_16KHZ/2;i++) {
          if (param.u.coef[i] != i)
            drc_api_err2 |= 0x2000; 
        }

        if (drc_api_err2 & 0x3800) {
          fprintf(dbg_out, "Test Failed. \n\n");
        }
        else {
          fprintf(dbg_out, "Test Passed. \n\n");
        }

        printf("DRC API test is completed. Error Bitmap: 0x%x and 0x%x\n", drc_api_err, drc_api_err2);

        fprintf(dbg_out, "End of The Test! \n");
        fclose(dbg_out);

        drcClose(inst);  /* the instance state is set to drc_CLOSED */

        drcCfg.samp_rate  = drc_samp_rate;
        drcCfg.cfgbits    = 0x31;
        drcOpen (inst, &drcCfg);

        ctl.ctl_code = drc_CTL_DRC_ON;
        drcControl(inst, &ctl);

		    drc_test = 0;      

      break;

    default:
      drcProcess (inst, sin, sout);
      break;
  }
  
} /* siu_drc_process */

/* nothing past this point */
