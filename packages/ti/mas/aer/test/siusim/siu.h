#ifndef _SIU_H
#define _SIU_H
/******************************************************************************
 * FILE PURPOSE: Software Integration Unit Definitions.
 *
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/

#include <ti/mas/types/types.h>                      /* DSP types            */

/* SIU Error codes */
enum {
  SIU_NOERR = 0,
  SIU_ERROR = -1
};

/******************************************************************************
 * DATA DEFINITION: SIU Configuration Structure.
 ******************************************************************************
 * DESCRIPTION: Used when configuring SIU module.
 *
 *****************************************************************************/

typedef struct {
  tuint ID;                                 /* Module ID with chnum = 0 */
  void  (*exception)(tuint id, char *str);  /* master exception function */
  void  (*debug)(tuint id, char *str);      /* master debug function */
  tint  companding_law;   /* Provides information on which type of companding
                           *  is used before linear samples are obtained. Valid
                           *  values are const_COMP_ALAW_8, const_COMP_MULAW_8,
                           *  const_COMP_LINEAR (no companding). Invalid value
                           *  indicates 8-bit transparent mode. */
  tint  pcm_bits;         /* # bits in a PCM sample (8 for u-law, A-law, and
                           *  transparent mode, [1,16] range for linear). */
  void  *core_heap;       /* Core heap handle */
  void  *voice_heap;      /* Voice heap handle */
} siuConfig_t;

/******************************************************************************
 * DATA DEFINITION: SIU Tx Task Send Out Object.
 ******************************************************************************
 * DESCRIPTION: Transmit task send-out object description. Current
 *              implementation takes send-in and receive-in frame like in
 *              the case of ECU.
 *
 *****************************************************************************/

typedef struct {
  tint (*fcn) (void *targetInst, void *send_in_frame, void *send_out_frame, 
               void *send_out_8kHz, void *recv_frame, void *sout_pnlp_8kHz,
               void *trace_buf);
  void *targetInst;   /* instance of the target object which owns the fcn */
} siuTxSendOut_t;

/******************************************************************************
 * DATA DEFINITION: SIU Tx Task Configuration Structure.
 ******************************************************************************
 * DESCRIPTION: Used when configuring SIU Tx Task with siuTxOpen().
 *
 *****************************************************************************/

typedef struct {
  siuTxSendOut_t  TxSendOut;            /* Tx Send-out object description    */
  tint            send_out_frame_length;/* send out frame length in samples  */
  tint            send_in_frame_length; /* send in frame length in samples   */
  tint            num_HF_mic;
} siuTxConfig_t;

/******************************************************************************
 * DATA DEFINITION: SIU Rx Task Configuration Structure.
 ******************************************************************************
 * DESCRIPTION: Used when configuring SIU Rx Task with siuRxOpen().
 *
 *****************************************************************************/

typedef struct {
  tint            rin_frm_len;    /* receive in frame length in samples   */
  tint            rout_frm_len;    /* receive out frame length in samples   */
} siuRxConfig_t;

/* External function prototypes */
/* siu.c */
tint siuInit (void *siu_inst, siuConfig_t *cfg); 



#endif /* _SIU_H */
/* nothing past this point */
