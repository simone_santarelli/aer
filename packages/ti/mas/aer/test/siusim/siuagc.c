/****************************************************************************** 
 * FILE PURPOSE: SIU functions to support simulation testing of AER
 ****************************************************************************** 
 * FILE NAME:   siuagc.c
 * 
 * DESCRIPTION: Contains constants and arrays for simulation testing of AER
 * 
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/agc.h>

#include <ti/mas/aer/test/siusim/siuloc.h>
#include <ti/mas/aer/test/siusim/siuagc.h>
#include <ti/mas/aer/test/siusim/siuport.h>        

#include <ti/mas/aer/test/aersim/aerbuffs.h>
#include <ti/mas/aer/test/aersim/aersimcfg.h>
#include <ti/mas/aer/test/aersim/aersim.h>

#define SIU_NUM_AGC_BUFFS 1

extern siuInst_t siu_inst;
extern aepSimInst_t aepSimInst;


/******************************************************************************
 * FUNCTION PURPOSE: AER Information relayed to AGC through SIU
 ******************************************************************************
 * DESCRIPTION: Informs AGC about the AER parameters.
 *
 *
 *  void siuAgcAerEvent (
 *    tuint id,                   - ID of AER instance
 *    aerInfo_t *event)           - pointer to siuEvent structure which reports 
 *                                  SIU of gain change data.  
 *
 * NOTE: Here we are not using the AER ID to get the channel number for SIU
 *       Once multiple instances are implemented correctly, we can change the 
 *       logic here
 *
 *****************************************************************************/
void siuAgcAerEvent (void *handle, aerInfo_t *event)
{
  void *agcInst;
  agcControl_t   agcCtl;
  tulong aer_id;
  tuint mic_id;
  siuInst_t *inst;
  
  aer_id = (tulong)handle;     /* not use void * handle yet */
  mic_id = siuExtrMicID(aer_id);

  inst = &siu_inst;
    
  /* find the instance pointer according to mic id */
  if(mic_id == 0)   /* HES/HS instance */
    agcInst = inst->agcHESInst;
  else  /* HF instance */
    agcInst = inst->agcHFInst[mic_id-1];
    
  if (agcInst==NULL) {

    return;
  }
  
  agcCtl.valid_bitfield          = agc_CTL_VALID_AERDATA;
  agcCtl.aer_data.maxabs_pecho   = event->maxabs_pred_echo;      
  agcCtl.aer_data.nlp_on         = event->nlp_on;                
  agcCtl.aer_data.log2_attn      = event->max_canc_l2;           
  agcCtl.aer_data.SWR            = event->rx_to_tx_hangover;     
  agcCtl.aer_data.SWS            = event->tx_to_rx_hangover;     
  agcCtl.aer_data.TS             = event->active_path;           
  agcCtl.aer_data.XS             = event->switch2tx_period;      
  agcCtl.aer_data.XR             = event->switch2rx_period;      
  agcCtl.aer_data.pecho_pwr      = event->pred_echo_pwr;         
  agcCtl.aer_data.Si_with_echo   = event->tx_mic_pwr;            
  agcCtl.aer_data.Si_minus_echo  = event->tx_mic_pwr_minus_echo; 
  
  agcControl (agcInst, &agcCtl); 

}  /* siuAgcAerEvent */

/******************************************************************************
 * FUNCTION PURPOSE: SIU AGC Event function.
 ******************************************************************************
 * DESCRIPTION: Processes event out (call out) request from AGC. 
 *              Possible events are:
 *                 - agc_EVC_SAT_FALSE:    mic saturation ends
 *                 - agc_EVC_SAT_DETECTED: mic saturation starts
 *                 - agc_EVC_MGAIN_CHANGE: mic gain needs to be adjusted
 *                 - agc_EVC_GET_PRENLP_GAIN: please give me AER pre-NLP gain
 *
 *  void siuAgcEvent (
 *    void *handle,               - channel ID and AER/AGC ID
 *    agcEvent_t *event)          - AGC event structure
 *
 *****************************************************************************/
void siuAgcEvent(void *handle, agcEvent_t *event)
{
  siuInst_t *inst;
  void *aerInst;
  aerControl_t aerCtl;
  tulong agc_id;
  tuint mic_id;
  tint tx_dgain, rx_dgain;
  tint event_code;
  
  agc_id = (tulong)handle;     /* not use void * handle yet */
  mic_id = siuExtrMicID(agc_id);

  inst = &siu_inst;

  /* find the AGC instance pointer according to mic id */
  if(mic_id == 0) {   /* HES/HS instance */
    aerInst = inst->aerHESInst;
  }
  else {  /* HF instance */
    aerInst = inst->aerHFInst[mic_id-1];
  }

  aerCtl.valid_bitfield_0 = 0x0;
  aerCtl.valid_bitfield_1 = 0x0;
  aerCtl.valid_bitfield_2 = 0x0;
  aerCtl.valid_bitfield_3 = 0x0;
  aerCtl.valid_bitfield_4 = 0x0;
  
  event_code = event->event_code;
  
  switch (event_code) {
  case agc_EVC_SAT_FALSE:
    /* Inform AER of the ending of mic saturation */     
    aerCtl.valid_bitfield_0 = aer_CTL_VALID0_MODES_CTL2;
    aerCtl.modes_2.mask = aer_CTL2_MICSAT;
    aerCtl.modes_2.value = 0;
    aerControl (aerInst, &aerCtl);
    break;

  case agc_EVC_SAT_DETECTED:
    /* Inform AER of the mic saturation */     
    aerCtl.valid_bitfield_0 = aer_CTL_VALID0_MODES_CTL2;
    aerCtl.modes_2.mask = aerCtl.modes_2.value = aer_CTL2_MICSAT;
    aerControl (aerInst, &aerCtl);
    break;

  case agc_EVC_MGAIN_CHANGE: 
    /* Indicate to change microphone gain as requested by AGC */
    inst->gain_ctl.ctl_code    = siu_AGC_EVENT_MIC_GAIN_CHANGE;
    inst->gain_ctl.u.mic_gain  = event->u.analog_gain;
    inst->pending_gain_request[mic_id-1] = TRUE; /* to call siuGainControl */
    break;

  case agc_EVC_GET_PRENLP_GAIN: 
    /* Get the existing pre-NLP gain from AER */
    aerGetGains (aerInst, &tx_dgain, &rx_dgain);
    event->u.pNLPgain = tx_dgain;
    break;

  default:
    break;
  }
  
} /* siuAgcEvent */
                                             

/******************************************************************************
 * FUNCTION PURPOSE: SIU gain control function.
 ******************************************************************************
 * DESCRIPTION: Process gain change request and inform AER and AGC of new gain.
 *
 *  void siuGainControl (
 *    void *handle,                   - channel ID and AER/AGC ID
 *    siuGainControl_t *siu_gain_ctl) - SIU gain control structure 
 *
 *****************************************************************************/
void siuGainControl(void *handle, siuGainControl_t *siu_gain_ctl)
{
  siuInst_t *inst;
  aerControl_t aerCtl;
  agcControl_t agcCtl;
  void *aerInst;
  void *agcInst;
  tulong agc_id;
  tuint mic_id;
  tint tx_dgain, rx_dgain;
  tint control_code;
  
  agc_id = (tulong)handle;     /* not use void * handle yet */
  mic_id = siuExtrMicID(agc_id);

  inst = &siu_inst;

  /* find the AER/AGC instance pointer according to mic id */
  if(mic_id == 0) {   /* HES/HS instance */
    agcInst = inst->agcHESInst;
    aerInst = inst->aerHESInst;
  }
  else {  /* HF instance */
    agcInst = inst->agcHFInst[mic_id-1];
    aerInst = inst->aerHFInst[mic_id-1];
  }
  
  aerCtl.valid_bitfield_0 = 0x0;
  aerCtl.valid_bitfield_1 = 0x0;
  aerCtl.valid_bitfield_2 = 0x0;
  aerCtl.valid_bitfield_3 = 0x0;
  aerCtl.valid_bitfield_4 = 0x0;
  
  control_code = siu_gain_ctl->ctl_code;
  
  switch (control_code) {

  case siu_SET_MIC_GAIN: 

    /* External request to change the programmable analog mic gain */
    aer_sim_aep_mic_gain_adj(&aepSimInst, siu_gain_ctl->u.mic_gain);

    /* Inform AER of new mic analog gain */
    aerCtl.valid_bitfield_0 = aer_CTL_VALID0_TX_ANALOG;
    aerCtl.gain_tx_analog   = siu_gain_ctl->u.mic_gain;
    aerControl (aerInst, &aerCtl);
        
    /* Inform AGC of new mic analog gain and inform AGC that this gain change 
       is initiated externally */
    agcCtl.valid_bitfield = agc_CTL_VALID_MICGAIN;     /* gain set by system */
    agcCtl.mic_gain       = siu_gain_ctl->u.mic_gain;
    agcControl (agcInst, &agcCtl);

    break;
    
  case siu_SET_SPK_GAIN: 

    /* Change the programmable analog speaker gain */
    aer_sim_aep_spk_gain_adj(&aepSimInst, siu_gain_ctl->u.spk_gain);

    /* Inform AGC of new speaker gain */
    agcCtl.valid_bitfield = agc_CTL_VALID_SPKGAIN;
    agcCtl.spk_gain       = siu_gain_ctl->u.spk_gain;
    agcControl (agcInst, &agcCtl);
    
    /* Inform AER of new speaker gain */
    aerCtl.valid_bitfield_0 = aer_CTL_VALID0_RX_ANALOG;
    aerCtl.gain_rx_analog   = siu_gain_ctl->u.spk_gain;
    aerControl (aerInst, &aerCtl);
    break;

  case siu_AGC_EVENT_MIC_GAIN_CHANGE: 

    /* Change the programmable analog mic gain as requested by AGC */
    aer_sim_aep_mic_gain_adj(&aepSimInst, siu_gain_ctl->u.mic_gain);

    /* Get AER pre-NLP (Tx digital) gain to be updated by AGC */        
    aerGetGains (aerInst, &tx_dgain, &rx_dgain);

    /* Inform AGC of new mic analog gain and inform AGC that this gain change 
       is initiated by adaptive AGC. Also ask for new AER pre-NLP gain which is
       returned in agcCtl.preNLP_gain.  */ 
    agcCtl.valid_bitfield = agc_CTL_VALID_MICGAIN_ADAPT; 
    agcCtl.mic_gain       = siu_gain_ctl->u.mic_gain; /* gain asked by adaptive AGC */
    agcCtl.preNLP_gain    = tx_dgain;
    agcControl (agcInst, &agcCtl);

    /* Inform AER of new mic gain and set new AER pre-NLP gain returned by AGC */
    aerCtl.valid_bitfield_0 = aer_CTL_VALID0_TX_ANALOG;
    aerCtl.gain_tx_analog   = siu_gain_ctl->u.mic_gain;
    if(agcCtl.preNLP_enable) {
      aerCtl.valid_bitfield_0 |= aer_CTL_VALID0_TX_DIGITAL;
      aerCtl.gain_tx_digital = agcCtl.preNLP_gain; 
    }
    aerControl(aerInst, &aerCtl);

    break;
    
  default:
    break;
  } 
} /* siuGainControl */



/******************************************************************************
 * FUNCTION PURPOSE: Create AGC module.
 ******************************************************************************
 * DESCRIPTION: Obtains info on AGC buffers, and creates one instance of AGC.
 *
 *              NOTE: Should save number of buffers and buffer descriptors
 *                    in order to make delete possible.
 *                    Allocates AGC instance buffer. 
 *
 *  void siu_new_agc (
 *    tint chnum,              - channel number (1 to SIU_MAX_CHANNELS)
 *    tint max_frame_length,   - maximum # samples in the frame (must be
 *                               greater than or equal to maximum sub-segment
 *                               size)
 *    tint max_sampling_rate)  - maximum sampling rate
 *
 *****************************************************************************/
void siu_new_agc (siuInst_t *inst, tint num_HF_mic) 
{
  const ecomemBuffer_t *bufs_dscrp_by_agc;
  ecomemBuffer_t        bufs_dscrp_by_siu[SIU_NUM_AGC_BUFFS];
  agcCreateConfig_t agcCreateCfg;
  agcSizeConfig_t   agcCfgSize;
  agcNewConfig_t    agcCfgNew;
  tint              agcNbufs, mic_num, err_code;

  /* AGC create */
  agcCreateCfg.config_bitfield  = 0x0;
  if(num_HF_mic > 1) {  
    aersim_setbit(agcCreateCfg.config_bitfield, agc_CRT_MULT_MIC_MODE);
  }
  agcCreateCfg.debugInfo = siuDbgDrc;
  agcCreateCfg.eventOut  = siuAgcEvent;
  agcCreateCfg.max_sampling_rate = agc_SRATE_16K; 
  
  err_code = agcCreate(&agcCreateCfg);
  if(err_code != agc_NOERR) {
    printf("Error when calling agcCreate() with error code %d!\n", err_code);
    exit(1);
  }
  
  /* AGC get sizes */
  err_code = agcGetSizes (&agcNbufs, &bufs_dscrp_by_agc, &agcCfgSize);
  if (err_code != agc_NOERR) {
    printf("Error when calling agcGetSizes() with error code %d!\n", err_code);
    exit(1);
  }

  /* check if enough memory is allocated for SIU buffer descriptors */
  if (agcNbufs > SIU_NUM_AGC_BUFFS) {
    printf("No enough memory allocated by SIU for AGC buffer descriptors!\n");
	exit(1);
  }

  printf("AGC required buffers:\n");
  siu_print_buff_usage(bufs_dscrp_by_agc, agcNbufs);
  
  /* create new AGC instances */  
  if (bufs_dscrp_by_agc[0].size <= AGC_SIM_BUF0_SIZE) {
    /* create one HS/HES instance */  
    bufs_dscrp_by_siu[0].base = &(agc_buff0_hs[0]); 
    bufs_dscrp_by_siu[0].size = AGC_SIM_BUF0_SIZE; 
    bufs_dscrp_by_siu[0].log2align = AGC_SIM_BUF0_ALGN_LOG2; 
    bufs_dscrp_by_siu[0].volat = FALSE; 

    printf("Buffers allocated by SIU for AGC:\n");
    siu_print_buff_usage(bufs_dscrp_by_siu, agcNbufs);
    
    /* agcNew */
    agcCfgNew.handle = (void *)siuMakeID((SIU_MID_AGC|AER_HES_ID), 0);
    agcCfgNew.sizeCfg = agcCfgSize;
    inst->agcHEShandle = agcCfgNew.handle;
    err_code = agcNew (&inst->agcHESInst, agcNbufs, bufs_dscrp_by_siu, &agcCfgNew);
    if (err_code != agc_NOERR) {
      printf("Error when calling agcNew() for HES with error code %d!\n", err_code);
      exit(1);
    }
     
    /* create multiple HF instances */  
    for(mic_num = 0; mic_num < num_HF_mic; mic_num++) {
      bufs_dscrp_by_siu[0].base = &(agc_buff0_hf[mic_num][0]); 
      bufs_dscrp_by_siu[0].size = AGC_SIM_BUF0_SIZE; 
      bufs_dscrp_by_siu[0].log2align = AGC_SIM_BUF0_ALGN_LOG2;
      bufs_dscrp_by_siu[0].volat = FALSE; 

      agcCfgNew.handle = (void *)siuMakeID((SIU_MID_AGC|(AER_HF0_ID+mic_num)), 0);
      agcCfgNew.sizeCfg = agcCfgSize;
      inst->agcHFhandles[mic_num] = agcCfgNew.handle;
      err_code = agcNew (&inst->agcHFInst[mic_num], agcNbufs, bufs_dscrp_by_siu, &agcCfgNew);
      if (err_code != agc_NOERR) {
        printf("Error when calling agcNew() for HF with error code %d!\n", err_code);
        exit(1);
      }
    }
  }
  else {
    printf("No enough memory allocated for AGC!\n");
	exit(1);
  }

} /* siu_new_agc */


/******************************************************************************
 * FUNCTION PURPOSE: Open AGC module.
 ******************************************************************************
 * DESCRIPTION: Opens one instance of AGC module.
 *
 *  void siu_open_agc (
 *    tint chnum,                    - channel number (1 to SIU_MAX_CHANNELS)
 *    tint frame_size,               - # samples in the frame
 *    tint sampling_rate,            - sampling rate
 *    tint gain_init)                - initial gain value in DB
 *****************************************************************************/
void siu_open_agc(siuInst_t *inst, tint sampling_rates, tint num_mic)
{
  agcConfig_t agcCfg;
  tint err_code;
  int mic_num;

  if(aersim_chkbit(sampling_rates, aer_SRATE_RXOUT_TXIN_16K)) { 
    agcCfg.sampling_rate = agc_SRATE_16K; 
  }
  else {
    agcCfg.sampling_rate = agc_SRATE_8K; 
  }
  agcCfg.reset_bitfield = 0xffff;
  agcCfg.valid_bitfield = 0x0001;

  agcOpen (inst->agcHESInst, &agcCfg);

  for(mic_num = 0; mic_num < num_mic; mic_num++) {
    err_code = agcOpen (inst->agcHFInst[mic_num], &agcCfg);
    if (err_code != agc_NOERR) {
      printf("Error when calling agcOpen() for HF with error code %d!\n", err_code);
      exit(1);
    }
  }  
} /* siu_open_agc */

/******************************************************************************
 * FUNCTION PURPOSE: Close AGC module.
 ******************************************************************************/
void siu_close_agc(siuInst_t *inst)
{
  int i;
  
  agcClose(inst->agcHESInst);
  
  for(i=0; i<inst->phone_sys.num_HF_mic; i++)
  {
    agcClose(inst->agcHFInst[i]);
  }
} /* siu_close_agc */
             
/* nothing past this point */
