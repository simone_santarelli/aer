/******************************************************************************
 * FILE PURPOSE: Multi-mic control routines
 ******************************************************************************
 * FILE NAME:   siummic.c
 *
 * DESCRIPTION: Contains routines that implement SIU multi-mic control. 
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 *****************************************************************************/

/* Ansi C header files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* System definitions and utilities */
#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>

/* AER, AGC, MSS and SVD header files */
#include <ti/mas/aer/aer.h>
#include <ti/mas/aer/mss.h>

#include <ti/mas/aer/test/siusim/siuloc.h>

#define AER_SIM_SIU_DEF_HF_MIC 0
#define AER_SIM_HIGH_LOAD_MIC_INIT 1

/******************************************************************************
 * FUNCTION PURPOSE: Initialize multi-mic control. 
 ******************************************************************************/
void siu_mmic_init(siuInst_t *inst)
{
  mssControl_t      mss_ctl_cfg;
  tint err_code;

  /* Set the 1st hands-free mic instance as the active instance for AER */
  err_code = aerActivate (inst->aerHFInst[AER_SIM_SIU_DEF_HF_MIC], NULL);
  if(err_code != aer_NOERR) {
    printf("Error when calling aerActivate() with error code %d!\n", err_code);
    exit(1);
  }
  
  /* Set 1st hands-free mic as the selected source for MSS */
  mss_ctl_cfg.valid_bitfield = mss_CTL_SELECT_SRC;
  mss_ctl_cfg.source.group   = mss_SRC_MIC_FIXED;
  mss_ctl_cfg.source.index   = AER_SIM_SIU_DEF_HF_MIC;
  err_code = mssControl (inst->mssInst, &mss_ctl_cfg);
  
  /* Keep record of the selected hands-free mic */
  inst->phone_sys.active_mic = AER_SIM_SIU_DEF_HF_MIC;  
  
  inst->phone_sys.inactive_highload_mic = AER_SIM_HIGH_LOAD_MIC_INIT;
  
} /* siu_mmic_init */

/******************************************************************************
 * FUNCTION PURPOSE: Multi-mic control - activates/deactivates mic instances.
 ******************************************************************************/
void siu_mmic_control(siuInst_t *inst)
{
  mssDebugStat_t mss_dbg;

  /* Get selected mic information from MSS */
  mssDebugStat(inst->mssInst, &mss_dbg);
  
  /* Check if a different mic has just been selected */
  if(mss_dbg.cur_src.index != inst->phone_sys.active_mic) {    
    /* Activate the AER instance for the newly selected mic */
    aerActivate(inst->aerHFInst[mss_dbg.cur_src.index], 
                inst->aerHFInst[inst->phone_sys.active_mic]);
    inst->phone_sys.active_mic = mss_dbg.cur_src.index;
  }    
} /* siu_mmic_control */

/******************************************************************************
 * FUNCTION PURPOSE: Multi-mic processing load control
 ******************************************************************************/
void siu_mmic_proc_load_control(siuInst_t *inst, tint num_mic)
{
  tint highload_mic_old, highload_mic_new;
  
  /* put the inactive AER instance with high processing load (normal operation) 
     to low processing load mode */
  highload_mic_old = inst->phone_sys.inactive_highload_mic;  
  
  /* put next inactive AER instance to high processing load mode */
  highload_mic_new = highload_mic_old + 1;
  if(highload_mic_new == num_mic) { 
    highload_mic_new = 0;  /* wrap around */
  }
  
  /* don't put active AER instance to low processing load mode */
  if(highload_mic_new == inst->phone_sys.active_mic) {
    highload_mic_new += 1;
    if(highload_mic_new == num_mic) {
      highload_mic_new = 0; /* wrap around */
	}
  }

  if(highload_mic_old != highload_mic_new) {
    siu_put_aer_low_load(inst->aerHFInst[highload_mic_old]);
    siu_put_aer_high_load(inst->aerHFInst[highload_mic_new]);
    inst->phone_sys.inactive_highload_mic = highload_mic_new;    
  }  
} /* siu_mmic_proc_load_control */

/******************************************************************************
 * FUNCTION PURPOSE: Put inactive AER instance to low processing mode
 ******************************************************************************/
void siu_put_aer_low_load(void *aer_inst)
{
  aerControl_t aer_ctl;
  
  /* low processing load: freeze tail update, skip coherence detection, freeze
                          noise estimate */
  aer_ctl.valid_bitfield_0 =   aer_CTL_VALID0_MODES_CTL1
                             | aer_CTL_VALID0_MODES_CTL2;  
  aer_ctl.modes_1.mask  = aer_CTL1_FREEZE_TAIL_UPDATE;
  aer_ctl.modes_1.value = aer_CTL1_FREEZE_TAIL_UPDATE;
  aer_ctl.modes_2.mask  = aer_CTL2_SKIP_COHERENCE | aer_CTL2_FREEZE_NOISE_EST;
  aer_ctl.modes_2.value = aer_CTL2_SKIP_COHERENCE | aer_CTL2_FREEZE_NOISE_EST;
  
  aer_ctl.valid_bitfield_1 = 0x0;  
  aer_ctl.valid_bitfield_2 = 0x0;  
  aer_ctl.valid_bitfield_3 = 0x0;  
  aer_ctl.valid_bitfield_4 = 0x0;
  
  aerControl(aer_inst, &aer_ctl);
} /* siu_put_aer_low_load */

/******************************************************************************
 * FUNCTION PURPOSE: Put active AER instance to normal (high) processing mode
 ******************************************************************************/
void siu_put_aer_high_load(void *aer_inst)
{
  aerControl_t aer_ctl;
  
  /* high processing load (normal operation): enable tail update, run coherence 
                                              detection, update noise estimate */
  aer_ctl.valid_bitfield_0 =   aer_CTL_VALID0_MODES_CTL1
                             | aer_CTL_VALID0_MODES_CTL2;  
  aer_ctl.modes_1.mask  = aer_CTL1_FREEZE_TAIL_UPDATE;
  aer_ctl.modes_1.value = 0;
  aer_ctl.modes_2.mask  = aer_CTL2_SKIP_COHERENCE | aer_CTL2_FREEZE_NOISE_EST;
  aer_ctl.modes_2.value = 0;
  
  aer_ctl.valid_bitfield_1 = 0x0;  
  aer_ctl.valid_bitfield_2 = 0x0;  
  aer_ctl.valid_bitfield_3 = 0x0;  
  aer_ctl.valid_bitfield_4 = 0x0;
  aerControl(aer_inst, &aer_ctl);
} /* siu_put_aer_high_load */

/* Nothing past this point */
