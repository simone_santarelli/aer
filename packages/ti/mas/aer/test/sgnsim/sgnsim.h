#ifndef _SGNSIM_H
#define _SGNSIM_H

/****************************************************************************** 
 * FILE PURPOSE: SGN functions to support simulation testing of AER
 ****************************************************************************** 
 * FILE NAME:   sgnsim.h
 * 
 * DESCRIPTION: Contains constants and arrays for simulation testing of AER
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#include <ti/mas/types/types.h>  
#include <ti/mas/sdk/sgn.h>             


#define NUMBER_OF_FRAMES      1           
#define MAX_STREAM_SIZE       (NUMBER_OF_FRAMES*SIU_MAX_FRAME_LENGTH)
#define SGN_SIM_INST_SIZE     100

/* Signal Generator Parameters */
typedef struct {
  sgnFileIo_t* (*file_io_fcn)(sgnFileIo_t *dsc);          /* file io function */
  void  (*exception)(tuint id, char *str);  
  void  (*debug)(tuint id, char *str);      
  tint      fsk_data[4];
  tbool     state;                  /* TRUE: generator is on.                 */
  tint      samp_freq;              /* sampling frequency                     */
  linSample dcoffset;               /* dc offset (Q0 format)                  */
  tint      wftype;                 /* one of sgn_WFT_'s                      */
  tint      f1;                     /* the first frequency in Hz              */
  tint      f2;                     /* the second frequency in Hz             */
  linSample amp1;                   /* amplitude of the first component (Q0)  */
  linSample amp2;                   /* amplitude of the second component (Q0) */
  tint      nlevel;                 /* idle noise level in dBm0               */
  tulong    nseed;                  /* idle noise seed                        */
  tint      ntype;                  /* noise type SGN_NT_xxx                  */
  tint      pow_level;              /* Hoth or CSS power level in dBm0        */
  tulong    hoth_seed;              /* Hoth noise seed                        */
} sgnSimPars_t;

/* To be used to statically allocate memory for SGN module */ 
typedef struct {
  Fract sgn_inst_buf[SGN_SIM_INST_SIZE];
}sgnInstBuf_t; 


void aer_sim_sgn_init(void *sgn_inst, sgnSimPars_t *sgn_params);
void aer_sim_sgn_set (sgnInstBuf_t *inst, sgnSimPars_t *sgn);
void aer_sim_sgn_gen_tx_frame (sgnInstBuf_t *tx_sgns, linSample ne_data[][160],
                               tint frame_size, tint num_mic);
void aer_sim_sgn_gen_rx_frame (sgnInstBuf_t *rx_sgn, linSample *fe_data, 
                               tint frame_size);
void aer_sim_sgn_reset (sgnInstBuf_t *inst);

#endif /* _SGNSIM_H */

/* nothing past this point */
