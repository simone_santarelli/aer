#ifndef _AEP_SIM_H
#define _AEP_SIM_H

#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/sdk/src/aeploc.h>

typedef struct aepSimPars_s {
  void  (*exception)(tuint id, char *str);  
  void  (*debug)(tuint id, char *str);      

  tint aep_seg1_duration;  /* aep segment 1 duration in samples @ 8kHz */
  tint aep_seg2_duration;  /* aep segment 2 duration in samples @ 8kHz */
  tint aep_delay;          /* echo path delay in # of 125us, or samples @8khz */
  tint aep_gain_l2;        /* AEP gain in log2 scale */
  tint aep_frame_M60dB;    /* # of frames for AEP to decay to -60dB */
  tint aep_alpha;          /* AEP nonlinearity constant */
  Fract aep_delta_a;       /* for echo path change simulation */
  tint aep_morph_delay;    /* for echo path change simulation */

  tint aep_hseg1_length;   /* in # of samples, 8kHz or 16kHz */
  tint aep_hseg2_length;   /* in # of samples, 8kHz or 16kHz */
  tint aep_filter_length;  /* in # of samples, 8kHz or 16kHz */
  tint aep_buf_length;     /* in # of samples, 8kHz or 16kHz */
  tint aep_delay_samp;     /* in # of samples, 8kHz or 16kHz */ 
} aepSimPars_t;

typedef struct aepSimInst_s {
  aepInst_t sdk_aep;       /* aepInst_t is defined in sdk/src/aeploc.h*/

  Fract  spk_gain_lin;     /* linear speaker gain, S4.11 format, value of 2048 
                              corresponds to 0dB gain, with range [-66, 24]dB */   
  Fract  spk_gain_lin_ref; /* linear speaker gain reference, corresponding to
                              dB gain AER_SIM_HF_SPKGAIN_0dB.                 */
  Fract  mic_gain_lin;     /* linear mic gain: S4.11 format, value of 2048 
                              corresponds to 0dB gain, with range [-66, 24]dB */   
  Fract  mic_gain_lin_ref; /* linear mic gain reference, corresponding to
                              dB gain AER_SIM_HF_MICGAIN_0dB.                 */  
} aepSimInst_t;

extern void aer_sim_aep_init(aepSimInst_t *aep_inst, tint delay, tint samp_rate, 
                             tbool aep_morph_flag);
extern void aer_sim_aep_open(aepSimInst_t *aep_inst, aepSimPars_t *aep_params, 
                             tint frm_size, tint samp_rate);
extern void aer_sim_aep_config(aepSimInst_t *aep_inst, aepSimPars_t *aep_params,
                               tint gain_adj_exp, Fract gain_adj_mant);
extern void aer_sim_aep_add_echo(aepSimInst_t *inst, linSample *data_spk,  
                    linSample data_ne_spch[][160], linSample data_mic[][160], 
                    tint num_mic, tbool txin_has_echo);
                    
extern void aer_sim_aep_spk_gain_adj(aepSimInst_t *aep_inst, tint spk_gain_dB);
extern void aer_sim_aep_mic_gain_adj(aepSimInst_t *aep_inst, tint mic_gain_dB);

#endif
/* nothing past this line */
