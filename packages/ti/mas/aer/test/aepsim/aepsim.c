
/* Ansi C header files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ti/mas/types/types.h>                      
#include <ti/mas/fract/fract.h>
#include <ti/mas/util/utl.h>

#include <ti/mas/aer/test/aersim/aersimcfg.h>           
#include <ti/mas/aer/test/aersim/aerbuffs.h>           

#include <ti/mas/sdk/aep.h>                      
#include <ti/mas/sdk/src/aeploc.h>                      

#include <ti/mas/aer/test/aepsim/aepsim.h>           

#include <ti/mas/aer/test/siusim/siuloc.h>            /* to be removed */

#define AER_SIM_AEP_DELAY_RX 0
#define AER_SIM_AEP_DELAY_TX 0

#define AER_SIM_AEP_GAIN_dB_Q        4
#define AER_SIM_AEP_GAIN_LIN_Q       11
#define AER_SIM_AEP_UNITY_GAIN_LIN  ((1<<AER_SIM_AEP_GAIN_LIN_Q)-1)

/* Analog gain values in dB (S11.4 format) */
#define AER_SIM_AGAIN_0dB                    0      /* 0dB  */
#define AER_SIM_AGAIN_PLUS_6dB              96      /* 6dB  in 1/16dB steps   */
#define AER_SIM_AGAIN_MINUS_6dB           (-96)     /* -6dB in 1/16dB steps   */
#define AER_SIM_AGAIN_PT5DB                  8      /* 0.5dB in 1/16dB steps  */

/* Digital Gain Values in dB (S14.1 format) */
#define AER_SIM_DGAIN_0dB                    0    /*  0dB  */
#define AER_SIM_DGAIN_PLUS_6dB              12    /*  6dB  */
#define AER_SIM_DGAIN_MINUS_6dB           (-12)   /* -6dB  */

/* Gain Tables */
#define AER_SIM_LENGTH_dB_TO_LIN_TBL  12    /* length of table to convert from
                                             * dB values to linear values     */
#define AER_SIM_LENGTH_AGAINCORR_TBL   8    /* length of table for 1/16dB 
                                             * steps for gain correction      */
                                             
/* ----- dB to linear conversion table ----- */
/* Range 0 to -5.5dB in steps of 0.5dB each. Access is done as 
 * aersim_dBLinTable[-gaindiff_dB] where gaindiff_dB is mapped in the above range */
const tint aersim_dBLinTable[AER_SIM_LENGTH_dB_TO_LIN_TBL] = {
     32767,             /*     0 dB  */
     30935,             /* - 0.5 dB  */
     29205,             /* - 1.0 dB  */
     27571,             /* - 1.5 dB  */
     26029,             /* - 2.0 dB  */
     24573,             /* - 2.5 dB  */
     23198,             /* - 3.0 dB  */
     21900,             /* - 3.5 dB  */
     20675,             /* - 4.0 dB  */
     19519,             /* - 4.5 dB  */
     18427,             /* - 5.0 dB  */
     17396              /* - 5.5 dB  */
};

/* ----- Table of analog gain corrections ----- */
/* dB to linear conversion table for the range 0:-0.4375dB in steps of 0.0625dB
 * or 1/16dB. This table increases the precision of the gains to 1/16th of a dB
 * and allows correcting the errors in the analog gains set in the hardware  
 * codec with respect to the requested gain. The precision of 1/16dB is better 
 * than the maximum error that AER can tolerate without losing on its 
 * performance. A 0.25dB error in gain can cause -30dB loss in cancellation. 
 * So the errors in gains should be lower than 0.25dB */

const tint aersim_dBCorrLinTable[AER_SIM_LENGTH_AGAINCORR_TBL] = {
     32767,   /*     0  dB  */
     32532,   /* - 1/16 dB */
     32298,   /* - 2/16 dB */
     32067,   /* - 3/16 dB */
     31837,   /* - 4/16 dB */
     31609,   /* - 5/16 dB */
     31382,   /* - 6/16 dB */
     31157    /* - 7/16 dB */
};
 
/* AEP related definitions/extern declarations */
linSample aepTxDBuffer[AER_SIM_MAX_FRAME_LENGTH];    
linSample aepTxBuffer [AER_SIM_MAX_FRAME_LENGTH];    
Fract aepFilterIram[AER_SIM_AEP_COEF_LEN_16K];
Fract aepFilterTemp[AER_SIM_AEP_COEF_LEN_16K];
Fract aepNewFilterTemp[AER_SIM_AEP_COEF_LEN_16K];
linSample aep_pure_echo[AER_SIM_MAX_FRAME_LENGTH];

/* aepbuffer is defined in sdk\src\aep.c - violation of ECO model */
extern Fract aepbuffer[]; 
extern const Fract aepFilterTemp_8K[];
extern const Fract aepFilterTemp_8K_new[];
extern const Fract aepFilterTemp_16K[];
extern const Fract aepFilterTemp_16K_new[];

extern siuInst_t  siu_inst; 


/******************************************************************************
 * FUNCTION PURPOSE:  Convert S11.4 gain in dB value to the format mant*2^(exp)
 ******************************************************************************
 * DESCRIPTION:  Convert S11.4 gain in dB value to the format: 
                 mant1*mant2*(2^(exponent))
 *               exponent  = number of 6dB steps
 *               mantissa  = mant1*mant2
 *               mant1     = accounts for 0.5dB steps
 *               mant2     = accounts for 1/16dB steps
 * 
 *  void aersim_gain_convert (
 *    Fract        gain_dB,     - gain in dB S11.4 format
 *    Fract        *mantissa,   - Pointer to the mantissa of the gain in S0.15 
 *    tint         *exponent)   - Pointer to exponent of the gain 
 *
 *****************************************************************************/
void aersim_gain_convert(Fract gain_dB, Fract *mantissa, tint *exponent)
{
  tint  alpha_6dB   = 0;
  tint  alpha_pt5dB = 0;
  tint  temp;
  Fract mant1, mant2;
  
  /* Map the gain in the range 0 to -5.5dB with the use of exponent */
  while (gain_dB > AER_SIM_AGAIN_0dB){             /* if gain_dB > 0dB            */ 
    gain_dB += AER_SIM_AGAIN_MINUS_6dB;            /* subtract 6 dB from gain_dB  */
    alpha_6dB++;                               /* increment alpha             */
  }
  while (gain_dB <= AER_SIM_AGAIN_MINUS_6dB) {     /* if gain_dB <= -6dB          */   
    gain_dB += AER_SIM_AGAIN_PLUS_6dB;             /* add 6dB to gainddB          */
    alpha_6dB--;                               /* decrement the alpha         */
  }
    
  temp = -gain_dB;
  while (temp >= AER_SIM_AGAIN_PT5DB) {            /* if temp >= 0.5dB            */
    temp -= AER_SIM_AGAIN_PT5DB;                   /* subtract 0.5dB from it      */
    alpha_pt5dB++;                             /* increment # of 0.5dB steps  */
  } 
  
  /* obtain the linear S15.0 gain from the 0.5dB table */
  mant1 = aersim_dBLinTable[alpha_pt5dB];

  /* obtain the linear S15.0 gain from the 1/16dB table */
  mant2 = aersim_dBCorrLinTable[temp];  
  
  /* overall linear gain mantissa = mant1*mant2 - 1/16dB precision */ 
  *mantissa = (Fract)frctMul(mant1, 15, mant2, 15, 15);           

  /* number of 6dB steps */
  *exponent = alpha_6dB;                     
  
} /* aersim_gain_convert */

/******************************************************************************
 * Function aer_sim_aep_init: Initilize AEP module for AER simulation. 
 * Note: AEP module of SDK component simulates the acoustic echo path. This module
 *       is not a strict ECO module with complete APIs and needs future work. 
******************************************************************************/
void aer_sim_aep_init(aepSimInst_t *aep_inst, tint delay, tint samp_rate, 
                      tbool aep_morph_flag)
{   
  /* clear AEP filter delay line */
  memset(&siu_rx_del_buf[0], 0, SIU_RX_DELAY_LINE_LENGTH*sizeof(linSample));
  
  /* clear AEP filter coefficients */
  utlFractMemSet (&aepFilterIram[0], 0, AER_SIM_AEP_COEF_LEN_16K);

  /* all-pass filter (1.0 * 2^gain_l2) - dummy filter, not used in AEP */
  aepFilterIram[delay] = frctAdjustQ ((Fract)1, 0, aep_FILTER_Q);
  
  /* clear AEP partial results buffer */
  utlFractMemSet (&aepbuffer[0], 0, SIU_MAX_AEP_LENGTH);
 
  /* choose AEP filter according to sampling rate */
  if (samp_rate == AER_SIM_SRATE_8000_DIV_8K) {
    /* initial filter */
    memcpy(&aepFilterTemp[0], &aepFilterTemp_8K[0], 
           sizeof(Fract)*AER_SIM_AEP_COEF_LEN_8K);

    /* new filter to simulate echo path change */
    if(aep_morph_flag) {
      memcpy(&aepNewFilterTemp[0], &aepFilterTemp_8K_new[0], 
             sizeof(Fract)*AER_SIM_AEP_COEF_LEN_8K);
    }
  }    
  else if (samp_rate == AER_SIM_SRATE_16000_DIV_8K) {
    /* initial filter */
    memcpy(&aepFilterTemp[0], &aepFilterTemp_16K[0], 
           sizeof(Fract)*AER_SIM_AEP_COEF_LEN_16K);

    /* new filter to simulate echo path change */
    if(aep_morph_flag) {
      memcpy(&aepNewFilterTemp[0], &aepFilterTemp_16K_new[0], 
             sizeof(Fract)*AER_SIM_AEP_COEF_LEN_16K);
    }
  }
  else {
    printf("Sampling rate not supported for AEP!\n");
	exit(1);
  }     
} /* aer_sim_aep_init */

/******************************************************************************
 * Function aer_sim_aep_open: Open AEP module for AER simulation.
 *   Description: This function configures AEP parameters such as gain, filter,
 *                echo path change speed, etc. 
******************************************************************************/
void aer_sim_aep_open(aepSimInst_t *aep_inst, aepSimPars_t *aep_params, 
                      tint frm_size, tint samp_rate)
{
  aepConfig_t cfg;

  /* SDK AEP module is not a strict ECO module and needs rework in future */
  cfg.samples_per_frame = frm_size; //siu_setup->aer_sin_frm_size;
  cfg.h_seg1_length     = aep_params->aep_hseg1_length;
  cfg.h_seg2_length     = aep_params->aep_hseg2_length;
  cfg.rx_delay          = AER_SIM_AEP_DELAY_RX;
  cfg.tx_delay          = AER_SIM_AEP_DELAY_TX;
  cfg.txd_buffer        = &aepTxDBuffer[0];
  cfg.send_out          = &aepTxBuffer[0];
  cfg.gain_l2           = aep_params->aep_gain_l2;
  cfg.sampling_rate     = samp_rate;
  cfg.filter_iram       = &aepFilterIram[0]; /* dummy filter */
  cfg.filter_temp       = &aepFilterTemp[0]; /* initial filter */
  cfg.new_filter_temp   = &aepNewFilterTemp[0]; /* new filter for echo path change */
  cfg.delta_a           = aep_params->aep_delta_a;;
  cfg.morph_delay       = aep_params->aep_morph_delay;
  cfg.receive_in_length = SIU_RX_DELAY_LINE_LENGTH; /* AEP delay line length */
  cfg.receive_in        = &siu_rx_del_buf[0];  /* AEP delay line start address */
  cfg.aepbuf_start      = &aepbuffer[0];
  cfg.aepbuf_length     = aep_params->aep_buf_length;
  cfg.exception         = aep_params->exception;
  cfg.debug             = aep_params->debug;
  cfg.thw_hf_micgain_init = AER_SIM_HF_MICGAIN_0dB;         /* 42.5dB */

  /* Call AEP API function to open AEP instance:
     - filter coefficients: aepFilterTemp
     - gain: 2^(aep_gain_l2), e.g. aep_gain_l2=1 means gain is 2 (i.e. 6dB). 
  */
  aepOpen (&aep_inst->sdk_aep, &cfg);
  
  /* unity or 0dB gain for speaker and mic analog gains */
  aep_inst->spk_gain_lin = aep_inst->spk_gain_lin_ref = AER_SIM_AEP_UNITY_GAIN_LIN; 
  aep_inst->mic_gain_lin = aep_inst->mic_gain_lin_ref = AER_SIM_AEP_UNITY_GAIN_LIN; 
  
} /* aer_sim_aep_open */

/******************************************************************************
 * Function aer_sim_aep_config: Configures AEP module for AER simulation.
 *   Description: This function does more configuration on top of what function
 *   aer_sim_aep_open already configures.  
******************************************************************************/
void aer_sim_aep_config(aepSimInst_t *aep_inst, aepSimPars_t *aep_params,
                        tint gain_adj_exp, Fract gain_adj_mant)
{
  aepControl_t aep_ctl;
  
  /* Configure AEP parameters */
  aep_ctl.u.aepath.gain_l2 = aep_params->aep_gain_l2 + gain_adj_exp;
  aep_ctl.u.aepath.delay   = aep_params->aep_delay_samp;
  aep_ctl.u.aepath.aep_frame_M60dB = aep_params->aep_frame_M60dB;
  aep_ctl.code = aep_CTL_RESET;
  aepControl (&aep_inst->sdk_aep, &aep_ctl);
  
  aep_ctl.code = aep_CTL_ALPHA;                                                              
  aep_ctl.u.alpha = aep_params->aep_alpha;
  aepControl (&aep_inst->sdk_aep, (aepControl_t *)&aep_ctl);
  
  /* configure AEP gain (or -ERL) according to configuration parameter:
     - configure SDK AEP gain to be power of 2, i.e. multiple of 6dB
     - use spk_gain_lin to make adjustments less than 6dB      */
  aep_inst->spk_gain_lin = aep_inst->spk_gain_lin_ref 
                         = frctMul(aep_inst->spk_gain_lin, SPK_GAIN_Q, 
                                   gain_adj_mant, TYP_FRACT_SIZE-1, SPK_GAIN_Q);
} /* aer_sim_aep_config */

/******************************************************************************
 * Function aer_sim_aep_add_echo: Generates echo and adds to near end signal.
 *   Description: This function generates echo based on the configured filter, 
 *                gain, and non-linear factor. 
******************************************************************************/
void aer_sim_aep_add_echo(aepSimInst_t *aep_sim_inst, linSample *data_spk, 
                          linSample data_ne_spch[][AER_SIM_MAX_FRAME_LENGTH], 
                          linSample data_mic    [][AER_SIM_MAX_FRAME_LENGTH], 
						  tint num_mic, tbool txin_has_echo)
{
  aepInst_t *inst;
  int i, j;
  LFract samp_f, samp_lf;
  
  inst = &aep_sim_inst->sdk_aep;
  
  /* Apply gain to speaker data and copy to AEP delay line.
   * Note: data copy violates ECO rules, to be fixed later. */
  if(aep_sim_inst->spk_gain_lin != AER_SIM_AEP_UNITY_GAIN_LIN) {
    for(i=0; i<inst->samples_per_frame; i++) {
      samp_lf = frctMul(data_spk[i], 0, aep_sim_inst->spk_gain_lin, AER_SIM_AEP_GAIN_LIN_Q, 0);
      siu_inst.aer_recv_in_buf[i] = frctLF2FS(samp_lf, 0, 0);
    }
  }
  else {
    memcpy(siu_inst.aer_recv_in_buf, data_spk, sizeof(linSample)*inst->samples_per_frame);
  }

  /* clear echo buffer */
  memset(aep_pure_echo, 0, sizeof(linSample)*inst->samples_per_frame);

  /* Generate echo if Tx input doesn't already have echo */
  if(!txin_has_echo) {
    aepAddEcho (inst, &aep_pure_echo[0], siu_inst.aer_recv_in_buf);
  }

  /* Adjust delay line pointer. Function aepAddEcho does circular buffer addressing
     with knowledge of start and end of this delay line, but doesn't change 
     pointer aer_recv_in_buf.  */
  siu_inst.aer_recv_in_buf += inst->samples_per_frame;
  if(siu_inst.aer_recv_in_buf >= &inst->receive_in[inst->receive_in_length]) {
    siu_inst.aer_recv_in_buf = &inst->receive_in[0];
  }
 
  /* Mix echo with near end speech and mic analog gain */
  for(i=0; i<inst->samples_per_frame; i++) {
    for(j=0; j<num_mic; j++) {
      /* There is only one AEP instance due to violation of ECO rule of AEP.
         To make AEP a true ECO module in future to generate different echo for
         different mics. */
      samp_lf = (LFract)data_ne_spch[j][i] + (LFract)aep_pure_echo[i];
      samp_f  = frctLF2FS(samp_lf, 0, 0);
      samp_lf = frctMul(samp_f, 0, aep_sim_inst->mic_gain_lin, AER_SIM_AEP_GAIN_LIN_Q, 0);
	  data_mic[j][i] = frctLF2FS(samp_lf, 0, 0);
	}
  }
} /* aer_sim_aep_add_echo */



/******************************************************************************
 * Function aer_sim_aep_spk_gain_adj: Adjust speaker gain.
******************************************************************************/
void aer_sim_aep_spk_gain_adj(aepSimInst_t *aep_inst, tint spk_gain_dB)
{
  Fract gain_mant, gain_exp; 

  if(spk_gain_dB != AER_SIM_HF_SPKGAIN_0dB) { 
    aersim_gain_convert((spk_gain_dB-AER_SIM_HF_SPKGAIN_0dB), &gain_mant, &gain_exp);
  
    aep_inst->spk_gain_lin = frctMul(aep_inst->spk_gain_lin_ref, AER_SIM_AEP_GAIN_LIN_Q, 
                                     gain_mant, 15, AER_SIM_AEP_GAIN_LIN_Q+gain_exp);
  }
  else {
    aep_inst->spk_gain_lin = aep_inst->spk_gain_lin_ref;
  }  
} /* aer_sim_aep_spk_gain_adj */

/******************************************************************************
 * Function aer_sim_aep_mic_gain_adj: Adjust microphone gain.
******************************************************************************/
void aer_sim_aep_mic_gain_adj(aepSimInst_t *aep_inst, tint mic_gain_dB)
{
  Fract gain_mant, gain_exp; 

  if(mic_gain_dB != AER_SIM_HF_MICGAIN_0dB) { 
    aersim_gain_convert((mic_gain_dB-AER_SIM_HF_MICGAIN_0dB), &gain_mant, &gain_exp);
  
    aep_inst->mic_gain_lin = frctMul(aep_inst->mic_gain_lin_ref, AER_SIM_AEP_GAIN_LIN_Q, 
                                     gain_mant, 15, AER_SIM_AEP_GAIN_LIN_Q+gain_exp);
  }
  else {
    aep_inst->mic_gain_lin = aep_inst->mic_gain_lin_ref;
  }
} /* aer_sim_aep_mic_gain_adj */

/* nothing past this line */



























