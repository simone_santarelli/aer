1
1
7
0
0x1887
0x01C6
0x0003
0x0001
0x000f
1
0x31
0
0
0
1
1
vectors/inp/file.pcm
1
0
1
vectors/inp/audio.raw

# scroll down and see an exmple of beamforming or multi-source selection simulation 
########### parameters are: ##############
1.  Microphone type (0 or 1): 
      0 - mic array
      1 - single mic or multiple remote mics
2.  Number of mics:
      for mic array: up to 4
      for single mic: 1
      form multiple remote mics: up to 2 for C55x, up to 3 for C64x+ or Coretx-A8
3.  Sampling rate bitfield (0-7):
       This bitfield defines the sampling rates on the following 3 interfaces:
          - Bit 0: Rx in,        0: 8k samples/second, 1: 16k samples/second
          - Bit 1: Tx out,       0: 8k samples/second, 1: 16k samples/second
          - Bit 2: Rx out/Tx In, 0: 8k samples/second, 1: 16k samples/second
4.  Band split option (0 or 1): 
      1 - bandsplit    (16kHz bandsplit)
      0 - no bandsplit (16kHz full band or 8kHz full band)
5.  AER Control bitfield 0 (refer to aer.h or aerapi.chm)
6.  AER Control bitfield 1 (refer to aer.h or aerapi.chm)
7.  AGC Control bitfield   (refer to agc.h or aerapi.chm)
8.  BF control bitfield (operation)   (refer to bf.h or aerapi.chm)
9.  BF mic selection bitfield (which mics are used for BF) (refer to bf.h or aerapi.chm)
10. DRC On/off flag (0 or 1): 1 - on; 0 - off
11. DRC cfgbits (refer to drc.h or aerapi.chm)
12. Far end (Rx) signal generator (0 or 1):  0 - off, 1 - on
13. Near end (Tx) signal generator (0 or 1): 0 - off, 1 - on
14. Profiling flag (0 or 1): 1 - profiling MHz usage; 0 - no profiling
15  Rx input file I/O flag (0 or 1): 
     1 - Rx input read from a file (Rx signal generator should be off)
     0 - Rx input not read from a file
16.  Rx output file I/O flag (0 or 1): 
     1 - Rx output written to a file named rxOut.pcm
     0 - Rx output not written to a file
17. Rx input file path and name (must be provided even if Rx I/O flag is 0)  
     File format: 16-bit binary (raw PCM), Motorola (MSB, LSB). 
18. Tx input file I/O flag (0 or 1): 
     1 - Tx input read from one or more files (Tx signal generator should be off)
     0 - Tx input not read from files 
19. Tx input with echo (1 - yes; 0 - no), near end signal mixed with echo.
20. Tx output file I/O flag (0 or 1): 
     1 - Tx output written to a file named txOut.pcm
     0 - Tx output not written to a file
21. Tx input file path and name for mic 1 (must be provided even if Tx I/O flag is 0)  
22. Tx input file path and name for mic 2 (optional) 
23. Tx input file path and name for mic 3 (optional) 
24. Tx input file path and name for mic 4 (optional) 
25. Beamforming steering angle (can only be 0, 45 or 90)

########### Configuration example of beamforming test  ##############
0
4
7
0
0x1886
0x01C0
0x0000
0x0001
0x000f
0
0x31
0
0
0
0
0
vectors/inp/fe_16k.pcm
1
0
1
vectors/inp/bf_mic1_16k.pcm
vectors/inp/bf_mic2_16k.pcm
vectors/inp/bf_mic3_16k.pcm
vectors/inp/bf_mic4_16k.pcm
0

########### Configuration example of multi-source selection test  ##############
1
2
7
0
0x1887
0x01C0
0x0000
0x0001
0x000f
0
0x31
0
0
0
0
0
vectors/inp/fe_16k.pcm
1
0
1
vectors/inp/mss_mic1_16k.pcm
vectors/inp/mss_mic2_16k.pcm
0
