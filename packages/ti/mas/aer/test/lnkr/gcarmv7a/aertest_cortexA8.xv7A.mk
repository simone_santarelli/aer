#*******************************************************************************
#* FILE PURPOSE: Lower level makefile for Creating Component Libraries
#*******************************************************************************
#* FILE NAME: lnkr/gcarmv7a/aertest_cortexA8.xv7A.mk
#*
#* DESCRIPTION: Defines Source Files, Compilers flags and build rules
#*
#*
#* This is an auto-generated file          
#*******************************************************************************
#

#
# Macro definitions referenced below
#
empty =
space =$(empty) $(empty)
CC = $(GNU_ARM_CORTEXA8_GEN_INSTALL_DIR)//bin/arm-none-linux-gnueabi-gcc -c -MD -MF $@.dep -march=armv7-a -lasound -fPIC -Wunused -Dfar= 
AC = $(GNU_ARM_CORTEXA8_GEN_INSTALL_DIR)//bin/arm-none-linux-gnueabi-gcc -c -x assembler -march=armv7-a 
ARIN = $(GNU_ARM_CORTEXA8_GEN_INSTALL_DIR)/arm-none-linux-gnueabi/bin/ar cr
LD = $(GNU_ARM_CORTEXA8_GEN_INSTALL_DIR)//bin/arm-none-linux-gnueabi-gcc   -Wl,-Map=$(XDCCFGDIR)/$@.map -lasound -lstdc++ -L$(GNU_ARM_CORTEXA8_GEN_INSTALL_DIR)/arm-none-linux-gnueabi/lib  -pg 
CGINCS = 
RTSLIB = 
INCS = -I. -I$(strip $(subst ;, -I,$(subst $(space),\$(space),$(subst \,/,$(INCDIR)))))
OBJEXT = ov7A
AOBJEXT = sv7A
INTERNALDEFS =  -Dgnu_targets_arm_GCArmv7A -Dxdc_target_types__=gnu/targets/arm/std.h -eo.$(OBJEXT) -ea.$(AOBJEXT) -o $@ 
INTERNALLINKDEFS = -Wl,-Map=$@.map -o $@ 
OBJDIR = ./package/cfg/lnkr/gcarmv7a/aertest_cortexA8

#List the commontestsrc Files
COMMONTESTSRCC= \
    ./aersim/aersim.c\
    ./aersim/aersim_fileIO.c\
    ./aersim/aersim_setup.c\
    ./siusim/siu.c\
    ./siusim/siuaer.c\
    ./siusim/siuagc.c\
    ./siusim/siumss.c\
    ./siusim/siudrc.c\
    ./siusim/siurx.c\
    ./siusim/siutx.c\
    ./siusim/siummic.c\
    ./siusim/siubf.c\
    ./sgnsim/sgnsim.c\
    ./aepsim/aepsim.c\
    ./piusim/piu.c\
    ./aersim/audio_input_output.c\
    ./aersim/audio_thread_playback.c\
    ./aersim/audio_thread_record.c\
    ./aersim/threadfork.c

# FLAGS for the commontestsrc Files
COMMONTESTSRCCFLAGS =  -mcpu=cortex-a8 -mfpu=neon -mfloat-abi=softfp -O2 -ftree-vectorize 

# Make Rule for the commontestsrc Files
COMMONTESTSRCCOBJS = $(patsubst %.c, $(OBJDIR)/%.$(OBJEXT), $(COMMONTESTSRCC))

$(COMMONTESTSRCCOBJS): $(OBJDIR)/%.$(OBJEXT): %.c
	-@echo clv7A $< ...
	if [ ! -d $(@D) ]; then $(MKDIR) $(@D) ; fi;
	-$(RM) $@.dep
	$(CC) $(COMMONTESTSRCCFLAGS) $(INTERNALDEFS) $(INCS) -I$(CGINCS) -fc $< 
	-@$(CP) $@.dep $@.pp; \
         $(SED) -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
             -e '/^$$/ d' -e 's/$$/ :/' < $@.pp >> $@.dep; \
         $(RM) $@.pp 

#Create Empty rule for dependency
$(COMMONTESTSRCCOBJS):lnkr/gcarmv7a/aertest_cortexA8.xv7A.mk
lnkr/gcarmv7a/aertest_cortexA8.xv7A.mk:

#Include Depedency for commontestsrc Files
ifneq (clean,$(MAKECMDGOALS))
 -include $(COMMONTESTSRCCOBJS:%.$(OBJEXT)=%.$(OBJEXT).dep)
endif

#List the CortextA8testsrc Files
CORTEXTA8TESTSRCC= \
    ./aersim/arm/aersimbufs.c

# FLAGS for the CortextA8testsrc Files
CORTEXTA8TESTSRCCFLAGS =  -mcpu=cortex-a8 -mfpu=neon -mfloat-abi=softfp -O2 -ftree-vectorize  -pg 

# Make Rule for the CortextA8testsrc Files
CORTEXTA8TESTSRCCOBJS = $(patsubst %.c, $(OBJDIR)/%.$(OBJEXT), $(CORTEXTA8TESTSRCC))

$(CORTEXTA8TESTSRCCOBJS): $(OBJDIR)/%.$(OBJEXT): %.c
	-@echo clv7A $< ...
	if [ ! -d $(@D) ]; then $(MKDIR) $(@D) ; fi;
	-$(RM) $@.dep
	$(CC) $(CORTEXTA8TESTSRCCFLAGS) $(INTERNALDEFS) $(INCS) -I$(CGINCS) -fc $< 
	-@$(CP) $@.dep $@.pp; \
         $(SED) -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
             -e '/^$$/ d' -e 's/$$/ :/' < $@.pp >> $@.dep; \
         $(RM) $@.pp 

#Create Empty rule for dependency
$(CORTEXTA8TESTSRCCOBJS):lnkr/gcarmv7a/aertest_cortexA8.xv7A.mk
lnkr/gcarmv7a/aertest_cortexA8.xv7A.mk:

#Include Depedency for CortextA8testsrc Files
ifneq (clean,$(MAKECMDGOALS))
 -include $(CORTEXTA8TESTSRCCOBJS:%.$(OBJEXT)=%.$(OBJEXT).dep)
endif



lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/aer/lib/aer_c.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/aer/lib/aer_a.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/aer/lib/aer_c.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/sdk/lib/sdk_c.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/vpe/lib/vpe_c.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/vpe/lib/vpe_a.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(MAS_INSTALL_DIR)/ti/mas/util/lib/util_c.av7A
lnkr/gcarmv7a/aertest_cortexA8.xv7A :  $(COMMONTESTSRCCOBJS) $(CORTEXTA8TESTSRCCOBJS)
	@echo lnkv7A $@ ...
	$(LD) $^ $(INTERNALLINKDEFS) $(RTSLIB)
