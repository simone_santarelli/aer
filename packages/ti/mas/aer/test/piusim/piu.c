/******************************************************************************
 * PURPOSE : Main source file for the PCM Interface Unit (PIU).
 ******************************************************************************
 * FILE NAME:   piu.c
 *
 * DESCRIPTION: Contains main PIU functions.
 *
 *
 * (C) Copyright 2008 Texas Instruments, Inc.
 *****************************************************************************/

/* Ansi C header files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* System Level header files and Utilities' API's */
#include <ti/mas/types/types.h>                       /* DSP types            */
#include <ti/mas/fract/fract.h>                       /* fractional macros    */
#include <ti/mas/aer/test/aersim/aersimcfg.h>

/* PIU related header files */
#include <ti/mas/aer/test/piusim/piuloc.h>

void piuInit(piuInst_t *inst, tint frm_size)
{
  memset(&inst->del_buf_rx[0], 0, sizeof(linSample)*AER_SIM_PIU_DEL_BUF_LEN_RX);
  memset(&inst->del_buf_tx[0][0], 0, 
         sizeof(linSample)*AER_SIM_NUM_MIC*AER_SIM_PIU_DEL_BUF_LEN_TX);
  
  inst->del_buf_rxin_ind = AER_SIM_PIU_DEL_RX;
  inst->del_buf_txin_ind = AER_SIM_PIU_DEL_TX;
  inst->del_buf_rxout_ind = 0;
  inst->del_buf_txout_ind = 0;
  
  inst->del_buf_rxsync_ind = inst->del_buf_rxin_ind - AER_SIM_SIU_Y2X_DEL_ADJ;
  if(inst->del_buf_rxsync_ind < 0) {
    inst->del_buf_rxsync_ind += AER_SIM_PIU_DEL_BUF_LEN_RX;
  } 
  
  inst->frame_size = frm_size;
}

void piuReceiveIn(piuInst_t *inst, linSample *data_in, linSample *data_dac, 
                  linSample *data_rx_sync)
{
  int i;
  
  /* Copy input samples into PIU delay buffer.
     Copy samples from delay buffer to output buffer to go to DAC. 
     Copy samples from delay buffer to Rx sync buffer for AER Tx processing. */
  for (i=0; i<inst->frame_size; i++) {
    /* copy samples */
    inst->del_buf_rx[inst->del_buf_rxin_ind++] = data_in[i];
    data_dac[i] = inst->del_buf_rx[inst->del_buf_rxout_ind++];
    data_rx_sync[i] = inst->del_buf_rx[inst->del_buf_rxsync_ind++];
    
    /* adjust circular buffer pointers */    
    if(inst->del_buf_rxin_ind == AER_SIM_PIU_DEL_BUF_LEN_RX) {
      inst->del_buf_rxin_ind = 0;
    }
    if(inst->del_buf_rxout_ind == AER_SIM_PIU_DEL_BUF_LEN_RX) {
      inst->del_buf_rxout_ind = 0;
    }
    if(inst->del_buf_rxsync_ind == AER_SIM_PIU_DEL_BUF_LEN_RX) {
      inst->del_buf_rxsync_ind = 0;
    }
  } /* for (i=0; i<inst->frame_size; i++) */
} /* piuReceiveIn */

void piuSendIn(piuInst_t *inst, linSample data_adc[][AER_SIM_MAX_FRAME_LENGTH], 
                                linSample data_out[][AER_SIM_MAX_FRAME_LENGTH],
                                tint num_mic)
{
  int i, j;
  
  /* Copy input samples into PIU delay buffer.
     Copy samples from delay buffer to output buffer for AER processing.    */
  for (i=0; i<inst->frame_size; i++) {
    /* copy samples for all mics */
    for (j=0; j<num_mic; j++) {
      inst->del_buf_tx[j][inst->del_buf_txin_ind] = data_adc[j][i];
      data_out[j][i] = inst->del_buf_tx[j][inst->del_buf_txout_ind];
    }
    
    /* adjust circular buffer pointers */
    inst->del_buf_txin_ind++;  
    if(inst->del_buf_txin_ind == AER_SIM_PIU_DEL_BUF_LEN_TX) {
      inst->del_buf_txin_ind = 0;
    }
      
    inst->del_buf_txout_ind++;
    if(inst->del_buf_txout_ind == AER_SIM_PIU_DEL_BUF_LEN_TX) {
      inst->del_buf_txout_ind = 0;
    }
  } /* for (i=0; i<inst->frame_size; i++) */
} /* piuSendIn */

/* nothing past this point */
