#ifndef _PIULOC_H
#define _PIULOC_H
/******************************************************************************
 * FILE PURPOSE: Definitions local to the PCM Interface Unit (PIU)
 ******************************************************************************
 * FILE NAME:   piuloc.h
 *
 * DESCRIPTION: This file contains API and definitions that are local to
 *              the PIU module.
 *
 * $Id: piuloc.h 1.19 1998/01/29 15:53:49 BOGDANK Exp $
 * 
 * (C) Copyright 2008, 1997, 1998, Texas Instruments, Inc.
 *****************************************************************************/

/* System level header files and utilities' API's */
#include <ti/mas/types/types.h>                      /* DSP types            */
#include <ti/mas/fract/fract.h>                      /* fractional macros    */
#include <ti/mas/aer/test/aersim/aersimcfg.h>


/******************************************************************************
 * DATA DEFINITION: PIU instance data structure
 ******************************************************************************
 * DESCRIPTION: This structure specifies the state of an instance of a PIU.
 *
 *****************************************************************************/
typedef struct piuInst_s {
  linSample del_buf_rx[AER_SIM_PIU_DEL_BUF_LEN_RX];
  linSample del_buf_tx[AER_SIM_NUM_BF_MIC][AER_SIM_PIU_DEL_BUF_LEN_TX];
  tint frame_size;
  tint del_buf_rxin_ind;
  tint del_buf_rxout_ind;
  tint del_buf_rxsync_ind;
  tint del_buf_txin_ind;
  tint del_buf_txout_ind;
} piuInst_t;

void piuInit(piuInst_t *inst, tint frm_size);
void piuReceiveIn(piuInst_t *inst, linSample *data_in, linSample *data_dac, 
                  linSample *data_rx_sync);
void piuSendIn(piuInst_t *inst, linSample data_adc[][AER_SIM_MAX_FRAME_LENGTH], 
                                linSample data_out[][AER_SIM_MAX_FRAME_LENGTH],
                                tint num_mic);

#endif /* _PIULOC_H */
/* nothing past this point */
