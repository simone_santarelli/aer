/******************************************************************************
 * FILE PURPOSE: Defines libarary directory name using getLibs
 ******************************************************************************
 * FILE NAME: package.xs
 *
 * DESCRIPTION: This file defines the library directory name for proper build
 *              in case a different directory name for storing library files 
 *              other than "lib" is used. XDC by default assumes that the 
 *              library directory is "lib" is not sepcifically indicated by use
 *              the attributes in a file called package.xs  
 *
 * TABS: NONE
 *
 * Copyright (C) 2006, Texas Instruments, Inc.
 *****************************************************************************/

function getLibs(prog)
{
  var lib="";

  /* "mangle" program build attrs into an appropriate directory name */
  if(prog.build.target.name == 'C55_large') {
    lib = "lib/aer_c.a55L;lib/aer_a.a55L";
  }
  else if (prog.build.target.name == 'C64P' && prog.build.target.suffix == '64P') {
    lib = "lib/aer_c.a64P;lib/aer_a.a64P";
  }
  else if (prog.build.target.name == 'C64P' && prog.build.target.suffix == 'e64P') {
    lib = "lib/aer_c.ae64P;lib/aer_a.ae64P";
  }
  else if (prog.build.target.name == 'C64P_big_endian' && prog.build.target.suffix == '64Pe') {
    lib = "lib/aer_c.a64Pe;lib/aer_a.a64Pe";
  }
  else if (prog.build.target.name == 'C64P_big_endian' && prog.build.target.suffix == 'e64Pe') {
    lib = "lib/aer_c.ae64Pe;lib/aer_a.ae64Pe";
  }
  else if (prog.build.target.name == 'C674' && prog.build.target.suffix == '674') {
    lib = "lib/aer_c.a674;lib/aer_a.a674";
  }
  else if (prog.build.target.name == 'C674' && prog.build.target.suffix == 'e674') {
    lib = "lib/aer_c.ae674;lib/aer_a.ae674";
  }
  else if (prog.build.target.name == 'GCArmv7A') {
    lib = "lib/aer_c.av7A;lib/aer_a.av7A;lib/aer_c.av7A";
  }

  return (lib);
}
/* nothing past this point */
