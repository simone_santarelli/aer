#ifndef _AER_H
#define _AER_H

/**
 *  @file   aer.h
 *  @brief  Contains external API for the AER component.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 */

/* System include files and utilities' APIs */
#include <ti/mas/types/types.h>               /* DSP types                   */
#include <ti/mas/util/ecomem.h>               /* memory descriptor structure */
#include <ti/mas/util/debug.h>                /* debuging utility            */

/* Define AER Module as a master group in Doxygen format and add all AER API 
   definitions to this group. */
/** @defgroup aer_module AER Module API
 *  @{
 */
/** @} */

/** @defgroup aer_api_functions AER Functions
 *  @ingroup aer_module
 */

/** @defgroup aer_api_structures AER Data Structures
 *  @ingroup aer_module
 */

/** @defgroup aer_api_constants AER Constants (enum's and define's)
 *  @ingroup aer_module
 */

/** @defgroup aer_err_code AER Error Codes
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER error codes
 *
 *  Error codes returned by AER API functions.
 */
/*@{*/
enum {
  aer_NOERR              = 0,  /**< success, no error */
  aer_ERR_NOTCREATED     = 1,  /**< aerCreate() has not been called yet        */
  aer_ERR_NOMEMORY       = 2,  /**< supplied memory are not enough             */
  aer_ERR_NOTOPENED      = 3,  /**< AER instance has not been opened yet       */
  aer_ERR_NOTCLOSED      = 4,  /**< AER instance has not been closed yet       */
  aer_ERR_INVALIDPAR     = 5,  /**< configuration parameter is invalid         */
  aer_ERR_MEMBUFVECOVF   = 6,  /**< no enough space to return buffer addresses */
  aer_ERR_NOINITGAIN     = 7,  /**< tx/rx initial analog gains not set         */
  aer_ERR_LOAD_INPROCESS = 8   /**< loading of tail model not finished         */
};
/*@}*/
/** @} */


/** @defgroup aer_samp_rate AER Sampling Rates Definitions
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER sampling rates
 *
 *  Sampling rates that are supported by AER. Used to define max_sampling_rate
 *  of \ref aerCreateConfig_t.
*/
/*@{*/
enum {
  aer_SRATE_8K  = 0,                   /**< 8kHz  */
  aer_SRATE_16K = 1                    /**< 16kHz */
};

typedef tint aerSrate_t;               /**< used by \ref aerCreateConfig_t   */

/*@}*/
/** @} */

/** @addtogroup aer_samp_rate
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER sampling rate bitfield
 *
 *  AER sampling rate bitfield that specifies the sampling rates at four 
 *  interfaces: Rx in, Tx out, Rx out and Tx in. It is required that Rx out
 *  and Tx in have the same sampling rate. 
 *
 *  @verbatim
    Four interfaces of AER:
             Tx in          Tx out
     |------|  ->  |-------| ->  |---------|
     | TDM  |      |  AER  |     | Network |
     |------|  <-  |-------| <-  |---------|
             Rx out         Rx in 

    Sampling rate bitfield: 
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

    bits (from LSB)    values   meaning
     0                   0       Rx in at 8kHz
                         1       Rx in at 16kHz
     1                   0       Tx out at 8kHz
                         1       Tx out at 16kHz
     2                   0       Rx out and Tx in at 8kHz
                         1       Rx out and Tx in at 16kHz
     3 - 15                      RESERVED
    \endverbatim
 */
/*@{*/
enum {
  aer_SRATE_BIT_RXIN       = 0, /**< Bit 0 indicates Rx in sampling rate:
                                     0 = 8kHz, 1 = 16kHz.                     */
  aer_SRATE_BIT_TXOUT      = 1, /**< Bit 1 indicates Tx out sampling rate:
                                     0 = 8kHz, 1 = 16kHz.                     */
  aer_SRATE_BIT_RXOUT_TXIN = 2  /**< Bit 2 indicates Rx out and Tx in sampling 
                                     rate: 0 = 8kHz, 1 = 16kHz.               */
};
/*@}*/
/** @} */

/** @addtogroup aer_samp_rate
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER sampling rate bitfield masks
 *
 *  Masks that are used to construct the sampling rate bitfield of aerConfig_s
 *  and aerControl_s. 
 *
 */
/*@{*/
#define aer_SRATE_RXIN_16K  (aer_SRATE_16K<<aer_SRATE_BIT_RXIN)
#define aer_SRATE_RXIN_8K  (aer_SRATE_8K<<aer_SRATE_BIT_RXIN)
#define aer_SRATE_TXOUT_16K (aer_SRATE_16K<<aer_SRATE_BIT_TXOUT)
#define aer_SRATE_TXOUT_8K (aer_SRATE_8K<<aer_SRATE_BIT_TXOUT)
#define aer_SRATE_RXOUT_TXIN_16K (aer_SRATE_16K<<aer_SRATE_BIT_RXOUT_TXIN)
#define aer_SRATE_RXOUT_TXIN_8K (aer_SRATE_8K<<aer_SRATE_BIT_RXOUT_TXIN)
/*@}*/
/** @} */


/** @defgroup aer_create_bitfield AER Create Bitfield Masks
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER create bitfield masks
 *
 *  Bitfield mask to construct config_bitfield of aerCreateConfig_s.
 * 
    @verbatim

    Create bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       request support of multiple HF microphones
                   0       don't request support of multiple HF microphones
        0 - 15             RESERVED
    \endverbatim
 */                                           
/*@{*/
#define aer_CRT_MULT_MIC_MODE  0x0001    /**< request multiple HF microphones */
/*@}*/
/** @} */

/** @defgroup aer_size_cfg_bitfield AER GetSize Configuration Bitfield Masks
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER getSize configuration bitfield masks
 *
 *  Bitfield mask to construct config_bitfield of aerSizeConfig_s.
 *
    @verbatim

    Size config bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       request memory for FDNLP
                   0       don't request memory for FDNLP
        1          1       use bandsplit to save memory
                   0       don't use bandsplit 
        2 - 15             RESERVED
    \endverbatim
 */                                           
/*@{*/     
#define aer_SIZE_FDNLP_REQ     0x0001      /**< Request FDNLP memory          */
#define aer_SIZE_USE_BANDSPLIT 0x0002      /**< Use bandsplit to save memory  */
/*@}*/     
/** @} */


/** @defgroup aer_phone_mode AER Phone Modes
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER phone modes
 *
 *  Parameter phone_mode of aerConfig_s or aerControl_s should be set to
 *  one of these phone modes.
 *  
 */
/*@{*/
#define aer_PHM_HS                     0x8000 /**< Handset                    */
#define aer_PHM_HES                    0x4000 /**< Headset                    */
#define aer_PHM_HF                     0x2000 /**< Hands free                 */
#define aer_PHM_HSGL                   0x1000 /**< Handset group listen       */
#define aer_PHM_HESGL                  0x0800 /**< Headset group listen       */
/*@}*/
/** @} */


/** @defgroup aer_control_bitfields AER Control Bitfield Masks
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER control bitfield masks
 *
 *  Bitfield masks aer_CTL0_XYZ and aer_CTL1_XYZ may be used to change AER 
 *  operation modes through aerControl(), by constructing mask and value of 
 *  aerModeControl_s.
 *  
 *  The control codes to change operation modes are aer_CTL_MODES_0 for control
 *  bitfield 0 and aer_CTL_MODES_1 for control bitfield 1. The corresponding 
 *  parameter of aerControl_s is ctl_modes.
 *
 *  @verbatim
    Control bitfield 0: 
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       Enable AER or turn on AER.
                   0       Disable AER or turn off AER.
                           Note: when AER disabled, aerSendIn and aerReceiveIn
                                 will still process data by applying equalizer
                                 and digital gains. Upsampling and downsampling
                                 will also be applied if necessary (in and out
                                 sampling rates are different).
        1          1       Enable update (tail model and NLP state machine).
                   0       Disable update.
        2          1       Enable NLP.
                   0       Disable NLP.
        3          1       Continue updating AER when phone is in mute.
                   0       Stop updating AER when phone is in mute.
                           Note: If mute is achieved by cutting off Tx path PGA
                                 output, this bit should not be set. If mute is
                                 achieved by providing a zero signal to vocoder,
                                 and PGA output is still sent to AER to process,
                                 this bit should be set. It is recommended that
                                 mute be implemented the second way and this bit
                                 bet set. Otherwise, when phone goes out of mute,
                                 some brief clik or echo leakage may be heard.
        4          1       Clear tail model filter coefficients.
                   0       Do nothing.
                           Note: coefficients are not cleared in aerContro(), but
                                 in aerSendIn() of next frame processing.
                           Note: this bit will be automatically cleared once the
                                 the coefficients are cleared by AER.
        5          1       Clear all AER states and delay lines and reset tail
                           model, but will not reset configurable parameters 
                           (including gains and equalizer).
                   0       Do nothing.
                           Note: reset is not performed in aerContro(), but
                                 in aerSendIn() of next frame processing.
                           Note: this bit will be automatically cleared once the
                                 clear all reset is finished by AER.
        6          1       Put AER in half duplex mode.
                   0       Put AER in full duplex mode.
        7          1       Relax Tx NLP during Tx/Rx mutual silence.
                   0       Don't relax Tx NLP in mutual silence.
        8          1       Disable Rx NLP.
                   0       Enable Rx NLP.
        9          1       Disable Tx NLP.
                   0       Enable Tx NLP.
        10         1       Disable Tx NLP clipper.
                   0       Enable Tx NLP clipper.
        11         1       Enable normal Rx comfort noise generation (CNG).
                   0       Disable normal Rx CNG.
        12         1       Enable normal Tx comfort noise generation (CNG).
                   0       Disable normal Tx CNG.
        13         1       Enable forced Rx CNG - full configured CNG overwrites
                           Rx path signal.
                   0       Disable forced Rx CNG.
        14         1       Enable forced Tx CNG - full configured CNG overwrites
                           Tx path signal.
                   0       Disable forced Tx CNG.
                           Note: Forced Tx CNG is available when TDNLP is used,
                                 but not available when FDNLP is used.
        15         1       High band attenuation follows low band for 
                           subdominant path.
                   0       Infinite high band attenuation for subdominant path.
    \endverbatim
  *

    @verbatim
    Control bitfield 1: 
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       Enable noise guard.
                   0       Disable noise guard.
        1          1       Enable Tx equalizer.
                   0       Disable Tx equalizer.
        2          1       Enable Rx equalizer.
                   0       Disable Rx equalizer.
        3          1       Enable HLC.
                   0       Disable HLC.
        4          1       AER is informed that phone is on mute.
                   0       AER is informed that phone is not on mute.
        5          1       Enable scaling of NLP gain ramp. All four NLP linear 
                           attenuation ramping in/out time constants will be 
                           decreased by 1 for every 6 dB that the linear 
                           attenuation goes above 6dB, until they reach a 
                           minimum of 1. 
                   0       Disable scaling of NLP gain ramp.
        6          1       Use frequency domain NLP (FDNLP)
                   0       Use time domain NLP (TDNLP)
                           Note: in order to use FDNLP, memory for FDNLP must 
                                 first be requested throught aerGetSizes()
                                 by setting bit 0 of config_bitfield in 
                                 aerSizeConfig_t. Otherwise, the setting of
                                 this bit has no effect.
        7          1       Enable adaptive spectral noise reduction (ASNR) in 
                           send path.
                   0       Disable ASNR in send path.
                           Note: bit 6 must be set to 1 (use FDNLP) in order to 
                                 enable ASNR. Otherwise, setting of this bit has
                                 no effect.
        8          1       Generate adaptive comfort noise (adaptive CNG) in
                           send path. 
                   0       Generate fixed comfort noise (fixed CNG) in send path.
                           Note: bit 6 must be set to 1 (use FDNLP) in order to 
                                 generate adaptive comfort noise. Otherwise, 
                                 setting of this bit has no effect.
        9          1       Freeze tail model coefficients update, which may be
                           used for MIPS control.
                           (will not disable state machine update)
                   0       Do nothing.
        10         1       Perform a partial reset to clear delay lines and
                           state information, which may be used when switching
                           phone mode on the same instance.
                           Note: AER will NOT automatically perform a partial
                                 reset when being informed of phone mode change
                                 through aerControl() with control code 
                                 aer_CTL_PHONE_MODE.
                           Note: this bit will be automatically cleared once the
                                 partial reset is finished by AER.
                   0       Do nothing.
        11         1       Keep clipper during double talk
                   0       Remove clipper completely during double talk        
        12                 RESERVED       
        13         1       Set minimum tail length, which convergence starts 
                           with, to total tail length, for non-handsfree phone 
                           mode.
                   0       Don't increase minimum tail length.
        14         1       Drop Tx high band signal at send path output.
                   0       Keep Tx high band signal at send path output. 
        15         1       Drop Rx high band signal at receive path output.
                   0       Keep Rx high band signal at receive path output.
    \endverbatim
 */


/*@{*/
#define aer_CTL0_ENABLE_ECHO_CANCELLER 0x0001 /**< enable AER              */
#define aer_CTL0_ENABLE_UPDATE         0x0002 /**< adapt filter and NLP    */
#define aer_CTL0_ENABLE_NLP            0x0004 /**< enable NLP              */
#define aer_CTL0_ADAPT_IN_MUTE         0x0008 /**< adapt filter in mute    */
#define aer_CTL0_CLEAR_FILTER          0x0010 /**< clear tail model        */
#define aer_CTL0_CLEAR_ALL             0x0020 /**< clear everything,restart*/
#define aer_CTL0_HALF_DUPLEX           0x0040 /**< AEC halfduplex mode     */
#define aer_CTL0_RELAX_TXNLP_IDLE      0x0080 /**< relax Tx NLP during
                                               *   mutual silence          */
#define aer_CTL0_DISABLE_RX_NLP        0x0100 /**< disable Rx NLP atten    */
#define aer_CTL0_DISABLE_TX_NLP        0x0200 /**< disable Tx NLP atten    */
#define aer_CTL0_DISABLE_TX_CLIPPER    0x0400 /**< disable Tx NLP clipper  */
#define aer_CTL0_ENABLE_NORMAL_RX_CNG  0x0800 /**< enable normal Rx CNG    */
#define aer_CTL0_ENABLE_NORMAL_TX_CNG  0x1000 /**< enable normal Tx CNG    */
#define aer_CTL0_ENABLE_FORCED_RX_CNG  0x2000 /**< enable forced Rx CNG    */
#define aer_CTL0_ENABLE_FORCED_TX_CNG  0x4000 /**< enable forced Tx CNG    */
#define aer_CTL0_BS_HIGH_BAND_LOCK     0x8000 /**< high band of subdominant
                                               * path's attenuation follows
                                               * low band                  */
#define aer_CTL1_ENABLE_NGUARD        0x0001 /**< enable noise guard       */
#define aer_CTL1_ENABLE_TX_EQ         0x0002 /**< enable Tx path EQ        */
#define aer_CTL1_ENABLE_RX_EQ         0x0004 /**< enable Rx path EQ        */
#define aer_CTL1_ENABLE_HLC           0x0008 /**< enable HLC in Rx path    */
#define aer_CTL1_MUTE_ON              0x0010 /**< notify send path is muted*/
#define aer_CTL1_NLP_RAMP_SCALE_ON    0x0020 /**< enable scaling of   
                                              *   NLP gain ramp            */
#define aer_CTL1_USE_FDNLP            0x0040 /**< use frequency domain NLP */
#define aer_CTL1_ENABLE_NR            0x0080 /**< enable ASNR              */
#define aer_CTL1_CNG_ADAPT            0x0100 /**< use adaptive comfort noise*/
#define aer_CTL1_FREEZE_TAIL_UPDATE   0x0200 /**< freeze tail model update  */
#define aer_CTL1_PARTIAL_RESET        0x0400 /**< peform partial reset      */
#define aer_CTL1_BIT_11_RESERVED      0x0800 /**< reserved                  */
#define aer_CTL1_RESET_PARAMS         0x1000 /**< reset config. params      */
#define aer_CTL1_INCREASE_MIN_HVEC    0x2000 /**< min tail length is        */
                                             /**< set to total tail length  */
#define aer_CTL1_DROP_TX_HIGH_BAND    0x4000 /**< drop Tx high band         */
#define aer_CTL1_DROP_RX_HIGH_BAND    0x8000 /**< drop Rx high band         */

#define aer_CTL2_MICSAT               0x0001 /**< indicate mic saturation     */
#define aer_CTL2_ENABLE_TRACE         0x0002 /**< enable tracing              */
#define aer_CTL2_SKIP_COHERENCE       0x0004 /**< skip coherence detection    */
#define aer_CTL2_FREEZE_NOISE_EST     0x0008 /**< freeze ASNR/CNG noise estimate */
#define aer_CTL2_BIT_4_RESERVED       0x0010 /**< bit 4 is reserved           */
#define aer_CTL2_BIT_5_RESERVED       0x0020 /**< bit 5 is reserved           */
#define aer_CTL2_BIT_6_RESERVED       0x0040 /**< bit 6 is reserved           */
#define aer_CTL2_BIT_7_RESERVED       0x0080 /**< bit 7 is reserved           */
#define aer_CTL2_BIT_8_RESERVED       0x0100 /**< bit 8 is reserved           */     
#define aer_CTL2_BIT_9_RESERVED       0x0200 /**< bit 9 is reserved           */
#define aer_CTL2_BIT_10_RESERVED      0x0400 /**< bit 10 is reserved          */
#define aer_CTL2_BIT_11_RESERVED      0x0800 /**< bit 11 is reserved          */
#define aer_CTL2_BIT_12_RESERVED      0x1000 /**< bit 12 is reserved          */
#define aer_CTL2_BIT_13_RESERVED      0x2000 /**< bit 13 is reserved          */
#define aer_CTL2_BIT_14_RESERVED      0x4000 /**< bit 14 is reserved          */
#define aer_CTL2_BIT_15_RESERVED      0x8000 /**< bit 15 is reserved          */
/*@}*/
/** @} */


/** @defgroup aer_reset_bitfield AER Reset Bitfield Masks
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER reset bitfield mask
 *
 *  Bitfield mask aer_RESET_XYZ to reset AER information through aerOpen(). 
 *  Used to construct reset_bitfield of aerConfig_s. 
 *
    @verbatim

    Reset bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       perform full reset (see remark below about full reset)
                   0       don't perform full reset
                           Note: if the bit is 1, all other bits in this bitfield
                                 will be ignored.
        1          1       reset adaptive tail model
                   0       keep adaptive tail model
        2          1       reset Tx/Rx analog and digital gains to default
                   0       keep Tx/Rx analog and digital gains
        3          1       reset Tx/Rx equalizer filter coefficients to all 0's
                   0       keep Tx/Rx equalizer filter coefficients
        4          1       reset all non-system parameters (those configurable
                           only through aerControl, not aerOpen) except Tx/Rx
                           gains and equalizer coefficients.
                   0       keep all other parameters
        5 - 15             RESERVED
    \endverbatim
 *
 * @remark The full reset does the following:
 *         - reset all parameters to their default values
 *         - clear all AER states and delay lines including gains
 *         - reset equalizer filter coefficients
 *         - reset tail model filter coefficients
 *         - control bitfield 0 and 1 (\ref aer_control_bitfields) to their 
 *           default values
 */
/*@{*/
#define aer_RESET_FULL             0x0001   /**< full reset             */
#define aer_RESET_TAIL_MODEL       0x0002   /**< reset tail model       */
#define aer_RESET_TXRX_GAINS       0x0004   /**< reset Tx/Rx gains      */
#define aer_RESET_TXRX_EQ_FILT     0x0008   /**< reset Tx/Rx Eq parameters    */
#define aer_RESET_OTHER_PARAMS     0x0010   /**< reset all other parameters   */
/*@}*/
/** @} */


/** @defgroup aer_cfg_valid_bitfield AER Configuration Valid Bitfield Masks
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER configuration valid bitfield masks
 *
 *  Bitfield mask aer_CFG_VALID_XYZ to configure AER system parameters 
 *  through aerOpen(). Used to construct valid_bitfield of aerConfig_s.
 *
    @verbatim

    Config valid bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       configure y to x delay
                   0       don't configure y to x delay
        1          1       configure sampling rates
                   0       don't configure sampling rates
        2          1       configure tail length
                   0       don't configure tail length
        3          1       configure delay of setting Tx analog gain
                   0       don't configure delay of setting Tx analog gain
        4          1       configure delay of setting Rx analog gain
                   0       don't configure delay of setting Rx analog gain
        5          1       configure number of interpolation samples for Tx path
                           gain change
                   0       don't configure number of interpolation samples for
                           Tx path gain change
        6          1       configure phone mode
                   0       don't configure phone mode
        7 - 15             RESERVED
    \endverbatim
 */
/*@{*/
#define aer_CFG_VALID_Y2X_DELAY    0x0001  /**< config y2x delay              */
#define aer_CFG_VALID_SAMP_RATES   0x0002  /**< config samp. rates            */
#define aer_CFG_VALID_TAIL_LENGTH  0x0004  /**< config tail length            */
#define aer_CFG_VALID_DELAY_TXAG   0x0008  /**< config Tx analog gain
                                            *   setting delay                 */
#define aer_CFG_VALID_DELAY_RXAG   0x0010  /**< config Rx analog gain
                                            *   setting delay                 */
#define aer_CFG_VALID_INTER_SAMP   0x0020  /**< config interpolation samples
                                                for Tx gain change            */
#define aer_CFG_VALID_PHONE_MODE   0x0040  /**< config phone mode             */
#define aer_CFG_VALID_BITS_ALL_1   0x007F  /**< all bits must be set to 1 when
                                            *   aerOpen() is called the first 
                                            *   time after aerNew() is called.*/
/*@}*/     
/** @} */
                                          

/** @defgroup aer_warning_codes AER Warning Messages
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER Warning Messages
 *
 *  Used by debugInfo provided by aerCreateConfig_s to pass warning code.
 */
/*@{*/
enum {
  aer_WARN_PATH_SELECT  = 1, /**< error in path select state machine          */
  aer_WARN_RX_NLP       = 2, /**< error in Rx NLP state machine               */
  aer_WARN_TX_NLP       = 3, /**< error in Tx NLP state machine               */
  aer_WARN_COMPUTE_CLIP = 4  /**< error in clipper calculation state machine  */               
};
/*@}*/
/** @} */


/** 
 *  @ingroup aer_api_structures
 *  @brief AER information reporting structure
 *  \brief This strucutre contains the parameters that AER needs to report to 
 *          an outside ECO through eventOut() function provided by 
 *          aerCreateConfig_s.
 */
typedef struct aerInfo_s {                    
  tbool  active_path;           /**< Enabled path. send: TRUE, receive: FALSE   */
  tbool  nlp_on;                /**< nlp_on = TRUE, NLP is enabled              */            
  tint   max_canc_l2;           /**< Maximal ERLE (Echo Return Loss Enhancement)*/
  tint   rx_to_tx_hangover;     /**< Current hangover count to decide making a 
                                 *   switch from receive path                   */
  tint   tx_to_rx_hangover;     /**< Current hangover count to decide making a 
                                 *   switch from send path                      */
  tint   switch2tx_period;      /**< Indicates receive to send NLP path change  */ 
  tint   switch2rx_period;      /**< Indicates send to receive NLP path change  */ 
  Fract  maxabs_pred_echo;      /**< Max absolute predicted echo over a frame   */
  LFract pred_echo_pwr;         /**< Predicted echo power                       */
  LFract tx_mic_pwr;            /**< Send frame power including echo            */
  LFract tx_mic_pwr_minus_echo; /**< Send frame power after echo subtraction    */ 
} aerInfo_t;

/** 
 *  @ingroup aer_api_structures
 *
 *  @brief Configuration structure for aerCreate()
 *  \brief This structure contains information to be shared by different AER 
 *         instances. This structure is used by function aerCreate().
 *
 */
typedef struct aerCreateConfig_s {
  dbgInfo_t   debugInfo;         /**< Function pointer used for debugging. 
              Type dbgInfo_t is defined in debug.h of util package:
              typedef void (*dbgInfo_t)(void *, tuint, tuint, tuint, tuint*).
              When AER calls this function, it passes the following information 
              through function arguments (from left to right):
              - void * handle: AER instance handle provided in aerNewConfig_s;
              - tuint dbgMsg : debugging message, either dbg_WARNING or 
                               dbg_TRACE_INFO, defined in util\debug.h.
              - tuint dbgCode: debugging code.
                  - warning code when dbgMsg is dbg_WARNING. 
                    See \ref aer_warning_codes.
                  - data tracing code when dbgMsg is dbg_TRACE_INFO. Currently,
                    AER doesn't have data tracing.
              - tuint dbgSize: size of AER tracing data per call.
              - tuint *dbgData: location where AER tracing data is stored.    */
  void        (*eventOut)(void *handle, aerInfo_t *aer_info);
                                 /**< Function pointer to relay internal AER 
                                  *   information to other ECOs.              */  
  aerSrate_t  max_sampling_rate; /**< Maximum sampling rate of the system:    
                                      8kHz:  set to aer_SRATE_8K. 
                                      16kHz: set to aer_SRATE_16K.            */  
  tuint       config_bitfield;   /**< Bitfield constructed by aer_CRT_XYZ.    
                                  *   see \ref aer_create_bitfield            */
} aerCreateConfig_t;

/** 
 *  @ingroup aer_api_structures
 *
 *  @brief Configuration structure for aerGetSizes()
 *  \brief This structure specifies the maximum filter length and maximum y to x
 *         delay. It also has a bitfield, which currently has only one bit 
 *         to indicate whether FDNLP is requested. It is used by function 
 *         aerGetSizes(). It is also part of structure aerNewConfig_s used by
 *         aerNew().
 */
typedef struct aerSizeConfig_s {
  tint      max_tail_length;     /**< Maximum supported tail length in units of
                                   *  125usec, and must be an integral multiple 
                                   *  of 20 msec (i.e, 160*125usec).          */
  tint      max_y2x_delay;       /**< Maximum system delay between receive and  
                                   *  send frame data, # of 125 micro seconds.*/
  tuint     config_bitfield;     /**< Bitfield constructed by aer_SIZE_XYZ.
                                   *  See \ref aer_size_cfg_bitfield.         */
} aerSizeConfig_t;

/** 
 *  @ingroup aer_api_structures
 *
 *  @brief Configuration structure for aerNew()
 *  \brief This structure is used for creating a new AER instance through 
 *         function aerNew().
 */
typedef struct aerNewConfig_s {
  void  *handle;                                     /**< handle              */
  aerSizeConfig_t  sizeCfg;                          /**< size configration   */
} aerNewConfig_t;


/** 
 *  @ingroup aer_api_structures
 *
 *  @brief Configuration structure for aerOpen()
 *  \brief This structure contains system parameters to configure an AER 
 *         instance through aerOpen(). AER doesn't have default value for these
 *         parameters, and they must be set explicitly when aerOpen() is called
 *         the first time after aerNew(). After that, these prameters may also 
 *         be changed through aerControl().
 */
typedef struct aerConfig_s {
  tuint   reset_bitfield;  /**< Used to reset certain information specified by
                                aer_RESET_XYZ. Will not affect system parameters
                                contained in aerConfig_t. This bitfield is 
                                ignored when aerOpen() is called the first time
                                after aerNew() is called.
                             *  See \ref aer_reset_bitfield.                  */
  tuint   valid_bitfield;  /**< Used to indicate which of the system parameters
                                in aerConfig_t are valid and can be read by 
                                aerOpen(). It must be set to all 1's
                                (aer_CFG_VALID_BITS_ALL_1) to configure all
                                system parameters when aerOpen() is called 
                                the first time after aerNew() is called.
                             *  See \ref aer_cfg_valid_bitfield.              */
  tint    y2x_delay;       /**< Corresponding to aer_CFG_VALID_Y2X_DELAY. See
                                description of y2x_delay in aerControl_s.     */
  tuint   srate_bitfield;  /**< Corresponding to aer_CFG_VALID_SAMP_RATES. See 
                                description of srate_bitfield in aerControl_s.*/
  tint    tail_length;     /**< Corresponding to aer_CFG_VALID_TAIL_LENGTH. See
                                description of tail_length in aerControl_s.   */
  tint    delay_tx_ag_chg; /**< Corresponding to aer_CFG_VALID_DELAY_TXAG. See
                                description of delay_tx_ag_chg in aerControl_s.*/
  tint    delay_rx_ag_chg; /**< Corresponding to aer_CFG_VALID_DELAY_RXAG. See
                                description of delay_rx_ag_chg in aerControl_s.*/
  tint    num_samp_interp; /**< Corresponding to aer_CFG_VALID_INTER_SAMP. See
                                description of num_samp_interp in aerControl_s.*/
  tint    phone_mode;      /**< Corresponding to aer_CFG_VALID_PHONE_MODE. See
                                description of phone_mode in aerControl_s.     */
} aerConfig_t;


/** @defgroup aer_eq_control_codes AER Equalizer Control Codes
 *  @ingroup aer_api_constants
 *  @{
 * 
 *  @name AER Equalizer control codes
 *
 *  Constants for loading Tx/Rx equalizer parameters. Used to set tx_or_rx 
 *  and num_params of aerEqConfig_s.
 */                                           
/*@{*/
#define aer_EQBQ_MAX_NUM_SECTION_PARAMS 17 /**< Max # of section parameters,
                                                which includes up to 2 LP/HP and
                                                up to 5 BP/BS sections.       */

/*@}*/                                        
/** @} */

/** 
 *  @ingroup aer_api_structures
 *
 *  @brief AER Bi-quad EQ configuration structure.
 *
 *  \brief This structure contains Bi-quad EQ configurations.
 *         It includes: 
 *         -  Total number of sections 
 *         -  Sections types. 
 *            \n There are 4 different types of sections used in an EQ.  
 *            Each type is encoded using 2 bits(aer_EQBQ_SECTION_TYPE_xx). 
 *            A 16-bit bitmap is used to represent the types for all the 
 *            sections in the EQ.
 *            The least significant 2 bits are used for the 1st
 *            section, the next 2 bits are used for the 2nd 
 *            section and so on till bit 12&13 which are 
 *            used for the last section.
 *            Bit 14 is used to indicate LP filter order. If 1, 2nd
 *            order LP filter is used. Otherwise, 1st order LP is used.
 *            Bit 15 is used to indicate HP filter order. If 1, 2nd
 *            order HP filter is used. Otherwise, 1st order HP is used.
 *         -  Section parameters. 
 *            \n Parameters are set in the order of sections. 
 *            A LP or HP section has one parameter - alpha.
 *            A BP or BS section has three parameters and they are in the
 *            order of alpha, gamma and G. Implementation of LP/HP and BP/BS
 *            filters and the corresponding parameters are shown in the figure
 *            below.
 *            All section parameters are in Q15 format.
 *
 * @image html bieq.jpg  
 */
typedef struct aerEQConfig_s {
  tint      num_sections;                /**< Number of sections used in the EQBQ */                                
    #define aer_EQBQ_MAX_NUM_SECTIONS  7 /**< Maximum number of sections.  */

  tuint section_types;    /**< Type bitmap consists all the section types 
                               (as defined by aer_EQBQ_SECTION_TYPE_XX) in an EQ.
                                   Bit 0-1: 1st section type                                
                                   Bit 2-3: 2nd section type
                                       �  
                                   Bit 12-13: 7th section type 
                                   Bit 14: if 1, 2nd order LP is used 
                                   Bit 15: if 1, 2nd order HP is used 
                                   Note: Bits are arranged in the 
                                   order of sections. */
    #define aer_EQBQ_SECTION_TYPE_LP             0x0000 /**< Low Pass Filter  */
    #define aer_EQBQ_SECTION_TYPE_HP             0x0001 /**< High Pass Filter */
    #define aer_EQBQ_SECTION_TYPE_BP             0x0002 /**< Band Pass Filter */
    #define aer_EQBQ_SECTION_TYPE_BS             0x0003 /**< Band stop Filter */ 
    #define aer_EQBQ_SECTION_TYPE_2NDORDER_LPBIT     14 /**< 2nd order LP     */ 
    #define aer_EQBQ_SECTION_TYPE_2NDORDER_HPBIT     15 /**< 2nd order HP     */ 

  Fract section_params[aer_EQBQ_MAX_NUM_SECTION_PARAMS];/**< Section Parameters arranged   
                                                             in the order of sections.*/
} aerEqConfig_t;

/** 
 *  @ingroup aer_api_structures
 *
 *  @brief AER mode control structure.
 *  \brief This structure is used to change AER operational modes through
 *         function aerControl() using control code aer_CTL_MODES_0 
 *         or aer_CTL_MODES_1. For example, to enable AER, enable update, and
 *         disable NLP would need to set the mask and value to:
 *
   @verbatim
    mask = aer_CTL0_ENABLE_ECHO_CANCELLER | aer_CTL0_ENABLE_UPDATE
            | aer_CTL0_ENABLE_NLP;
    value = aer_CTL0_ENABLE_ECHO_CANCELLER | aer_CTL0_ENABLE_UPDATE;
   \endverbatim
 */
typedef struct aerModeControl_s{
  tuint mask;           /**< tells which bits are to be changed.             */
  tuint value;          /**< tells new value of each bit indicated by mask.  */
                        /**< aer_CTL0_XYZ and aer_CTL1_XYZ can be used to 
                             construct mask and value.                       */
} aerModeControl_t;


/** 
 *  @ingroup aer_api_structures
 *
 *  @brief AER tracing configuration structure.
 *  \brief This structure is used to configure AER tracing options and obtain
 *         from AER the size of traced data based on the configuration. The control
 *         valid bit for configuring trace is \ref aer_CTL_VALID2_TRACE_CONFIG.
 *
 *  \remark 
 *  - Tracing is off by default.
 *  - Tracing is not changed by AER reset or re-opening of an instance, i.e. if 
 *    tracing is on, it will still be on after reset or re-opening, and same 
 *    for tracing off.
 *  - If tracing is turned on, AER instance will always be traced out by default, 
 *    regardless of trace configuration, and will be written to both Tx and Rx
 *    tracing buffers.
 *  - If tracing is turned on but no buffer is provided to aerReceiveIn() or
 *    aerSendIn(), no data will be written out.
 */
typedef struct aerTraceConfig_s {
  tuint trace_bitfield; /**< Bitfield to configure AER tracing options. 
                             See \ref aer_trace_bitfield for bit masks.       */
  tsize get_size_rx;    /**< Return value by aerControl() - size of Rx traced
                             data in units of twords based on trace_bitfield. 
                             Refer to types.h for definition of twords.       
							 See \ref aer_trace_max_sizes for maximum sizes.*/
  tsize get_size_tx;    /**< Return value by aerControl() - size of Tx traced
                             data in units of twords based on trace_bitfield. */
} aerTraceConfig_t;

/** @defgroup aer_trace_max_sizes AER Trace Maximum Sizes
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER trace maximum sizes for Tx and Rx
 *
 */
/*@{*/
#define aer_TRACE_MAX_SIZE_TX (3630 * sizeof(Fract))  /**< Maximum size of Tx
                                                           tracing data       */
#define aer_TRACE_MAX_SIZE_RX (400 * sizeof(Fract))   /**< Maximum size of Rx
                                                           tracing data       */
/*@}*/
/** @} */


/** @defgroup aer_trace_bitfield AER Trace Config Bitfield Masks
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER trace config bitfield masks

 *  These bit masks may be used to contruct aerTraceConfig_s::trace_bitfield.
 *
    @verbatim
    Bits and explanations of trace_bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       adaptive filter coefficients will be traced
                   0       adaptive filter coefficients will not be traced
        1          1       coherence data will be traced
                   0       coherence data will not be traced
        2          1       FDNLP data will be traced
                   0       FDNLP data will not be traced
        3          1       ASNR data will be traced
                   0       ASNR data will not be traced
        4 - 15             RESERVED
    \endverbatim
 *
 */
/*@{*/
#define aer_TRACE_FILTER_COEF    0x0001       /**< trace filter coefficients  */
#define aer_TRACE_COH_DATA       0x0002       /**< trace coherence data       */
#define aer_TRACE_FDNLP_DATA     0x0004       /**< trace FDNLP data           */
#define aer_TRACE_ASNR_DATA      0x0008       /**< trace ASNR data            */
#define aer_TRACE_RESERVED_BITS  0xFFF0       /**< reserved for future usage  */
/*@}*/
/** @} */


/** 
 *  @ingroup aer_api_structures
 *
 *  @brief AER control structure
 *  \brief This structure is used to change AER configurable parameters and
 *         mode of operation through aerControl(). It has two elements, a 
 *         control code to tell what to change, and a union of parameters to
 *         tell the value of the new parameter.
 */
typedef struct aerControl_s {
  tuint valid_bitfield_0;
#define aer_CTL_VALID0_MODES_CTL0         0x0001
#define aer_CTL_VALID0_MODES_CTL1         0x0002
#define aer_CTL_VALID0_MODES_CTL2         0x0004
#define aer_CTL_VALID0_Y2X_DELAY          0x0008
#define aer_CTL_VALID0_SAMPLING_RATES     0x0010
#define aer_CTL_VALID0_TAIL_LENGTH        0x0020
#define aer_CTL_VALID0_TX_AG_CHG_DELAY    0x0040
#define aer_CTL_VALID0_RX_AG_CHG_DELAY    0x0080
#define aer_CTL_VALID0_TX_AG_CHG_INTERP   0x0100
#define aer_CTL_VALID0_PHONE_MODE         0x0200      
#define aer_CTL_VALID0_RX_ANALOG          0x0400
#define aer_CTL_VALID0_TX_ANALOG          0x0800
#define aer_CTL_VALID0_RX_DIGITAL         0x1000
#define aer_CTL_VALID0_TX_DIGITAL         0x2000
#define aer_CTL_VALID0_RX_EQ_PARAMS       0x4000
#define aer_CTL_VALID0_TX_EQ_PARAMS       0x8000
                                       
  aerModeControl_t modes_0;   /**< <B>Bit 0: AER operational modes bitfield 0: </B> */
  aerModeControl_t modes_1;   /**< <B>Bit 1: AER operational modes bitfield 1: </B> */
  aerModeControl_t modes_2;   /**< <B>Bit 2: AER operational modes bitfield 3: </B> 
      AER operation is defined by three bitfields, modes bitfield 0, 1 and 2.
      These bitfields disable or enable certain feature (or mode) such
      as Tx NLP, Rx NLP, CNG, ASNR, etc. The bit masks for these two bitfields 
      are defined in \ref aer_control_bitfields. 
      *
      Default values for bitfield 0, 1 and 2 are 0x0086, 0x0000 and 0x0000 respectively. */

  tint   y2x_delay;      /**< <B>Bit 3: System delay between AER receive out and 
      send in: </B> the delay between two interfaces referred to by the input 
      pointers of aerSendIn(): send_in and recv_out_sync. It is specified in 
      units of 125-usec (1/8000Hz) periods. For example, a value of 16 means 2 
    msec. This parameter does not have a default value and must be initialized
    through aerOpen(). 
      *
      \remark y2x_delay must be positive and less than or equal to 
      max_y2x_delay that is set in aerSizeConfig_t. Otherwise, aerControl() 
      or aerOpen() will return an error.                                      */

  tuint  srate_bitfield; /**< <B>Bit 4: Sampling rates bitfield:</B>
      This bitfield defines the sampling rates on the following 3 interfaces:
         - Bit 0: Rx in,        0: 8k samples/second, 1: 16k samples/second
         - Bit 1: Tx out,       0: 8k samples/second, 1: 16k samples/second
         - Bit 2: Rx out/Tx In, 0: 8k samples/second, 1: 16k samples/second
      * 
      The sampling rate at the speaker and the microphone must be the same. 
      See \ref aer_samp_rate for bit masks that may be used to construct this 
    bitfield. This parameter does not have a default value and must be initialized
    through aerOpen(). 
    
      \remark Sampling rates at all 3 interfaces must be less than or equal to
             the max_sampling_rate of aerCreateConfig_t. Otherwise, aerControl()
             or aerOpen() will return an error.                               */

  tint   tail_length;   /**< <B>Bit 5: Tail model filter length:</B>
      The AEC tail model filter length in units of 125usec. For handsfree and 
      headset phone mode, it is an integral multiple of 160 (20msec) up to 
      maximum tail length. For handset mode, it is an integral multiple of 
    32 (4msec) up to 160 (20msec). This parameter does not have a default value 
    and must be initialized through aerOpen(). 
      *
      \remark tail_length must be less than or equal to the max_tail_length 
      of aerSizeConfig_t. Otherwise, aerControl() or aerOpen() will return 
      an error.                                                               */

  tint   delay_tx_ag_chg; /**< <B>Bit 6: Tx path gain change synchronization 
      delay:</B> Tx path analog gain changes can happen either during testing/
      calibration when the user requests an explicit gain change, or during 
      normal operation when adaptive AGC induces gain change. In the latter 
      case, AGC may alter Tx path analog gain (tx_ag) to optimize the Tx path 
      digital dynamic range. Preserving the sum of tx_ag and AER Tx digital gain 
      (aer_tx_dg) that affects each sample will avoid transmitting unwanted 
      "clicks" duringgain transitions introduced by adaptive AGC. 
      *
      When a Tx path analog gain change is requested, factors like buffering in 
      the BSP, sampling frequency, phone mode, Tx path equalization filter, 
      hardware codec and software implementation of the gain interface may 
      impact the delay between the event of requesting a gain change and the 
      first sample that AER observes to be actually impacted by the gain.
      *
      This parameter informs AER about the delay that is talked about above. 
      The basic idea is to synchronize the transitions in tx_ag and aer_tx_dg  
      gains. When AER is informed about a pending tx_ag change, it makes a 
      corresponding change in its echo prediction after a delay of 
      delay_tx_ag_chg samples. The request to apply a new aer_tx_dg is also 
      processed after this delay. If properly configured, this parameter  will 
      reflect the number of future Tx path samples that will not be affected by 
      a pending tx_ag gain change. The valid range for this parameter is 
      [0, 240] (i.e. up to 30msec) and is specified in number of 8kHz samples 
    (or 125usec). One setting per phone mode should be available.           

    This parameter does not have a default value and must be initialized
    through aerOpen().                                                       */

  tint   delay_rx_ag_chg; /**< <B>Bit 7: Rx path gain change synchronization 
      delay:</B> When AER is informed about a pending Rx analog gain (rx_ag)
      change, it makes a corresponding change in its echo prediction after a 
      delay of delay_rx_ag_chg samples. Generally optimizing this parameter 
      is not recommended, as this can add value to the phone in only 
      extraordinary circumstances. Volume changes come with many more 
      significant disturbances due to a hand interfering with the acoustic echo 
      path to punch the volume button. Optimal values for delay_rx_ag_chg can 
      change with the sampling frequency, phone mode, Tx equalization filter, 
      hardware codec and software implementation of the gain interface.
      *
      The valid range for this parameter is [0, 240] (i.e. up to 30msec) and 
      is specified in number of 8kHz samples (or 125usec). One setting per phone 
    mode should be available. This parameter does not have a default value and 
    must be initialized through aerOpen().                                   */

  tint   num_samp_interp; /**< <B>Bit 8: Tx path analog gain change settling 
      period:</B> This parameter informs AER about the number of samples that 
      get garbled during Tx analog gain changes due to the ADC PGA settling 
      period. If this parameter is optimally configured, AER takes measures to 
      conceal this transient amplifier noise. The number of samples after a gain 
      change that ringing persists is observed in data to determine 
      num_samp_interp.
      *
      The valid range for this parameter is [1, mod(delay_tx_ag_chg, 80) - 1] 
      and is specified in number of 8kHz samples. If out of range, this 
    parameter has no effect. One setting per phone mode should be available.

    This parameter does not have a default value and must be initialized
    through aerOpen().                                                       */

  tint   phone_mode;     /**< <B>Bit 9: Supported phone mode:</B>
      AER supports 5 different phone modes: aer_PHM_HS (handset), aer_PHM_HES 
      (headset), aer_PHM_HF (hands-free), aer_PHM_HSGL (handset group listen), 
    and aer_PHM_HESGL (group listen with headset). See \ref aer_phone_mode. 

    This parameter does not have a default value and must be initialized
    through aerOpen().                                                       */ 

  Fract  gain_rx_analog; /**< <B>Bit 10: Receive path (speaker) analog gain:</B>
      This gain is relative to a constant reference in S11.4 dB (or in 1/16 dB
      units). The default is 0dB. It is applied downstream of AER in the receive 
      path. User adjustments of a programmable speaker volume should immediately 
      result in a aerControl() call with notification of a gain change to avoid 
      de-converging AER. 
      *
      \remark Before AER processing starts, this gain must be set explicitly to 
      an initial value through aerControl() in order to ensure proper tail model 
      rescaling. Otherwise, aerSendIn() and aerReceiveIn() will process data as 
    if AER is disabled.                                                     

    This parameter does not have a default value and must be initialized
    through aerOpen().                                                       */

  Fract  gain_tx_analog; /**< <B>Bit 11: Send path (mic) analog gain:</B>
      This gain is relative to a constant reference in S11.4 dB (or in 1/16 dB
      units). The default is 0dB. It is applied upstream of AER in the send path. 
      AGC adjustments of the microphone gain should immediately result in an 
      aerControl() call with notification of the gain change. 
      *
      \remark Note that activating the mic mute feature is achieved through 
      aerControl() call with ctl_code set to aer_CTL_MODES_1, but not by setting 
      tx_analog_gain to zero. 
      *
      \remark Before AER processing starts, this gain must be set explicitly to 
      an initial value through aerControl() in order to ensure proper tail model 
      rescaling. Otherwise, aerSendIn() and aerReceiveIn() will process data as 
    if AER is disabled.                                                     

    This parameter does not have a default value and must be initialized
    through aerOpen().                                                       */

  Fract  gain_rx_digital; /**< <B>Bit 12: Receive path digital gain:</B>
      AER Rx digital gain in S11.4 dB (or in 1/16 dB units). This gain is 
      increased from a default of 0dB when the Rx analog gain fails to provide 
      further gain without saturation. The range is [-200, 200] in 1/16 dB 
      steps. Default value is 0.                                              */     

  Fract  gain_tx_digital; /**< <B>Bit 13: Send path digital gain:</B>
      AER Tx digital gain in S11.4 dB (or in 1/16 dB units), also known as the 
      pre NLP gain. The range is [-200, 200] in 1/16 dB steps. Default is 0. 
      If AGC module monitors and automatically adjusts the microphone gain, 
      this Tx digital gain should be changed accordingly to maintain the SLR. */

  aerEqConfig_t *rxeq_params;  /**< <B>Bit 14: Rx path equalizer parameters:</B> 
    For Equalizer design and parameters format, refer to AER Equalizer Design
    Tool Documentation (delivered separately).
    *
    \remark If parameters of aerEqConfig_t are not valid, aerControl()
            will return an error.                                           */

  aerEqConfig_t *txeq_params;  /**< <B>Bit 15: Tx path equalizer parameters:</B> 
    For Equalizer design and parameters format, refer to AER Equalizer Design
    Tool Documentation (delivered separately).
    *
    \remark If parameters of aerEqConfig_t are not valid, aerControl()
            will return an error.                                           */
    
  tuint valid_bitfield_1;
#define aer_CTL_VALID1_NLP_CLOSS_TARGET       0x0001
#define aer_CTL_VALID1_NLP_TX_CLIPAGG         0x0002
#define aer_CTL_VALID1_NLP_LINATTN_MAX_ERLE   0x0004
#define aer_CTL_VALID1_NLP_CLIPPER_MAX_ERLE   0x0008
#define aer_CTL_VALID1_NLP_TOTAL_LINATTN_MIN  0x0010
#define aer_CTL_VALID1_NLP_RX_LINATTN_MIN     0x0020
#define aer_CTL_VALID1_NLP_RX_LINATTN_MAX     0x0040
#define aer_CTL_VALID1_NLP_TX_LINATTN_MIN     0x0080
#define aer_CTL_VALID1_NLP_TX_LINATTN_MAX     0x0100
#define aer_CTL_VALID1_NLP_TX_IN_TC           0x0200
#define aer_CTL_VALID1_NLP_TX_OUT_TC          0x0400
#define aer_CTL_VALID1_NLP_RX_IN_TC           0x0800
#define aer_CTL_VALID1_NLP_RX_OUT_TC          0x1000
#define aer_CTL_VALID1_GAIN_SPLIT_TC          0x2000
#define aer_CTL_VALID1_CNG_RX_LEVEL           0x4000
#define aer_CTL_VALID1_CNG_TX_LEVEL           0x8000

  Fract  nlp_combloss_target; /**< <B>Bit 0: Maximum NLP attenuation:</B>
      This parameter determines the maximal NLP attenuation of subdominant path. 
      It is in S0.15 giving the minimum gain which the NLP can apply to the 
      signal. The actual attenuation applied by NLP also depends 
    on ERLE and the estimated ERL. E.g., if this parameter is set to -40dB, 
    and ERL and ERLE are 5dB and 30dB respectively, then NLP will apply 
    40-5-30 = 5dB attenuation.                               
      *
      Conversion from the value of this parameter to attenuation in dB is:
          Attenuation (dB) = 20*log10(2^15/x), where x is nlp_combloss_target.
      *
    Range is [1, 32767] and default is 327 corresponding to a -40 dB 
    minimum gain (or 40dB maximal attenuation).                             */

  tint   nlp_clip_agg_l2; /**< <B>Bit 1: Send path NLP clipper aggression:</B> 
      This parameter specifies the Tx path NLP center clipper rail amplitude 
      scaling. The larger this parameter, the more aggressive the Tx NLP. 
      *
      This parameter is in number of 3dB steps and is in the range [-10,10]. 
    Default is 7.                                                           */   

  tint   nlp_linattn_max_erle;/**< <B>Bit 2: Maximum ERLE for linear attenuation
      reduction:</B>NLP linear attenuation is adaptively reduced when ERLE 
      increases (or AEC convergence depth increases). However, the adaptive 
      reduction halts when the ERLE obtained by subtracting predicted echo 
      increases beyond this configurable level.
      *
      This parameter is in dB with range [0, +45], and is truncated by AER to 
      the nearest multiple of 3 dB. Default value is 21 (dB).                 */

  tint   nlp_clipper_max_erle;/**< <B>Bit 3: Maximum ERLE for clipper reduction:</B>
      Similar to the reduction of NLP linear attenuation, the adaptive reduction 
      of NLP clipper halts when the ERLE increases beyond this configurable 
      level. Same format and range as nlp_linattn_max_erle. Default is 21.    */
      
  tint   nlp_total_linattn_min;/**< <B>Bit 4:Minimum Rx and Tx total NLP linear
      attenuation:</B> The sum of Rx and Tx NLP linear attenuation will not drop 
      below this configurable level. 
      *
      This parameter is in dB with input range [0, +70] dB. Default is 0.     */

  tint   nlp_rx_linattn_min; /**< <B>Bit 5: Minimum Rx NLP linear attenuation:</B>
      Rx NLP linear attenuation stays above this level. This parameter will be 
      ignored if it is larger than nlp_rx_linattn_max. It has no effect when set
      to zero. 
      *
      This parameter is in dB with input range [0, +70] dB. Default is 0.     */

  tint   nlp_rx_linattn_max;/**< <B>Bit 6: Maximum Rx NLP linear attenuation:</B>  
      Rx NLP linear attenuation stays below this level. This parameter has no
      effect when set to 32767.
      *
      This parameter is in dB with input range [0, +70] dB and the value of
      32767. Default is 32767.                                                */

  tint   nlp_tx_linattn_min; /**< <B>Bit 7: Minimum Tx NLP linear attenuation:</B>
      Same as nlp_rx_linattn_min.                                             */

  tint   nlp_tx_linattn_max; /**< <B>Bit 8: Maximum Rx NLP linear attenuation:</B>
      Same as nlp_rx_linattn_max.                                             */

  tint   nlp_tx_in_tc;       /**< <B>Bit 9: Tx NLP ramping in time constant:</B>
      This parameter is used to control the Tx linear attenuation slew rate, a 
      time constant determining the speed at which the linear attenuation can 
      change when Tx path becomes subdominant. 
      *
      Input range is [1, 10]. Default is 1.                                   */

  tint   nlp_tx_out_tc;    /**< <B>Bit 10: Tx NLP ramping out time constant:</B>
      To control the speed at which the Tx linear attenuation can change when Tx 
      path becomes dominant. Same range and default value as nlp_tx_in_tc.    */

  tint   nlp_rx_in_tc;     /**< <B>Bit 11: Rx NLP ramping in time constant:</B>
      To control the speed at which the Rx linear attenuation can change when Rx 
      path becomes subdominant. Same range and default value as nlp_tx_in_tc. */

  tint   nlp_rx_out_tc;    /**< <B>Bit 12: Rx NLP ramping out time constant:</B>
      To control the speed at which the Rx linear attenuation can change when Rx 
      path becomes dominant. Same range and default value as nlp_tx_in_tc.    */
    
  tint   gain_split_tc;    /**< <B>Bit 13: Tx/Rx NLP gain splitting time 
      constant:</B> Gain split slew rate time constant, which controls the speed 
      at which the total linear attenuation is split between Tx and Rx paths 
      during double talk. 
      *
      Input range is [1, 10]. Default is 5.                                   */

  tint   cng_rx_level;     /**< <B>Bit 14: Rx comfort noise level:</B>
      The white noise level generated by Rx CNG. It is in dBm and the valid
      range is [\ref aer_CNG_LEVEL_LOWER_LIMIT, \ref aer_CNG_LEVEL_UPPER_LIMIT]. 
    Default is -65.
      *
      \remark If the configured value is out of the valid range, aerControl()
              will return an error.                                           */

  tint   cng_tx_level;     /**< <B>Bit 15: Tx comfort noise level:</B>
      The meaning of this parameter depends on the type of the Tx CNG:
      - For adaptive CNG, it is the minimum level, which means the Tx CNG will
        generate comfort noise that spectrally matches the background noise, but 
        the total power will stay above this level.
      - For fixed CNG, it is the actual power of the generated comfort noise.
      *
    This parameter is in dBm, and the valid range is same as cng_rx_level.  
    Default is -65.                                                         */
  
  tuint valid_bitfield_2;
#define aer_CTL_VALID2_TX_FDNLP_MSEC_DELAY   0x0001
#define aer_CTL_VALID2_TX_FDNLP_BIN_LO1      0x0002
#define aer_CTL_VALID2_TX_FDNLP_BIN_LO2      0x0004
#define aer_CTL_VALID2_TX_FDNLP_BIN_HI1      0x0008
#define aer_CTL_VALID2_TX_FDNLP_BIN_HI2      0x0010
#define aer_CTL_VALID2_TX_FDNLP_CNG_MAX      0x0020
#define aer_CTL_VALID2_NR_FBIN1_LIM          0x0040
#define aer_CTL_VALID2_NR_FBIN2_LIM          0x0080
#define aer_CTL_VALID2_NR_FBAND1_MAX_ATTEN   0x0100
#define aer_CTL_VALID2_NR_FBAND2_MAX_ATTEN   0x0200
#define aer_CTL_VALID2_NR_FBAND3_MAX_ATTEN   0x0400
#define aer_CTL_VALID2_NR_SIG_UPD_RATE_MAX   0x0800
#define aer_CTL_VALID2_NR_SIG_UPD_RATE_MIN   0x1000
#define aer_CTL_VALID2_NR_NOISE_THRESH       0x2000
#define aer_CTL_VALID2_NR_NOISE_HANGOVER     0x4000
#define aer_CTL_VALID2_TRACE_CONFIG          0x8000
  
  tint   txfdnlp_msec_delay;  /**< <B>Bit 0: Tx FDNLP processing delay:</B>
      This parameter (in msecs) specifies the delay determining shape of Tx   
      FDNLP pre-FFT windowing function. It may be 3, 4, 5, or 6 (ms). FDNLP,
      including ASNR, may produce less unwanted noise and speech artifacts for 
    a longer delay. Default is 5ms. 
      *
      \remark If this parameter is out of the valid range specified by
            [\ref aer_FDNLP_DELAY_LOWER_LIMIT \ref aer_FDNLP_DELAY_UPPER_LIMIT],  
          aerControl() will take no action and return an error.               */

  tint   txfdnlp_bin_lo1;     /**< <B>Bit 1: Tx FDCC action boundary 1:</B>
      High FDCC (frequency domain center clipper) attenuation is applied for 
      frequency bins < txfdnlp_bin_lo1*62.5 Hz, because no echo subtraction ERLE 
    is anticipated for these bins. Default is 1 (or 62.5 Hz) for both 8kHz and 
    16kHz sampling rates.    
    *
    \remark If this parameter is out of the valid range specified by
          [\ref aer_FDNLP_BIN_LOWER_LIMIT \ref aer_FDNLP_BIN_UPPER_LIMIT_8K] or   
          [\ref aer_FDNLP_BIN_LOWER_LIMIT \ref aer_FDNLP_BIN_UPPER_LIMIT_16K],  
          aerControl() will take no action and return an error.               */

  tint   txfdnlp_bin_lo2;     /**< <B>Bit 2: Tx FDCC action boundary 2:</B>
      Low FDCC attenuation is applied for frequency bins >= txfdnlp_bin_lo2*
      62.5 Hz, because good echo subtraction ERLE is anticipated for these bins. 
    It must be >= tx_fdnlp_bin_lo1. Default is 6 (or 375 Hz) for both 8kHz and 
    16kHz sampling rates. This parameter has the same valid range as 
    txfdnlp_bin_lo1.                                                          */

  tint   txfdnlp_bin_hi1;     /**< <B>Bit 3: Tx FDCC action boundary 3:</B>
      Low FDCC attenuation is applied for frequency bins <= txfdnlp_bin_hi1*
      62.5 Hz, because good echo subtraction ERLE is anticipated for these bins. 
    It must be > tx_fdnlp_bin_lo2. Default is 56 (or 3500 Hz) for 8kHz sampling 
    rate or 112 (or 7000Hz) for 16kHz sampling rates. This parameter has the 
    same valid range as txfdnlp_bin_lo1.                                      */

  tint   txfdnlp_bin_hi2;     /**< <B>Bit 4: Tx FDCC action boundary 4:</B>
      High FDCC attenuation is applied for frequency bins > tx_fdnlp_bin_hi2*
      62.5 Hz, because no echo subtraction ERLE is anticipated for these bins.
    It must be >= tx_fdnlp_bin_hi1. Default is 60 (or 3750 Hz) for 8kHz sampling 
    rate or 120 (or 7500Hz) for 16kHz sampling rates. This parameter has the 
    same valid range as txfdnlp_bin_lo1.                                      */

  tint   txfdnlp_cng_max;     /**< <B>Bit 5: Maximum adaptive Tx CNG level:</B>
      FDCNG adaptively matches Tx NE stationary noise in level and spectrum, so 
      FDCNG output may become very high at times. This parameter will limit the 
      Tx FDCNG signal power (which is heard during Tx NLP attenuation). It sets 
      a limit on maximum power in dBm0 units for the adaptive FDCNG noise 
      estimate & FDCNG output. This parameter doesn't impact independent ASNR 
      noise estimate. 
      *
      The valid range of this parameter is specified by [\ref 
      aer_CNG_LEVEL_LOWER_LIMIT, \ref aer_CNG_LEVEL_UPPER_LIMIT] dBm0. 
    Default is -60 dBm0.
      *
      \remark If this parameter is out of the valid range, aerControl() will 
              return an error.                                                */

  tint   nr_fbin1_lim;       /**< <B>Bit 6: Boundary 1 for ASNR attenuation limit:
      </B> This parameter defines the first boundary of frequency bins for which 
    ASNR attenuation is limited. Range is [1, 62] and default is 10 (625Hz). 
    *
    \remark If this parameter is out of the valid range specified by
          [\ref aer_FDNLP_BIN_LOWER_LIMIT \ref aer_FDNLP_BIN_UPPER_LIMIT_8K] or   
          [\ref aer_FDNLP_BIN_LOWER_LIMIT \ref aer_FDNLP_BIN_UPPER_LIMIT_16K],  
          aerControl() will take no action and return an error.               */

  tint   nr_fbin2_lim;       /**< <B>Bit 7: Boundary 2 for ASNR attenuation limit:
      </B> This parameter defines the second boundary of frequency bins for  
      which ASNR attenuation is limited. Range is [2, 63] and it must be larger
    than nr_fbin1_lim. Default is 32 (2000Hz). This parameter has the same valid 
    range as nr_fbin1_lim.                                                    */

  tint   nr_fband1_max_atten; /**< <B>Bit 8: ASNR maximum attenuation in band 1:
    </B> The maximum attenuation (dB) that ASNR may apply for frequency band 
    [0, asnr_fbin1_lim-1]*62.5 Hz. Range is [0, 90], and default is 9.     */

  tint   nr_fband2_max_atten; /**< <B>Bit 9: ASNR maximum attenuation in band 2:
      </B> The maximum attenuation (dB) that ASNR may apply for frequency band 
      [asnr_fbin1_lim, asnr_fbin2_lim]*62.5 Hz. Range is [0, 90], and default 
    is 9.                                                                  */

  tint   nr_fband3_max_atten; /**< <B>Bit 10: ASNR maximum attenuation in band 3:
      </B> The maximum attenuation (dB) that ASNR may apply for frequency band
    [asnr_fbin2_lim+1, 64]*62.5 Hz. Range is [0, 90], and default is 9.    */

  Fract  nr_sig_upd_rate_max; /**< <B>Bit 11: ASNR maximum signal update rate:</B>
      ASNR adaptive speech signal estimate inverse update rate upper bound.
      The actual inverse update rate is varied internally by AER between 
      the configured maximum (this parameter) and mimimum constraints. A higher 
      inverse update rate reduces frame to frame changes yielding greater 
      temporal smoothing and less time varying ASNR distortion artifacts during 
      speech or noise. A lower inverse update rate yields quicker speech onset 
      "breaking in" to remove ASNR attenuation. 
      *
      This parameter is in Q.15 format. The range is [2, 32767] and default is 
      31129 (0.95).                                                           */

  Fract  nr_sig_upd_rate_min; /**< <B>Bit 12: ASNR minimum signal update rate:</B> 
      This is the minimum constraint on the ASNR adaptive speech signal estimate 
      inverse update rate.
      *
      This parameter is in Q.15 format. The range is [1, 32766] and default is 
      29818 (0.91).                                                           */

  tint   nr_noise_thresh;    /**< <B>Bit 13: ASNR noise threshold:</B>
      ASNR will not reduce noise below this level, and when noise is already 
      lower than this level, ASNR will not apply any attenuation to the signal. 
      ASNR spectral noise estimate will continue to update even if Tx signal 
      is below this noise threshold.
      *
      This parameter is in dBm0. Default is -75dBm0. Valid range is specified by 
      [\ref aer_NR_NOISE_THR_LOWER_LIMIT, \ref aer_NR_NOISE_THR_UPPER_LIMIT]. 
      If it is set to any value less than aer_NR_NOISE_THR_LOWER_LIMIT, 
      this parameter has no effect. 
      *
      \remark If this parameter is set above aer_NR_NOISE_THR_UPPER_LIMIT,
              aerControl() will return an error.                              */

  tint   nr_noise_hangover;  /**< <B>Bit 14: ASNR hangover:</B>
      When noise is lower than nr_noise_thresh, ASNR will not apply any 
      attenuation to the signal. After noise goes above nr_noise_thresh, ASNR 
      waits a period of time specified by this parameter before applying 
      attenuation again. 
      *
      This parameter is in 10 msec units. Range is [0, 32767], default is 150 
      (1.5 seconds).                                                          */

  aerTraceConfig_t trace_cfg; /**< <B>Bit 15: AER tracing configuration:</B>       
   It can NOT be used to turn on/off the tracing, which has to be done through 
   control bitfield mask \ref aer_CTL2_ENABLE_TRACE.
   *
   \remark Tracing MUST be turned off before being configured.              */
    
  tuint valid_bitfield_3;
#define aer_CTL_VALID3_COH_HANGOVER      0x0001
#define aer_CTL_VALID3_COH_RATIO_THRESH  0x0002
#define aer_CTL_VALID3_COH_CNT_THRESH    0x0004
#define aer_CTL_VALID3_HLC_THRESHOLD     0x0008
#define aer_CTL_VALID3_HLC_RAMP_OUT_TC   0x0010
#define aer_CTL_VALID3_HLC_POWER_TC      0x0020
#define aer_CTL_VALID3_THRESH_RX_NLD     0x0040
#define aer_CTL_VALID3_THRESH_NOISE_RX   0x0080
#define aer_CTL_VALID3_THRESH_NOISE_TX   0x0100
#define aer_CTL_VALID3_HANGOVER_RXTOTX   0x0200
#define aer_CTL_VALID3_HANGOVER_TXTORX   0x0400
#define aer_CTL_VALID3_TX_SLIM_MODE      0x0800
#define aer_CTL_VALID3_RX_SLIM_MODE      0x1000
#define aer_CTL_VALID3_NG_HANGOVER       0x2000
#define aer_CTL_VALID3_NG_RAMPIN_PERIOD  0x4000
#define aer_CTL_VALID3_NG_NOISE_LEVEL    0x8000
    
  tint   coh_hangover;    /**< <B>Bit 0: Coherence detection hangover:</B>
      This parameter specifies the time to wait before looking for coherence 
      again after coherence has been detected.
      *
      It is in 10 msec units. Range is [1,32767]. Default is 600 (6 seconds). */

  UFract coh_ratio_thresh;/**< <B>Bit 1: Coherence detection ratio threshold:
      </B> To detect coherence, a ratio, which is the net LMS update drift
      divided by net LMS update length, is calculated and evaluated. This ratio
      must exceed this parameter in order to consider coherence presence.
      *
    It is in Q.15 format. Range is [0, 32767], and the default is 6554 (0.2). */

  tint   coh_cnt_thresh;  /**< <B>Bit 2: Coherence detection counter threshold:
    </B> To declare coherence presence, the coherence ratio must exceed 
    coh_ratio_thresh for a number of consecutive frames specified by this 
    parameter.
    *
  It is in number of frames. Range is [1,100] and the default is 30 (30 frames
  or 300 msec).                                                              */
  
  Fract  thresh_hlc;        /**< <B>Bit 3: HLC threshold or target level: </B>   
    It is in S14.1 format or in units of 0.5dBm0. This parameter defines the 
    threshold level to which the HLC attenuates the signal in case the signal 
    exceeds this level. This parameter must lie between -48 dBm0 and +3 dBm0 
    or in the range [-96,+6] in 0.5dBm0 units. Default value is 0.          */

  tint   ramp_out_tc_hlc; /**< <B>Bit 4: HLC ramp out time constant (ms/dB):</B>
    In case the signal level goes below HLC threshold level, HLC attenuation 
    should be gradually reduced, based on the setting of this parameter. It 
    should be set in 10msec/dB steps between 10ms/dB and 1000ms/dB. Default 
    value is 700.                                                           */

  tint   power_tc_hlc;   /**< <B>Bit 5: HLC power measurement time constant(ms):
    </B> Estimates of signal power is exponentially averaged using a low pass 
    power measurement time constant. The allowed values are [1, 2, 3], 
    corresponding to 4ms, 8ms or 16ms respectively. Default is 1.           */

  Fract  thresh_rx_nld;  /**< <B>Bit 6: AER Rx nonlinear detection threshold:</B>
    This is the signal level threshold to detect non-linear distortion in the
    receive path. This parameter warns the AER that non-linear breakdown may 
    occur when the digital signal level driving the hands-free speaker exceeds 
    a certain fixed level. The determination of whether the threshold is 
    exceeded takes place downstream of the AER receive path digital gain, 
    aer_rx_dg. This parameter helps attain the best degree of full duplex 
    performance when the HF speaker is driven below a fixed threshold, and 
    helps AER avoid leaking echo due to a non-linear breakdown that occurs 
    when the handsfree speaker is driven above a the fixed threshold. 
    *
    By setting this parameter to a value below 0x8000, there is an added 
    benefit of detecting flat-top saturation in the receive path data (which 
    may suddenly change the spectral content and cause AER performance 
    degradation). Therefore, we do not reset this parameter for every 
    different value of the handsfree speaker analog gain (speaker volume). 
    *
    This parameter is specified in the range [0x0000 - 0x8000]. Default value 
    is 32000. 
    - When threshold is in [0x0000,0x7FFF], it corresponds to signal power of 
      (10*log10(rx_linear_thresh/2^15) + 3) dBm0.
    - When threshold is 0x8000, the Rx saturation detector is disabled.
    *
    \remark This parameter is valid only for the hands-free phone mode.     */
                                              
  LFract thresh_rx_noise;  /**< <B>Bit 7: Rx Minimum Noise Power Threshold:</B>
    Long integer value in S0.31 with range [0, 2^31]. Expected range is [335, 
    65535], corresponding to a range of -60dBm0 to -37.1dBm0 approximately. 
    Default value = 5380L (corresponding to -48 dBm0).
    *
    Conversion from the value of this parameter to dBm0 is:  
      P_dBm0 = 10*log10(x/(2^23*80)) + 3,,where x is thresh_tx_noise.       */

  LFract thresh_tx_noise;  /**< <B>Bit 8: Tx Minimum Noise Power Threshold:</B> 
    Meaning and format same as thresh_rx_noise.                             */

  tint   hangover_rx_to_tx; /**< <B>Bit 9: AER Rx to Tx switching hangover:</B>
    The hangover to consider switching away from receive path dominace to 
    send path dominace. It is specified in 5 msec steps. The range is [0, 50], 
    corresponding to 0 msec to 250 msec switching hangover. Default value is 
    30 (150 msec).                                                          */

  tint   hangover_tx_to_rx; /**< <B>Bit 10: AER Tx to Rx switching hangover:</B>
    The hangover to consider switching away from send path dominace to receive 
    path dominace. It is specified in 5 msec steps. The range is [0, 50], 
    corresponding to 0 msec to 250 msec switching hangover. Default value is 
    10 (50msec).                                                            */

  tint   tx_slim_mode;     /**< <B>Bit 11: Signal limiter mode for Tx path:</B>
    It may be 0, 1, 2, 3, 4, or 5 with 0 disabling the limiter and 1 being 
    most aggressive. Default is 0 (disabled).
    *
    \remark If this parameter is out of the valid range specified by
            [\ref aer_SLIM_MODE_LOWER_LIMIT, \ref aer_SLIM_MODE_UPPER_LIMIT],
            aerControl() will return an error.                              */

  tint   rx_slim_mode;    /**< <B>Bit 12: Signal limiter mode for Rx path:</B>
    Same range as tx_slim_mode.                                             */

  tint   hangover_ng;    /**< <B>Bit 13: Noise Guard hangover period (msec):</B>
    Before actually starting to attenuate the noise signal, noise guard lets 
    a certain hangover period (specified in msec) expire to make sure that 
    what seems like near end noise isn't just a pause in speech. This hangover 
    is a configurable parameter for AER noise guard. The default for hangover 
    period is 1000msec and valid range is [1-2500]ms.                       */

  tint   rampin_period_ng; /**< <B>Bit 14: Noise Guard ramp in period (msec):</B>
    The hangover period is followed by a ramping in period (specified in msec) 
    that controls how fast the noise guard ramps in. These two together 
    determine the total duration for the noise guard action to take effect. 
    The default ramping in period is 800msec. When near end speech is detected 
    though, the noise-speech transition is instantaneous. Valid range for 
    ramping in period is [1-2000]ms. Default value is 800msec.              */

  tint   noise_level_ng;  /**< <B>Bit 15: Noise Guard Tx noise level (dBm0):</B>       
    The level of noise that needs to be sent to the far end so that it can 
    break-in comfortably. If the noise guard detects the presence of near end 
    speech, it should pass the signal across unaltered. If it, however, 
    detects noise presence at the near end, it should attenuate the signal 
    locally to get as close as possible to the configured send noise level. 
    If the estimated noise level is at or below this level, the noise guard 
    should not do anything. This parameter is specified in dBm0. The default 
    is (-75) dBm0. The recommended range is (-50) to (-90) dBm0.            */

  tuint valid_bitfield_4;
#define aer_CTL_VALID4_DT_THRESH      0x0001
#define aer_CTL_VALID4_CLIP_SCALE     0x0002
#define aer_CTL_VALID4_DT_HANGOVER    0x0004

  tint   dt_thresh;             /**< <B>Double talk detection threshold:
  </B> This parameter controls the threshold for double talk detection. The 
       smaller this parameter is, the more easily the double talk will be 
       detected, which means more duplex but potentially more echo leaks. The 
       larger this parameter is, the less easily the double talk will be detected, 
       which means less duplex but less echo leaks. 
       
       Typical range is [64,128] and the default is 96. */
       
  tulong clip_scale_curve;      /**< <B>Clipper scaling curve: </B>
    This parameter configures a curve to scale the clipper based on double talk
    detection. This 32-bit value contains 4 numbers, each of 8-bit, c1, c2, c3
    and c4 from MSB to LSB. These 4 numbers determine the scaling curve and must
    be such that c1<c2<=c3<c4 and (c4-c1)<128. The scaling factor is 4 at c1,
    decreasing to 1 at c2, staying at 1 between c2 and c3, and then decreasing
    to 0 at c4. The larger these 4 numbers, the more aggressive the clipper is. 
    
    Default values are: c1=40, c2=64, c3=64, c4=128. 
  */
  
  tint dt_hangover;            /** <B>Double talk detection hangover: </B>
    This parameter controls the hangover period for double talk detection. 
    NLP state machine will stay in double talk state for this hangover period 
    after conditions for double talk are not met anymore. It is in number of 
    frames. Typical range is [0 200] and the default is 10, i.e. 100msec.     */
  
} aerControl_t;



/** @defgroup aer_param_valid_range AER parameter configuration ranges
 *  @ingroup aer_api_constants
 *  @{
 *
 *  @name AER parameter configuration ranges
 *
 *  @brief Upper and lower limit for AER configurable parameters.
 */
/*@{*/
#define aer_CNG_LEVEL_UPPER_LIMIT       0 /**< CNG level upper limit: 0dBm0   */ 
#define aer_CNG_LEVEL_LOWER_LIMIT   (-80) /**< CNG level lower limit: -80dBm0 */ 
#define aer_FDNLP_DELAY_UPPER_LIMIT    6  /**< FDNLP delay upper limit: 6msec */
#define aer_FDNLP_DELAY_LOWER_LIMIT    3  /**< FDNLP delay lower limit: 3msec */
#define aer_FDNLP_BIN_LOWER_LIMIT      0  /**< FDNLP bin lower limit: 0Hz */
#define aer_FDNLP_BIN_UPPER_LIMIT_8K  64  /**< FDNLP bin upper limit for 8kHz 
                                               sampling rate: 4000Hz          */ 
#define aer_FDNLP_BIN_UPPER_LIMIT_16K 128 /**< FDNLP bin upper limit for 16kHz 
                                               sampling rate: 8000Hz          */ 

#define aer_NR_NOISE_THR_LOWER_LIMIT (-80)/**< ASNR noise thr low lim: -80dBm0*/
#define aer_NR_NOISE_THR_UPPER_LIMIT (-40)/**< ASNR noise thr up lim:  -40dBm0*/
#define aer_SLIM_MODE_UPPER_LIMIT      5  /**< signal limiter mode upper limit:
                                               5 (most aggressive)            */
#define aer_SLIM_MODE_LOWER_LIMIT      0  /**< signal limiter mode lower limit:
                                               0 (disabled)                   */
/*@}*/
/** @} */

          
/**
 *  @ingroup aer_api_structures
 *
 *  @brief AER Statistics Structure.
 *  \brief This structure contains statistical counters to monitor the 
 *         occurrance of particular events in AER.
 */
typedef struct aerEventStats_s {

  tint  num_reset_events;   /**< 39 - # of resets or aggressive startup occurences   */
  tint  num_howl_events;    /**< 40 - # of times howling detected, stored at lower 
                                 8 bits                                         */
  tint  num_coh_events;     /**< 41 - # of times rx & error signal coherence detected,
                                 stored at lower 8 bits                         */
  tint  num_sat_events;     /**< 42 - # of Tx saturations that AGC reported to AER   */
  tint  num_rxgain_events;  /**< 43 - # of Rx AIC gain changes AGC reported to AER   */
  tint  num_txgain_events;  /**< 44 - # of Tx AIC gain changes AGC reported to AER   */
  tint  num_filter_updates; /**< 45 - # of filter updates since startup <= FRCT_MAX  */
  tint  num_dt_potential;   /**< 46 - # of times double talk is potential */
  tint  num_eqsat_events;   /**< 47 - # of times saturation occurs in equalizers:
                                        - upper 8 bits: Tx Eq saturation events 
                                        - lower 8 bits: Rx Eq saturation events       */  
  tint  num_dt_detected;    /**< 48 - # of times double talk is detected */
  tint  num_bssat_events;   /**< 49 - # of times saturation occurs in band split:    
                                        - upper 8 bits: Tx in saturation events 
                                        - lower 8 bits: Rx sync saturation events     */  
  tint  num_coh_potential;      /**< 50 - # of times coherence is potential */
} aerEventStats_t;

/**
 *  @ingroup aer_api_structures
 *
 *  @brief AER PIQUA Statistics Structure.
 *  \brief This structure contains measurements necessary for PIQUA performance
 *         monitoring.
 */
typedef struct aerPiquaStats_s {
  Fract far_end_pow;              /**< 51 - Far end power (dBm) in S11.4           */
  Fract near_end_pow;             /**< 52 - Near end power (dBm) in S11.4          */
  Fract echo_removal_err_pow;     /**< 53 - Residual echo power (dBm) in S11.4     */
  Fract near_end_noise;           /**< 54 - Near end noise power (dBm) in S11.4    */
  Fract erl;                      /**< 55 - Echo return loss (dB) in S11.4         */
  tint erle;                      /**< 56 - Echo return loss enhancement by AEC    */
} aerPiquaStats_t;

/**
 *  @ingroup aer_api_structures
 *
 *  @brief AER tail model save/load structure.
 *  \brief This structure is used for saving or loading AER adaptive tail model, 
 *         which are defined by:
 *           - the adaptive filter coefficients, mantissas and exponents,
 *           - number of active tail model segments (or H vectors), 
 *           - length of tail model in units of sampling period, e.g. 125us for
 *             8kHz and 62.5us for 16kHz. 
 *           - the speaker and microphone analog gains.
 *
 *         All information above can be retrieved from AER by API aerGetFilter() 
 *         or loaded to AER through aerPutFilter(). The filter coefficients are 
 *         stored in two buffers, one for mantissa and the other for exponents. 
 *         The rest of the tail model information are stored in aerTailModel_t.
 *         
 */
typedef struct aerTailModel_s {
  tint  num_H_vec;         /**< number of active H vectors, each vector has \ref
                                aer_NUM_WORDS_PER_H_VEC words of coefficients.*/
  tint  tail_length;       /**< tail length, in units of sampling period.     */
  Fract rx_ag;             /**< speaker analog gain,  
                                see aerControl_s::gain_rx_analog.             */
                                 
  Fract tx_ag;             /**< microphone analog gain, 
                                see aerControl_s::gain_tx_analog.             */
} aerTailModel_t;

/**
 *  @name AER H vector size
 */
/*@{*/
#define aer_NUM_WORDS_PER_H_VEC_8K  258 /**< 258 words per H vector for 8kHz  */
#define aer_NUM_WORDS_PER_H_VEC_16K 514 /**< 514 words per H vector for 16kHz */
/*@}*/


/**
 *  @ingroup aer_api_structures
 *
 *  @brief AER debug information data structure.
 *  \brief This structure contains debug information regarding AER operation.  
 *         It is one of the arguments of function aerGetPerformance().
 *  \remark In the comments, elements of this structure are numbered in units 
 *          of the size of type tint.
 */
typedef struct aerDebugStat_s {
  tuint  control_bitfield0;   /**< 1 - current state of the control bitfield0. 
                                *      See \ref aer_control_bitfields.          */ 
  tuint  control_bitfield1;   /**< 2 - current state of the control bitfield1.  
                                *      See \ref aer_control_bitfields.          */ 
  tint   coherence_state;     /**< 3 - current state of coherence detector state
                                       machine: 
                                       - 0: coherence detected 
                                       - 1: looking for coherence event 
                                       - 2: wait to start looking for coherence */
  tint   path_select_state;   /**< 4 - current state of the path select state 
                                       machine: 
                                       - 0: send path active
                                       - 1: switching from send to receive
                                       - 2: receive path active 
                                       - 3: switching from receive to send 
                                       - 4: idle entered from send 
                                       - 5: idle entered from receive 
                                       - 6: switching to state 4 from state 5   */
  tint   nguard_state;        /**< 5 - current state of noise guard state 
                                       machine                                  */  
  tuint  flag_bitfield0;      /**< 6 - AER flags_bitfield[0]:
                                       - 0x0020: undergoing full reset
                                       - 0x0080: saturation of mic
                                       - 0x4000: inhibit updates of all state
                                                 machines                       */
  tuint  flag_bitfield1;      /**< 7 - AER flags_bitfield[1]:
                                       - 0x0001: bypass the noise guard
                                       - 0x0008: saturation of Tx equalizer
                                       - 0x0040: saturation of Tx input 
                                                  bandsplit filter
                                       - 0x0400: saturation of Rx sync output
                                                  bandsplit filter              */
  tint   tail_length;         /**< 8 - tail length in units of sampling period  */
  tuint  state_bitfield;      /**< 9 - description of AER state machines:
                                       - 0x0004: Rx path nonlinear distortion 
                                                 present
                                       - 0x0020: Howling present
                                       - 0x0040: Double talk present            */  
  tint   max_canc_l2;         /**< 10 - maximal ERLE record in 3 dB steps       */   
  tint   curr_canc_l2;        /**< 11 - current frame's ERLE in 3 dB steps, 
                                        ratio of send path signal power  
                                        (exponentially averaged) before and 
                                        after the cancellation.                 */    
  tint   curr_echo_loss_l2;   /**< 12 - ratio of send path power before and
                                        after the cancellation, in 3dB steps.   */
  linSample maxabs_error;     /**< 13 - absolute value of max error sample      */
  linSample maxabs_pred_echo; /**< 14 - absolute value of max predicted echo 
                                        sample                                  */
  Fract  gain_tx_digital;     /**< 15 - AER tx path digital gain in dB/16 steps */
  Fract  gain_rx_digital;     /**< 16 - AER rx path digital gain in dB/16 steps */ 
  Fract  gain_tx_analog;      /**< 17 - AER tx path analog gain in dB/16 steps  */
  Fract  gain_rx_analog;      /**< 18 - AER rx path analog gain in dB/16 steps  */ 
  Fract  nlp_linear_atten;    /**< 19 - S0.15 linear gain for current NLP 
                                        attenuation                             */ 
  tint   nlp_max_closs_target;/**< 20 - S0.15 linear gain for NLP maximum 
                                        attenuation                             */
  tint   nlp_clip_agg_l2;     /**< 21 - clipper aggression                      */ 
  tint   clip_level;          /**< 22 - current clipper level                   */
  tint   hangover_rx2tx;      /**< 23 - current hangover count to decide making  
                                        a switch from rx path active to tx path 
                                        active                                  */
  tint   hangover_tx2rx;      /**< 24 - current hangover count to decide making
                                        a switch from tx path active to rx path 
                                        active                                  */
  Fract  nguard_atten;        /**< 25 - Noise Guard attenuation in dB           */  
  Fract  hlc_gain;            /**< 26 - HLC gain applied to signal in dB/2 steps*/  
  LFract dt_thresh;           /**< 27:28 - double talk detection threshold, 
                                           equal to aerControl_s::dt_thresh*907 */ 
  LFract rx_sig_pwr;          /**< 29:30 - current recive path frame power      */
  LFract rx_noise_pwr;        /**< 31:32 - receive path background noise power  */
  LFract tx_pNLP_total_pwr;   /**< 33:34 - pre NLP total power of Tx path signal*/  
  LFract tx_pNLP_sig_pwr;     /**< 35:36 - current send path pre NLP frame power*/
  LFract tx_pNLP_noise_pwr;   /**< 37:38 - send path pre NLP background noise 
                                           power                                */
  aerEventStats_t event_stats;/**< 39:50 - AER event statistics                 */
  aerPiquaStats_t piqua_stats;/**< 51:56 - PIQUA statistics                     */
  LFract clip_scale;          /**< 57:58 - clipper scaling curve configuration  */
  LFract dt_ratio;            /**< 59:60 - double talk detection measurements   */
  tint   dt_hangover;         /**< 61 - double talk detection hangover time     */
  Fract  nlp_linear_atten_tx; /**< 62 - S0.15 linear gain for current NLP 
                                        attenuation in Tx path.                 */ 
  Fract  nlp_linear_atten_rx; /**< 63 - S0.15 linear gain for current NLP 
                                        attenuation in Rx path.                 */ 
} aerDebugStat_t;

/**
 *  @ingroup aer_api_structures
 *
 *  @brief AER version structure
 *  \brief This structure contains the version information of the AER software.
 */
typedef struct aerVersion_s {
  tuint  major;  /**< Identifies major departure from the previous release in 
                      terms of algorithms, external API, and/or features.     */  
  tuint  minor;  /**< Identifies any new feature added, bug fixes, improvements 
                      to the algorithm as well as MIPS/memory optimizations.  */
  tuint  patch; /**<  Identifies all situations other than above when source 
                      code would change.                                      */
  tuint  build;   /**< Identifies different build number with same major, minor
                       and patch numbers.                                     */     
  tuint  quality; /**< Identifies release quality: 
                       - 0(prealpha): Under development. Cannot be delivered to 
					                  customers. (default)  
                       - 1(alpha): Substantial features included. Not fully 
					               tested.  
                       - 2(beta):  All features included. Not fully tested.  
                       - 3(empty): General availability (GA). All features 
					               included and fully tested.                 */
  tuint  btype;   /**< Identifies build type:
                       - 0(empty): Regular build as described by the QUALITY 
					               field. (default)  
					   - 1(eng): Engineering build provided prior to moving to 
					             next quality stage. Not fully tested.  
					   - 2(dbg): Debug build provided prior to moving to next 
					             quality stage. Not fully tested. May contain 
								 temporary code and traces that will be removed 
								 in the next regular or engineering build.    */
} aerVersion_t;                            



 
/* ---------------------- AER EXTERNAL APIs ----------------------------------*/

/* aerinit.c */

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerCreate() provides AER with the system information 
 *         that is shared among multiple instances.   
 *  \remark Function aerCreate() must be called before any other AER API  
 *          function is called. For examle, aerGetSizes() needs the 
 *          max_sampling_rate of aerCreateConfig_t.
 *
 *  @param[in]   create_cfg     Pointer to a create configuration structure.
 *  @retval                     AER error code. See \ref aer_err_code.
 *  @verbatim
    error code              description
    aer_NOERR               success
    aer_ERR_INVALIDPAR      max_sampling_rate of create_cfg is not equal to any
                            value defined in aerSrate_t.
    \endverbatim
 *
 */
tint aerCreate (aerCreateConfig_t *create_cfg);

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerGetSizes() obtains from AER the memory requirements of  
 *         an instance, which depend on provided configuration parameters.   
 *
 *  @param[in]   cfg     Pointer to a size configuration structure.
 *  @param[out]  nbufs   Memory location to store the returned number of buffers
 *                       required by the instance.
 *  @param[out]  bufs    Memory location to store the returned address of the 
 *                       vector of memory buffer descriptions required by the 
 *                       instance.
 *  @remark Type ecomemBuffer_t is defined in ecomem.h of util package.
 *
 *  @remark For multi-mic (single speaker) hands-free applications, certain 
 *          permanent buffers are shared among multiple AER instances (one per mic). 
 *          The list below shows all the buffers requested by aerGetSizes() and 
 *          shows which buffers are shared and which are not. For shared
 *          permanent buffers, the same base address must be provided through 
 *          aerNew() to all multi-mic instances. 
 *  @verbatim
    Buffer number     Permanent or Scratch        Shared or not
          0                 Permanent               Not Shared
          1                 Scratch                   N/A
          2                 Permanent               Not Shared
          3                 Permanent               Must Be Shared 
          4                 Permanent               Not Shared
          5                 Permanent               Must Be Shared
          6                 Permanent               Not Shared
          7                 Permanent               Not Shared
          8                 Scratch                   N/A
          9                 Scratch                   N/A
         10                 Permanent               Not Shared
         11                 Permanent               Must Be Shared
         12                 Permanent               Must Be Shared
         13                 Scratch                   N/A
         14                 Permanent               Must Be Shared
         15                 Permanent               Not Shared
         16                 Permanent               Not Shared
         17                 Permanent               Not Shared
         18                 Permanent               Not Shared
    \endverbatim
 *
 * @remark Note that aerGetSizes() does NOT return the information about shared
 *         buffers that is shown above. 
 *
 *  @retval              AER error code. See \ref aer_err_code.         
 *  @verbatim
    error code              description
    aer_ERR_NOTCREATED      aerCreate() has not been called
    aer_ERR_INVALIDPAR      max_y2x_delay or max_tail_length in cfg is negative
    \endverbatim
 *
 *  \remark Even if a buffer is not needed based on information passed through
 *          aerCreateConfig_t and aerSizeConfig_t, aerGetSizes() will still
 *          count this buffer in the total number of required buffers, but will
 *          set the required size to 0.
 */
tint aerGetSizes(tint *nbufs, const ecomemBuffer_t **bufs, aerSizeConfig_t *cfg);

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerNew() creates a new instance of AER.  
 *  \remark Function aerNew() must be called before aerOpen()is called.
 *
 *  @param[in]     nbufs     Number of memory buffers allocated by the user.
 *  @param[in]     bufs      Pointer to memory buffer descriptors defined by
 *                           user.
 *  \remark Buffer alignment property of each buffer passed to aerNew() must be 
 *          equal to or better than what is returned by aerGetSizes(), and must
 *          be in consistency with the base address of the buffer.
 *
 *  @param[in]     cfg       Pointer to new instance configuration structure.
 *  @param[in,out] aerInst   Memory location that will receive a pointer to 
 *                           the created AER instance.
 *  @retval                  AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_INVALIDPAR      *aerInst is not NULL or nbufs is not correct 
     aer_ERR_NOMEMORY        properties of certain buffer are bad:
                             - size is not zero but base address is NULL,
                             - alignment and base address are not consistent,
                             - volatility does not meet requirement.
    \endverbatim
 *
 *  @pre  The pointer at the location pointed to by aerInst must be set to NULL 
 *        before this function is called.
 *  @post A pointer to the AER instance buffer will be returned to the location
 *        pointed to by aerInst. Instance state will be set to closed.
 */
tint aerNew (void **aerInst, tint nbufs, ecomemBuffer_t *bufs,
             aerNewConfig_t *cfg);


/* aer.c */
/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerClose() closes an AER instance. 
 *  \remark Function aerClose() must be called before aerDelete() is called.
 *
 *  @param[in]  aerInst      pointer to the instance to be closed
 *  @retval                  AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTOPENED       AER instance is not in open state
    \endverbatim
 *
 */
tint aerClose (void *aerInst);

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerControl() enables or disables a type of AER function 
 *         or changes the value of a certain parameter. 
 *
 *  @param[in]      aerInst   Pointer to AER instance.
 *  @param[in,out]  ctl       Pointer to AER control structure. Following fields
 *                            in the control structure will be written by 
 *                            aerControl():
 *                              - trace_cfg.get_size_rx and get_size_tx for
 *                                control code aer_CTL_TRACE_CONFIG.
 *
 *  @retval                   AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTOPENED       AER instance has not been opened
     aer_ERR_INVALIDPAR      following parameters are out of the allowed range:
                               tail_length            
                               eq_params
                               cng_rx_level
                               cng_tx_level
                               txfdnlp_msec_delay
                               txfdnlp_cng_max
                               y2x_delay
                               tx_slim_mode
                               rx_slim_mode
    \endverbatim
 *
 */
tint aerControl (void *aerInst, aerControl_t *ctl);

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerDelete() deletes the AER instance identified by aerInst
 *         and returns the addresses of those buffers used by this instance.  
 *
 *  @param[in]      nbufs    Number of buffers used by this instance.
 *  @param[in]      bufs     Pointer to buffer descriptors to store returned
 *                           addresses of the buffers used by this instance.
 *  @param[in,out]  aerInst  Memory location where the pointer to AER instance
 *                           is stored
 *  @retval                  AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTCLOSED       AER instance has not been closed.
     aer_ERR_MEMBUFVECOVF    Not enough buffer descriptors to store all returned 
                             buffer addresses.
    \endverbatim
 *
 *  @pre  AER instance must be closed by aerClose() before aerDelete() is called. 
 *  @post After aerDelete() is called, AER instance pointer stored at aerInst 
          will be set to NULL, and the addresses of the buffers used by this 
          instance will be returned to the location pointed to by bufs.
 */
tint aerDelete (void **aerInst, tint nbufs, ecomemBuffer_t *bufs);


/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerGetFilter() gets elements from the current adaptive impulse 
 *         response 'tail' model coefficients(H) and saves into a block of memory specified 
 *         by the user. Moreover, it returns other tail model related parameters
 *         to facilitate loading of H coefficients at a later time, or displaying
 *         the impulse reponse model in time domain.
 *
 *  @param[in]   aerInst    Pointer to AER instance.
 *  @param[in]   start      Start point of the mantissa elements in H to be 
 *                          copied, and start mut be >= 0.
 *  @param[in]   length     Number of successive mantissa elements to be copied.
 *
 *  @param[out]  tail_model Ponter to a strucutre holding tail model parameters
 *                          returned by this function. This information is 
 *                          necessary for loading the saved coefficients.
 *  @param[out]  coeff_mant External array to store the coefficients mantissa to
 *                          be returned by this function. 
 *  @param[out]  coeff_exp  External array to store the coefficients exponents,
 *                          one per H vector, to be returned by this function. 
 *                          Total number of H vectors is specified by
 *                          aerTailModel_S.num_H_vec.
 *
 *  @retval                 AER error code (always aer_NOERR).         
 *
 *  @remark   The obtained coefficients are in frequency domain and their values 
 *            are equal to mantissa*2^exponent. To convert these coefficients 
 *            to time domain impulse response, tools provided by Texas Instruments 
 *            must be used.
 *
 *  @sa aerPutFilter
 */
tint aerGetFilter (void *aerInst, aerTailModel_t *tail_model, Fract *coeff_mant, 
                   tint *coeff_exp, tint start, tint length);

/* aer.c */
/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerPutFilter() does the reverse of aerGetFilter: it loads 
 *         the previously saved tail model to AER instance. 
 *
 * @remark If this function is to be used, it must be called after aerOpen() is
           called and after microphone and speaker analog gains are set to
           some initial values explicitly through aerControl() using control 
           codes aer_CTL_GAIN_RX_ANALOG and aer_CTL_GAIN_TX_ANALOG. Otherwise, 
           it will refuse to load the coefficients.
 *
 * \remark Number of total words to be loaded must be equal to 
 *         tail_model.num_H_vec * aer_NUM_WORDS_PER_H_VEC_8K for 8kHz or
 *         tail_model.num_H_vec * aer_NUM_WORDS_PER_H_VEC_16K for 16kHz.
 *
 * \remark If this function needs to be called several times to finish the 
 *         loading, then between the first and last call, aerSendIn() and 
 *         aerReceiveIn() will process data as if AER is disabled.
 *
 *  @param[in]   aerInst    Pointer to AER instance.
 *  @param[in]   tail_model Ponter to strucutre holding tail model parameters. 
 *  @param[in]   coeff_mant External array holding the coefficients mantissas to
 *                          be loaded to AER by this function. 
 *  @param[in]   coeff_exp  External array holding the coefficients exponents,
 *                          one per H vector, to be loaded to AER by this 
 *                          function. Total number of H vectors is specified
 *                          by aerTailModel_S.num_H_vec.
 *  @param[in]   start      Start point of the mantissa elements in H to be 
 *                          copied, and start mut be >= 0.
 *  @param[in]   length     Number of successive mantissa elements to be copied.
 *  @param[in]   last_seg   Flag to indicate whether this is the last call to
 *                          load the coefficients.
 *  @retval                 AER error code. 
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTOPENED       AER instance has not been opened
     aer_ERR_INVALIDPAR      start+length > number of total # of mantissa
     aer_ERR_NOINITGAIN      initial mic and speaker gains not set
    \endverbatim
 *
 *  @sa aerGetFilter
 */
tint aerPutFilter (void *aerInst, aerTailModel_t *tail_model, Fract *coeff_mant,
                   tint *coeff_exp, tint start, tint length, tbool flag_last_seg);


/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerGetGains reports the AER Tx and Rx digital gains in 
 *         S11.4 dB (or 1/16 of dB) units.
 *
 *  @param[in]   aerInst      Pointer to AER instance
 *  @param[out]  tx_dg_gain   Address to store AER Tx digital gain 
 *  @param[out]  rx_dg_gain   Address to store AER Rx digital gain     
 *  @retval                   AER error code (always aer_NOERR).
 *
 */
tint aerGetGains (void *aerInst, tint *tx_dg_gain, tint *rx_dg_gain);

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerGetPerformance() reports the AER performance debug  
 *         statistics and the version information. 
 *
 *  @param[in]      aerInst  Pointer to AER instance.
 *  @param[in]      dbgstat  Pointer to debug status structure.
 *  @param[in]      version  Pointer to version information structure.
 *  @param[in]      reset    Flag to tell AER to reset event counters.
 *  @retval                  AER error code (always aer_NOERR).
 *
 *  @post AER event counters will all be set to 0s if reset flag is true.
 *
 */
tint aerGetPerformance (void *aerInst, aerDebugStat_t *dbgstat, aerVersion_t 
                        *version, tbool reset);

/**
 *  @ingroup aer_api_functions
 *
 *  @brief Function aerOpen() initializes and configures an AER instance.
 *
 *  @remark This function may be called after aerNew() to initialize a new AER
 *          instance. It may also be called to reconfigure an instance that 
 *          has been closed by aerClose() but not deleted by aerDelete().
 *
 *  @param[in]      cfg      Pointer to AER configuration parameter.
 *  @param[in]      aerInst  Pointer to AER instance.
 *  @retval                  AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTCLOSED       AER instance state has not been set to closed, i.e,
                             aerNew() or aerClose() not called for the instance.
     aer_ERR_INVALIDPAR      bad configuration parameters:
                             - cfg is NULL
                             - valid_bitfield of aerConfig_t is not all 1's for  
                               first time open after aerNew().
                             - sampling rate of aerConfig_t is larger than 
                               max_sampling_rate specified in aerCreateConfig_t.
                             - tail_length of aerConfig_t is larger than 
                               max_tail_length specified in aerSizeConfig_t.
                             - y2x_delay of aerConfig_t is larger than 
                               max_y2x_delay specified in aerSizeConfig_t.
    \endverbatim
 *
 *  @pre  Function aerNew() must be called before aerOpen() is called the first
 *        time to open a new instance. For subsequent calls to open an existing
 *        instance, aerClose() must be called before aerOpen() to close the
 *        instance.
 *  @post All the parameters that are not part of aerConfig_t are set to 
 *        default values by aerOpen. For example, control bitfield 0 (changeable
 *        through aer_CTL_MODES_0) is set to 0x86, which disables AER. For 
 *        detailed information about AER parameters and their default values, 
 *        refer to AER User's Guide.
 *  @post After AER instance is opened, aerControl(), aerSendIn(), or
 *        aerReceiveIn() may be called for control or processing.  
 *
 */
tint aerOpen (void *aerInst, aerConfig_t *cfg);


/* aerrx.c */         
/**
 *  @ingroup aer_api_functions
 *
 *  @brief  Function aerReceiveIn() performs receive path processing.
 *
 *  @param[in]      recv_in    A frame of received data to be processed.
 *  @param[in]      aerInst    Pointer to AER instance. 
 *  @param[out]     recv_out   A frame of processed data.
 *  @param[out]     trace_buf  Buffer to store AER trace data in Rx path.
 *
 *  @pre Initial microphone and speaker analog gains must be set explicitly
 *       through aerControl(). Otherwise, aerReceiveIn() will process data as if 
 *       AER is disabled.
 *  @pre If aerPutFilter() is loading the tail model, until it has completed
 *       the process, aerReceiveIn() will process data as if AER is disabled.
 *  @sa  aerPutFilter
 *
 *  @retval                    AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTOPENED       AER instance has not been opened
     aer_ERR_NOINITGAIN      initial mic and speaker gains not set
     aer_ERR_LOAD_INPROCESS  tail model loading is in process
    \endverbatim
 *
 *  @remark   recv_in and recv_out may be the same. If they are different, 
 *            content in recv_in will not be modified by this function.
 *  \remark   If the rate on AER recv_in buffer is 16kHz, the recv_out buffer 
 *            must have enough space to hold 16kHz frame no matter what the 
 *            actual receive_out rate is. This is because AER uses recv_out 
 *            buffer for internal calculations at 16kHz.
 *  \remark   Tracing data will not be written to trace_buf unless tracing is 
 *            turned on through control code aer_CTL_TRACE_TURN_ON.
 */
tint aerReceiveIn (void *aerInst, void *recv_in, void *recv_out, void *trace_buf);

/* aertx.c */
/**
 *  @ingroup aer_api_functions
 *
 *  @brief  Function aerSendIn() performs send path processing.
 *
 *  @param[in]      send_in        A frame of data to be processed and sent.
 *  @param[in]      recv_out_sync  A frame of synchronized receive path output data.
 *  @param[in]      aerInst        Pointer to AER instance. 
 *  @param[out]     send_out       A frame of processed data.
 *  @param[out]     sout_8kHz      Processed data at 8kHz for wideband build
 *  @param[out]     sout_pnlp_8kHz Pre-NLP data at 8kHz
 *  @param[out]     trace_buf      Buffer to store AER trace data in Tx path.
 *                                 This buffer may be same as or different from
 *                                 the buffer passed to aerReceiveIn, but trace
 *                                 data of Tx path and Rx path must be written 
 *                                 to two different files.
 *
 *  @pre Initial microphone and speaker analog gains must be set explicitly
 *       through aerControl(). Otherwise, aerSendIn() will process data as if 
 *       AER is disabled.
 *  @pre If aerPutFilter() is loading the tail model, until it has completed
 *       the process, aerSendIn() will process data as if AER is disabled.
 *  @sa  aerPutFilter
 *
 *  @retval                        AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTOPENED       AER instance has not been opened
     aer_ERR_NOINITGAIN      initial mic and speaker gains not set
     aer_ERR_LOAD_INPROCESS  tail model loading is in process
    \endverbatim
 *
 *  @remark   Pointers send_in and send_out may be the same. If they are different, 
 *            content in send_in will not be modified by this function.
 *  \remark   If the rate on AER send_in buffer is 16kHz, the send_out buffer
 *            must have enough space to hold 16kHz frame no matter what the 
 *            actual send_out rate is. This is because AER uses send_out buffer
 *            for internal calculations at 16kHz.
 *  \remark   Pointers sout_8kHz and sout_pnlp_8kHz may be set to NULL if they 
 *            are not used. 
 *  \remark   Tracing data will not be written to trace_buf unless tracing is 
 *            turned on through control code aer_CTL_TRACE_TURN_ON.
 */
tint aerSendIn (void *aerInst, void *send_in, void *send_out, 
                void *sout_8kHz, void *recv_out_sync, void *sout_pnlp_8kHz,
                void *trace_buf);

/* aermmic.c */
/**
 *  @ingroup aer_api_functions
 *
 *  @brief  Function aerActivate() performs activation and/or deactivation of 
 *          handsfree micropone instances. This function is used in a system 
 *          with multiple handsfree microphones.
 * \remark  The active instance MUST be the first one among all instances to 
 *          be processed by aerSendIn().
 * \remark  Instance is set to be inactive by default in aerOpen(). Therefore, 
 *          this function must be called after aerOpen() to set one of the 
 *          multiple instances to be active. 
 *
 *
 *  @param[in]  newActiveInst  Pointer to new active microphone instance.
 *  @param[in]  oldActiveInst  Pointer to old active microphone instance.
 *  @retval                    AER error code. See \ref aer_err_code.
 *  @verbatim
     error code              description
     aer_NOERR               success
     aer_ERR_NOTOPENED       AER instance has not been opened
    \endverbatim
 *
 * \remark One or both arguments can be NULL and the corresponding actions are:
 *  @verbatim
     newActiveInst == NULL: Deactivate instance pointed to by oldActiveInst.
     oldActiveInst == NULL: Activate instance pointed to by newActiveInst.
     both are NULL        : Do nothing.
     both point to the same instance: Do nothing. 
     neither is NULL      : Activate new active instance and deactivate old one.
    \endverbatim
 */
tint aerActivate (void *newActiveInst, void *oldActiveInst);


/* -------------------------- AER Call Table -------------------------------- */
/**
 *  @ingroup aer_api_structures
 *
 *  @brief AER call table
 *
 */
typedef struct {
  tint  (*aerActivate)       (void *newActiveInst, void *oldActiveInst);
  tint  (*aerCreate)         (aerCreateConfig_t *create_cfg);
  tint  (*aerClose)          (void *aerInst);
  tint  (*aerControl)        (void *aerInst, aerControl_t *ctl);
  tint  (*aerDelete)         (void **aerInst, tint nbufs, ecomemBuffer_t *bufs);
  tint  (*aerGetFilter)      (void *aerInst, tint start, tint length, Fract 
                              coeff[], tint *hscale_l2); 
  tint  (*aerGetGains)       (void *aerInst, tint* tx_digital_gain, tint* 
                              rx_digital_gain);                             
  tint  (*aerGetPerformance) (void *aerInst, aerDebugStat_t *dbgstat, 
                              aerVersion_t *version, tbool reset);
  tint  (*aerGetSizes)       (tint *nbufs, const ecomemBuffer_t **bufs,
                              aerSizeConfig_t *cfg);
  tint  (*aerOpen)           (void *aerInst, aerConfig_t *cfg);
  tint  (*aerNew)            (void **aerInst, tint nbufs, ecomemBuffer_t *bufs,
                              aerNewConfig_t *cfg);
  tint  (*aerReceiveIn)      (void *aerInst, void *recv_in, void *recv_out, 
                              void *trace_buf);
  tint  (*aerSendIn)         (void *aerInst, void *send_in, void *send_out, 
                              void *send_out_8kHz, void *recv_out_sync,
                              void *sout_pnlp_8kHz, void *trace_buf);
  tint  (*aerPutFilter)      (void *aerInst, aerTailModel_t *tail_model, 
                              Fract *coeff, tint start, tint length);
} aerCallTable_t;

#endif  /* _AER_H */
/* nothing past this point */
