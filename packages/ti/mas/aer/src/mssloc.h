#ifndef _MSSLOC_H
#define _MSSLOC_H

#include <ti/mas/aer/mss.h>


/* MSS states */
enum {
  MSS_CLOSED  = 0,
  MSS_OPEN    = 1
};


#define MSS_STATE_BIT_SWITCH_SRC 0x0001

#define MSS_FRAME_SIZE_8KHZ 80   /* samples */
#define MSS_NUM_SAMP_1MS_8KHZ 8  /* samples per msec at 8kHz */
#define MSS_FADE_PHA_INIT      0xC0000000  /* Q31 initial phase of crossfade:
                                              -1/2 corresponding to -pi/2  */

typedef struct mssParams_s {
  tint   switch_threshold;
  tint   switch_duration;
  tint   switch_hangover;
} mssParams_t;

typedef struct mssInst_s {
  tint      state;                          /* MSS_CLOSED or MSS_OPEN         */
  void *    handle;                         /* MSS instance handle            */
  
  mssParams_t params;
  mssSrc_t    cur_src;
  mssSrc_t    new_src;
  tword       *svd;
  
  tint   num_src_per_group[mss_MAX_NUM_SRC_TYPES];
  tint   modes_bf;
  tint   state_bf;

  LFract crossfade_phase;
  LFract crossfade_phase_inc;
  tint   crossfade_cnt;
  tint   switch_hang_cnt;
  tint   svd_size;
  tint   max_sampling_rate;
  tint   sampling_rate;
  tint   frame_size;
  tint   num_mic_array;

  tint   switch_src_cnt;
  tuint  ramp_up;
  Fract  ramp_alpha;
  
  /* for debugging only */
  mssSrc_t temp_src;  
  Fract    gain_in;   
  Fract    gain_out;  
} mssInst_t;

void mss_src_selection(mssInst_t *inst, void *mic_fix[], void *mic_rem[], 
                        void *mic_cln[]);
void mss_src_output(mssInst_t *inst, void *out, void *mic_fix[], 
                     void *mic_rem[], void *mic_cln[]);
tint mss_src_validation(mssInst_t *inst, void *rx_out_sync, void *mic_fix[],
                void *mic_rem[], void *mic_cln[], void *mic_arr[], void *beam[]);
void * mss_get_first_src(mssInst_t *inst, void *mic_fix[], void *mic_rem[], 
                         void *mic_cln[]);

#endif
/* Nothing past this point */
