/**
 *  @file   bfport.h
 *  @brief  To include the header file specific to platforms for BF.
 *
 *  \remark:    If this file needs any changes, they should be reflected
 *              in ALL bfport.h files that exist for other projects if
 *              necessary. For example, if new symbol is added (removed), it
 *              should be added (removed) in all bfport.h files, otherwise the
 *              symbol has no place in this file. If an existing symbol changes
 *              its value, some, none, or all other bfport.h files may need a
 *              similar, not necessarily the same change.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *
 */
/******************************************************************************
 * FILE PURPOSE: To include the header file specific to platforms for BF.
 ******************************************************************************
 * FILE NAME:   bfport.h
 *
 * DESCRIPTION: Based on compiler switch, include platform specific header file.
 *
 * (C) Copyright 2008, Texas Instruments, Inc.
 *****************************************************************************/

#ifndef _BFPORT_H
#define _BFPORT_H

#ifdef ti_targets_C55_large
#define _BFPORT_C55L 1
#else
#define _BFPORT_C55L 0
#endif

#ifdef ti_targets_C64P
#define _BFPORT_C64P 1
#else
#define _BFPORT_C64P 0
#endif

#ifdef ti_targets_elf_C64P
#define _BFPORT_C64P_ELF 1
#else
#define _BFPORT_C64P_ELF 0
#endif

#ifdef ti_targets_C64P_big_endian
#define _BFPORT_C64P_BIG_ENDIAN 1
#else
#define _BFPORT_C64P_BIG_ENDIAN 0
#endif

#ifdef ti_targets_elf_C64P_big_endian
#define _BFPORT_C64P_BIG_ENDIAN_ELF 1
#else
#define _BFPORT_C64P_BIG_ENDIAN_ELF 0
#endif

#ifdef ti_targets_C674
#define _BFPORT_C674 1
#else
#define _BFPORT_C674 0
#endif

#ifdef ti_targets_elf_C674
#define _BFPORT_C674_ELF 1
#else
#define _BFPORT_C674_ELF 0
#endif

/* C674 devices do not support big endian */

#ifdef gnu_targets_arm_GCArmv7A
#define _BFPORT_ARM_GCARMV7A 1
#else
#define _BFPORT_ARM_GCARMV7A 0
#endif

#if _BFPORT_C55L
#include <ti/mas/aer/src/c55/bfport.h>
#elif _BFPORT_C64P || _BFPORT_C64P_BIG_ENDIAN || _BFPORT_C674 || _BFPORT_C64P_ELF || _BFPORT_C64P_BIG_ENDIAN_ELF || _BFPORT_C674_ELF
#include <ti/mas/aer/src/c64P/bfport.h>
#elif _BFPORT_ARM_GCARMV7A
#include <ti/mas/aer/src/arm/bfport.h>
#else
#error invalid target
#endif

#endif /* _BFPORT_H */

/* Nothing past this point */


