/**
 *  @file   bfapplygain.c
 *  @brief  Contains gain adjustment Routine for Beamforming (BF) module.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 *  
 */
#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>
#include <ti/mas/aer/src/bf/bfloc.h>

extern LFract bf_lfxf (LFract x, Fract y);

/******************************************************************************
 * FUNCTION PURPOSE: Apply gain to BF filtering output.
 ******************************************************************************
 * DESCRIPTION: This function applies gain to the output of BF filtering such 
 *              that BF has unity gain. The gain is the reciprocal of the number
 *              of active mics used for BF. 
 *
 *  tint bf_apply_gain (
 *  Input:
 *    LFract *workout,    - temporary output buffer of BF filtering: summation 
 *                          of the output of all filters
 *    tint frm_len,       - number of samples per frame
 *    tint num_active_mic - number of active microphones used for BF
 *  Output:
 *    Fract *out          - final output buffer of BF
 *****************************************************************************/
void bf_apply_gain(LFract *workout, Fract *out, tint frm_len, tint num_active_mic)
{
  tint i;
  Fract gain; 
      
  /* gain adjustment for the total summed signal when more than 1 mic is active */
  if (num_active_mic > 1) {  
    /* calculate the gain 1/num_mics in Q15 */
    gain =(Fract)( frctAdjustQ(1L,bf_Q0,bf_Q15)/num_active_mic); 
    for (i=0;i<frm_len;i++)
    {
      /* apply gain: when gain is Q15, workout Q format unchanged */  
      workout[i] = bf_lfxf(workout[i],gain);
    }
  } 
  
  /* calculate the final output */
  for (i=0;i<frm_len;i++)
  {
    /* round the output and adjust to the original Q format */ 
    workout[i] = frctRoundLF(workout[i], bf_QWorkOut, bf_Q0);
	
    /* Make sure the output is within [frct_MIN, frct_MAX] */
    out[i] = frctLF2FS(workout[i],bf_Q0,bf_Q0);
  }
} /* bf_apply_gain */                    

/* Nothing past this point */
