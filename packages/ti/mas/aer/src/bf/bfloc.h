#ifndef _BFLOC_H
#define _BFLOC_H

/**
 *  @file   bfloc.h
 *  @brief  Contains functions prototypes, definitions not externally accessible 
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 * 
 */

#include <ti/mas/aer/bf.h>


/** @defgroup bf_open_state BF State Definitions
 *  @name BF open state
 *
 *  State of BF module, no operation when it's closed. 
*/
/*@{*/

enum {
  bf_CLOSED  = 0,
  bf_OPEN    = 1
};

/*@}*/


/** @defgroup bf_mic_status BF mic active definitions
 *  @name BF microphone status
 *
 *  Only when the mic is active, its input signal will be passed 
 *  into beamformer. 
*/
/*@{*/
enum {
  bf_MIC_NONACTIVE = 0,
  bf_MIC_ACTIVE    = 1
};
/*@}*/


/** @defgroup bf_bitfield_macros BF bitfield macro
 *  @name bf bitfield macros
 *
 *  These macros are used to set, clear and check bitfield information.
 *   
*/
/*@{*/
#define bf_setbit(field,b)  ( (field) |= (  b))
#define bf_setbitN(field,n) ( (field) |= (1<<n))
#define bf_clrbit(field,b)  ( (field) &= (~(b)))
#define bf_chkbit(field,b)  (((field) &  (  b)) == (b))
#define bf_chkbitN(field,b) (((field) &  (1<<b)) == (1<<b))
/*@}*/

#define   bf_FRAME_SIZE_8KHZ      80  /**< 10ms frame */

#define   bf_MAX(x,y)        ( ((x) > (y)) ? (x) : (y) )
#define   bf_MIN(x,y)        ( ((x) < (y)) ? (x) : (y) )

/** @defgroup bf_Q_format Q-format definitions 
 *  @name bf Q-format definitions
 *
 *  Q format that's being used in BF implementation.
 *   
*/
/*@{*/
#define   bf_Q0          0
#define   bf_Q5          5
#define   bf_Q8          8
#define   bf_Q11         11
#define   bf_Q14         14
#define   bf_Q15         15 
#define   bf_QACC        (bf_Q15-6)   /**< bf_MAX_SUPPORTED_FILT_LEN = 64  */
#define   bf_QWorkOut    (bf_QACC-4)  /**< bf_MAX_SUPPORTED_NUM_MICS = 16 */
/*@}*/

/** 
 *
 *  @brief Instance structure for BF
 *
 *  \brief This structure is used to store state information, 
 *   buffer information and all parameters for a single instance of BF. 
 */

typedef struct bfInst_s {
  void *     handle;          /**< BF instance handle   */  

  linSample *flt_dl_buf[bf_MAX_SUPPORTED_NUM_MICS];  /**< BF filter delay lines */ 
  Fract     *filter_coef[bf_MAX_SUPPORTED_NUM_MICS]; /**< BF filter coefficients*/ 

  LFract    *workout_buf;     /**< work buffer to accumulate filtering output */  
  Fract     *work_buf;        /**< work buffer for filtering delay line       */
  tint      state;            /**< bf_CLOSED or bf_OPEN                       */
  tuint     sys_bits;         /**< bit 0: enable/disable, 
                                   bits 1-2: operation mode, ref to bf.h      */
  tint      max_num_mics;     /**< max number of mics supported by BF         */
  tint      max_sampling_rate;/**< max sampling rate supported by BF          */
  tint      max_filt_len;     /**< max filter length supported by BF          */ 
  tint      num_mics;         /**< number of mics in this BF instance         */
  tuint     act_mics;         /**< bitfield indicating whether a mic is active
                                   (connected for beamforming):
                                      1: active, 
                                      0: inactive.                            */
  tuint     flags_filt_loaded; /**< bitfield indicating whether a mic's filter 
                                    has been loaded with valid coefficients:
                                      1: filter is loaded, 
                                      0: filter is not loaded.                */
  tint      sampling_rate;     /**< sampling rate for this BF instance        */
  tint      frame_len;         /**< freame length in number of samples        */
  tint      filter_len;        /**< filter length, assuming all filters have
                                    the same length.                          */
} bfInst_t;

/* BF internal function prototypes */
tint bf_reset(bfInst_t *inst);
void bf_apply_filter(linSample *hist_buf, const Fract *coef_buf, Fract *work_buf,
                     const linSample *mics_in, LFract *workout, tint frame_len, 
                     tint filter_length);
void bf_apply_gain(LFract *workout, Fract *out, tint frm_len, tint num_active_mic);

tint bf_output(bfInst_t *inst, void *mics_in[], Fract *out, tuint opt_mode);

#endif
/* Nothing past this point */
