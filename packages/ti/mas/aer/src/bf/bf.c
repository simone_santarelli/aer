/**
 *  @file   bf.c
 *  @brief  Contains external API for BeamForming (BF) module.  
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 * 
 */

#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>
#include <ti/mas/util/utl.h>

#include <ti/mas/aer/bf.h>
#include <ti/mas/aer/src/bf/bfloc.h>
#include <ti/mas/aer/src/bf/bfport.h>

/* BF memory buffer descriptors */
#define bf_NUM_BUFS           5

#define bf_INSTANCE_BUF       0       /* instance buffer                   */
#define bf_FILT_DLINE_BUF     1       /* filter delay line buffer          */
#define bf_FILT_COEFF_BUF     2       /* filter coefficients buffer        */
#define bf_WORK1_BUF          3       /* filter delay line work buffer     */
#define bf_WORK2_BUF          4       /* filter accumulation work buffer   */

/* macro to validate provided buffers */
#define bf_buffer_invalid(bufp, buf_size, buf_align) (  (bufp->size<buf_size) \
                                 ||((int)bufp->log2align<buf_align) \
                                 ||(!typChkAlign(bufp->base, bufp->log2align)) )

/* memory buffer descriptors used by BF */                          
ecomemBuffer_t bfBufs[bf_NUM_BUFS] = {                             
  /* CLASS, LOG2 ALIGNMENT, BUFFER SIZE, VOLATILITY, BASE */
  ecomem_CLASS_EXTERNAL, bf_INSTANCE_BUF_ALIGN,   0, FALSE, NULL,
  ecomem_CLASS_EXTERNAL, bf_FILT_DLINE_BUF_ALIGN, 0, FALSE, NULL,
  ecomem_CLASS_INTERNAL, bf_FILT_COEFF_BUF_ALIGN, 0, FALSE, NULL,
  ecomem_CLASS_INTERNAL, bf_WORK1_BUF_ALIGN,      0, TRUE,  NULL,
  ecomem_CLASS_INTERNAL, bf_WORK2_BUF_ALIGN,      0, TRUE,  NULL 
};


/******************************************************************************
 * FUNCTION PURPOSE: Return memory buffer requirements of the BF module
 ******************************************************************************
 * DESCRIPTION: Calculate the worst case buffer sizes for the BF. Also specify
 *              alignment and volatility requirements.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfGetSizes(tint *nbufs, const ecomemBuffer_t **bufs, bfSizeConfig_t *cfg)
{
  tint max_num_mics, max_filt_len, max_samp_rate, frame_size;
  
  /* Validate configuration parameters */
  max_num_mics = cfg->max_num_mics;
  max_filt_len = cfg->max_filter_length;
  max_samp_rate = cfg->max_sampling_rate;
  if(  (max_num_mics > bf_MAX_SUPPORTED_NUM_MICS)
     ||((max_samp_rate!=bf_SAMP_RATE_8K) && (max_samp_rate!=bf_SAMP_RATE_16K))
     ||(cfg->bf_type != bf_TYPE_FIXED)
     ||(max_filt_len > bf_MAX_SUPPORTED_FILT_LEN) ) {
    return(bf_ERR_INVALIDPAR);
  }
  
  /* Calculate worst case buffer sizes of BF */
  frame_size = bf_FRAME_SIZE_8KHZ*max_samp_rate;
  bfBufs[bf_INSTANCE_BUF].size  = sizeof(bfInst_t); 
  bfBufs[bf_FILT_DLINE_BUF].size= max_num_mics*(max_filt_len-1)*sizeof(linSample);
  bfBufs[bf_FILT_COEFF_BUF].size= max_num_mics*max_filt_len*sizeof(Fract);
  bfBufs[bf_WORK1_BUF].size     = (frame_size + max_filt_len)*sizeof(linSample);
  bfBufs[bf_WORK2_BUF].size     = frame_size*sizeof(LFract);
  
  /* Return number of buffers required by BF */  
  *nbufs = bf_NUM_BUFS;
  
  /* Return buffer descriptions */
  *bufs = &bfBufs[0];                  
  
  return(bf_NOERR);
  
} /* bfGetSizes */

/******************************************************************************
 * FUNCTION PURPOSE: Create an instance of the BF module.
 ******************************************************************************
 * DESCRIPTION: Creates an instance of BF and initializes memory buffers.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfNew (void **bfInst, tint nbufs, ecomemBuffer_t *bufs, bfNewConfig_t *cfg)
{
  bfInst_t        *inst;
  ecomemBuffer_t  *bufp;
  tint max_num_mics, max_filt_len, frame_size;
  tsize buff_size;
  int i;
   
  /* Instance pointer must be NULL and number of buffers must be correct */
  if ((*bfInst != NULL) || (nbufs != bf_NUM_BUFS)) {
    return(bf_ERR_INVALIDPAR);
  }

  /* Check all buffers: if size is not 0, base address must not be NULL. */
  for (i=0, bufp=&bufs[0]; i<bf_NUM_BUFS; i++, bufp++) {
    if ((bufp->size>0) && (bufp->base==NULL)) {
      return(bf_ERR_NOMEMORY);
    }
  }

  /* Get the instance buffer provided by the user and validate it */
  bufp = &bufs[bf_INSTANCE_BUF];
  buff_size = sizeof(bfInst_t);
  if (bf_buffer_invalid(bufp, buff_size, bf_INSTANCE_BUF_ALIGN) || bufp->volat){
    return(bf_ERR_INVALIDBUF);
  }
  
  /* Assign instance memory */
  inst = (bfInst_t *)bufp->base;
  
  /* Get the filter delay line buffer provided by the user and validate it */
  bufp = &bufs[bf_FILT_DLINE_BUF];
  max_num_mics = cfg->sizeCfg.max_num_mics;
  max_filt_len = cfg->sizeCfg.max_filter_length;
  buff_size  = max_num_mics*(max_filt_len-1)*sizeof(linSample);
  if (bf_buffer_invalid(bufp, buff_size, bf_FILT_DLINE_BUF_ALIGN) || bufp->volat){
    return(bf_ERR_INVALIDBUF);
  }

  /* Assign delay line buffers to instance */  
  for (i=0;i<max_num_mics;i++) {
    inst->flt_dl_buf[i] = (linSample *)bufp->base+i*(max_filt_len-1);
  }

  /* Get the filter coefficients buffer provided by the user and validate it */
  bufp = &bufs[bf_FILT_COEFF_BUF];
  buff_size = max_num_mics*max_filt_len*sizeof(Fract);
  if (bf_buffer_invalid(bufp, buff_size, bf_FILT_COEFF_BUF_ALIGN) || bufp->volat) {
    return(bf_ERR_INVALIDBUF);
  }
  
  /* Assign memory for beamformer filters */
  for (i=0;i<max_num_mics;i++) {
    inst->filter_coef[i] = (Fract *)bufp->base+i*max_filt_len;
  }  
  
  /* Get the delay line work buffer provided by the user and validate it */
  bufp = &bufs[bf_WORK1_BUF];
  frame_size = bf_FRAME_SIZE_8KHZ*cfg->sizeCfg.max_sampling_rate;
  buff_size = (frame_size + max_filt_len)*sizeof(linSample);
  if (bf_buffer_invalid(bufp, buff_size, bf_WORK1_BUF_ALIGN)) {
    return(bf_ERR_INVALIDBUF);
  }

  /* Assign memory for filtering work buffer for delay line */
  inst->work_buf = (Fract *)bufp->base;

  /* Get the output work buffer provided by the user and validate it */
  bufp = &bufs[bf_WORK2_BUF];
  buff_size = frame_size*sizeof(LFract);
  if (bf_buffer_invalid(bufp, buff_size, bf_WORK2_BUF_ALIGN)) {
    return(bf_ERR_INVALIDBUF);
  }
  
  /* Assign memory for filtering work buffer for output */
  inst->workout_buf = (LFract *)bufp->base;
  
  /* Save size configuration parameters to instance */
  inst->max_sampling_rate = cfg->sizeCfg.max_sampling_rate;
  inst->max_num_mics      = cfg->sizeCfg.max_num_mics;
  inst->max_filt_len      = cfg->sizeCfg.max_filter_length;
  
  inst->handle = cfg->handle;                  /* init handle in instance */
  inst->state  = bf_CLOSED;                    /* set BF state to CLOSED  */

  /* Return instance address */
  *bfInst = (void *)inst;

  return(bf_NOERR);
  
} /* bfNew() */

/******************************************************************************
 * FUNCTION PURPOSE: Control of BF
 ******************************************************************************
 * DESCRIPTION: Controls the operation and parameters of the BF.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfControl (void *bfInst, bfControl_t *ctrl)
{
  bfInst_t *inst = (bfInst_t *)bfInst;
  tuint bf_enable, active_mics, op_mode;

  /* Check if BF has been opened */
  if (inst->state != bf_OPEN) {
    return (bf_ERR_NOTOPENED);
  }  
  
  /* Check if control structure is valid */
  if (ctrl == NULL) {
    return (bf_ERR_INVALIDPAR);
  }  
   
  /* Select microphones for bypass mode or subset mode */ 
  if(bf_chkbit(ctrl->valid_bitfield,bf_CTL_MICS)) {
    inst->act_mics = ctrl->mics; /* bitfield indicating which mics are selected */
  }  
  
  /* Enable/disable BF, or change operatin modes of BF */
  if(bf_chkbit(ctrl->valid_bitfield, bf_CTL_CONFIG)) {
    /* check operation mode before checking enable/disable, because enabling BF
       depends on whether filters are loaded for normal or subset mode. */
    if (ctrl->config.mask & bf_CTL_CFG_OPERATION) {
      /* Change operation mode */
      op_mode = ctrl->config.value & bf_CTL_CFG_OPERATION;
      if(  (op_mode!=bf_OPT_BYPASS) && (op_mode!=bf_OPT_SUBSET)
         &&(op_mode!=bf_OPT_NORMAL) ) {
        return(bf_ERR_INVALIDOPT);
      }
      else { 
        bf_clrbit(inst->sys_bits, bf_CTL_CFG_OPERATION);  
        bf_setbit(inst->sys_bits, op_mode);  
      }  
    } /* bf_CTL_CFG_OPERATION */

    if(ctrl->config.mask & bf_CTL_CFG_ENABLE) {
      /* Enable/disable BF */
      bf_enable = ctrl->config.value & bf_CTL_CFG_ENABLE; 
      
      if(bf_enable) {
        /* Before enabling BF, verify filters are loaded for normal or subset 
           operation mode. */
        op_mode = inst->sys_bits&bf_CTL_CFG_OPERATION;
        if(op_mode == bf_OPT_BYPASS) {
          /* Enable BF, no need to check filters for bypass mode. */
          bf_setbit(inst->sys_bits,bf_CTL_CFG_ENABLE);
        }
        else {
          if(op_mode == bf_OPT_NORMAL) {
            /* all mics are used in normal operation mode */
            active_mics = (1<<inst->num_mics) - 1;
          }
          else {
            /* only selected mics are used in subset operation mode */
            active_mics = inst->act_mics;
          }
      
          /* check if filter coefficients are loaded for all selected mics */
          if(inst->flags_filt_loaded == active_mics) {
            /* enable BF if all filters are loaded */
            bf_setbit(inst->sys_bits,bf_CTL_CFG_ENABLE);
          }
          else {
            /* return error if not all filters are loaded */
            return(bf_ERR_FILTNOCOEF);  
          }
        } /* op_mode != bf_OPT_BYPASS */
      } /* bf_enable */
      else {
        bf_clrbit(inst->sys_bits,bf_CTL_CFG_ENABLE);
      }
    } /* bf_CTL_CFG_ENABLE */
  } /* bf_CTL_CONFIG */
  
  /* reset BF */
  if(bf_chkbit(ctrl->valid_bitfield,bf_CTL_RESET)) {
    bf_reset(inst);
  }
  
  return(bf_NOERR);
} /* bfControl */
  
/******************************************************************************
 * FUNCTION PURPOSE: Open and configure an BF instance.
 ******************************************************************************
 * DESCRIPTION: This function configures an instance of the BF.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfOpen (void *bfInst, bfConfig_t *cfg)
{
  bfInst_t * inst = (bfInst_t *)bfInst;
  tint num_mics, sampling_rate, max_filt_len;
  
  /* Return error if state is not CLOSED */  
  if(inst->state != bf_CLOSED) {
    return(bf_ERR_NOTCLOSED);
  }
 
  /* Return error if no configuration parameters */
  if(cfg == NULL){
    return(bf_ERR_INVALIDPAR);
  } 
  
  /* Read and validate configuration parameters */
  num_mics = cfg->num_mics;
  if(num_mics <= 0 || num_mics > inst->max_num_mics) {
    return(bf_ERR_INVALIDPAR);
  }
  
  sampling_rate = cfg->sampling_rate;
  if(sampling_rate <=0 || sampling_rate > inst->max_sampling_rate) {
    return(bf_ERR_INVALIDPAR);
  }

  /* Initialize BF instance */
  inst->sampling_rate  = sampling_rate;
  inst->frame_len      = sampling_rate * bf_FRAME_SIZE_8KHZ;
  inst->num_mics       = num_mics;
  
  /* Disable BF by default. Set operation mode to NORMAL by default. */ 
  inst->sys_bits = bf_DISABLE | bf_OPT_NORMAL;
  
  /* Use first mic (mic 0) for bypass operation by default. */
  inst->act_mics = 0x01;  
  
  /* Initialize delay lines and coefficients */
  max_filt_len = inst->max_filt_len;
  utlFractMemSet(inst->flt_dl_buf[0], (Fract)0, (max_filt_len-1)*num_mics);
  utlFractMemSet(inst->filter_coef[0], (Fract)0, max_filt_len*num_mics);
  inst->flags_filt_loaded = 0x0;  /* no filter is loaded by default       */
  inst->filter_len = 0; /* don't know the filter length before filter is loaded */
  
  /* Declare the instance OPEN */
  inst->state = bf_OPEN;   

  return(bf_NOERR);
} /* bfOpen */

/******************************************************************************
 * FUNCTION PURPOSE: Returns debuging information for an BF instance.
 ******************************************************************************
 * DESCRIPTION: This function returns debuging information.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfDebugStat(void *bfInst, bfDebugStat_t *bfDbg)
{
  bfInst_t *inst = (bfInst_t *)bfInst;

  /* Make sure that BF instance is openned */
  if (inst->state != bf_OPEN) {
    return (bf_ERR_NOTOPENED);
  }  
  
  bfDbg->sys_bits = inst->sys_bits;
  bfDbg->num_mics = inst->num_mics;
  bfDbg->act_mics = inst->act_mics;
  bfDbg->flags_filt_loaded = inst->flags_filt_loaded;
  bfDbg->frame_len = inst->frame_len;
  bfDbg->filter_len = inst->filter_len;
  
  return(bf_NOERR);  
} /* bfDebugStat */

/******************************************************************************
 * FUNCTION PURPOSE: Close an instance of BF.
 ******************************************************************************
 * DESCRIPTION: This function closes an instance of BF by simply changing
 *              the state to CLOSED.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfClose (void *bfInst)
{
  bfInst_t *inst = (bfInst_t *)bfInst;

  if(inst->state != bf_OPEN) {
    return(bf_ERR_NOTOPENED);
  }
  
  /*Change the BF state to CLOSED */
  inst->state = bf_CLOSED;     
  
  return(bf_NOERR);
} /* bfClose */

/******************************************************************************
 * FUNCTION PURPOSE: Delete an BF instance.
 ******************************************************************************
 * DESCRIPTION: Deletes an instance of BF. Clears the instance pointer.
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfDelete (void **bfInst, tint nbufs, ecomemBuffer_t *bufs)
{
  bfInst_t *inst = (bfInst_t *)*bfInst;
  
  /* check if instance is already closed */
  if (inst->state != bf_CLOSED) {      
    return (bf_ERR_NOTCLOSED);
  }
  
  /* check if enough descriptors are provided to store all buffer addresses */
  if (nbufs < bf_NUM_BUFS) { 
    return (bf_ERR_NOMEMORY);
  }

  /* return buffer addresses used by BF */
  bufs[bf_INSTANCE_BUF].base   = inst;                 
  bufs[bf_FILT_DLINE_BUF].base = inst->flt_dl_buf[0];  
  bufs[bf_FILT_COEFF_BUF].base = inst->filter_coef[0]; 
  bufs[bf_WORK1_BUF].base      = inst->work_buf;       
  bufs[bf_WORK2_BUF].base      = inst->workout_buf;    

  /* clear the instance pointer */
  *bfInst = NULL;                              

  return(bf_NOERR);
} /* bfDelete */

/******************************************************************************
 * FUNCTION PURPOSE: Obtain the BF FIR or mic EQ filter coefficients for 1 mic
 ******************************************************************************
 * DESCRIPTION: Allows to read back the BF or mic EQ filter coefficients for 
 *              given mic.
 *
 * Refer to bf.h for detailed description.
 *
 *****************************************************************************/
tint bfGetFilter (void *bfInst, Fract *coeff, tint *length, tint filter_type, 
                  tint mic_index)
{
  int i;
  tint filter_length;
  bfInst_t *inst = (bfInst_t *)bfInst;
  
  filter_length = inst->filter_len;
                    
  if(filter_type == bf_FG_BF) {  /* This release has only FIR filter */
    /* read filter coefficients for this mic */ 
    for(i=0; i<filter_length; i++) {
	  /* coefficients order is reversed for easy implementation */
      coeff[filter_length-1-i] = inst->filter_coef[mic_index][i]; 
    }
	
    *length = filter_length;     /* return the filter length */
  }
  else {
    return (bf_ERR_INVALIDPAR);                    
  }
  
  return (bf_NOERR);
} /* bfGetFilter */  


/******************************************************************************
 * FUNCTION PURPOSE: Load the BF FIR or mic EQ filter coefficients for 1 mic
 ******************************************************************************
 * DESCRIPTION: Allows user to load a previously designed filter bank for beam
 *              forming or use to reset mic EQ filter.
 *
 * Refer to bf.h for detailed description. 
 *****************************************************************************/
tint bfPutFilter (void *bfInst, Fract *coeff, tint filter_type, tint mic_index, 
                  tint length)
{
  int i;
  bfInst_t *inst = (bfInst_t *)bfInst;
 
  /* make sure the instance is open */
  if (inst->state != bf_OPEN) {
    return (bf_ERR_NOTOPENED);
  }  
  
  /* copy filter coefficients */
  if(filter_type == bf_FG_BF) {
    if(length > inst->max_filt_len) {
      return(bf_ERR_INVALIDPAR);
    }

    inst->filter_len = length;     
     
    /* copy filter coefficients for this mic */ 
    for(i=0; i<length; i++) {
	  /* reverse order for easy implementation */
      inst->filter_coef[mic_index][i] = coeff[length-1-i];  
    }
    
    /* set flag to indicate filter is loaded for this mic */
    bf_setbitN(inst->flags_filt_loaded, mic_index);
  } /* filter_type == bf_FG_BF */
  else {
    return (bf_ERR_INVALIDPAR);                    
  }
            
  return(bf_NOERR);
} /* bfPutFilter */


/******************************************************************************
 * FUNCTION PURPOSE: Reset filter delayline when called 
 ******************************************************************************
 * DESCRIPTION: Allows user to reset filter delay lines when needed. May add 
 *              more in future. 
 *
 *****************************************************************************/
tint bf_reset(bfInst_t *inst)
{  
  /* Clear the filter delay lines for all the mics */
  utlFractMemSet(inst->flt_dl_buf[0], (Fract)0, inst->num_mics*(inst->filter_len-1));
  
  return(bf_NOERR);
} /* bf_reset */

 
/* Nothing past this point */
