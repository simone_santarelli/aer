/**
 *  @file   bfapplyflt.c
 *  @brief  Contains filtering routine for Beamforming (BF) module.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 * 
 */

#include <stdio.h>
#include <string.h>  

#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>
#include <ti/mas/util/utl.h>
#include <ti/mas/aer/bf.h>
#include <ti/mas/aer/src/bf/bfloc.h>

/******************************************************************************
 * FUNCTION PURPOSE: BF FIR filtering
 ******************************************************************************
 * DESCRIPTION: This function applies FIR filtering to one input and accumulates
 *              the filter output:
 *                 y(n) += sum(xl(n-k)*hl(k)), k=0:K-1, where xl is input of 
 *                 mic l and hl are the coefficients of mic l. 
 *
 *              This fuction is called multiple times to process all inputs. The
 *              output buffer is initialized before this function is called the 
 *              first time. 
 *
 *  tint bf_apply_filter (
 *  Input:
 *    linSample  *hist_buf,    - input delay line
 *    Fract      *coef_buf     - filter coefficients corresponding to the input,
 *                               coef_buf[0]=h(K-1), coef_buf[1]=h(K-2), etc. 
 *    Fract      *work_buf,    - scratch buffer    
 *    linSample  *mics_in      - input signal from one mic
 *    tint       frame_len     - number of samples per frame
 *    tint       filter_length - number of filter coefficients
 *
 *  Output:
 *    LFract     *workout      - output buffer, which accumulates the output of 
 *                               the filtering of all active mic signals: 
 *                               for i=0:frame_len-1
 *                                  for j=0:filter_length-1
 *                                      workout(i) += sum(delay(i+j)*coef(j))
 *                                  end
 *                               end
 *  Assumptions:
 *    - filter lenghth is an even number, and larger than 4 
 *    - coefficient buffer is aligned to 32-bit (two 16-bit coefficients)
 *    - frame length is at least 20 samples, multiple of 20 samples
 *****************************************************************************/
void bf_apply_filter(linSample *hist_buf, const Fract *coef_buf, Fract *work_buf,
                     const linSample *mics_in, LFract *workout, tint frame_len, 
                     tint filter_length)
{ 
  LFract temp;
  Fract  *delay_line = work_buf;  
  tint   i, j;

  /* copy history delay line into work buffer */
  memcpy(delay_line, hist_buf, sizeof(Fract)*(filter_length-1));

  /* copy input buffer into the work buffer */
  memcpy(&delay_line[filter_length-1], mics_in, sizeof(Fract)*frame_len);
 
  /* convolve the work buffer with the filter coefficients */
  for (i = 0; i < frame_len; i++) 
  { 
    /* initialize the accumulator */
    temp = (LFract)0;
    
    /* convolution loop */
    for (j = 0; j < filter_length; j++) 
    {
      /* keep intermediate result in Q15 */
      temp += frctMul(delay_line[i+j], bf_Q0, coef_buf[j], bf_Q15, bf_QACC);
    }    
    /* accumlate filter results in Q5, since the max_number_mics=16, 
       assume max_filter_len=64 */
    workout[i] += frctAdjustQ(temp,bf_QACC,bf_QWorkOut);
  }

  /* update history/delay line */
  memcpy(hist_buf, &delay_line[frame_len], sizeof(Fract)*(filter_length-1));
} /* bf_apply_filter */                    


/* Nothing past this point */
