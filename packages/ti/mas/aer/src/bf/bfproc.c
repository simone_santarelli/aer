/**
 *  @file   bfproc.c
 *  @brief  Contains Main Processing Routines for Beamforming (BF) module.
 *
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 * 
 */

#include <stdio.h>
#include <string.h>  

#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>
#include <ti/mas/util/utl.h>
#include <ti/mas/aer/bf.h>
#include <ti/mas/aer/src/bf/bfloc.h>


/******************************************************************************
 * FUNCTION PURPOSE: Main Processing API funciton of BF 
 ******************************************************************************
 * DESCRIPTION: This function performs the beamforming processing. 
 *
 * Refer to bf.h for detailed description. 
 *
 *****************************************************************************/
tint bfProcess(void *bfInst, void *mics_in[], void *out)
{
  bfInst_t *inst = (bfInst_t *)bfInst;
  tint ret_val, num_active_mic;
  int i; 
  tuint opt_bits;
    
  /* Return error if state is not OPENED */  
  if(inst->state != bf_OPEN) {
    return(bf_ERR_NOTOPENED);
  }

  /* check if BF is enabled */
  if(bf_chkbit(inst->sys_bits, bf_CTL_CFG_ENABLE)==bf_DISABLE) { 
    /* put silence to output if disabled*/
    utlFractMemSet((Fract *)out, (Fract)0, inst->frame_len);
    return(bf_ERR_DISABLED);
  }
  
  ret_val = bf_NOERR;
 
  /* check BF operation mode */
  opt_bits = (inst->sys_bits&bf_CTL_CFG_OPERATION);  
  if ((opt_bits == bf_OPT_NORMAL) || (opt_bits == bf_OPT_SUBSET)) {
    /* normal beamforming or subset mic beamforming - go through the filters */
    ret_val = bf_output(inst, mics_in, (Fract *)out, opt_bits);
  }
  else if (opt_bits == bf_OPT_BYPASS) {
    /* bypass mode, not going through the filters */
	num_active_mic = 0;
    for (i=0; i<inst->num_mics; i++) 
	{ 
	  /* check through each mic to see whether it's active */
      if(bf_chkbitN(inst->act_mics,i) == bf_MIC_ACTIVE) {
        /* copy the mic input to output, if mic is connected */
        memcpy(out, mics_in[i], sizeof(linSample)*inst->frame_len);      
		
		num_active_mic++;
      }
    } /* for */
	
	if(num_active_mic == 0) {
      ret_val = bf_ERR_INVALIDSRC;  /* error: no active mic */
	} 
	else if(num_active_mic > 1) {
      ret_val = bf_ERR_INVALIDOPT;  /* error: more than 1 active mic */
	}
	else {
	  ret_val = bf_NOERR;
	}
  } /* bf_OPT_BYPASS */
  else {
    ret_val = bf_ERR_INVALIDOPT;  
  }
  
  return (ret_val);
} /* bfProcess */

/******************************************************************************
 * FUNCTION PURPOSE: Filtering mic array signals using user-configured filters 
 ******************************************************************************
 * DESCRIPTION: This function generates single output from multiple input sources.
 *              First, each mic signal are filtered using the corresponding filter, 
 *              then all the filtered signals are summed together and pass through 
 *              a gain unit to produce the final output. 
 *
 *  tint bf_output (
 *  Input:
 *    bfInst_t *inst,      - pointer to BF instance
 *    void *mics_in[],     - signal from all active mics
 *    tuint opt_mode       - operation mode: bf_OPT_NORMAL or bf_OPT_SUBSET 
 *  Output:
 *    Fract *out           - single output from BF
 *****************************************************************************/
tint bf_output(bfInst_t *inst, void *mics_in[], Fract *out, tuint opt_mode)
{
  LFract *workout;
  tint i, frm_len, filt_len, num_active_mic = 0;   
   
  workout = inst->workout_buf;
  frm_len = inst->frame_len;
  filt_len= inst->filter_len;
  
  /* Initialize the temporary output buffer */
  utlLFractMemSet(workout, (LFract)0, inst->frame_len);
        
  /* Filter the microphone signals */
  if (opt_mode == bf_OPT_NORMAL) {
    /* filter all mics in normal operation mode */
    for (i=0;i<inst->num_mics;i++)
    {
	  /* apply filter and accumulate output in buffer workout */
      bf_apply_filter(inst->flt_dl_buf[i], inst->filter_coef[i], inst->work_buf, 
                     (linSample *)mics_in[i], workout, frm_len, filt_len);
    }
    num_active_mic = inst->num_mics;
  }
  else {
    /* filter active mics in subset mode */
    for (i=0;i<inst->num_mics;i++)
    {
      if(bf_chkbitN(inst->act_mics,i) == bf_MIC_ACTIVE) {
        /* apply filter and accumulate output in buffer workout */
        bf_apply_filter(inst->flt_dl_buf[i], inst->filter_coef[i], inst->work_buf, 
                       (linSample *)mics_in[i], workout, frm_len, filt_len);
        num_active_mic++;    
      }
    }
  }

  /* if no active mic, return error code */
  if (num_active_mic == 0) {
    return (bf_ERR_INVALIDSRC);
  }
  
  /* apply gain to the summed signal to have unity gain for BF */
  bf_apply_gain(workout, out, frm_len, num_active_mic); 
  
  return (bf_NOERR); 
} /* bf_output */                    

/* Nothing past this point */
