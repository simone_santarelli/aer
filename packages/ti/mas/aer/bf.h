#ifndef _BF_H
#define _BF_H

/**
 *  @file   bf.h
 *  @brief  Contains external API for BeamForming (BF) module.
 *
 *        Copyright (c) 2007 � 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 */

#include <ti/mas/types/types.h>
#include <ti/mas/util/ecomem.h>               /* memory descriptor structure */

/* Define BF Module as a master group in Doxygen format and add all BF API 
   definitions to this group. */
/** @defgroup bf_module BF Module API
 *  @{
 */
/** @} */

/** @defgroup bf_api_functions BF Functions
 *  @ingroup bf_module
 */

/** @defgroup bf_api_structures BF Data Structures
 *  @ingroup bf_module
 */

/** @defgroup bf_api_constants BF Constants (enum's and define's)
 *  @ingroup bf_module
 */

/** @defgroup bf_err_code BF Error Codes
 *  @ingroup bf_api_constants
 *  @{
 *
 *  @name BF Error Codes
 *
 *  Error codes returned by BF API functions. 
 */
/*@{*/
enum {
  bf_NOERR          = 0, /**< success, no error */ 
  bf_ERR_NOMEMORY   = 1, /**< supplied memory are not enough                  */
  bf_ERR_NOTOPENED  = 2, /**< BF instance not in opened state                 */
  bf_ERR_DISABLED   = 3, /**< BF instance not in enabled state                */
  bf_ERR_NOTCLOSED  = 4, /**< BF instance not in closed state                 */
  bf_ERR_INVALIDPAR = 5, /**< configuration parameter is invalid              */
  bf_ERR_INVALIDSRC = 6, /**< no valid source is provided to process          */
  bf_ERR_INVALIDOPT = 7, /**< invalid operation mode                          */
  bf_ERR_INVALIDBUF = 8, /**< provided buffers are not valid                  */
  bf_ERR_FILTNOCOEF = 9  /**< BF FIR filters have not been loaded.            */
};
/*@}*/
/** @} */

/** @defgroup bf_samp_rate BF Sampling Rate Definitions
 *  @ingroup bf_api_constants
 *  @{
 *
 *  @name BF sampling rates
 *
 *  Sampling rates that are supported by BF module. 
*/
/*@{*/
enum {
  bf_SAMP_RATE_8K  = 1,                   /**< 8kHz  */
  bf_SAMP_RATE_16K = 2                    /**< 16kHz */
};
/*@}*/
/** @} */

/** @defgroup bf_size_cfg_max BF Size Configuration Max
 *  @ingroup bf_api_constants
 *  @{
 *
 *  Maximum supported parameters by BF module. 
*/
/*@{*/
#define bf_MAX_SUPPORTED_NUM_MICS  16  /**< Maximum supported number of mics */
#define bf_MAX_SUPPORTED_FILT_LEN  64  /**< Maximum supported filter length  */
/*@}*/
/** @} */

/** @defgroup bf_type BF Type Definitions
 *  @ingroup bf_api_constants
 *  @{
 *
 *  @name BF type
 *
 *  Beamforming types that are supported by BF module.
*/
/*@{*/
enum {
  bf_TYPE_FIXED     = 1         /**< fixed beamforming    */
};
/*@}*/
/** @}*/

/** @defgroup bf_filter_type BF Filter Type Definitions 
 *  @ingroup bf_api_constants
 *  @{
 *  Filter groups within the BF module.
*/
/*@{*/
enum {
  bf_FG_BF      = 1   /**< fixed beamforming FIR filters */
};
/*@}*/
/*@}*/

/** 
 *  @ingroup bf_api_structures
 *
 *  @brief Configuration structure for bfGetSizes()
 *  \brief This structure is used for getting memory requirements of BF module 
 *         through function bfGetSizes().
 */
typedef struct bfSizeConfig_s {
  tint max_sampling_rate; /**< Maximum sampling rate: bf_SAMP_RATE_8K (8kHz) 
                               or bf_SAMP_RATE_16K (16kHz).                   */  
  tint max_num_mics;      /**< Maximum number of microphones in the mic array:
                               must be smaller than or equal to bf_MAX_SUPPORTED_NUM_MICS,
                               otherwise, bfGetSizes() will return an error.  */
  tint max_filter_length; /**< Maximum filter length for BF FIR filters:
                               must be smaller than or equal to bf_MAX_SUPPORTED_FILT_LEN,  
                               otherwise, bfGetSizes() will return an error.  */                               
  tint bf_type;           /**< BF type: must be bf_TYPE_FIXED in this release.*/
} bfSizeConfig_t; 

/** 
 *  @ingroup bf_api_structures
 *
 *  @brief Configuration structure for bfNew()
 *  \brief This structure is used for creating a new BF instance through 
 *         function bfNew().
 */
typedef struct bfNewConfig_s {
  void  *handle;                                    /**< handle              */
  bfSizeConfig_t  sizeCfg;                          /**< size configration   */
} bfNewConfig_t;

/** 
 *  @ingroup bf_api_structures
 *
 *  @brief Configuration structure for bfOpen()
 *  \brief This structure contains system parameters to configure a BF 
 *         instance through bfOpen(). BF doesn't have default value for these
 *         parameters, and they must be set explicitly when bfOpen() is called
 *         the first time after bfNew(). 
 *  \remarks This structure does not contain any configurable
 *           parameters that are controlled through bfControl_s.
 */
typedef struct bfConfig_s {
  tint  sampling_rate;          /**< Sampling rate: 
                                     bf_SAMP_RATE_8K:  8kHz
                                     bf_SAMP_RATE_16K: 16kHz  */  
  tint  num_mics;               /**< Number of microphones    */
} bfConfig_t;

/** 
 *  @ingroup bf_api_structures
 *
 *  @brief BF control configuration bitfield
 *
 *  Bitfield masks bf_CTL_CFG_XYZ may be used to change BF
 *  configuration through bfControl(), by
 *  constructing elements "mask" and "value" of bfCtlCfg_s.
 *
 *  @verbatim
    BF Control Configuration Bitfield: 
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------
       bits      values       meaning
    (from LSB)
    
        0                     bf_CTL_CFG_ENABLE mask
                   0          Disable BF: Ignore input signals and produce all 
                                          0's as output. 
                   1          Enable BF: Process input signals as configured 
                                         by other control bits and commands.
    
       1:2                    bf_CTL_CFG_OPERATION mask
                   00 (0)     Perform normal beamforming operation using all 
                              microphones in the array.
                   01 (1)     Bypass mode: single input is passed to the output 
                              without any processing. The input must be selected 
                              using the bf_CTL_MICS control. Note: only one mic 
                              can be active. Will return error message if more 
                              than one mic is selected.
                   10 (2)     Array subset mode: inputs that are selected by 
                              the bf_CTL_MICS control will be used for beamforming. 
                              All others are ignored. Note: even when just one
                              microphone is selected, the filtering will be still 
                              applied. To avoid filtering use the bypass mode.
                   11 (3)     Invalid configuration: will result in error.

    +-------------------+----------------+ 
    | Operation Modes   | BF status      | 
    +---------+---------+----------------+ 
    |    0    |    0    | Normal BF      | 
    +---------+---------+----------------+ 
    |    0    |    1    | Bypass mode    | 
    +---------+---------+----------------+ 
    |    1    |    0    | Array subset   | 
    +---------+---------+----------------+ 
    |    1    |    1    | INVALID        | 
    +---------+---------+----------------+ 
  \endverbatim
 *
 */
typedef struct bfCtlCfg_s {
  tuint mask;     /**< Indicates the bits to be changed.                      */
  tuint value;    /**< Provides new values for each bit selected by the mask. */
/*@{*/
#define bf_CTL_CFG_ENABLE     0x0001  /**< Bit  0   : BF Enable mask */
#define bf_CTL_CFG_OPERATION  0x0006  /**< Bits 1:2 : Operation mode mask */
/*@}*/
} bfCtlCfg_t;

/** @defgroup bf_enable_const BF Enable/Disable Constants
 *  @ingroup bf_api_constants
 *
*/
/*@{*/
enum {
  bf_DISABLE = 0,
  bf_ENABLE  = 1
};
/*@}*/
/** @}*/

/** @defgroup bf_op_mode BF Operation Mode Constants
 *  @ingroup bf_api_constants
 *  @{
 *
 *  These operation modes are associated with the mode control in bfCtlCfg_t.
 *   
*/
/*@{*/
enum {
  bf_OPT_NORMAL    = 0,          /**<  normal BF operational mode */
  bf_OPT_BYPASS    = 2,          /**<  bypass mode, no filtering  */
  bf_OPT_SUBSET    = 4,          /**<  subset microphone mode  */
  bf_OPT_ERROR     = 6           /**<  invalid operation code */
};
/*@}*/
/** @}*/

/** 
 *  @ingroup bf_api_structures
 *
 *  @brief Control structure for bfControl()
 *
 *  \brief This structure is used to change BF configurable parameters. 
 *         Element bfControl_s.valid_bitfield indicates which parameter to be
 *         changed, and following elements give the parameter values. 
 */
typedef struct bfControl_s {
    tuint       valid_bitfield; 
#define bf_CTL_CONFIG  0x0001   /**< Use config structure to configure BF     */
#define bf_CTL_MICS    0x0002   /**< Use mics bitfield to select active mics  */
#define bf_CTL_RESET   0x0004   /**< Reset delay lines and all internal state 
                                     status, debug stats, etc.                */
    bfCtlCfg_t  config;         /**< Configuration bitfield */
    tuint       mics;           /**< Microphone selection bitfield */
} bfControl_t;

/** 
 *  @ingroup bf_api_structures
 *
 *  @brief BF debug structure 
 *
 *  \brief This structure is used to provide BF debugging information.
 */
#define bf_DEBUG_STATE_NUM_TUINT 16 
typedef struct bfDebugStat_s {
  tuint  sys_bits;              /**< BF system control bits (\ref bfCtlCfg_t):
                                       bit 0: enable/disable,  
                                       bits 1~2: operation mode.              */     
  tint   num_mics;              /**< Number of total mics.                    */
  tuint  act_mics;              /**< Active mic bitfield. For each bit mask, 
                                       0: this mic is inactive,
                                       1: this mic is active.                 */
  tuint  flags_filt_loaded;     /** Filter loaded bitfield. For each bit mask,
                                        0: filter is not loaded for this mic,
                                        1: filter is loaded for this mic.     */
  tint   sampling_rate;         /**< Sampling rate.                           */
  tint   frame_len;             /**< Freame length.                           */
  tint   filter_len;            /**< filter length.                           */  
} bfDebugStat_t;


/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfGetSizes() obtains from BF the memory requirements of  
 *         an instance, which depend on provided configuration parameters.   
 *
 *  @param[in]   cfg     Pointer to a size configuration structure.
 *  @param[out]  nbufs   Memory location to store the returned number of buffers
 *                       required by the instance.
 *  @param[out]  bufs    Memory location to store the returned address of the 
 *                       vector of memory buffer descriptions required by BF. 
 *
 *  @remark Type ecomemBuffer_t is defined in ecomem.h of util package.
 *
 *  @retval BF error code:
 *     - bf_NOERR: success
 *     - bf_ERR_INVALIDPAR, any of the parameters in bfSizeConfig_t is invalid: 
 *          - max_num_mics is larger than bf_MAX_SUPPORTED_NUM_MICS,
 *          - max_filter_length is larger than bf_MAX_SUPPORTED_FILT_LEN,
 *          - max_sampling_rate is not bf_SAMP_RATE_8K and not bf_SAMP_RATE_16K,
 *          - bf_type is not bf_TYPE_FIXED.
 */
tint bfGetSizes(tint *nbufs, const ecomemBuffer_t **bufs, bfSizeConfig_t *cfg);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfNew() creates a new instance of BF.  
 *  \remark Function bfNew() must be called before bfOpen()is called.
 *
 *  @param[in]     nbufs     Number of memory buffers allocated by the user.
 *  @param[in]     bufs      Pointer to memory buffer descriptors defined by
 *                           user.
 *  @param[in]     cfg       Pointer to new instance configuration structure.
 *  @param[in,out] bfInst    Memory location that will receive a pointer to 
 *                           the created BF instance. 
 *  \remark Buffer alignment property of every buffer passed to bfNew() must be 
 *          equal to or better than what is returned by bfGetSizes(), and must
 *          be in consistency with the base address of the buffer.
 *
 *  @pre  The pointer at the location pointed to by bfInst must be set to NULL 
 *        before this function is called.
 *  @post A pointer to the BF instance buffer will be returned to the location
 *        pointed to by bfInst. Instance state will be set to closed.
 *
 *  @retval  BF error code:
 *     - bf_NOERR:           success
 *     - bf_ERR_INVALIDPAR:  *bfInst is not NULL or nbufs is not correct 
 *     - bf_ERR_INVALIDBUF:  one or more provided buffers are invalid: 
 *                           - provided buffer size is less than requested, or
 *                           - buffer alignment does not meet requirement, or
 *                           - alignment and base address are not consistent, or
 *                           - volatility does not meet requirement.
 *     - bf_ERR_NOMEMORY    buffer size is not zero but base address is NULL.
 *
 */
tint bfNew (void **bfInst, tint nbufs, ecomemBuffer_t *bufs, bfNewConfig_t *cfg);

/**
 *  @ingroup bf_api_functions 
 *
 *  @brief Function bfOpen() initializes and configures a BF instance.
 *
 *  @remark This function must be called after bfNew() to initialize a new BF
 *          instance. It may also be called to reconfigure an instance that 
 *          has been closed by bfClose() but not deleted by bfDelete(). After BF 
 *          instance is opened, bfControl() and bfProcess() may be called for 
 *          control or processing.  
 *
 *  @param[in]          cfg      Pointer to BF configuration parameter.
 *  @param[in,out]      bfInst   Pointer to BF instance.
 *
 *  @retval   BF error code:
 *     - bf_NOERR: success
 *     - bf_ERR_NOTCLOSED: BF instance state has not been set to closed, i.e, 
 *                         bfNew() or bfClose() not called for the instance.
 *     - bf_ERR_INVALIDPAR: bad configuration parameters:
 *                          - bfConfig_s.sampling_rate is larger than 
 *                            bfSizeConfig_s.max_sampling_rate, or
 *                          - bfConfig_s.num_mics is larger than 
 *                            bfSizeConfig_s.max_num_mics. 
 *
 *  @pre  Function bfNew() must be called before bfOpen() is called the first
 *        time to open a new instance. For subsequent calls to open an existing
 *        instance, bfClose() must be called before bfOpen() to close the
 *        instance.
 *
 *  @post All the parameters and operation modes that are not part of bfConfig_t 
 *        are set to default by bfOpen:
 *           - BF is disabled,
 *           - BF operation mode is set to bypass,
 *           - First mic is selected for bypass operation, 
 *           - BF FIR filter coefficients are reset to 0's.
 *
 */
tint bfOpen (void *bfInst, bfConfig_t *cfg);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfControl() changes BF operation modes or parameters. 
 *
 *  @param[in,out]      bfInst    Pointer to BF instance.
 *  @param[in]          ctl       Pointer to BF control structure. 
 *
 *  @retval   BF error code:
 *     - bf_NOERR: success     
 *     - bf_ERR_NOTOPENED: BF instance has not been opened     
 *     - bf_ERR_INVALIDPAR: argument ctl is NULL
 *     - bf_ERR_INVALIDOPT: new operation mode is invalid
 *     - bf_ERR_FILTNOCOEF: try to enable BF before filters are loaded     
 *
 */
tint bfControl (void *bfInst, bfControl_t *ctl);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfGetFilter() gets FIR filter coefficients for a given mic. 
 *        
 *
 *  @param[in]   bfInst         Pointer to BF instance.
 *  @param[in]   filter_type    BF FIR filter or mic EQ filter (only FIR filter 
 *                              is supported in this release).
 *  @param[in]   mic_index      Indicating which mic's coefficients to get.
 *  @param[out]  coeff          External array to store the returned coefficients. 
 *                              It must have enough space to store maximum number 
 *                              of coefficients.
 *  @param[out]  filter_length  Length of the filter for the indicated mic. 
 *     
 *  @retval   BF error code:
 *     - bf_NOERR: success     
 *     - bf_ERR_INVALIDPAR: filter_type is not bf_FG_BF.
 *
 *  @sa bfPutFilter
 */
tint bfGetFilter (void *bfInst, Fract *coeff, tint *filter_length, 
                  tint filter_type, tint mic_index);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfPutFilter() loads FIR filters for beamforming. 
 *        
 *
 *  @param[in]   bfInst         Pointer to BF instance.
 *  @param[in]   coeff          External array of the coefficients to be loaded.
 *  @param[in]   filter_type    Beamforming FIR filter or mic EQ filter. Refer to
 *                              \ref bf_filter_type. For this release, only BF 
 *                              FIR filter (bf_FG_BF) is supported.
 *  @param[in]   mic_index      Indicating which mic's coefficients to be loaded.
 *                              Taking values 0, 1, 2, ... 
 *  @param[in]   filter_length  Number of coefficients. Must be less than 
 *                              bfSizeConfig_s.max_filter_length and must be 
 *                              the same for all mics. 
 *     
 *  @retval      BF error code:
 *     - bf_NOERR: success     
 *     - bf_ERR_NOTOPENED: BF instance has not been opened     
 *     - bf_ERR_INVALIDPAR: one or more of following parameters are invalid:
 *          - filter_type is not bf_FG_BF. 
 *          - filter_length is larger than bfSizeConfig_s.max_filter_length. 
 *
 *  \remark   Coefficients must be loaded for all FIR filters before enabling BF
 *            to process data, unless BF is configured to run in subset mode. 
 *
 *  \remark   BF assumes delay compensation is achieved solely through the FIR
 *            filter for every microphone, and doesn't make any additional delay 
 *            compensation. Therefore filter coefficients must be designed to
 *            compensate all of the desired delay for each microphone. 
 *
 *  \remark   For C64x+\C674x platforms, filter length MUST be even. If the 
 *            actual number of coefficients is odd, one padding zero can be 
 *            added at the end to make filter length to be even.
 *
 *  @sa bfGetFilter
 */
tint bfPutFilter (void *bfInst, Fract *coeff, tint filter_type, tint mic_index, 
                  tint filter_length);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfProcess() processes input microphone signals according to 
 *         the operation mode configured by bfControl().
 *
 *         In normal operation or subset mode, all active mic signals are first 
 *         filtered by the corresponding BF filters and then added together 
 *         before going through a gain adjustment to produce the final BF output.
 *         Refer to bfCtlCfg_t for more information. 
 *
 *  @param[in]  bfInst          Pointer to BF instance.
 *  @param[in]  mics_in         Vector of pointers to signal frames of all mics.
 *                                 - mics_in[0] corresponds to mic 0,
 *                                 - mics_in[1] corresponds to mic 1,
 *                                 - mics_in[L-1] corresponds to mic L-1.
 *                              Where L is the total number of mics. 
 *  \remark If operation mode is set to subset, not all mics will be used for 
 *          beamforming, but only the selected mics according to bfControl_s.mics. 
 *
 *  @param[out] out             Buffer to store the output signal
 *
 *  @remark All input signals and output signal are 16-bit linear samples 
 *          of 10 msec. 
 *
 *  @retval BF error code:
 *  @verbatim
     error code         description
     bf_NOERR           success
     bf_ERR_NOTOPENED   BF instance has not been opened, no operation is allowed
     bf_ERR_DISABLED    BF instance is not enabled, will output silence
     bf_ERR_INVALIDSRC  no microphone is active, output is invalid
     bf_ERR_INVALIDOPT  >1 mic is active in bypass mode, output is invalid
    \endverbatim
 *
 */
tint bfProcess(void *bfInst, void *mics_in[], void *out);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfDebugStat() returns debug information.
 *
 *  @param[in]  bfInst         Pointer to BF instance.
 *  @param[out] bfDbg          Debug information.
 *
 *  @retval   BF error code. 
 *     - bf_NOERR: success     
 *     - bf_ERR_NOTOPENED: BF instance has not been opened     
 *
 */         
tint bfDebugStat(void *bfInst, bfDebugStat_t *bfDbg);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfClose() closes a BF instance. 
 *  \remark Function bfClose() must be called before bfDelete() is called.
 *
 *  @param[in,out]  bfInst   pointer to the instance to be closed
 *
 *  @retval   BF error code. 
 *     - bf_NOERR: success     
 *     - bf_ERR_NOTOPENED: BF instance has not been opened     
 *
 */
tint bfClose(void *bfInst);

/**
 *  @ingroup bf_api_functions
 *
 *  @brief Function bfDelete() deletes the BF instance identified by bfInst
 *         and returns the addresses of the buffers used by this instance.  
 *
 *  @param[in]      nbufs    Number of buffers used by this instance.
 *  @param[in]      bufs     Pointer to buffer descriptors to store returned
 *                           addresses of the buffers used by this instance.
 *  @param[in,out]  bfInst   Memory location where the pointer to BF instance
 *                           is stored.
 *
 * \remark  This function doesn't deallocate the memory used by BF. The actual 
 *          deallocation of instance should be done elsewhere after 
 *          this function is called. Hence, the user has to save the buffer 
 *          descriptors to make "deallocation" possible.
 *
 *  @retval   BF error code. 
 *     - bf_NOERR: success     
 *     - bf_ERR_NOTCLOSED: BF instance has not been closed. 
 *     - bf_ERR_NOMEMORY:  Not enough buffer descriptors to store the 
 *                         buffer addresses to be returned by this function. 
 *
 *  @pre  BF instance must be closed by bfClose() before bfDelete() is called. 
 *  @post After bfDelete() is called, BF instance pointer stored at bfInst 
          will be set to NULL, and the addresses of the buffers used by this 
          instance will be returned to the location pointed to by bufs.
 */
tint bfDelete(void **bfInst, tint nbufs, ecomemBuffer_t *bufs);


/* -------------------------- BF Call Table -------------------------------- */
/**
 *  @ingroup bf_api_structures
 *
 *  @brief BF call table
 *
 */
typedef struct {
  tint  (*bfClose)     (void *bfInst);
  tint  (*bfControl)   (void *bfInst, bfControl_t *ctl);
  tint  (*bfDelete)    (void **bfInst, tint nbufs, ecomemBuffer_t *bufs);
  tint  (*bfGetSizes)  (tint *nbufs, const ecomemBuffer_t **bufs,
                        bfSizeConfig_t *cfg);
  tint  (*bfNew)       (void **bfInst, tint nbufs, ecomemBuffer_t *bufs,
                        bfNewConfig_t *cfg);
  tint  (*bfOpen)      (void *bfInst, bfConfig_t *cfg);
  tint  (*bfPutFilter) (void *bfInst, Fract *coeff, tint filter_type, 
                        tint mic_index, tint filter_length);
  tint  (*bfGetFilter) (void *bfInst, Fract *coeff, tint *filter_length, 
                        tint filter_type, tint mic_index);
  tint  (*bfProcess)   (void *bfInst, void *mics_in[], void *out);                           
} bfCallTable_t;

#endif
/* nothing past this point */

