#ifndef _DRC_H
#define _DRC_H

/**
 *  @file   drc.h
 *  @brief  Contains definitions and external APIs for DRC.
 *
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 */

/* System include files and utilities' APIs */
#include <ti/mas/types/types.h>               /* DSP types                   */
#include <ti/mas/util/ecomem.h>               /* memory descriptor structure */
#include <ti/mas/util/debug.h>                /* debug structure defines     */

/*  DRC API definition group. */
/** @defgroup drc_module DRC Module API
 *  @{
 */
/** @} */

/** @defgroup drc_api_functions DRC Functions
 *  @ingroup drc_module
 */

/** @defgroup drc_api_structures DRC Data Structures
 *  @ingroup drc_module
 */

/** @defgroup drc_api_constants DRC Constants (enum's and define's)
 *  @ingroup drc_module
 */

/** @defgroup drc_err_code DRC Error Codes
 *  @ingroup drc_api_constants
 *  @{
 *
 *  @name DRC error codes
 *
 *  Error codes returned by DRC API.
 */
/*@{*/
enum {
  drc_NOERR            = 0, /**< success, no error                          */
  drc_ERR_NOMEMORY     = 1, /**< supplied memory are not enough             */
  drc_ERR_CREATED      = 2, /**< DRC has been created already               */
  drc_ERR_NOTCREATED   = 3, /**< DRC has not been created yet               */
  drc_ERR_NOTOPENED    = 4, /**< DRC instance has not been opened yet       */
  drc_ERR_NOTCLOSED    = 5, /**< DRC instance has not been closed yet       */
  drc_ERR_INVALIDPAR   = 6, /**< configuration parameter is invalid         */
  drc_ERR_MEMBUFVECOVF = 7, /**< no enough space to return buffer addresses */
  drc_ERR_INVALIDCFGBITS=8, /**< Invalid CFGBITS while enable DRC           */
  drc_ERR_INVALIDFIR   = 9, /**< FIRs are not set when multiband comp is on */
  drc_ERR_INVALIDSRATE= 10  /**< Invalid sampling rate.                     */

};                            
/* @} */
/** @} */

/** @defgroup drc_operation_bitfield DRC Operation Mode bitfield
 *  @ingroup drc_api_constants
 *  @{
 *
 *  @name DRC Operation Mode bitfield is used to set DRC operation mode/state.
 *   The constants used in drcOpen(), drcControl() with control 
 *   code drc_CTL_SET_CFGBITS and drcGetParameter() with get code drc_GET_CFGBITS
 *
 *  See \ref sys_cfg
 */
/* @{ */
enum {
  drc_CFGBITS_FBAND       = 0, /**< Bit 0: if 1, Full band is enabled */
  drc_CFGBITS_LBAND       = 1, /**< Bit 1: if 1, Low band is enabled  */
  drc_CFGBITS_MBAND       = 2, /**< Bit 2: if 1, Mid band is enabled  */
  drc_CFGBITS_HBAND       = 3, /**< Bit 3: if 1, High band is enabled */
  drc_CFGBITS_LIM         = 4, /**< Bit 4: if 1, limiter is enabled   */
  drc_CFGBITS_VAD         = 5  /**< Bit 5: if 1, vad is enabled       */
};  
/* @} */
/** @} */

/**
 *  @ingroup drc_api_structures
 *  @brief The DRC gain curve configuration structure.
 *  \brief This structure specifies DRC gain curve configuration parameters.
 *         The parameters are set in drcBandCfg_t through drcControl() with  
 *         control code drc_CTL_SET_{FBAND, LBAND, MBAND and HBAND}.
 *
 *  See \ref compressor_param
 *  @sa drcBandCfg_t
 */
typedef struct drcCurveParam_s {
  tint  exp_knee;          /**< The expansion knee point in dBm0.       */
  tuint ratios;            /**< Q4 - Expansion and Compression Ratios
                                     from 16 to 255 in Q4 format.
                             *  @verbatim
                                Bit 0-7: expansion ratio = E_out/E_in. From 1.0 - 15.9375.  
                                Bit 8-15:compression ratio = E_in/E_out. From 1.0 - 15.9375.      
                                 \endverbatim
                             */
  tint  max_amp;           /**< Maximum amplification in dB.            */
  tint  com_knee;          /**< The compression knee point in dBm0.     */
  tint  energy_lim;        /**< The energy limiting threshold in dBm0.  */
} drcCurveParam_t;         /**< It is used in drcBandCfg_t for cfg band.*/

/** @defgroup drc_bandcfg_validbitmap DRC Band Configuration Valid Bit
 *  @ingroup drc_api_constants
 *  @{
 *
 *  @name DRC valid bitfield used in drc_CTL_SET_XXX_BAND operation.
 *  Bit field to select parameters to be configured through 
 *  drc_CTL_SET_XXX_BAND operation in drcControl(). 
 *
 *  @sa drcBandCfg_t
 */

/* @{ */
enum {
  drc_BAND_CFG_VALID_LEV_TC_BIT      = 0,/**< SET Bit 0 to enable level TC cfg    */
  drc_BAND_CFG_VALID_GAIN_TC_BIT     = 1,/**< SET Bit 1 to enable gain TC cfg     */
  drc_BAND_CFG_VALID_CURVE_PARAM_BIT = 2 /**< SET Bit 2 to enable curve param cfg */
};  
/* @} */
/** @} */

/** 
 *  @ingroup drc_api_structures
 *  @brief The DRC per band configurations.
 *  \brief This structure specifies per band configurations through drcControl()
 *         with control code drc_CTL_SET_{FBAND, LBAND, MBAND and HBAND}.
 *
 *  See \ref compressor_param
 *  @sa drcControl_t
 */
typedef struct drcBandCfg_s {
  tuint valid_bitfield;   /**< Bitfield for configuration parameters
                               as defined in drc_BAND_CFG_VALID_xxx_BIT       */
  tuint tc_log2;          /**< 
                               @verbatim
                              Q0 - tc_log2 of TC configuration for power and 
                              gain estimation for compressor. tc = 1/2^tc_log2. 
                                    Bit 0-3  :power attack log2.         
                                    Bit 4-7  :power release log2.         
                                    Bit 8-11 :gain attack log2.         
                                    Bit 12-15:gain release log2.             
									
                                Default values for compressor time constants (TC):
                                                    8KHz              16KHz
                                             tc_log2    TC     tc_log2     TC
                                power attack:   8      32ms       9       32ms
                                power release:  8      32ms       9       32ms
                                gain attack:    8      32ms       9       32ms
                                gain release:   8      32ms       9       32ms
									
                                \endverbatim
                             */					 
  drcCurveParam_t curve;  /**< parameters for generating gain curve table */
} drcBandCfg_t;         
/* @} */

/** @defgroup drc_limitercfg_validbitmap DRC Limiter Configuration Valid Bit
 *  @ingroup drc_api_constants
 *  @{
 *
 *  @name DRC configuration valid bitfield used in drc_CTL_SET_LIMITER operation.
 *  Bit field to select parameters to be configured through
 *  drc_CTL_SET_LIMITER in drcControl(). 
 *
 *  @sa drcLimiterCfg_t
 */

/* @{ */
enum {
  drc_LIM_CFG_VALID_LEV_TC_BIT  = 0, /**< SET Bit 0 to enable level TC cfg     */
  drc_LIM_CFG_VALID_GAIN_TC_BIT = 1, /**< SET Bit 1 to enable gain TC cfg      */
  drc_LIM_CFG_VALID_THRESH_BIT  = 2, /**< SET Bit 2 to enable thresh cfg       */
  drc_LIM_CFG_VALID_DELAY_BIT   = 3  /**< SET Bit 3 to enable delay length cfg */
};  
/* @} */
/** @} */

/** 
 *  @ingroup drc_api_structures
 *  @brief DRC Limiter configuration structure.
 *  \brief This structure specifies DRC Limiter configuration parameters
 *         through drcControl() with drc_CTL_SET_LIMITER.
 *
 *  See \ref limiter_param
 *  @sa drcControl_t
 */
typedef struct drcLimiterCfg_s {
  tuint valid_bitfield;/**< Bitfield for configuration parameters
                            as defined in drc_SET_LIMITER_xxx        */
  tuint tc_log2;        /**< 
                           @verbatim
                           tc_log2 of TC configuration for limiter power
                           and gain estimation .  tc = 1/2^tc_log2 
                                Bit 0-3  :power attack log2. This parameter is 
                                          not configurable, but fixed to 0.
                                Bit 4-7  :power release log2. 
                                Bit 8-11 :gain attack log2. 
                                Bit 12-15:gain release log2. 
                                               
                                Default values for limter time constants (TC):
                                                    8KHz              16KHz
                                             tc_log2    TC     tc_log2     TC
                                power attack:   0     (1/8)ms     0     (1/16)ms 
                                power release:  8      32ms       9       32ms
                                gain attack:    1     (1/4)ms     2     (1/4)ms
                                gain release:   9      64ms       10      64ms
     
                            \endverbatim
                        */
  Fract thresh_dBm;    /**< Q4 - Limiter threshold in dBm0                 */
  tint  delay_len;     /**< Limiter forward looking delay in samples 
                           @verbatim
                           and gain estimation .  tc = 1/2^tc_log2 
                                Bit 0-7  :current configured delay length.         
                                Bit 8-15 :maximum allowed delay length.         
                            \endverbatim
                        */
} drcLimiterCfg_t;     /**< It is used in drcControl_t for cfg limiter    */
/* @} */

/** @defgroup drc_coef DRC Filter Coefficient Configuration Size.
 *  @ingroup drc_api_constants
 *  @{
 *
 *  \brief Constants used in configuring DRC Filter Coefficients. The 1ms delay
 *         symmetric FIRs are used. Low pass for lowband, band pass for midband
 *         and high pass for high band. Since FIRs are symmetric, only half+1
 *         of the filter coefficients are needed in the configuration. The extra
 *         one is for the middle coefficient. The size of the configuration 
 *         array is fixed to 17. For 8Khz filters, only the first 9 elements in  
 *         the array are used.
 *
 *  See \ref fir_coef
 */
/* @{ */
#define drc_FIR_HALF_COEF_8KHZ   9  /**< (half+1) of the filter length for 8K */
#define drc_FIR_HALF_COEF_16KHZ  17 /**< (half+1) of the filter length for 16K*/
/* @} */
/** @} */
  
/** @defgroup drc_control_code DRC Control Codes
 *  @ingroup drc_api_constants
 *  @{
 *
 *  @name DRC control operation code.
 *  Constants drc_CTL_XXX to change DRC operation modes through drcControl(). 
 *
 *  @sa drcControl_t
 */

/* @{ */
enum {
  drc_CTL_DRC_ON        = 1, /**< ENABLE DRC                               */
  drc_CTL_DRC_OFF       = 2, /**< DISABLE DRC                              */
  drc_CTL_SET_BITS      = 3, /**< ENABLE: DRC configuration bitfield change*/
  drc_CTL_SET_FBAND     = 4, /**< ENABLE: full band param configuration    */

  drc_CTL_SET_LBAND
    = (drc_CTL_SET_FBAND+1), /**< ENABLE: low band param configuration     */
  drc_CTL_SET_MBAND   
    = (drc_CTL_SET_FBAND+2), /**< ENABLE: mid band param configuration     */
  drc_CTL_SET_HBAND 
    = (drc_CTL_SET_FBAND+3), /**< ENABLE: high band param configuration    */

  drc_CTL_SET_LIM       = 8, /**< ENABLE: limiter param configuration      */

  drc_CTL_SET_FIR_LBAND = 9, /**< ENABLE: low band fir coef configuration  */
  drc_CTL_SET_FIR_MBAND
    = (drc_CTL_SET_FIR_LBAND+1),/**< ENABLE: mid band fir coef configuration  */
  drc_CTL_SET_FIR_HBAND
    = (drc_CTL_SET_FIR_LBAND+2) /**< ENABLE: high band fir coef configuration */
};  
/* @} */
/** @} */

/**
 *  @ingroup drc_api_structures
 *  @brief DRC control data structure.
 *  \brief This structure contains parameters needed to control DRC 
 *         parameters and mode of operation used in drcControl().
 *
 *  See \ref sys_param
 */
typedef struct drcControl_s {
  tint ctl_code;  /**< Control operation defined in drc_CTL_XXX  */
  union { 
    tuint cfgbits;     /**< See \ref drc_operation_bitfield. Set by drc_CTL_SET_BITS */
    drcBandCfg_t band; /**< band cfg. Set by drc_CTL_SET_{FBAND,..., HBAND} */  
    drcLimiterCfg_t limiter; /**< limiter cfg. Set by drc_CTL_SET_LIMITER   */   
    Fract coef[drc_FIR_HALF_COEF_16KHZ]; /**< FIR coef. Set by drc_CTL_SET_FIR_{LBAND..} */
  }u;                              
} drcControl_t;         

/** @defgroup drc_sampling_rate DRC Supported Sampling Rate
 *  @ingroup drc_api_constants
 *  @{
 *
 *  @name DRC sampling rates
 *  Sampling rates that are supported by DRC and set in drcOpen().
 *
 *  See \ref sys_cfg
 *  @sa drcOpenConfig_t
 */
/* @{ */
#define drc_SAMP_RATE_8K   1                   /**< 8kHz  */
#define drc_SAMP_RATE_16K  2                   /**< 16kHz */
/* @} */
/** @} */

/**
 *  @ingroup drc_api_structures
 *  @brief DRC system/band independent configuration structure.
 *  \brief This structure specifies DRC band independent configuration parameters
 *  used in drcOpen().
 *
 *  See \ref sys_cfg
 */
typedef struct drcOpenConfig_s   {
  tuint          cfgbits;   /**< bitfield. See \ref drc_operation_bitfield   */
  tint           samp_rate; /**< Sampling rate. See \ref drc_sampling_rate  */
} drcOpenConfig_t;

/** 
 *  @ingroup drc_api_structures
 *  @brief The configured DRC band parameters.
 *  \brief This structure holds the DRC band parameters reported through 
 *         drcGetParameter().
 *
 *  See \ref compressor_param
 *  @sa drcReportParam_t
 */
typedef struct drcBandParam_s {
  tuint tc_log2;           /**< 
                               @verbatim
                               tc_log2 of TC configuration for power and
                               gain estimation .  tc = 1/2^tc_log2 
                                    Bit 0-3  :power attack log2.         
                                    Bit 4-7  :power release log2.         
                                    Bit 8-11 :gain attack log2.         
                                    Bit 12-15:gain release log2.             
                                \endverbatim
                             */
  drcCurveParam_t curve;  /**< curve parameters                       */
} drcBandParam_t;

/** 
 *  @ingroup drc_api_structures
 *  @brief The configured DRC limiter parameters.
 *  \brief This structure holds the DRC limiter parameters reported through 
 *         drcGetParameter().
 *
 *  See \ref limiter_param
 *  @sa drcReportParam_t
 */
typedef struct drcLimParam_s {
  tuint tc_log2;       /**< Refer to drcLimiterCfg_s:tc_log2.                 */
  Fract thresh_dBm;    /**< Refer to drcLimiterCfg_s:thresh_dBm.              */
  tint  delay_len;     /**< Refer to drcLimiterCfg_s:delay_len.               */
} drcLimParam_t;

/** @defgroup drc_paramter_report_code DRC Get Parameter Codes
 *  @ingroup drc_api_constants
 *
 *  @{
 *
 *  @name DRC get parameter operation code.
 *  Constants drc_GET_XXX are used to select which set of DRC parameters 
 *  to be reported to HOST through drcGetParameter(). 
 *
 *  @sa drcReportParam_t
 */
/* @{ */
enum {
  drc_GET_CFGBITS        = 1,  /**< GET: Operation mode configuration       */
  drc_GET_FBAND_PARAM    = 2,  /**< GET: full band configuration parameters */

  drc_GET_LBAND_PARAM
    = (drc_GET_FBAND_PARAM+1), /**< GET: low band configuration parameters  */
  drc_GET_MBAND_PARAM    
    = (drc_GET_FBAND_PARAM+2), /**< GET: mid band configuration parameters  */
  drc_GET_HBAND_PARAM    
    = (drc_GET_FBAND_PARAM+3), /**< GET: high band configuration parameters */

  drc_GET_LIM_PARAM      = 6,  /**< GET: limiter parameters in an instance  */

  drc_GET_LBAND_FIR_COEF = 7,  /**< GET: low band filter coefficients       */
  drc_GET_MBAND_FIR_COEF
    = (drc_GET_LBAND_FIR_COEF+1), /**< GET: mid band filter coefficients    */
  drc_GET_HBAND_FIR_COEF                                                   
    = (drc_GET_LBAND_FIR_COEF+2)  /**< GET: high band filter coefficients   */
};  
/* @} */
/** @} */

/**
 *  @ingroup drc_api_structures
 *  @brief The DRC parameters reported upon getParameter request.
 *  \brief This structure specifies DRC parameters reported upon 
 *         getParameter request through drcGetParameter().
 *
 *  See \ref sys_param
 */
typedef struct drcReportParam_s {
  union {
    tuint          bits;  /**< See \ref drc_operation_bitfield - requested drc_GET_CFGBITS.*/
    drcBandParam_t bandParam; /**< band param - requested by drc_GET_{FBAND. }*/
    drcLimParam_t  limParam;  /**< lim param - requested by drc_GET_LIM_PARAM   */
    Fract coef[drc_FIR_HALF_COEF_16KHZ]; /**< coef in an inst- drc_GET_{LBAND.}*/
  } u;
} drcReportParam_t;

/**
 *  @ingroup drc_api_structures
 *  @brief DRC create structure which contains information  
 *         to be shared by different DRC instances through drcCreate().
 *
 */
typedef struct {
  dbgInfo_t     debugInfo;       /**< Used for exception handling.            */ 
} drcCreateConfig_t;

/**
 *  @ingroup drc_api_structures
 *  @brief DRC size configuration structure.
 *  \brief This structure specifies the parameters in drcGetSize() and drcNew().
 *
 *  See \ref sys_cfg
 *  @sa drcNewConfig_t
 */
typedef struct drcSizeConfig_s {
  tint    samp_rate;      /**< The sampling rate as drc_SAMP_RATE_{8K,16K}.   */
  tint    max_delay_len;  /**< Limiter forward looking delay in samples.      */
} drcSizeConfig_t;

/**
 *  @ingroup drc_api_structures
 *  @brief DRC new configuration structure.
 *  \brief This contains parameters needed at the creation of a DRC instance
 *         through drcNew().
 *
 */
typedef struct {
  void  *handle;               /**< handle              */
  drcSizeConfig_t sizeCfg;     /**< size configuration  */
} drcNewConfig_t;

/* ---------------------- DRC EXTERNAL APIs ----------------------------------*/

/* drcinit.c */

/**
 *  @ingroup drc_api_functions
 *
 *  @brief Function drcCreate() provides DRC with the system information 
 *         that is shared among multiple instances.   
 *
 *  @param[in]   create_cfg     Pointer to a create configuration structure.
 *  @retval                     DRC error code. See \ref drc_err_code.
 *  drc_NOERR               
 *  drc_ERR_CREATED         
 *
 */
tint drcCreate (drcCreateConfig_t *create_cfg);

/**
 *  @ingroup drc_api_functions
 *
 *  @brief Function drcGetSizes() obtains from DRC the memory requirements of  
 *         an instance, which depend on provided configuration parameters.   
 *
 *  @param[in]   cfg     Pointer to a size configuration structure.
 *  @param[out]  nbufs   Memory location to store the returned number of buffers
 *                       required by the instance.
 *  @param[out]  bufs    Memory location to store the returned address of the vector
 *                       of memory buffer descriptions required by the instance.
 *  @remark Type ecomemBuffer_t is defined in ecomem.h of util package.
 *
 *  @retval              DRC error code. See \ref drc_err_code.
 *  drc_NOERR            success
 *
 */

tint  drcGetSizes       (tint *nbufs, const ecomemBuffer_t **bufs,
                         drcSizeConfig_t *cfg);
/**
 *  @ingroup drc_api_functions
 *
 *  @brief This function creates an instance of DRC and initializes the memory 
 *         buffer pointers in the instance and drcContext.  
 *
 *  @param[in]     nbufs   Number of memory buffers
 *  @param[in]     bufs    Pointer to memory buffer descriptors
 *  @param[in]     cfg     Pointer to new instance configuration structure
 *  @param[in,out] drcInst Pointer to DRC instance
 *  @retval                DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code              
     drc_NOERR               
     drc_ERR_INVALIDPAR      
     drc_ERR_NOMEMORY        
     drc_ERR_NOTCREATED      
     drc_ERR_INVALIDSRATE    
    \endverbatim
 *
 *  @pre  The pointer at the location pointed to by drcInst must be set to NULL 
 *        before this function is called.
 *  @post A pointer to the DRC instance buffer will be returned to the location
 *        pointed to by drcInst. Instance state will be set to closed.
 *
 *  @sa drcNewConfig_t
 */
tint  drcNew            (void **drcInst, tint nbufs, ecomemBuffer_t *bufs,
                         drcNewConfig_t *cfg);
/* drc.c */
/**
 *  @ingroup drc_api_functions
 *
 *  @brief Function drcClose() closes the DRC instance identified by drcInst. 
 *
 *  @param[in,out]  drcInst     pointer to the instance to be closed
 *  @retval                     DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code              
     drc_NOERR               
     drc_ERR_NOTOPENED       

    \endverbatim
 *
 */
tint  drcClose          (void *drcInst);

/**
 *  @ingroup drc_api_functions
 *
 *  @brief Function drcControl() enables or disables a type of DRC function 
 *         or changes the value of a certain parameter. 
 *
 *  @param[in]      ctl       Pointer to DRC control structure.
 *  @param[in,out]  drcInst   Pointer to DRC instance.
 *  @retval                   DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code              
     drc_NOERR               
     drc_ERR_NOTOPENED       
     drc_ERR_INVALIDPAR      
     drc_ERR_INVALIDCFGBITS  
     drc_ERR_INVALIDFIR      

    \endverbatim
 *
 */
tint  drcControl        (void *drcInst, drcControl_t *ctl);

/**
 *  @ingroup drc_api_functions
 *
 *  @brief Function drcDelete() deletes the DRC instance identified by drcInst
 *         and returns the addresses of those buffers used by this instance.  
 *
 *  @param[in]      nbufs    Number of buffers used by this instance.
 *  @param[in]      bufs     Pointer to buffer descriptors to store returned
 *                           addresses of the buffers used by this instance.
 *  @param[in,out]  drcInst  Memory location where the pointer to DRC instance
 *                           is stored
 *  @retval                  DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code             
     drc_NOERR               
     drc_ERR_NOTCLOSED       
     drc_ERR_MEMBUFVECOVF    

    \endverbatim
 *
 *  @pre  DRC instance must be closed by drcClose() before drcDelete() is called. 
 *  @post After drcDelete() is called, DRC instance pointer stored at drcInst 
          will be set to NULL, and the addresses of the buffers used by this 
          instance will be returned to the location pointed to by bufs.
 */
tint  drcDelete         (void **drcInst, tint nbufs, ecomemBuffer_t *bufs);

/**
 *  @ingroup drc_api_functions
 *
 *  @brief This function reports the DRC performance debug statistics and the
 *         version information. 
 *
 *  @param[in]      drcInst  Pointer to DRC instance.
 *  @param[in]      get_code Operation modes selection
 *  @param[out]     param    Report parameter values.
 *  @retval                  DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code              
     drc_NOERR               
     drc_ERR_NOTOPENED       
     drc_ERR_INVALIDPAR      
    \endverbatim
 *
 */
tint  drcGetParameter (void *drcInst, tint get_code, drcReportParam_t *param);

/**
 *  @ingroup drc_api_functions
 *
 *  @brief Function drcOpen() initializes and configures an DRC instance.
 *
 *  @remark This function may be called after drcNew() to initialize a new DRC
 *          instance. It may also be called to reconfigure an instance that 
 *          has been closed by drcClose() but not deleted by drcDelete().
 *
 *  @param[in]      cfg      Pointer to DRC configuration parameter.
 *  @param[in,out]  drcInst  Pointer to DRC instance.
 *  @retval                  DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code              
     drc_NOERR               
     drc_ERR_NOTCLOSED                                  
     drc_ERR_INVALIDSRATE     
    \endverbatim
 *
 *  @pre  Function drcNew() must be called before drcOpen() is called the first
 *        time to open a new instance. For subsequent calls to open an existing
 *        instance, drcClose() must be called before drcOpen() to close the
 *        instance.
 *  @post After DRC instance is opened, drcControl() or drcProcess()
 *        may be called for control or processing.  
 *
 */
tint  drcOpen           (void *drcInst, drcOpenConfig_t *cfg);

/* drcproc.c */         
/**
 *  @ingroup drc_api_functions
 *
 *  @brief  It performs compressor and limiter processing. The input samples are
 *          passed in through sin pointer in linSample type. The output samples
 *          are placed to the buffer pointed by sout in linSample type.
 *
 *  @param[in,out]  drcInst Pointer to DRC instance. 
 *  @param[in]      sin     Pointer to a frame of data in linSample type to be processed.
 *  @param[out]     sout    Pointer to a frame of processed data in linSample type. 
 *
 *  @remark  The location of input and output buffers are defined by 
 *           sin and sout pointers. These two buffers are in linSample type
 *           and CANNOT be overlaid with each other. These buffers have no  
 *           specific memory type or alignment requirment.        
 *
 *  @retval                    DRC error code. See \ref drc_err_code.
 *  @verbatim
     error code              
     drc_NOERR                
     drc_ERR_NOTOPENED        
    \endverbatim
 */

tint drcProcess         (void *drcInst, void *sin, void *sout);


/* -------------------------- DRC Call Table -------------------------------- */
/**
 *  @brief DRC call table
 */
typedef struct {
  tint  (*drcGetSizes)       (tint *nbufs, const ecomemBuffer_t **bufs,
                              drcSizeConfig_t *cfg);
  tint  (*drcNew)            (void **drcInst, tint nbufs, ecomemBuffer_t *bufs,
                              drcNewConfig_t *cfg);
  tint  (*drcCreate)         (drcCreateConfig_t *create_cfg);
  tint  (*drcClose)          (void *drcInst);
  tint  (*drcControl)        (void *drcInst, drcControl_t *ctl);
  tint  (*drcDelete)         (void **drcInst, tint nbufs, ecomemBuffer_t *bufs);
  tint  (*drcGetParameter)   (void *drcInst, tint get_code, drcReportParam_t *param); 
  tint  (*drcOpen)           (void *drcInst, drcOpenConfig_t *cfg);
  tint  (*drcProcess)        (void *drcInst, void *sin, void *sout);
} drcCallTable_t;

#endif  /* _DRC_H */
/* nothing past this point */

