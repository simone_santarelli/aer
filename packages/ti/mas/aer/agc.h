#ifndef _AGC_H
#define _AGC_H

/**
 *  @file   agc.h
 *  @brief  Contains external API for the AGC component.
 *
 *        Copyright (c) 2007 – 2013 Texas Instruments Incorporated                
 *                     ALL RIGHTS RESERVED
 */

/* System definitions and utilities */
#include <ti/mas/types/types.h>               /* DSP types                    */
#include <ti/mas/util/ecomem.h>               /* memory descriptor structure  */
#include <ti/mas/util/debug.h>                /* for debug function prototype */

/* Define AGC Module as a master group in Doxygen format and add all AGC API 
   definitions to this group. */
/** @defgroup agc_module AGC Module API
 *  @{
 */
/** @} */

/** @defgroup agc_api_functions AGC Functions
 *  @ingroup agc_module
 */

/** @defgroup agc_api_structures AGC Data Structures
 *  @ingroup agc_module
 */

/** @defgroup agc_api_constants AGC Constants (enum's and define's)
 *  @ingroup agc_module
 */

/** @defgroup agc_err_code AGC Error Codes
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC error codes
 *  \brief Error codes returned by AGC API functions.
 */
/*@{*/
enum {
  agc_NOERR              = 0, /**< success, no error */
  agc_ERR_NOTCREATED     = 1, /**< agcCreate() has not been called yet        */
  agc_ERR_NOMEMORY       = 2, /**< supplied memory are not enough             */
  agc_ERR_NOTOPENED      = 3, /**< AGC instance has not been opened yet       */
  agc_ERR_NOTCLOSED      = 4, /**< AGC instance has not been closed yet       */
  agc_ERR_INVALIDPAR     = 5, /**< configuration parameter is invalid         */
  agc_ERR_MEMBUFVECOVF   = 6, /**< no enough space to return buffer addresses */
  agc_ERR_GAIN_REQ_NOT_SERVICED = 7,/**< last gain change request not serviced*/
  agc_ERR_MIC_GAIN_DUPLICATE    = 8 /**< bits agc_CTL_VALID_MICGAIN and 
                                         agc_CTL_VALID_MICGAIN_ADAPT of 
                                         agcControl_s.valid_bitfield are both set. */
};
/*@}*/
/** @} */

/** @defgroup agc_event_code AGC Event Codes
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC event codes
 *  \brief AGC event codes for sending external message or request through 
 *         eventOut function provided externally. Used to set event_code of 
 *         structure \ref agcEvent_t.
 */
/*@{*/
enum {
  agc_EVC_SAT_FALSE       = 0,             /**< no recent saturation detected */
  agc_EVC_SAT_DETECTED    = 1,             /**< mic input saturation detected */
  agc_EVC_MGAIN_CHANGE    = 2,             /**< request PGA mic gain change   */
  agc_EVC_SGAIN_CHANGE    = 3,             /**< request PGA spk gain change   */
  agc_EVC_GET_PRENLP_GAIN = 4,             /**< Get the present Pre-NLP gain  */
  agc_EVC_SET_PRENLP_GAIN = 5              /**< Set the new  Pre-NLP gain     */
};
/*@}*/
/** @} */


/** @defgroup agc_control_bitfields AGC Control Bitfield Masks
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC control bitfield masks
 *  \brief Bitfield mask agc_CTL_MODES_XYZ to change AGC operation modes
 *         through agcControl(). Used to construct mask and value of \ref
 *         agcModeControl_t.
 *
    @verbatim

    AGC control bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       enable AGC
                   0       disable AGC
        1          1       enable adaptive mode
                   0       disable adaptive mode
        2          1       clear everything and restart
                   0       do nothing
        3 - 15             RESERVED
 
    \endverbatim
 */
 /*@{*/
#define agc_CTL_MODES_ENABLE_AGC    0x0001   /**< enable AGC.                 */
#define agc_CTL_MODES_ENABLE_ADAPT  0x0002   /**< enable adaptive mode of AGC */
#define agc_CTL_MODES_CLEAR_ALL     0x0004   /**< clear everything, restart   */
/*@}*/
/** @} */

/** @defgroup agc_reset_bitfields AGC Reset Bitfield Masks
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC reset bitfield masks
 *  \brief Bitfield mask agc_RESET_XYZ to reset AGC information through function 
 *         agcOpen(). Used to construct reset_bitfield of \ref agcConfig_t.
 *
    @verbatim

    AGC reset bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       perform a full reset
                   0       don't perform full reset
        1          1       clear signal power history
                   0       keep signal power history
        2          1       reset mic and speaker analog gains to default
                   0       keep mic and speaker analog gains
        3          1       reset cofigurable parameters except mic/speaker gains
                   0       keep cofigurable parameters
        4 - 15             RESERVED
 
    \endverbatim
 */
/*@{*/
#define agc_RESET_FULL_RESET       0x0001 /**< full reset.                    */
#define agc_RESET_POWER_HIST       0x0002 /**< reset signal power history.    */
#define agc_RESET_MIC_SPK_GAINS    0x0004 /**< reset mic and speaker gains.   */ 
#define agc_RESET_OTHER_PARAMS     0x0008 /**< reset configurable parameters. */
/*@}*/
/** @} */


/** @defgroup agc_config_bitfields AGC Configuration Valid Bitfield Masks
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC configuration valid bitfield masks
 *  \brief Bitfield mask agc_CFG_VALID_XYZ to configure AGC parameters 
 *         through agcOpen(). Used to construct valid_bitfield of \ref 
 *         agcConfig_t.
 *
    @verbatim

    AGC config valid bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       configure sampling rate
                   0       don't configure sampling rate
        1 - 15             RESERVED
 
    \endverbatim
 */
/*@{*/
#define agc_CFG_VALID_SAMP_RATE    0x0001 /** config sampling rate.           */
#define agc_CFG_VALID_BITS_ALL_1   0x0001 /** all bits must be set to 1 when
                                           *  agcOpen() is called the first time
                                           *  after agcNew() is called.       */
/*@}*/
/** @} */

/** @defgroup agc_create_bitfields AGC Create Bitfield Masks
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC create configuration bitfield masks
 *  \brief Bitfield mask to construct config_bitfield of \ref agcCreateConfig_t.
 *
    @verbatim

    AGC create bitfield
    ---------------------------------------
    |15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0|
    ---------------------------------------

       bits      values       meaning
    (from LSB)
        0          1       request support of multiple HF microphones
                   0       don't request support of multiple HF microphones
        0 - 15             RESERVED
 
    \endverbatim
 */
/*@{*/
#define agc_CRT_MULT_MIC_MODE  0x0001   /**< request multiple HF mic support. */
/*@}*/
/** @} */

/** @defgroup agc_samp_rate AGC Sampling Rates Definitions
 *  @ingroup agc_api_constants
 *  @{
 *
 *  @name AGC sampling rates
 *  \brief AGC sampling rate definitions to set sampling_rate of \ref 
 *         agcConfig_t and max_sampling_rate of \ref agcSizeConfig_t.
 */
/*@{*/
enum {
  agc_SRATE_8K  = 0,                   /**< 8kHz  */
  agc_SRATE_16K = 1                    /**< 16kHz */
};

typedef tint agcSrate_t;               /**< used by \ref agcCreateConfig_t   */

/*@}*/
/** @} */


/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC event structure. It contains the event code and the
 *         associated parameters to be read or set by AGC eventOut function. 
 *         It is one argument of AGC eventOut function passed to AGC by the
 *         user through \ref agcCreateConfig_t and agcCreate().
 *
 *  @remark Usage of parameters depend on each of \ref agc_event_code :
 *          - \ref agc_EVC_SAT_FALSE :       no parameter is needed.  
 *          - \ref agc_EVC_SAT_DETECTED :    no parameter is needed.
 *          - \ref agc_EVC_MGAIN_CHANGE :    analog_gain has the new gain value 
 *                                           requested by adaptive AGC.
 *          - \ref agc_EVC_SGAIN_CHANGE :    not used by AGC event out function. 
 *          - \ref agc_EVC_GET_PRENLP_GAIN : event out function should return
 *                                           AER's pre-NLP (or AER Tx digital)
 *                                           gain in pNLPgain. 
 *          - \ref agc_EVC_SET_PRENLP_GAIN : not used by AGC eventOut function.
 *
 *  \remark Refer to agcSendIn() for adaptive operation of AGC.
 */
typedef struct agcEvent_s{        /**< AGC->SIU gain change report data       */
  tint event_code;                /**< see enum agc_EVC_XYZ                   */
  union { 
    Fract analog_gain;            /**< speaker/mic gain in dB, S11.4 format   */
    Fract pNLPgain;               /**< pre-NLP gain in dB, S11.4 format       */
  }u;
} agcEvent_t;

/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC create configuration structure which contains information  
 *         to be shared by multiple AGC instances. It is used by function
 *         agcCreate().
 *
 *  \remark Type debugInfo_t is defined in debug.h of util package.
 */
typedef struct agcCreateConfig_s{
  dbgInfo_t debugInfo;           /**< Debug/exception function                */
  void (*eventOut)(void *handle, agcEvent_t *event); 
                                 /**< pointer to event out function           */  
  agcSrate_t max_sampling_rate;  /**< maximum sampling rate                   */  
  tuint config_bitfield;         /**< bitfield constructed by \ref 
                                      agc_create_bitfields.                   */
} agcCreateConfig_t;                                                  

/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC size configuration structure used by function agcGetSizes(). 
 *         It is also part of structure \ref agcNewConfig_t.
 */
typedef struct agcSizeConfig_s{
  tuint config_bitfield;       /**< configuration bitfield - currently unused */  
} agcSizeConfig_t;

/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC configuration structure for creating a new AGC instance. It is
 *         used by function agcNew().
 */
typedef struct agcNewConfig_s{ 
  void  *handle;                                    /**< handle               */
  agcSizeConfig_t sizeCfg;                          /**< buffer sizes info    */
} agcNewConfig_t;


/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC configuration structure. It contains parameters needed to open 
 *         an instance of AGC, and is used by function agcOpen().
 */
typedef struct agcConfig_s { 
  tuint reset_bitfield;  /**< Used to reset certain information specified by
                              \ref agc_reset_bitfields. 
                              *
                              \remark This bitfield is ignored
                              when agcOpen() is called the first time after
                              agcNew() is called.                             */
  tuint valid_bitfield;  /**< Used to indicate which of the system parameters
                              in \ref agcConfig_t are valid and may be read by 
                              agcOpen(). See \ref agc_config_bitfields.
                              *
                              \remark It must be set to all 1's (\ref 
                              agc_CFG_VALID_BITS_ALL_1) to configure the
                              system parameters when agcOpen() is called the
                              first time after agcNew() is called.            */
  agcSrate_t sampling_rate;   /**< sampling rate.                             */  
} agcConfig_t;     

/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC Version structure
 *
 *  @remark AGC doesn't have its own version number. It uses AER's 
 *          version number defined in aerVersion_s of aer.h.
 */

typedef struct agcVersion_s{
  tuint  major;  
  tuint  minor;  
  tuint  patch; 
  tuint  build;      
  tuint  quality;
  tuint  btype;  
} agcVersion_t;

/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC debug statistics data structure.
 *
 */
typedef struct agcDebugStat_s{
  tuint control_bitfield;      /**< control bitfield reflecting agc_CTL_XYZ   */
  tuint flag_bitfield;         /**< flag bitfield showing AGC states          */
  tint  sat_threshold;         /**< saturation threshold Q0                   */
  tint  sat_hangover;          /**< saturation hangover in msec               */
  tuint sat_cntr;              /**< saturation counter                        */
  tint  echo_clip_penalty;     /**< current value of saturation penalty cntr  */     
  tint  ne_clip_cntr;          /**< # frames mic has saturated to high NE sig */
  tint  lo_dynamic_range_cntr; /**< mic low dynamic range counter in of frames*/
  Fract echo_max_0;            /**< echo signal max. abs value: 0th level     */
  Fract echo_max_1;            /**< echo signal max. abs value: 1st  level    */
  Fract echo_max_2;            /**< echo signal max. abs value: 2nd level     */
  Fract echo_max_3;            /**< echo signal max. abs value: 3rd level     */
  Fract ne_max_0;              /**< near-end signal max. abs value: 0th level */
  Fract ne_max_1;              /**< near-end signal max. abs value: 1st  level*/
  Fract ne_max_2;              /**< near-end signal max. abs value: 2nd level */
  Fract ne_max_3;              /**< near-end signal max. abs value: 3rd level */
  tint  glim_reach_cntr;       /**< counts how many times adaptive AGC reaches
                                *   gain limits: max/min mic gain, min pre-NLP
                                *   gain, or max gain swing.                  */
} agcDebugStat_t;


/**
 *  @ingroup agc_api_structures
 *
 *  @brief Data structure to hold AER parameters needed by AGC. These parameters
 *         are provided by the user through agcControl() via control valid bit 
 *         \ref agc_CTL_VALID_AERDATA. 
 *
 *  @remarks These information must be provided to AGC every frame for AGC's
 *           adaptive operation.
 */
typedef struct agcAerData_s{
   LFract pecho_pwr;                 /**< send frame pwr predicted echo only */
   LFract Si_with_echo;              /**< send frame pwr including echo      */
   LFract Si_minus_echo;             /**< send frame pwr after echo sub      */       
   Fract  maxabs_pecho;              /**< maximum absolute predicted echo    */
   tbool  nlp_on;                    /**< nlp_on = TRUE, NLP is enabled      */
   tint   log2_attn;                 /**< maximal ERLE record                */ 
   tint   SWR;                       /**< Switch from Recv Path AER NLP hang */
   tint   SWS;                       /**< Switch from Send Path AER NLP hang */
   tbool  TS;                        /**< Enabled path. TS = TRUE - Send Path*/
                                     /**< = FALSE - Receive Path             */
   tint   XS;                        /**< path switchover to send from recv  */
   tint   XR;                        /**< path switchover to recv from send  */
} agcAerData_t;


/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC operation modes control structure.
 *  \brief Contains parameters needed to change AGC operational modes through
 *         function agcControl() via control valid bit \ref agc_CTL_VALID_MODES. 
 */ 
typedef struct agcModeControl_s{
  tuint mask;        /**< mask tells which bits are to be changed.            */
  tuint value;       /**< value gives the new value of each bit to be changed.*/
                     /**< \ref agc_control_bitfields can be used to construct 
                          mask and value.                                     */
} agcModeControl_t;

/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC control data structure. It contains information to change 
 *         operation of AGC through functin agcControl().
 *
 *  @remark Bits agc_CTL_VALID_MICGAIN and agc_CTL_VALID_MICGAIN_ADAPT should 
 *          not be set at the same time. Otherwise, bit agc_CTL_VALID_MICGAIN_ADAPT
 *          will be ignored and agcControl() will take actions based on 
 *          bit agc_CTL_VALID_MICGAIN, and error code agc_ERR_MIC_GAIN_DUPLICATE 
 *          will be returned by agcControl(). 
  */ 
typedef struct agcControl_s{ 
  tuint valid_bitfield;  
#define agc_CTL_VALID_MICGAIN           0x0001 /**< Inform AGC the mic analog 
                                                    gain. Initiated by user. 
                                                    Corresponding parameter:
                                                       - mic_gain. */
#define agc_CTL_VALID_MICGAIN_ADAPT     0x0002 /**< Inform AGC the new mic analog 
                                                    gain as a result of servicing
                                                    request by adaptive AGC.  
                                                    Corresponding parameter:
                                                       - mic_gain
                                                       - preNLP_enable
                                                       - preNLP_gain
                                                    Returned parameter:
                                                       - preNLP_gain          */
#define agc_CTL_VALID_SPKGAIN           0x0004 /**< Inform AGC the speaker gain.
                                                    Corresponding parameter:
                                                       - spk_gain             */
#define agc_CTL_VALID_AERDATA           0x0008 /**< Provide AER data for AGC to
                                                    do gain control.          
                                                    Corresponding parameter:
                                                       - aer_data             */                                                    
#define agc_CTL_VALID_MODES             0x0010 /**< Set AGC operation modes.  
                                                    Corresponding parameter:
                                                       - modes                */                                                    
#define agc_CTL_VALID_SAT_THRESHOLD     0x0020 /**< Set mic saturation detection
                                                    threshold.                
                                                    Corresponding parameter:
                                                       - sat_threshold        */                                                    
#define agc_CTL_VALID_SAT_HANGOVER      0x0040 /**< Set mic saturation detection 
                                                    hangover.                 
                                                    Corresponding parameter:
                                                       - sat_hangover         */                                                    
#define agc_CTL_VALID_MIC_GAIN_MAX      0x0080 /**< Set maximum mic gain.     
                                                    Corresponding parameter:
                                                       - max_micgain          */                                                    
#define agc_CTL_VALID_MIC_GAIN_MIN      0x0100 /**< Set minimum mic gain.     
                                                    Corresponding parameter:
                                                       - min_micgain          */                                                    
#define agc_CTL_VALID_MICGAIN_SWING_MAX 0x0200 /**< Set maximum gain swing.   
                                                    Corresponding parameter:
                                                       - max_micgain_swing    */                                                    
                                        
  Fract mic_gain;           /**< New mic analog gain (S11.4 dB) provided 
                             *   by user (default is 0dB). Corresponds to
                             *   agc_CTL_VALID_MICGAIN when this gain change
                             *   is initiated by the user, or corresponds to
                             *   agc_CTL_VALID_MICGAIN_ADAPT when this gain
                             *   change is initiated by the adaptive AGC.     */
  tbool preNLP_enable;      /**< Flag indicating whether to change the AER
                                 Tx digital gain, returned by agcControl(),
                                 when bit agc_CTL_VALID_MICGAIN_ADAPT is set. */
  Fract preNLP_gain;        /**< AER Tx digital gain (S11.4 dB), both input and
                             *   output of agcControl() corresponds to 
                             *   agc_CTL_VALID_MICGAIN_ADAPT is set.
                             *   As input, it is the current AER Tx digital 
                             *   gain (or pre-NLP gain) provided by the user.
                             *   As output when agcControl() returns, it
                             *   holds the new Tx digital gain requested by
                             *   adaptive AGC. The users needs to set the AER 
                             *   Tx digital gain afterward accordingly.       */                        
  Fract spk_gain;           /**< New speaker analog gain (S11.4 dB) provided 
                             *   by user (default is 0dB). Corresponds to
                             *   agc_CTL_VALID_SPKGAIN.                       */
  agcAerData_t aer_data;    /**< AER data provided by the user. Corresponds
                             *   to agc_CTL_VALID_AERDATA.                    */  
  agcModeControl_t modes;   /**< AGC operational modes. Corresponds to
                             *   agc_CTL_VALID_MODES.                         */ 
  tint  sat_threshold;      /**< ADC saturation threshold provided by the                
                             *   user (default is 32000). Corresponds to
                             *   agc_CTL_VALID_SAT_THRESHOLD.                 */
  tint  sat_hangover;       /**< Saturation hangover in msec provided by the
                             *   user (default is 20msec). Corresponds to
                             *   agc_CTL_VALID_SAT_HANGOVER.                  */
  Fract max_micgain;        /**< Maximum microphone analog gain in S11.4 dB
                             *   provided by the user (default is 864, 
                             *   i.e. 54dB). Adaptive AGC will not request 
                             *   new mic gain above this limit. Corresponds
                             *   to agc_CTL_VALID_MIC_GAIN_MAX.               */
  Fract min_micgain;        /**< Minimum microphone analog gain in S11.4 dB
                             *   provided by the user (default is 0). 
                             *   Adaptive AGC will not request new mic gain 
                             *   below this limit.  Corresponds to 
                             *   agc_CTL_VALID_MIC_GAIN_MIN.                  */
  Fract max_micgain_swing;  /**< Maximum gain swing from reference gain 
                             *   which can be introduced by adaptive AGC.
                             *   It is in S11.4 dB with default 384 (24dB).
                             *   Corresponds to agc_CTL_VALID_MICGAIN_SWING_MAX.
                             *   When adaptive AGC tries to change the mic
                             *   gain, it will not make the absolute gain 
                             *   change from the reference gain larger than
                             *   this parameter. The reference gain is the
                             *   last analog mic gain set by the user 
                             *   through bit agc_CTL_VALID_MICGAIN.           */
} agcControl_t;


/* Function Prototypes */
/* AGC APIs */

/* agcinit.c */

/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function provides system information that is shared among 
 *         multiple instances of AGC.   
 *  \remark Function agcCreate() must be called before any other AGC API function 
 *          is called.
 *  @param[in]   create_cfg     Pointer to a create configuration structure.
 *  @retval                     AGC error code.         
 *  @verbatim
    error code              description
    agc_NOERR               success
    agc_ERR_INVALIDPAR      max_sampling_rate of create_cfg is not equal to any
                            value defined in agcSrate_t.
    \endverbatim
 */
tint agcCreate (agcCreateConfig_t *create_cfg);

/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function obtains memory requirements for an instance of AGC.   
 *
 *  @param[in]   cfg     Pointer to a size configuration structure.
 *  @param[out]  nbufs   Number of memory buffers stored in this address.
 *  @param[out]  bufs    Address of buffer descriptors stored in this address.     
 *  @remark Type ecomemBuffer_t is defined in ecomem.h of util package.
 *
 *  @retval              AGC error code.         
 *  @verbatim
    error code              description
    agc_ERR_NOTCREATED      agcCreate() has not been called
    \endverbatim
 */
tint agcGetSizes (tint *nbufs, const ecomemBuffer_t **bufs, agcSizeConfig_t *cfg);

/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function creates an instance of AGC and initializes the memory 
 *         buffer pointers in the instance.  
 *  \remark Function agcNew() must be called before agcOpen()is called.
 *
 *  @param[in]     nbufs     Number of memory buffers allocated by the user.
 *  @param[in]     bufs      Pointer to memory buffer descriptors defined by
 *                           user.
 *  \remark Buffer alignment property of each buffer passed to aerNew() must be 
 *          equal to or better than what is returned by aerGetSizes(), and must
 *          be in consistency with the base address of the buffer.
 *
 *  @param[in]     cfg       Pointer to new instance configuration structure.
 *  @param[in,out] agcInst   Memory location that will receive a pointer to 
 *                           the created AGC instance.
 *  @retval                  AGC error code.
 *  @verbatim
     error code              description
     agc_NOERR               success
     agc_ERR_INVALIDPAR      *agcInst is not NULL or nbufs is not correct 
     agc_ERR_NOMEMORY        properties of certain buffer are bad:
                             - size is not zero but base address is NULL,
                             - alignment and base address are not consistent,
                             - volatility does not meet requirement.
    \endverbatim
 *
 *  @pre  The pointer at the location pointed to by agcInst must be set to NULL 
 *        before this function is called.
 *  @post A pointer to the AGC instance buffer will be returned to the location
 *        pointed to by agcInst. Instance state will be set to closed.
 */
tint agcNew (void **agcInst, tint nbufs, ecomemBuffer_t *bufs, 
             agcNewConfig_t *cfg);

/* agc.c */
/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function initializes and configures an AGC instance.
 *
 *  @param[in]      cfg      Pointer to AGC configuration parameter.
 *  @param[in,out]  agcInst  Pointer to AGC instance.
 *  @retval                  AGC error code.         
 *  @verbatim
     error code              description
     agc_NOERR               success
     agc_ERR_NOTCLOSED       AGC instance state has not been set to closed, i.e,
                             agcNew() or agcClose() not called for the instance.
     agc_ERR_INVALIDPAR      bad configuration parameters:
                             - cfg is NULL
                             - valid_bitfield of agcConfig_t is not all 1's for  
                               first time open after agcNew().
                             - sampling rate of agcConfig_t is larger than 
                               max_sampling_rate specified in agcCreateConfig_t.
    \endverbatim
 *
 *  @pre  Function agcNew() must be called before agcOpen() is called the first
 *        time to open a new instance. For subsequent calls to open an existing
 *        instance, agcClose() must be called before agcOpen() to close the
 *        instance.
 *  @post After AGC instance is opened, agcControl() or agcSendIn() may be
 *        called for control or processing.  
 */
tint agcOpen (void *agcInst, agcConfig_t *cfg);

/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function enables or disables a type of AGC function or changes
 *         the value of a certain parameter. 
 *
 *  @param[in]      ctl       Pointer to AGC control structure.
 *  @param[in,out]  agcInst   Pointer to AGC instance.
 *  @retval                   AGC error code.
 *  @verbatim
     error code              description
     agc_NOERR               success
     agc_ERR_NOTOPENED       AGC instance has not been opened
    \endverbatim
 */
tint agcControl (void *agcInst, agcControl_t *ctl);

/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function closes the AGC instance identified by agcInst. 
 *         It sets the state of AGC to CLOSED.  
 *
 *  @param[in,out]  agcInst
 *  @retval         AGC error code.         
 *  @verbatim
     error code              description
     agc_NOERR               success
     agc_ERR_NOTOPENED       AGC instance is not in open state
    \endverbatim
 */
tint agcClose (void *agcInst);

/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function deletes the AGC instance identified by agcInst.  
 *         It clears the instance pointer to NULL and returns the addresses of 
 *         the buffers used by this instance.
 *
 *  @param[in]      nbufs    Number of buffers used by this instance.
 *  @param[in]      bufs     Pointer to buffer descriptors to store returned
 *                           addresses of the buffers used by this instance.
 *  @param[in,out]  agcInst  Memory location where the pointer to AGC instance
 *                           is stored.
 *  @retval                  AGC error code.
 *  @verbatim
     error code              description
     agc_NOERR               success
     agc_ERR_NOTCLOSED       AGC instance has not been closed.
     agc_ERR_MEMBUFVECOVF    Not enough buffer descriptors to store all returned 
                             buffer addresses.
    \endverbatim
 *
 *  @pre  AGC instance must be closed by agcClose() before agcDelete() is called. 
 *  @post After agcDelete() is called, AGC instance pointer stored at agcInst 
          will be set to NULL, and the addresses of the buffers used by this 
          instance will be returned to the location pointed to by bufs.
 */
tint agcDelete (void **agcInst, tint nbufs, ecomemBuffer_t *bufs);


/**
 *  @ingroup agc_api_functions
 *
 *  @brief This function reports the AGC performance debug statistics.
 *
 *  @param[in]      agcInst  Pointer to AGC instance.
 *  @param[in]      dbgstat  Pointer to debug status structure.
 *  @param[in]      version  Pointer to AGC version structure.
 *  @param[in]      reset    Flag to tell AGC to reset event counters.
 *  @retval                  AGC error code (always agc_NOERR).
 *
 *  @post AGC event counters, sat_cntr and glim_reach_cntr of dbgstat, will be
 *        set to 0s if reset flag is true.
 */
tint agcGetPerformance (void *agcInst, agcDebugStat_t *dbgstat, 
                        agcVersion_t *version, tbool reset);

/* agctx.c */
/**
 *  @ingroup agc_api_functions
 *
 *  @brief  This function performs send path processing. It only examines the 
 *          data without changing it.<BR><BR>
 *
 *  \brief  When adaptive mode is disabled, AGC will just monitor the signal
 *          saturation status, and report to system through the eventOut funtion 
 *          provided by agcCreateConfig_t. If saturation is detected, the event 
 *          will be agc_EVC_SAT_DETECTED, and if saturation stops, it will be
 *          agc_EVC_SAT_FALSE.<BR><BR>
 *
 *  \brief  When adaptive mode is enabled, besides monitoring saturation, AGC  
 *          will also try to request for microphone analog gain change to
 *          optimize the Tx path digital dynamic range to enhance AER 
 *          performance. If the ADC output is persistently saturating, AGC will 
 *          ask to lower the mic gain, and if the ADC output is persistently 
 *          too low AGC will ask to increase the mic gain. During these mic
 *          gain changes the loudness of the Tx path, or SLR, is preserved by 
 *          maintaining the sum of mic analog gain and AER Tx digital gain (or
 *          known as pre-NLP gain).
 *
 *  @remarks Adaptive AGC will not ask for mic gain change is any of the
 *           following happens: <BR>
 *             - microphone gain max/min is going to be reached <BR>
 *             - pre-NLP gain min (-6dB) to be reached <BR>
 *             - gain swing max to be reached <BR>
 *           Instead, AGC will record the number of occurances of the above 
 *           events and make it available in glim_reach_cntr of agcDebugStat_t. 
 *
 *  @sa max_micgain, min_micgain, and max_micgain_swing of agcControl_t.
 *
 *  @param[in]      send_in        A frame of data to be examined.
 *  @param[in,out]  agcInst        Pointer to AGC instance. 
 *  @retval                        AGC error code.         
 *  @verbatim
     error code              description
     agc_NOERR               success
     agc_ERR_NOTOPENED       AGC instance has not been opened
     agc_ERR_INVALIDPAR      AGC has not been informed of the initial speaker 
                             and mic gains. 
    \endverbatim
 */
tint agcSendIn (void *agcInst, void *send_in);

/* Call Table */
/**
 *  @ingroup agc_api_structures
 *
 *  @brief AGC call table
 */
typedef struct{
  tint (*agcCreate)   (agcCreateConfig_t *create_cfg);
  tint (*agcGetSizes) (tint *nbufs, const ecomemBuffer_t **bufs, 
                       agcSizeConfig_t *cfg);
  tint (*agcNew)      (void **agcInst, tint nbufs, ecomemBuffer_t *bufs,
                       agcNewConfig_t *cfg);
  tint (*agcOpen)     (void *agcInst, agcConfig_t *cfg);
  tint (*agcSendIn)   (void *agcInst, void *send_in);
  tint (*agcControl)  (void *agcInst, agcControl_t *ctl);
  tint (*agcClose)    (void *agcInst);
  tint (*agcDelete)   (void **agcInst, tint nbufs, ecomemBuffer_t *bufs);
  tint (*agcGetPerformance)(void *agcInst, agcDebugStat_t *dbgstat, 
                            agcVersion_t *version, tbool reset);
} agcCallTable_t;

#endif    /* _AGC_H */

/* nothing past this point */

